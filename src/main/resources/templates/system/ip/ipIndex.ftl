<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>盘古游戏</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/jquery.pagination.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">


</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg" style="margin-left: 215px">
        <div class="wrapper wrapper-content animated fadeInRight">

            <ul class="breadcrumb" style="background-color: #dddbdf">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row" style="margin-left: 5px">
                        <form method="post" action="/apps/agent/agent" id="formId" class="form-inline">

                            <div clss="well">
                                <div class="form-group" style="width: 100%">
                                    <button type="button" class="btn btn-primary" onclick="$('#addIp').modal();">
                                        新增IP白名单
                                    </button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </ul>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                                       id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>IP</th>
                                        <th>操作人</th>
                                        <th>操作时间</th>
                                        <th>备注</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                            <#if list??>
                    <#list list as info>
                    <tr>
                        <td>
                            ${info.id}
                        </td>
                        <td>${info.ip} </td>
                        <td>${info.operator}</td>
                        <td>${info.operate_time}</td>
                        <td>${info.remark}</td>
                        <td>
                            <div class="btn-group ">
                                <button style="background-color: #e4e8e5" class="btn btn-white btn-sm edit"
                                        onclick="deleteAgent('${info.id}')">删除
                                </button>
                            </div>
                        </td>
                    </tr>
                    </#list>
                            </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="modal fade" id="addIp" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 180px;width: 900px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">新增IP白名单</h4>
            </div>
            <div class="modal-body">
                <form id="addIpForm" enctype="multipart/form-data"
                      action="${request.contextPath}/admin/ip/save" method="post">
                    <div class="form-group">
                        <label class="control-label">IP地址:</label>
                        <input type="text" style="height:30px" class="form-control" name="ip" id="ip"
                               placeholder="请输入IP地址"/>* 添加多个IP时，用;分开 例如:127.0.0.*;192.168.137.1-192.168.137.20
                    </div>
                    <div class="form-group">
                        <label class="control-label">备注信息:</label>
                        <input type="text" style="height:30px" class="form-control" name="remark" id="remark"
                               placeholder="请输入备注信息"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                        <button type="submit" class="btn btn-primary"
                                style="background: #ea7137; border-color: #f29365">提交
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<script src="${request.contextPath}/statics/js/jquery.js"></script>
<script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
<script src="${request.contextPath}/statics/js/My97DatePicker/WdatePicker.js"></script>
<script src="${request.contextPath}/statics/layui.all.js"></script>
<script src="${request.contextPath}/statics/js/jquery.form-3.14.js"></script>

<script>

    $(document).ready(function () {
        $("#reset").click(function () {
            var resetArr = $(this).parents("form").find(":input");
            for (var i = 0; i < resetArr.length; i++) {
                if (i > 0) {
                    resetArr.eq(i).val("");
                }
            }
            return false;
        });

    });


    $(function () {
        var load;
        $("#addIpForm").ajaxForm({
            success: function (result) {
                layer.close(load);
                layer.msg(result.message);
                setTimeout(function () {
                    //3秒后刷新
                    layer.close();
                    location.reload();
                }, 2000);

            },
            beforeSubmit: function () {
                //判断参数
                if ($.trim($("#ip").val()) == '') {
                    layer.msg('请输入IP地址', function () {
                    });
                    $("#ip").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                } else {
                    $("#ip").parent().removeClass('has-error').addClass('has-success');
                }
                load = layer.load('正在提交！！！');
            },
            dataType: "json",
            clearForm: false
        });

    });

    /**
     * @author Chris
     * @date 2018/5/12 14:28
     * @todo  删除ip
     */
    function deleteAgent(id) {

        $.ajax({
            type: "post",
            url: "/admin/ip/delete",
            data: {"id": id},
            cache: false,
            success: function (data) {
                layer.msg(data.message);
                setTimeout(function () {
                    layer.close();
                    location.reload();
                }, 2000);

            }, error: function (data) {
                layer.msg(data.message, function () {
                }), !1;
            }
        });


    }


</script>

</body>

</html>
