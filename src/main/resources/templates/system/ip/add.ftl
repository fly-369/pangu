<div class="uk-layui-form">
	<form class="layui-form"  method="post" enctype="multipart/form-data">
		<div class="layui-collapse">
			<div class="layui-colla-item">
				<div class="layui-colla-content layui-show">
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label" >IP地址</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text" name="ip" id="ip" required  lay-verify="required" autocomplete="off"
										class="layui-input">
								<font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>
							</div>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">备注信息</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text" name="remark" id="remark" required  lay-verify="required" autocomplete="off"
									   class="layui-input">
								<font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="layui-form-button">
			<div class="layui-button-block"  style="margin:0 auto;width:200px;">
				<button class="layui-btn" lay-submit lay-filter="formDemo" onclick="addIp()">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		</div>
	</form>
</div>

<script>

    /**
     * @author Chris
     * @date 2018/4/14 14:04
     * @todo   新增IP地址
     */
    function addIp() {
        var ip=$("#ip").val();
        if(ip==null){
            return  layer.msg("请输入你要新增的IP地址");
		}
        $.post('/admin/ip/save', {ip:ip,remark:$("#remark").val()}, function (result) {
            if (result && result.status == 200) {
                layer.msg(result.message);
                setTimeout(function () {
                    /* $("#close").click();*/
                    location.href="/admin/ip/ipwhitelist.html";
                }, 3000);
            }
            return layer.msg(result.message, so.default), !1;
        }, 'json');

    }




</script>