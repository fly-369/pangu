<div class="row">
    <div class="col-lg-12">
        <h1 class="site-h1" style="background-color:#FFFFFF;">
            IP白名单<#if ipWhiteList??>（${ipWhiteList.totalElements!''}）</#if>
        <span style="float:right;">
				<button class="layui-btn layui-btn-sm green" href="/admin/ip/add.html" data-toggle="ajax" data-width="400" data-height="300" data-title="新增IP">
					新增IP
				</button>
			</span>
        </h1>
        <div class="row" style="padding:5px;">
            <div class="col-lg-12">
                <table class="layui-table" lay-skin="line">
                    <thead>
                    <tr>
                        <th>编号</th>
                        <th>IP</th>
                        <th>操作人</th>
                        <th>操作时间</th>
                        <th>备注</th>
                        <th style="white-space:nowrap;" nowrap="nowrap">操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    <#if ipWhiteList?? && ipWhiteList.content??>
                    <#list ipWhiteList.content as info>
                    <tr>
                        <td>
                            ${info.id}
                        </td>
                        <td>${info.ip} </td>
                        <td>${info.operator}</td>
                        <td>${info.operate_time?string('yyyy-MM-dd HH:mm:ss')}</td>
                        <td>${info.remark}</td>
                        <td style="white-space:nowrap;" nowrap="nowrap">
                            <a href="/apps/shop/edit.html?id=${info.id!''}" data-toggle="ajax" data-width="950" data-height="600" data-title="编辑用户信息">
                                <i class="layui-icon">&#xe642;</i>
                                编辑
                            </a>
                            <#if !(wares.warestype?? && wares.warestype == "0") && wares.datastatus != true >
                            <a onclick="deleteIp(${info.id})" style="margin-left:10px;" data-toggle="tip" data-title="请确认是否删除记录？">
                                <i class="layui-icon" style="color:red;">&#x1006;</i>
                                删除
                            </a>
                        </#if></td>
                    </tr>
                    </#list>
                </#if>
    </tbody>
    </table>
</div>
</div>
<div class="row" style="padding:5px;">
    <div class="col-lg-12" id="page" style="text-align:center;"></div>
</div>
</div>
</div>
<script>
    layui.use('laypage', function(){
        var laypage = layui.laypage;
        laypage.render({
            elem: 'page'
            ,count: <#if ipWhiteList??>${ipWhiteList.totalElements}<#else>0</#if> //总记录
            ,limit: <#if ipWhiteList??>${ipWhiteList.size}<#else>0</#if> //总记录
            ,curr:<#if ipWhiteList??>${ipWhiteList.number+1}<#else>0</#if>
            ,groups: 5 //连续显示分页数
            ,jump:function(data , first){
            if(!first){
                location.href = "/admin/ip/ipwhitelist.html?p="+data.curr ;
            }
        }
    });
    });


</script>

<script>
    /**
     * @author Chris
     * @date 2018/4/14 14:26
     * @todo   删除IP地址
     */

    function deleteIp(id) {
        $.post('/admin/ip/delete', {id:id}, function (result) {
            if (result && result.status == 200) {
                layer.msg(result.message);
                setTimeout(function () {
                    /* $("#close").click();*/
                    location.href="/admin/ip/ipwhitelist.html";
                }, 3000);
            }
            return layer.msg(result.message, so.default), !1;
        }, 'json');

    }



</script>