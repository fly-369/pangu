<div class="uk-layui-form">
	<form class="layui-form"  enctype="multipart/form-data">
		<div class="layui-collapse">
			<div class="layui-colla-item">
				<div class="layui-colla-content layui-show">

					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">菜单名称</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text" name="name"  id="name" value="${menu.name}" required  lay-verify="required" autocomplete="off"
									   class="layui-input">
							</div>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">图标</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text"  name="icon" id="icon" value="${menu.icon}" required  lay-verify="required" autocomplete="off"
									   class="layui-input" >
							</div>
						</div>
					</div>

                    <input name="menuId" id="menuId" value="${menu.menuId}" style="display: none">
				</div>
			</div>
		</div>

		<div class="layui-form-button">
			<div class="layui-button-block" style="margin:0 auto;width:200px;">
				<button class="layui-btn" lay-submit lay-filter="formDemo" onclick="commitMenu()">立即提交</button>
				<#--<button type="reset" class="layui-btn layui-btn-primary">重置</button>-->
			</div>
		</div>
	</form>
</div>

<script>

    function commitMenu(){
        //判断参数
        if($.trim($("#name").val()) == ''){
            layer.msg('请输入菜单名称',function(){});
            return !1;
        }
        if($.trim($("#icon").val()) == ''){
            layer.msg('请输入菜单图标',function(){});
            return !1;
        }

        $.ajax({
            type:"post",
            url:"admin/user/updateMenu",
            data:{"name":$("#name").val(),"icon":$("#icon").val(),"menuId":$("#menuId").val()},
            cache:false,
            success:function(data){
                layer.msg(data.message);
            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });
    }
  /*  $(function(){
        var load;
        $("#eidtMainMenu").ajaxForm({
            success:function (result){
                layer.close(load);
                if(result && result.status != 200){
                    return layer.msg(result.message,function(){}),!1;
                }else{
                    layer.msg(result.message);
                    $("form :password").val('');

                    setTimeout(function(){
                        //3秒后刷新
                        layer.close();
                        location.reload();
                    },2000);
                }
            },
            beforeSubmit:function(){
                //判断参数
                if($.trim($("#menuname").val()) == ''){
                    layer.msg('请输入菜单名称',function(){});
                    return !1;
                }
				load = layer.load('正在提交！！！');
            },
            dataType:"json",
            clearForm:false
        });

    });*/

</script>