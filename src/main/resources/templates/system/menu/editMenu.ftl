<div class="uk-layui-form">
	<form class="layui-form"  id="eidtMainMenu"  enctype="multipart/form-data">
		<div class="layui-collapse">
			<div class="layui-colla-item">
				<div class="layui-colla-content layui-show">

					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">菜单名称</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text" name="name"  id="name" value="${menu.name}" required  lay-verify="required" autocomplete="off"
									   class="layui-input">
							</div>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">菜单地址</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text"  name="perms" id="perms" value="${menu.perms}" required  lay-verify="required" autocomplete="off"
									   class="layui-input" >
							</div>
						</div>
					</div>

                    <input name="menuId" id="menuId" value="${menu.menuId}" style="display: none">
				</div>
			</div>
		</div>

		<div class="layui-form-button">
			<div class="layui-button-block" style="margin:0 auto;width:200px;">
				<button class="layui-btn" lay-submit lay-filter="formDemo" onclick="commitMenu();">立即提交</button>
			</div>
		</div>
	</form>
</div>

<script>

	function commitMenu(){
        //判断参数
        if($.trim($("#name").val()) == ''){
            layer.msg('请输入菜单名称',function(){});
            return !1;
        }
        if($.trim($("#perms").val()) == ''){
            layer.msg('请输入菜单地址',function(){});
            return !1;
        }

        $.ajax({
            type:"post",
            url:"admin/user/updateMenu",
            data:{"name":$("#name").val(),"perms":$("#perms").val(),"menuId":$("#menuId").val()},
            cache:false,
            success:function(data){
                layer.msg(data.message);
            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });
	}


</script>