<div class="layui-layout layui-layout-content">
    <div class="layui-body" style="left: 10px">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="site-h1" style="background-color:#FFFFFF;">
                   菜单查询
                    <span style="float:right;">
				<#--<button class="layui-btn layui-btn-sm green" href="/admin/user/addMenu.html" data-toggle="ajax" data-width="550" data-title="创建新字典项">
				   新增菜单
				</button>-->
			</span>
                </h1>
                <div class="row" style="padding:5px;">
                    <div class="col-lg-12">
                        <table class="layui-table" lay-skin="line">
                            <thead>
                            <tr>
                                <th>菜单ID</th>
                                <th>菜单名称</th>
                                <th>权限</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if menuList?? && menuList.content??>
                            <#list menuList.content as info>
                            <tr>
                                <td>${info.menuId}</td>
                                <td><a href="/admin/user/menuIndex?parentId=${info.menuId}" >${info.name}</a> </td>
                                <td>${info.perms}</td>
                              <#--  <td>
                                   <#if info.type==1>
                                       主菜单
                                       <#elseif info.type==2>
                                       二级菜单
                                       <#else>
                                       子菜单
                                   </#if>


                                </td>
                                <td>fa fw-map</td>
                                <td>${info.craeteTime}</td>-->
                               <#-- <td>
                                   <#if info.type==1>
                                         <a href="/admin/user/editMainMenu.html?menuId=${info.menuId!''}" data-width="550" data-toggle="ajax" title="编辑${info.name!''}">
                                             <i class="layui-icon">&#xe642;</i>
                                             编辑
                                         </a>
                                   <#elseif info.type==2>
                                       <a href="/admin/user/editSecondMenu.html?menuId=${info.menuId!''}" data-width="550" data-toggle="ajax" title="编辑${info.name!''}">
                                           <i class="layui-icon">&#xe642;</i>
                                           编辑
                                       </a>
                                   <#else >
                                       <a href="/admin/user/editMenu.html?menuId=${info.menuId!''}" data-width="550" data-toggle="ajax" title="编辑${info.name!''}">
                                           <i class="layui-icon">&#xe642;</i>
                                           编辑
                                       </a>
                                   </#if>

                                    <a href="/admin/sysdic/delete.html?id=${dic.id!''}&p=${sysDicList.number+1}" data-toggle="tip" data-title="删除字典项同时会删除字典项下的所有子项，请确认是否删除字典项“${dic.name!''}”？" title="删除${dic.name!''}">
                                        <i class="layui-icon" style="color:red;">&#x1006;</i>
                                        删除
                                    </a>
                                </td>-->
                            </tr>
        </#list>
    </#if>
    </tbody>
    </table>
</div>
</div>
<div class="row" style="padding:5px;">
    <div class="col-lg-12" id="page" style="text-align:center;"></div>
</div>
</div>
</div>
</div>
</div>
<script>
    layui.use('laypage', function(){
        var laypage = layui.laypage;
        laypage.render({
            elem: 'page'
            ,count: <#if menuList??>${menuList.totalElements}<#else>0</#if> //总记录
            ,limit: <#if menuList??>${menuList.size}<#else>0</#if> //总记录
            ,curr:<#if menuList??>${menuList.number+1}<#else>0</#if>
            ,groups: 5 //连续显示分页数
            ,jump:function(data , first){
            if(!first){
                location.href = "/admin/user/menuIndex.html?p="+data.curr ;
            }
        }
    });
    });
</script>