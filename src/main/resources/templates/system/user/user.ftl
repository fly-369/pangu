<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="${request.contextPath}/statics/img/favicon.html">

    <title>管理员查询</title>

    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${request.contextPath}/statics/font-awesome/css/font-awesome.css">
    <link href="${request.contextPath}/statics/css/slidebars.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style-responsive.css" rel="stylesheet" />
</head>

<body>
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            管理员查询
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                        </header>

                        <div clss="well">
                            <div class="form-group" style="margin-bottom: 0px;margin-top: 10px;margin-left: 15px">
                            <#-- <shiro.hasPermission name="admin">-->
                                <a class="btn btn-success" style="background-color:#5160cd;border-color: #515bcd" onclick="$('#addOperator').modal();">增加</a>&nbsp;&nbsp;
                                <a class="btn btn-success" style="background-color:#5160cd;border-color: #515bcd" href="${request.contextPath}/admin/user/userIndex">刷新</a>
                            <#--   </shiro.hasPermission>-->
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" >
                                    <thead>
                                    <tr>
                                        <th>用户名称</th>
                                        <th>角色</th>
                                        <th>在线状态</th>
                                        <th>创建时间</th>
                                        <th>账号状态</th>
                                        <th>最后登录时间</th>
                                        <th>最后登录地址</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <#if userList??>
					                 <#list userList as info>
                                    <tr class="gradeX">
                                        <td>${info.username}</td>
                                        <td>
                                            <#if info.sysRoleEntities??>
                                                <#list info.sysRoleEntities as role>
                                                    ${role.roleName}&nbsp;
                                                </#list>
                                            </#if>
                                        </td>
                                           <#if info.online==1>
                                               <td style="color: #00B83F">在线</td>
                                           <#else >
                                            <td>离线</td>
                                           </#if>
                                        <td>${info.createTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                        <#if info.status==1>
                                                <td>正常</td>
                                        <#elseif info.status==0>
                                                <td>禁用</td>
                                        </#if>
                                        <td>${info.lastLoginTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                        <td>${info.lastLoginIp}</td>
                                        <td>
                                             <#if info.online==1>
                                              <div class="btn-group ">
                                                  <button style="background-color: #e4e8e5"  class="btn btn-white btn-sm edit"  onclick="forceLogout('${info.sessionId}')">下线</button>
                                              </div>
                                             </#if>
                                            <div class="btn-group ">
                                                <button style="background-color: #e4e8e5"  class="btn btn-white btn-sm edit"  onclick="operatorLocked('${info.userId}','${info.status}')">禁用/启用</button>
                                            </div>
                                            <div class="btn-group ">
                                                <button style="background-color: #e4e8e5"  class="btn btn-white btn-sm edit"  onclick="deleteUser('${info.username}')">删除</button>
                                            </div>
                                            <div class="btn-group ">
                                                <button style="background-color: #e4e8e5"  class="btn btn-white btn-sm edit"  onclick="updateRole('${info.username}')">修改角色</button>
                                            </div>

                                        </td>
                                    </tr>
                                    </#list>
                                    </#if>
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>


        </section>
    </section>




<div class="modal fade" id="addOperator" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 180px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">添加管理员</h4>
            </div>
            <div class="modal-body">
                <form id="operator" enctype="multipart/form-data" action="${request.contextPath}/admin/user/addUser"  method="post">
                    <div class="form-group">
                        <label  class="control-label">管理员名称:</label>
                        <input type="text" style="height:30px" class="form-control" name="username" id="account" placeholder="请输入管理员名称"/>
                    </div>
                    <div class="form-group">
                        <label  class="control-label">登录密码:</label>
                        <input type="text" style="height:30px" class="form-control" name="password" id="password" placeholder="请输入登陆密码"/>
                    </div>
                    <div class="form-group">
                        <label  class="control-label">确认密码:</label>
                        <input type="text" style="height:30px" class="form-control" name="rePassword" id="rePassword" placeholder="请再次输入密码"/>
                    </div>
                    <div class="form-group">
                        <label  class="control-label">选择角色:</label>
                       <#-- <select name="roleIdList[0]" id="role">
                              <#if roleList??>
                                  <#list roleList as role>
                                    <option value="${role.roleId}">${role.roleName}</option>
                                  </#list>
                              </#if>
                        </select>-->
                        <#if roleList??>
                            <#list roleList as role>
                        <input type="checkbox" value="${role.roleId}" id="role" name="roleIdList" />${role.roleName}
                            </#list>
                        </#if>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                        <button type="submit"  class="btn btn-primary" style="background: #ea7137; border-color: #f29365">提交</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

    <div class="modal fade" id="updateRoleModal" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="margin-top: 180px">
                <div class="modal-header" style="background: #beb04d">
                    <h4 class="modal-title" id="addroleLabel">修改管理员角色</h4>
                </div>
                <div class="modal-body">
                    <form id="updateRole" enctype="multipart/form-data" action="${request.contextPath}/admin/user/updateRole"  method="post">
                        <div class="form-group">
                            <label  class="control-label">管理员名称:</label>
                            <input type="text" style="height:30px" class="form-control" name="username" id="userNameByUpdateRole" disabled="disabled"/>
                        </div>
                        <div class="form-group">
                            <label  class="control-label">选择角色:</label>
                        <#if roleList??>
                            <#list roleList as role>
                        <input type="checkbox" value="${role.roleId}" id="roleUpdate" name="roleIdList" />${role.roleName}
                            </#list>
                        </#if>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                            <button type="submit"  class="btn btn-primary" style="background: #ea7137; border-color: #f29365">提交</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>



    <script src="${request.contextPath}/statics/js/jquery.js"></script>
    <script src="${request.contextPath}/statics/layui.all.js"></script>
    <script src="${request.contextPath}/statics/css/layui-xtree/layui-xtree.js"></script>
    <script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="${request.contextPath}/statics/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="${request.contextPath}/statics/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="${request.contextPath}/statics/js/slidebars.min.js"></script>
    <script src="${request.contextPath}/statics/js/common-scripts.js"></script>
    <script src="${request.contextPath}/statics/js/jquery.form-3.14.js"></script>


    <script>
        /**
         * @author Chris
         * @date 2018/3/29 17:05
         * @todo   新增管理员
        */
    $(function(){
        var load;
        $("#operator").ajaxForm({
            success:function (result){
                layer.close(load);
                if(result && result.status != 200){
                    return layer.msg(result.message,function(){}),!1;
                }else{
                    layer.msg(result.message);
                    $("form :password").val('');

                    setTimeout(function(){
                        //3秒后刷新
                        layer.close();
                        location.reload();
                    },2000);
                }
            },
            beforeSubmit:function(){
                //判断参数
                if($.trim($("#account").val()) == ''){
                    layer.msg('请输入管理员名称',function(){});
                    $("#account").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#account").parent().removeClass('has-error').addClass('has-success');
                }
                if($.trim($("#password").val()) == ''){
                    layer.msg('请输入密码',function(){});
                    $("#password").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#password").parent().removeClass('has-error').addClass('has-success');
                }

                if($.trim($("#rePassword").val()) == ''){
                    layer.msg('请再次输入密码',function(){});
                    $("#rePassword").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#rePassword").parent().removeClass('has-error').addClass('has-success');
                }
                if($("#rePassword").val() != $("#password").val()){
                    return layer.msg('2次密码输入不一致。',function(){}),!1;
                }
                if($.trim($("#role").val()) == ''){
                    layer.msg('请选择角色',function(){});
                    $("#role").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#role").parent().removeClass('has-error').addClass('has-success');
                }

                load = layer.load('正在提交！！！');
            },
            dataType:"json",
            clearForm:false
        });

    });

   /**
    * @author Chris
    * @date 2018/3/29 17:04
    * @todo   禁用启用用户
   */
         function operatorLocked(userId,status) {

           $.post('${request.contextPath}/admin/user/disable', {userId:userId,status:status}, function (result) {
               if (result && result.status == 200) {
                   layer.msg(result.message);
                   setTimeout(function () {
                       /* $("#close").click();*/
                       location.reload();
                   }, 3000);
               }
               return layer.msg(result.message, );
           }, 'json');


       }
    /**
     * @author Chris
     * @date 2018/3/29 17:04
     * @todo   删除用户
    */
    function deleteUser(username){
        $.post('${request.contextPath}/admin/user/deleteUser', {username:username}, function (result) {
            if (result && result.status == 200) {
                layer.msg(result.message);
                setTimeout(function () {
                    /* $("#close").click();*/
                    location.reload();
                }, 3000);
            }
            return layer.msg(result.message);
        }, 'json');
    }

    function forceLogout(userId){
        $.ajax({
            type:"post",
            url:"${request.contextPath}/admin/user/forceLogout",
            data:{"userId":userId},
            cache:false,
            success:function(data){
                layer.msg(data.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });
    }

    function updateRole(userName){
        $("#userNameByUpdateRole").val(userName);
        $('#updateRoleModal').modal();
    }

        $(function(){
            var load;
            $("#updateRole").ajaxForm({
                success:function (result){
                    layer.close(load);
                    if(result && result.status != 200){
                        return layer.msg(result.message,function(){}),!1;
                    }else{
                        layer.msg(result.message);
                        setTimeout(function(){
                            //3秒后刷新
                            layer.close();
                            location.reload();
                        },2000);
                    }
                },
                beforeSubmit:function(){
                    if($.trim($("#roleUpdate").val()) == ''){
                        layer.msg('请选择角色',function(){});
                        $("#roleUpdate").parent().removeClass('has-success').addClass('has-error');
                        return !1;
                    }else{
                        $("#roleUpdate").parent().removeClass('has-error').addClass('has-success');
                    }

                    load = layer.load('正在提交！！！');
                },
                dataType:"json",
                clearForm:false
            });

        });




</script>
</body>
</html>
