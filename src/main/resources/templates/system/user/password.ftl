<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="${request.contextPath}/statics/img/favicon.html">
    <title>修改密码</title>

    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${request.contextPath}/statics/font-awesome/css/font-awesome.css">
    <link href="${request.contextPath}/statics/css/slidebars.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style-responsive.css" rel="stylesheet" />

</head>

<body>

<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="panel">
            <header class="panel-heading">
                修改密码
            </header>
            <div class="panel-body">
                <div class="row toastr-row">
                    <div class="col-md-3">

                        <div class="form-group">
                            <div class="controls toastr-pad">
                                <label class="control-label" for="showEasing">用户名</label>
                                <input id="username" type="text" value="${username}" class="form-control input-small" disabled="disabled">
                                <label class="control-label" for="hideEasing">原密码</label>
                                <input id="password" type="text" placeholder="请输入原密码" class="form-control input-small">
                                <label class="control-label" for="showMethod">新密码</label>
                                <input id="newpassowrd" type="text" placeholder="请输入新密码" class="form-control input-small">
                                <label class="control-label" for="hideMethod">再次输入新密码</label>
                                <input id="agnewpassword" type="text" placeholder="请再次输入新密码" class="form-control input-small">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-default" data-dismiss="modal" id="close" onclick="toMainPage()"  target="pageframe">返回首页</button>
                            <button type="button"  class="btn btn-primary" style="background: #ea7137; border-color: #f29365" onclick="updatePas()">提交修改</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>



</body>

<script src="${request.contextPath}/statics/js/jquery.js"></script>
<script src="${request.contextPath}/statics/layui.all.js"></script>
<script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
<script src="${request.contextPath}/statics/js/jquery.nicescroll.js" type="text/javascript"></script>

<script>
    function updatePas(){
        var password=$("#password").val();
        var newPas=$("#newpassowrd").val();
        var agePas=$("#agnewpassword").val();
        if(password==null||password==""){
            return layer.msg('请输入原密码！',function(){}),!1;
        }
        if(newPas==null||newPas==""){
            return layer.msg('请输入新密码！',function(){}),!1;
        }
        if(agePas==null||agePas==""){
            return layer.msg('请再次输入新密码！',function(){}),!1;
        }
        if(agePas!=newPas){
            return layer.msg('两次密码不一致，请重新输入！',function(){}),!1;
        }

        $.ajax({
            type:"post",
            url:"${request.contextPath}/admin/user/updatePas",
            data:{"password":password,"newPassword":newPas},
            cache:false,
            success:function(data){
                layer.msg(data.message,function(){}),!1;

            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });


    }


    function toMainPage(){
        parent.location.href ='/index.html';
    }




</script>

</html>

