<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="${request.contextPath}/statics/img/favicon.html">

    <title>角色管理</title>

    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="${request.contextPath}/statics/font-awesome/css/font-awesome.css">
<#--    <link href="${request.contextPath}/statics/advanced-datatable/media/css/demo_page.css" rel="stylesheet" />
    <link href="${request.contextPath}/statics/advanced-datatable/media/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="${request.contextPath}/statics/assets/data-tables/DT_bootstrap.css" />-->
    <link href="${request.contextPath}/statics/css/slidebars.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style-responsive.css" rel="stylesheet" />

</head>

<body>

<section id="main-content">
        <section class="wrapper">
            <!-- page start-->
            <div class="row">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            角色查询
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                             </span>
                        </header>

                        <div clss="well">
                            <div class="form-group" style="margin-bottom: 0px;margin-top: 10px;margin-left: 15px">
                            <#-- <shiro.hasPermission name="admin">-->
                                <a class="btn btn-success" onclick="addRole()">增加</a>&nbsp;&nbsp;
                                <a class="btn btn-success" href="${request.contextPath}/admin/user/roleInfo">刷新</a>
                            <#--   </shiro.hasPermission>-->
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="adv-table">
                                <table  class="display table table-bordered table-striped" >
                                    <thead>
                                    <tr>
                                        <th>角色名称</th>
                                        <th>最后更新时间</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <#if list?? &&  (list?size > 0)>
                                    <#list list as info>
                                    <tr class="gradeX">
                                        <td>${info.roleName}</td>
                                        <td>${info.updateTime?string('yyyy-MM-dd hh:mm:ss')}</td>
                                        <td>
                                            <#if info.roleName!='admin'>
                                            <div class="btn-group ">
                                                <button style="background-color: #e4e8e5"  onclick="update('${info.roleName}','${info.roleId}');" class="btn btn-white btn-sm edit" >修改权限</button>
                                            </div>
                                            <div class="btn-group ">
                                                <button style="background-color: #e4e8e5"  class="btn btn-white btn-sm edit"  onclick="deleteRole('${info.roleId}')"  >删除</button>
                                            </div>
                                            </#if>
                                        </td>
                                    </tr>
                                    </#list>
                                    </#if>

                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </section>
                </div>
            </div>
 </section>


<div class="modal fade" id="updateP" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 80px;margin-bottom: 20px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">角色-权限管理</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label  class="control-label">角色名称:</label>
                    <input type="text" style="height:30px" class="form-control" name="roleName" id="roleName" placeholder="角色名称" disabled="disabled"/>
                    <input type="text" style="height:30px;display: none" class="form-control" name="roleId" id="roleId" placeholder="角色ID"/>
                </div>
                <form class="layui-form">
                    <div id="xtree3" style="border:1px solid #ebece7;padding: 10px 0 25px 5px;"></div>
                </form>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                    <button type="button"  id="save" class="btn btn-primary" style="background: #ea7137; border-color: #f29365">提交</button>
                </div>

            </div>

        </div>
    </div>
</div>



    <div class="modal fade" id="addRole" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="margin-top: 80px;margin-bottom: 20px">
                <div class="modal-header" style="background: #beb04d">
                    <h4 class="modal-title">新增角色</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label  class="control-label">角色名称:</label>
                        <input type="text" style="height:30px" class="form-control" name="roName" id="roName" placeholder="请输入角色名称"/>
                    </div>

                    <label  class="control-label">请赋予角色权限</label>
                    <form class="layui-form">
                        <div id="xtree2" style="border:1px solid #ebece7;padding: 10px 0 25px 5px;"></div>
                    </form>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                        <button type="button"  id="saveAddRole" class="btn btn-primary" style="background: #ea7137; border-color: #f29365">提交</button>
                    </div>

                </div>

            </div>
        </div>
    </div>


<script src="${request.contextPath}/statics/js/jquery.js"></script>
    <script src="${request.contextPath}/statics/layui.all.js"></script>
<script src="${request.contextPath}/statics/css/layui-xtree/layui-xtree.js"></script>
<script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="${request.contextPath}/statics/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="${request.contextPath}/statics/js/jquery.nicescroll.js" type="text/javascript"></script>
<script src="${request.contextPath}/statics/js/slidebars.min.js"></script>
<script src="${request.contextPath}/statics/js/common-scripts.js"></script>
</body>

<script>

    //新增角色
    function addRole(){
        $("#addRole").modal();
        //查询所有菜单，因为是新增角色 ，所以角色id就等于0吧
        $.ajax({
            type:"post",
            url:"${request.contextPath}/admin/user/roleInfo",
            cache:false,
            success:function(data){
                var  json=JSON.parse(data.role);
                tree(json);
            },error:function(data){
                alert("请求错误:"+data);
            }
        });

    }

    function update(roleName,roleId){
        $("#roleName").val(roleName);
        $("#roleId").val(roleId);
        $("#updateP").modal();

        $.ajax({
            type:"post",
            url:"${request.contextPath}/admin/user/roleInfo",
            data:{"roleId":roleId},
            cache:false,
            success:function(data){
              var  json=JSON.parse(data.role);
                loadTree(json);
            },error:function(data){
               alert("请求错误:"+data);
            }
        });

    }

    function loadTree(json){

        layui.use(['form'], function () {
            var form = layui.form;
            var xtree3 = new layuiXtree({
                elem: 'xtree3'                  //必填三兄弟之老大
                , form: form                    //必填三兄弟之这才是真老大
                , data: json //必填三兄弟之这也算是老大
                , isopen: false  //加载完毕后的展开状态，默认值：true
                , ckall: true    //启用全选功能，默认值：false
                , ckallback: function () {
                } //全选框状态改变后执行的回调函数
                , icon: {        //三种图标样式，更改几个都可以，用的是layui的图标
                    open: "&#xe7a0;"       //节点打开的图标
                    , close: "&#xe622;"    //节点关闭的图标
                    , end: "&#xe621;"      //末尾节点的图标
                }
                , color: {       //三种图标颜色，独立配色，更改几个都可以
                    open: "#ee4d3b"        //节点图标打开的颜色
                    , close: "#EEC591"     //节点图标关闭的颜色
                    , end: "#828282"       //末级节点图标的颜色
                }
                , click: function (data) {  //节点选中状态改变事件监听，全选框有自己的监听事件
                    console.log(data.elem); //得到checkbox原始DOM对象
                    console.log(data.elem.checked); //开关是否开启，true或者false
                    console.log(data.value); //开关value值，也可以通过data.elem.value得到
                    console.log(data.othis); //得到美化后的DOM对象
                }
            });

            document.getElementById('save').onclick = function () {
                var oCks = xtree3.GetChecked(); //这是方法
                var roleId=$("#roleId").val();
                var roleName=$("#roleName").val();
                var str="";
                for (var i = 0; i < oCks.length; i++) {
                    str+=oCks[i].value+"#";
                }
                alert("str"+str);
                $.ajax({
                    type:"post",
                    url:"${request.contextPath}/admin/user/roleUpdate",
                    data:{"roleId":roleId,"roleName":roleName,"menId":str.toString()},
                    cache:false,
                    success:function(data){
                        layer.msg(data.message,function(){}),!1;
                    },error:function(data){
                        layer.msg(data.message,function(){}),!1;
                    }
                });
            }
  });
    }

    function tree(json){

        layui.use(['form'], function () {
            var form = layui.form;
            var xtree3 = new layuiXtree({
                elem: 'xtree2'                  //必填三兄弟之老大
                , form: form                    //必填三兄弟之这才是真老大
                , data: json //必填三兄弟之这也算是老大
                , isopen: false  //加载完毕后的展开状态，默认值：true
                , ckall: true    //启用全选功能，默认值：false
                , ckallback: function () {
                } //全选框状态改变后执行的回调函数
                , icon: {        //三种图标样式，更改几个都可以，用的是layui的图标
                    open: "&#xe7a0;"       //节点打开的图标
                    , close: "&#xe622;"    //节点关闭的图标
                    , end: "&#xe621;"      //末尾节点的图标
                }
                , color: {       //三种图标颜色，独立配色，更改几个都可以
                    open: "#ee4d3b"        //节点图标打开的颜色
                    , close: "#EEC591"     //节点图标关闭的颜色
                    , end: "#828282"       //末级节点图标的颜色
                }
                , click: function (data) {  //节点选中状态改变事件监听，全选框有自己的监听事件
                    console.log(data.elem); //得到checkbox原始DOM对象
                    console.log(data.elem.checked); //开关是否开启，true或者false
                    console.log(data.value); //开关value值，也可以通过data.elem.value得到
                    console.log(data.othis); //得到美化后的DOM对象
                }
            });
            document.getElementById('saveAddRole').onclick = function () {
                var oCks = xtree3.GetChecked(); //这是方法
                var str="";
                var roleName=$("#roName").val();
                for (var i = 0; i < oCks.length; i++) {
                    str+=oCks[i].value+"#";
                }
              if(roleName==null||roleName===""){
                  return layer.msg("请输入角色名称",function(){}),!1;
              }
                $.ajax({
                    type:"post",
                    url:"${request.contextPath}/admin/user/addRole",
                    data:{"roleName":roleName,"menId":str.toString()},
                    cache:false,
                    success:function(data){
                        layer.msg(data.message,function(){}),!1;
                    },error:function(data){
                        layer.msg(data.message,function(){}),!1;
                    }
                });

            }
        });
    }

    function  deleteRole(roleId){

        $.post('${request.contextPath}/admin/user/deleteRole', {roleId:roleId}, function (result) {
            if (result && result.status == 200) {
                layer.msg(result.message);
                setTimeout(function () {
                     $("#close").click();
                    location.reload();
                }, 3000);
            }
            return layer.msg(result.message, so.default), !1;
        }, 'json');



    }



</script>

</html>
