<!DOCTYPE html>


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" type="image/x-icon" href="${request.contextPath}/statics/images/favicon.ico?t=${.now?long}"/>
    <title>登录</title>

    <link rel="stylesheet" href="${request.contextPath}/statics/css/bootstrap.min.css">
    <link rel="stylesheet" href="${request.contextPath}/statics/font-awesome/css/font-awesome.css">

    <link rel="stylesheet" href="${request.contextPath}/statics/css/style.css">
    <link rel="stylesheet" href="${request.contextPath}/statics/css/style-responsive.css">
    <link rel="stylesheet" href="${request.contextPath}/statics/css/layui.css">


    <script src="${request.contextPath}/statics/js/jquery.js"></script>
    <script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
    <script src="${request.contextPath}/statics/layui.all.js"></script>

    <style>
        body{
            background: url(${request.contextPath}/statics/images/bg.jpg) no-repeat;
            background-size:100%;
        }
    </style>
</head>

<body class="login-body"   onkeydown="keyLogin()">

<div class="container">

    <form class="form-signin" >
        <h2 class="form-signin-heading">棋牌后台</h2>
        <div class="login-wrap">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="用户名/邮箱"  id="username">
            </div>
            <div class="form-group">
                <input type="password" class="form-control" placeholder="密码" id="password">
            </div>

            <div class="form-group" id="vcode">
                <input type="text" name="vcode"   placeholder="请输入验证码" style="width: 130px;height: 42px;">
                <img src="/getGifCode"/>

            </div>

            <button class="btn btn-lg btn-login btn-block" type="button" onclick="login()">登录</button>
        </div>

    </form>

</div>

</body>

<script>

    jQuery(document).ready(function() {
        //动态验证码
        $("#vcode").on("click",'img',function(){
            /**动态验证码，改变地址，多次在火狐浏览器下，不会变化的BUG，故这样解决*/
            var i = new Image();
            i.src = '/getGifCode?'  + Math.random();
            $(i).replaceAll(this);
        });
    });

    function login(){

        var account=$("#username").val();
        var password=$("#password").val();
        var vcode=$("input[name=vcode]").val();

        if(account==null||account==""){
            return layer.msg('用户名不能为空！',function(){}),!1;
        }
        if(password==null||password==""){
            return layer.msg('密码不能为空！',function(){}),!1;
        }
        /* if(vcode==null||vcode==""){
             return layer.msg('验证码不能为空！',function(){}),!1;
         }
         if($('[name=vcode]').val().length !=4){
             return layer.msg('验证码的长度为4位！',function(){}),!1;
         }*/

        $.ajax({
            type:"post",
            url:"/sys/login",
            data:{"username":account,"password":password,"vcode":vcode},
            cache:false,
            success:function(data){

                if(data.message=="success"){
                    parent.location.href ='/index.html';
                }else {
                    layer.msg(data.message,function(){}),!1;
                }
            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });

    }

    function keyLogin(){
        if(event.keyCode==13) {
            login();
        }
    }


</script>

</html>