<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <meta name="viewport"
          content="width=device-width, maximum-scale=1.0, initial-scale=1.0,initial-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <title>盘古游戏-运营管理平台</title>
    <link rel="shortcut icon" type="image/x-icon" href="/images/favicon.ico?t=1487250759056"/>
    <link rel="stylesheet" href="${request.contextPath}/statics/js/select/css/select2.min.css"/>
    <link rel="stylesheet" href="${request.contextPath}/statics/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="${request.contextPath}/statics/css/bootstrap.min.css">
    <link rel="stylesheet" href="${request.contextPath}/statics/css/style.css">
    <link rel="stylesheet" href="${request.contextPath}/statics/css/style-responsive.css">
    <link rel="stylesheet" href="${request.contextPath}/statics/css/slidebars.css">
    <link rel="stylesheet" href="${request.contextPath}/statics/css/layui.css">

    <script src="${request.contextPath}/statics/js/jquery-1.10.2.min.js"></script>
    <script src="${request.contextPath}/statics/js/jquery.form.js"></script>
    <script src="${request.contextPath}/statics/js/jquery.nicescroll.js"></script>
    <script src="${request.contextPath}/statics/js/slidebars.min.js"></script>
    <script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
    <script src="${request.contextPath}/statics/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="${request.contextPath}/statics/js/common-scripts.js"></script>
    <script src="${request.contextPath}/statics/js/ztree/jquery.ztree.all.min.js"></script>
    <script type="text/javascript" src="${request.contextPath}/statics/js/jquery.darktooltip.js"></script>
    <script src="${request.contextPath}/statics/layui.js"></script>
   <#--<script src="/lay/modules/layer.js"></script>-->
    <script src="${request.contextPath}/statics/js/ukefu.js"></script>


</head>

<body>
<section id="container">
    <header class="header white-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <a class="logo" href="/index"><img src="${request.contextPath}/statics/images/logo.png"></a>
        <!--<a href="index.html" class="logo">盘古<span>游戏</span></a>-->
        <div class="top-nav ">
            <ul class="nav pull-right top-menu">

                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="username">Chris</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu extended logout">
                        <div class="log-arrow-up"></div>
                        <li><a href="#"><i class=" fa fa-suitcase"></i>个人信息</a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> 设置</a></li>
                        <li><a href="/admin/user/pas.html" target="pageframe"><i class="fa fa-bell-o"></i>修改密码</a></li>
                        <li><a href="login.html"><i class="fa fa-key"></i> 退出登录</a></li>
                    </ul>
                </li>

            </ul>


        </div>
    </header>

    <aside>
        <div id="sidebar" class="nav-collapse ">
            <ul class="sidebar-menu" id="nav-accordion">

                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-users"></i>
                        <span>游戏设置</span>
                    </a>
                    <ul class="sub">
                        <li><a href="/apps/platform/config/game.html" target="pageframe">游戏设置</a></li>
                   <#--     <li><a href="/apps/platform/config/account.html" target="pageframe">用户账号</a></li>
                        <li><a href="/apps/platform/config/ai.html" target="pageframe">AI设置</a></li>-->
                    </ul>
                </li>

                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa  fa-play-circle-o"></i>
                        <span>玩法设置</span>
                    </a>
                    <ul class="sub">

							<#if gameTypeList??>
							<#list gameTypeList as gameType>


							<li>
                               <#-- <#if subtype?? && subtype == 'playway' && game?? && game.id == gameType.id>class="layui-this"</#if>>-->
                                <a href="/apps/platform/playway.html?id=${gameType.id!''}"  target="pageframe">${gameType.name!''}</a>
                            </li>
                            </#list>
                            </#if>
                    </ul>
                </li>

                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa  fa-gears (alias)"></i>
                        <span>游戏信息</span>
                    </a>
                    <ul class="sub">
                        <li><a href="/apps/platform/roomList.html" target="pageframe">游戏房间</a></li>
                        <li><a href="/apps/platform/gameusers.html?playertype=normal" target="pageframe">注册玩家</a></li>
                        <li><a href="/apps/platform/gameusers.html?playertype=ai" target="pageframe">AI玩家</a></li>
                        <li><a href="/apps/report/playRecord.html" target="pageframe">牌局列表</a></li>
                        <!--	<li><a  href="todo_list.html">AI设置</a></li>
                            <li><a  href="draggable_portlet.html">房间设置</a></li>
                            <li><a  href="tree.html">场景设置</a></li>
                            <li><a  href="tree.html">试玩设置</a></li>-->
                    </ul>
                </li>
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-table"></i>
                        <span>统计报表</span>
                    </a>
                    <ul class="sub">

                        <li><a href="advanced_form_components.html" target="pageframe">帐变记录</a></li>
                        <li><a href="form_component.html" target="pageframe">输赢统计</a></li>
                        <li><a href="/apps/report/operation.html" target="pageframe">操作记录</a></li>
                    </ul>
                </li>


              <#--  <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-eye"></i>
                        <span>系统监控</span>
                    </a>
                    <ul class="sub">
                        <li><a href="${request.contextPath}/druid/index.html" target="pageframe">SQL监控</a></li>
                        <li><a href="inbox_details.html">信息管理</a></li>
                    </ul>
                </li>-->
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-envelope"></i>
                        <span>信息管理</span>
                    </a>
                    <ul class="sub">
                        <li><a href="/apps/message/notice.html" target="pageframe">系统通知</a></li>
                        <li><a href="/apps/message/questions.html" target="pageframe">玩家提问</a></li>
                    </ul>
                </li>

                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-gear (alias)"></i>
                        <span>系统设置</span>
                    </a>
                    <ul class="sub">
                    <#--    <li><a href="/admin/config/index.html" target="pageframe">系统设置</a></li>-->
                        <li><a href="/admin/sysdic/index.html" target="pageframe">字典管理</a></li>
                    <#-- <li><a href="/admin/metadata/index.html" target="pageframe">元数据</a></li>
                        <li><a href="/admin/template/index.html" target="pageframe">系统模板</a></li>-->
                        <li><a href="/admin/ip/ipwhitelist.html" target="pageframe">IP管理</a></li>
                    </ul>
                </li>

                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa  fa-link"></i>
                        <span>代理商管理</span>
                    </a>
                    <ul class="sub">
                        <li><a href="/apps/agent/agent.html" target="pageframe">查询代理商</a></li>
                    </ul>
                </li>

                  <@shiro.hasRole name="admin">
                <li class="sub-menu">
                    <a href="javascript:;">
                        <i class="fa fa-user"></i>
                        <span>用户管理</span>
                    </a>
                    <ul class="sub">
                        <li><a href="/admin/user/userIndex.html" target="pageframe">查询管理员</a></li>
                        <li><a href="/admin/user/roleIndex.html" target="pageframe">角色管理</a></li>
                        <li><a href="/admin/user/menuIndex.html?parentId=0" target="pageframe">菜单管理</a></li>
                    </ul>
                </li>
                  </@shiro.hasRole>

        <#--        <li class="sub-menu">
                    <a href="javascript:;">
                        <i class=" fa fa-book"></i>
                        <span>用户手册</span>
                    </a>
                    <ul class="sub">
                        <li><a href="inbox_details.html" target="pageframe">用户手册</a></li>
                    </ul>
                </li>-->


            </ul>
        </div>
    </aside>
    <section style="height:715px">
        <iframe id="pageframe" frameborder="0" src="/apps/content.html" name="pageframe"
                style="width: 100%; height: 100%;" allowTransparency="true"></iframe>
    </section>
</section>
<script>
    /**
     * @author Chris
     * @date 2018/4/16 12:19
     * @todo   菜单点击事件
     */
    $("#nav-accordion").on('click', 'li', function () {
        $(this).addClass('active').siblings().removeClass('active');
    })

</script>
</body>

</html>