<div class="layui-layout layui-layout-content">
    <div class="layui-body" style="left: 10px">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="site-h1" style="background-color:#FFFFFF;">
                    玩家提问查询<#if userQuestion??>（${userQuestion.totalElements!''}）</#if>
                </h1>
                <div class="row" style="padding:5px;">
                    <div class="col-lg-12">
                        <table class="layui-table" lay-skin="line">
                            <colgroup>
                                <col width="15%">
                                <col width="25%">
                                <col width="10%">
                                <col width="20%">
                                <col width="20%">
                                <col width="10%">
                                <col>
                            </colgroup>
                            <thead>
                            <tr>
                                <th>用户ID</th>
                                <th>内容</th>
                                <th>当前状态</th>
                                <th>创建时间</th>
                                <th>更新时间</th>
                                <th style="white-space:nowrap;" nowrap="nowrap">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if userQuestion?? && userQuestion.content??>
                            <#list userQuestion.content as info>
                            <tr>
                                <td>${info.userid} </td>
                                <td>${info.content}</td>
                                <td>
                                    <#if info.status==1>
                                    已回复
                                    <#elseif info.status==0>
                                    未回复
                                    </#if>

                                </td>
                                <td>${info.createtime}</td>
                                <td>${info.updatetime}</td>
                                <td style="white-space:nowrap;" nowrap="nowrap">
                                    <a href="/apps/message/reply?userId=${info.userid!''}&content=${info.content}&questionsId=${info.id}" data-toggle="ajax" data-width="500" data-height="400" data-title="回复信息">
                                        <i class="layui-icon">&#xe642;</i>
                                        回复
                                    </a>
                                </td>
            </tr>
        </#list>
    </#if>
    </tbody>
    </table>
</div>
</div>
<div class="row" style="padding:5px;">
    <div class="col-lg-12" id="page" style="text-align:center;"></div>
</div>
</div>
</div>
</div>
</div>
<script>
    layui.use('laypage', function(){
        var laypage = layui.laypage;
        laypage.render({
            elem: 'page'
            ,count: <#if userQuestion??>${userQuestion.totalElements}<#else>0</#if> //总记录
            ,limit: <#if userQuestion??>${userQuestion.size}<#else>0</#if> //总记录
            ,curr:<#if userQuestion??>${userQuestion.number+1}<#else>0</#if>
            ,groups: 5 //连续显示分页数
            ,jump:function(data , first){
            if(!first){
                location.href = "/apps/message/question.html?p="+data.curr ;
            }
        }
    });
    });
</script>