<div class="layui-layout layui-layout-content">

    <div class="layui-body" style="left: 10px">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="site-h1" style="background-color:#FFFFFF;">
                    系统通知列表<#if notice??>（${notice.totalElements!''}）</#if>
                </h1>
                <div class="row" style="padding:5px;">
                    <div class="col-lg-12">
                        <table class="layui-table" lay-skin="line">
                            <thead>
                            <tr>
                                <th>标题</th>
                                <th>内容</th>
                                <th>通知类型</th>
                                <th>状态</th>
                                <th>创建者</th>
                                <th>创建时间</th>
                                <th style="white-space:nowrap;" nowrap="nowrap">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if notice?? && notice.content??>
                            <#list notice.content as info>
                            <tr>
                                <td>${info.noticetitle} </td>
                                <td>${info.content}</td>
                                <td>
                                    <#if info.noticetype==1>
                                    玩家反馈
                                    <#elseif info.noticetype==0>
                                    系统通知
                                </#if>

                                </td>

                             <!--   <td>${info.userid} </td>-->
                                <td>
                                    <#if info.status==1>
                                    已推送
                                    <#elseif info.status==0>
                                    未推送
                                    </#if>
                                </td>
                              <!--  <td>${info.userquestionid}</td>-->
                                <td>${info.createrid}</td>
                                <td>${info.createtime}</td>

                                <td style="white-space:nowrap;" nowrap="nowrap">
                                    <a  onclick="deleteNotify('${info.id}')">
                                    <i class="layui-icon">&#xe642;</i>
                                     删除
                                     </a>
                                </td>
            </tr>
        </#list>
    </#if>
    </tbody>
    </table>
</div>
</div>
<div class="row" style="padding:5px;">
    <div class="col-lg-12" id="page" style="text-align:center;"></div>
</div>
</div>
</div>
</div>
</div>
<script>
    layui.use('laypage', function(){
        var laypage = layui.laypage;
        laypage.render({
            elem: 'page'
            ,count: <#if notice??>${notice.totalElements}<#else>0</#if> //总记录
            ,limit: <#if notice??>${notice.size}<#else>0</#if> //总记录
            ,curr:<#if notice??>${notice.number+1}<#else>0</#if>
            ,groups: 5 //连续显示分页数
            ,jump:function(data , first){
            if(!first){
                location.href = "/apps/message/notice.html?p="+data.curr ;
            }
        }
    });
    });


    // 删除通知
    function deleteNotify(id){
        $.ajax({
            type:"post",
            url:"${request.contextPath}/apps/message/deleteNotify",
            data:{"id":id},
            cache:false,
            success:function(data){
                layer.msg(data.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });
    }
</script>