<div class="uk-layui-form">
	<form class="layui-form" action="/apps/message/reply/save.html" method="post" enctype="multipart/form-data">
		<div class="layui-collapse">
			<div class="layui-colla-item">
				<div class="layui-colla-content layui-show">

					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">玩家:</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text" name="userid"  value="${userName}" required  lay-verify="required" autocomplete="off"
									   class="layui-input" disabled="disabled">
								<font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>
							</div>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">提问内容:</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text"   value="${UserQuestion.content}" required  lay-verify="required" autocomplete="off"
									   class="layui-input"  disabled="disabled">
								<font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>
							</div>
						</div>
					</div>

					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label" >标题:</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text" name="noticetitle" required  lay-verify="required" autocomplete="off"
										class="layui-input">
								<font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>
							</div>
						</div>
					</div>
					<div class="layui-form-item">
						<div class="layui-inline">
							<label class="layui-form-label">回复内容:</label>
							<div class="layui-input-inline" style="width:190px;">
								<input type="text" name="content" required  lay-verify="required" autocomplete="off"
									   class="layui-input">
								<font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>

							</div>
						</div>
					</div>
                    <input name="userquestionid" value="${UserQuestion.id}" style="display: none">
                    <input name="userid" value="${UserQuestion.userid}" style="display: none">
				</div>
			</div>
		</div>

		<div class="layui-form-button">
			<div class="layui-button-block" style="margin:0 auto;width:200px;">
				<button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
				<button type="reset" class="layui-btn layui-btn-primary">重置</button>
			</div>
		</div>
	</form>
</div>

<script>
	layui.use('form', function(){
	  var form = layui.form;
	  form.render(); //更新全部
	});
	layui.use('element', function(){
		var element = layui.element;
	});

</script>