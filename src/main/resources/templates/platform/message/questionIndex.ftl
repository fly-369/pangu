<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>盘古游戏</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/jquery.pagination.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">


</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg" style="margin-left: 215px">
        <div class="wrapper wrapper-content animated fadeInRight">

            <ul class="breadcrumb" style="background-color: #dddbdf">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row" style="margin-left: 5px">
                        <form method="post" action="/apps/message/questions" id="formId" class="form-inline">
                            <div clss="well">
                                <div class="form-group" style="width: 100%">
                                    玩家账号:<input type="text" class="form-control"
                                                style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                                name="userid" placeholder="输入玩家账号">

                                    回复状态:<select class="form-control"  name="status" id="status" style="width: 9%;border: 1px solid #ccc;border-radius: 4px">
                                            <option value="2">全部</option>
                                            <option value="0">未回复</option>
                                            <option value="1">已回复</option>
                                        </select>
                                    <button type="submit" class="btn btn-primary">查询</button>
                                    <button type="reset" id="reset" class="btn  btn-danger">清空</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </ul>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                                       id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th>玩家名称</th>
                                        <th>内容</th>
                                        <th>当前状态</th>
                                        <th>创建时间</th>
                                        <th>更新时间</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                         <#if list??>
                            <#list list as info>
                            <tr>
                                <td>${info.userName} </td>
                                <td>${info.content}</td>
                                <td>
                                    <#if info.status==1>
                                        已回复
                                    <#elseif info.status==0>
                                    未回复
                                    </#if>

                                </td>
                                <td>${info.createtime}</td>
                                <td>${info.updatetime}</td>
                                <td>
                                <div class="btn-group ">
                                    <button style="background-color: #e4e8e5" class="btn btn-white btn-sm edit"
                                            onclick="reply('${info.id}','${info.userid}','${info.content}');">回复
                                    </button>
                                </div>
                                </td>
                            </tr>
                            </#list>
                         </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="modal fade" id="reply" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 180px;width: 900px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">回复玩家</h4>
            </div>
            <div class="modal-body">
                <form id="replyForm" enctype="multipart/form-data" action="${request.contextPath}/apps/message/reply/save"  method="post">
                    <div class="form-group">
                        <label  class="control-label">玩家:</label>
                        <input type="text" style="height:30px" class="form-control"  name="userid" id="username"  disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label  class="control-label">提问内容:</label>
                        <input type="text" style="height:30px" class="form-control"    id="con"  disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label  class="control-label">标题:</label>
                        <input type="text" style="height:30px" class="form-control"  name="noticetitle" id="noticetitle" placeholder="请输入内容"/>
                    </div>
                    <div class="form-group">
                        <label  class="control-label">回复内容:</label>
                        <input type="text" style="height:30px" class="form-control"  name="content" id="content" placeholder="请输入内容" />
                        <input name="userquestionid" id="questionId" style="display: none">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                        <button type="submit"  class="btn btn-primary" style="background: #ea7137; border-color: #f29365">提交</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="${request.contextPath}/statics/js/jquery.js"></script>
<script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
<script src="${request.contextPath}/statics/js/My97DatePicker/WdatePicker.js"></script>
<script src="${request.contextPath}/statics/layui.all.js"></script>
<script src="${request.contextPath}/statics/js/jquery.form-3.14.js"></script>
<script>
    $(document).ready(function () {
        $("#reset").click(function () {
            var resetArr = $(this).parents("form").find(":input");
            for (var i = 0; i < resetArr.length; i++) {
                if (i > 0) {
                    resetArr.eq(i).val("");
                }
            }
            return false;
        });

    });

    /**
     * @author Chris
     * @date 2018/5/11 14:43
     * @todo   回复玩家
    */
    function reply(id,userId,content){
        $("#reply").modal();
        $("#username").val(userId);
        $("#questionId").val(id);
        $("#con").val(content);
    }

    /**
     * @author Chris
     * @date 2018/5/11 15:16
     * @todo   回复玩家
    */
    $(function(){
        var load;
        $("#replyForm").ajaxForm({
            success:function (result){
                layer.close(load);

                    layer.msg(result.message);
                    setTimeout(function(){
                        //3秒后刷新
                        layer.close();
                        location.reload();
                    },2000);

            },
            beforeSubmit:function(){
                //判断参数
                if($.trim($("#noticetitle").val()) == ''){
                    layer.msg('请输入通知标题',function(){});
                    $("#noticetitle").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#noticetitle").parent().removeClass('has-error').addClass('has-success');
                }
                if($.trim($("#content").val()) == ''){
                    layer.msg('请输入通知内容',function(){});
                    $("#content").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#content").parent().removeClass('has-error').addClass('has-success');
                }
                load = layer.load('正在提交！！！');
            },
            dataType:"json",
            clearForm:false
        });

    });




</script>

</body>

</html>
