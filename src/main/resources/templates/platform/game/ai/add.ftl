<#include "//macro.ftl">
<div class="uk-layui-form">
	<form class="layui-form"  enctype="multipart/form-data">
		<div class="layui-collapse">
			<div class="layui-colla-item">
				<h2 class="layui-colla-title">基本信息</h2>
				<div class="layui-colla-content layui-show">
					<div class="layui-form-item">
						<label class="layui-form-label">新增数量:</label>
						<div class="layui-input-inline">
							<input type="text" id="times" name="times" required lay-verify="required" autocomplete="off" class="layui-input">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">AI总数:</label>
						<div class="layui-input-inline">
							<input type="text" value="${aiTotal}" autocomplete="off" class="layui-input" disabled="disabled">
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">当前在线:</label>
						<div class="layui-input-inline">
							<input  value="${onlineTotal}"  autocomplete="off" class="layui-input" disabled="disabled">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="layui-form-button">
			<div class="layui-button-block"  style="margin:0 auto;width:200px;">
				<button class="layui-btn" onclick="addAi()">立即提交</button>
			</div>
		</div>
	</form>
</div>

<script>

    layui.use('form', function () {
        var form = layui.form;
        form.render(); //更新全部
    });
    layui.use('element', function () {
        var element = layui.element;
    });

    /**
     * @author Chris
     * @date 2018/4/9 17:04
     * @todo   新增ai
     */
    function addAi() {
        alert("进入新增ai");
        var times=$("#times").val();
        if(times==null||times==""){
            return  layer.msg("请输入新增Ai的数量");
		}
		$.post('/apps/platform/create/ai', {times:times}, function (result) {
            if (result && result.status == 200) {
                layer.msg(result.message);
                setTimeout(function () {
                    /* $("#close").click();*/
                    location.reload();
                }, 3000);
            }
            return layer.msg(result.message, so.default), !1;
        }, 'json');

	}
</script>