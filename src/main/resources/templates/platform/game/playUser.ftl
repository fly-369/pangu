<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>盘古游戏</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/jquery.pagination.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">


</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg" style="margin-left: 215px">
        <div class="wrapper wrapper-content animated fadeInRight">

            <ul class="breadcrumb" style="background-color: #dddbdf">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row" style="margin-left: 5px">
                        <form method="post" action="/apps/platform/gameusers" id="formId" class="form-inline">
                            <div clss="well">
                                <div class="form-group">
                                    在线:${total}/${online}&nbsp;
                                    用户名:<input type="text" class="form-control"
                                                style="width: 15%;border: 1px solid #ccc;border-radius: 4px"
                                                name="username"  value="${username}" placeholder="请输入玩家账号">
                                    平台CODE:<input type="text" class="form-control"
                                               style="width: 15%;border: 1px solid #ccc;border-radius: 4px"
                                               name="agentcode"  value="${agentcode}" placeholder="请输入平台的CODE">

                                    金币:<input type="text" class="form-control"
                                               style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                               name="minGoldCoins" value="${minGoldCoins}" placeholder="最低金币">-
                                        <input type="text" class="form-control"
                                               style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                               name="maxGoldCoins"  value="${maxGoldCoins}" placeholder="最大金币">

                                    在线:<select class="form-control"  name="online" id="online" style="width: 9%;border: 1px solid #ccc;border-radius: 4px">
                                            <option value="false">全部</option>
                                            <option value="true">在线</option>
                                        </select>
                                    <input name="playertype" value="normal" style="display: none">
                                    <button type="submit" class="btn btn-primary">查询</button>
                                    <button type="reset" id="reset" class="btn  btn-danger">清空</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </ul>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                                       id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th>用户名</th>
                                        <th>平台</th>
                                        <th>注册时间</th>
                                        <th>地区</th>
                                        <th>金币</th>
                                        <th>最后登录时间</th>
                                        <th>在线</th>
                                        <th>系统</th>
                                        <th>浏览器</th>
                                        <th>状态</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                         <#if playUserList??>
                            <#list playUserList as info>
                            <tr>
                                <td>${info.username!''}</td>
                                <td>${info.agentcode}</td>
                                <td>${info.createtime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td>${info.country!''}<#if info.province?? && info.province!="0">${info.province!''}</#if><#if info.city?? && info.city!="0">${info.city!''}</#if></td>
                                <td>${info.goldcoins!''}</td>
                                <td>${info.lastlogintime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td>
										<#if info.online>
                                           在线
                                        </#if>
                                </td>
                                <td>${info.ostype!''}</td>
                                <td>${info.browser!''}</td>
                                <td>
										<#if info.disabled == true>
                                            禁用
                                            <#else>
                                            正常
                                        </#if>
                                </td>
                                <td>
                                <div class="btn-group ">
                                    <button style="background-color: #e4e8e5" class="btn btn-white btn-sm edit"
                                            onclick="disablePalyer('${info.id}')">禁用/启用
                                    </button>&nbsp;
                                <#--    <button style="background-color: #e4e8e5" class="btn btn-white btn-sm edit"
                                            onclick="queryInfoByGameId()">查看详情
                                    </button>-->
                                </div>
                                </td>
                            </tr>
                            </#list>
                         </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="modal fade" id="queryInfo" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 180px;width: 900px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">显示牌局详情</h4>
            </div>
            <div class="form-group" style="margin-left: 10px">
                <label  class="control-label" >牌局ID:</label>
                <div id="gId"></div>
            </div>
            <div class="form-group" style="margin-left: 10px">
                <label  class="control-label" >房间编号:</label>
                <div id="rId"></div>
            </div>
            <div class="form-group" style="margin-left: 10px">
                <label  class="control-label" >游戏类型:</label>
                <div id="gt"></div>
            </div>
            <div class="modal-body">
                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                       id="table">
                </table>

            </div>

        </div>
    </div>
</div>


<script src="${request.contextPath}/statics/js/jquery.js"></script>
<script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
<script src="${request.contextPath}/statics/js/My97DatePicker/WdatePicker.js"></script>
<script src="${request.contextPath}/statics/layui.all.js"></script>
<script>

    $(document).ready(function () {
        $("#reset").click(function () {
            var resetArr = $(this).parents("form").find(":input");
            for (var i = 0; i < resetArr.length; i++) {
                if (i > 0) {
                    resetArr.eq(i).val("");
                }
            }
            return false;
        });

    });

    /**
     * @author Chris
     * @date 2018/5/17 20:28
     * @todo   禁用启用单个用户
    */
    function disablePalyer (id){
        $.ajax({
            type:"post",
            url:"${request.contextPath}/apps/platform/disable",
            data:{"id":id},
            cache:false,
            success:function(data){
                layer.msg(data.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });

    }


</script>

</body>

</html>
