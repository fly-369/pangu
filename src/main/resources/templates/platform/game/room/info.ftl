<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>盘古游戏</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/font-awesome/css/font-awesome.css" rel="stylesheet">
<#--    <link href="${request.contextPath}/statics/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/animate.css" rel="stylesheet">-->
    <link href="${request.contextPath}/statics/css/jquery.pagination.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">


</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg" style="margin-left: 215px">
        <div class="wrapper wrapper-content animated fadeInRight">

            <ul class="breadcrumb" style="background-color: #e5e6ea">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row" style="margin-left: 5px">
                        <form method="post" action="/apps/platform/roomList" id="formId" class="form-inline">
                            <div clss="well">
                                <div class="form-group">
                                  <#--  游戏名称:
                                    <select class="form-control" name="game" id="gameType" onchange="gameTypeChange();"
                                            style="width: 9%;border: 1px solid #ccc;border-radius: 4px">
                                        <option value="1" id="ddz">斗地主</option>
                                        <option value="2" id="zjh">炸金花</option>
                                        <option value="3" id="niuniu">牛牛</option>
                                        <option value="4" id="dzpk">德州扑克</option>
                                        <option value="5" id="hhdz">红黑大战</option>
                                    </select>-->
                                    房间号:<input type="text" class="form-control"
                                               style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                               name="roomid"  <#--value="${orderModel.orderId!}"--> placeholder="输入房间号">
                                    金币区间:<input type="text" class="form-control"
                                                      style="width: 6%;border: 1px solid #ccc;border-radius: 4px"
                                              >  -
                                              <input type="text" class="form-control"
                                                     style="width: 6%;border: 1px solid #ccc;border-radius: 4px"
                                              >


                                       <#-- <input name="startTime" class="Wdate" type="text" id="d4311" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01 00:00:00\'}'})"
                                           style="width: 10%;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择开始日期" &lt;#&ndash;value="${orderModel.startTime!}"&ndash;&gt;> --
                                         <input   id="d4312" class="Wdate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01 00:00:00'})"
                                             name="overTime"   style="width: 10%;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择结束日期" &lt;#&ndash;value="${orderModel.overTime!}"&ndash;&gt;>
                                       -->
                                 <#--   用户类型:
                                    <select class="form-control" name="playerType" style="width: 10%;border: 1px solid #ccc;border-radius: 4px">
                                        <option value="">请选择</option>
                                        <option value="ai" id="isUser">注册玩家</option>
                                        <option value="normal" id="isAi">AI</option>
                                    </select>-->


                                    <button type="submit" class="btn btn-primary">查询</button>
                                    <button type="reset" id="reset" class="btn  btn-danger">清空</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </ul>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                                       id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th>房间编号</th>
                                        <th>游戏类型</th>
                                        <th>创建人</th>
                                        <th>创建时间</th>
                                        <th>玩法</th>
                                        <th>玩家上限</th>
                                        <th>金币要求</th>
                                        <th>玩家数量</th>
                                        <th style="white-space:nowrap;width:1%;" nowrap="nowrap">操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                         <#if roomList??>
                            <#list roomList as info>
                            <tr>
                                <td>${info.roomid} </td>
                                <td>
										<#if info.gamePlayway>
											<#if gameModelList??>
                                                <#list gameModelList as model>
                                                    <#if model.id == info.gamePlayway.game>
                                                        ${model.name!''}
                                                    </#if>
                                                </#list>
                                            </#if>
                                        </#if>
                                </td>
                                <td>
										<#if info.masterUser??>${info.masterUser.username!''}</#if>
                                </td>
                                <td>${info.createtime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td>
										<#if info.gamePlayway??>${info.gamePlayway.name!''}</#if>
                                </td>
                                <td>
										<#if info.gamePlayway??>${info.gamePlayway.players!''}</#if>
                                </td>
                                <td>
										<#if info.gamePlayway??>${info.gamePlayway.mincoins}~${info.gamePlayway.maxcoins}</#if>
                                </td>

                                <td>${info.players!''}</td>

                                <td>
                                    <div class="btn-group ">
                                        <button style="background-color: #e4e8e5" class="btn btn-white btn-sm edit"
                                                onclick="queryInfoByRoomId('${info.roomid}')">查看详情
                                        </button>
                                    </div>
                                </td>


                             <#--   <td style="white-space:nowrap;width:1%;" nowrap="nowrap">
                                    <button class="layui-btn layui-btn-sm green"
                                            href="/apps/platform/roomInfo.html?roomId=${gameRoom.id!''}"
                                            data-toggle="ajax" data-width="300" data-height="200"
                                            data-title="查看详情">查看详情</button>

                                    <a href="/apps/platform/gameroom/delete.html?id=${gameRoom.id!''}" data-toggle="tip"
                                       data-title="请确认是否删除游戏房间？"> <i class="layui-icon"
                                                                     style="color:red;">&#x1006;</i>
                                        删除
                                    </a>
                                </td>-->
                            </tr>
                            </#list>
                         </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="modal fade" id="queryInfo" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 180px;width: 900px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">房间详情</h4>
            </div>
            <div class="form-group" style="margin-left: 10px">
                <label  class="control-label" >房间编号:</label>
                <div id="rId"></div>
            </div>
            <div class="form-group" style="margin-left: 10px">
                <label  class="control-label" >创建时间:</label>
                <div id="ctime"></div>
            </div>
            <div class="modal-body">
                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                       id="table">
                </table>

            </div>

        </div>
    </div>
</div>


<script src="${request.contextPath}/statics/js/jquery.js"></script>
<script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
<script src="${request.contextPath}/statics/js/My97DatePicker/WdatePicker.js"></script>
<script src="${request.contextPath}/statics/layui.all.js"></script>

</body>


<script>
    //时间选择器
    /*  laydate.render({
          elem: '#startTime'
          ,type: 'datetime'
          ,theme: '#393D49'
      });
      laydate.render({
          elem: '#overTime'
          ,type: 'datetime'
          ,theme: '#393D49'
      });*/



    function gameTypeChange() {
        $("#formId").submit();
    }

    /**
     * @author Chris
     * @date 2018/5/9 19:57
     * @todo   根据房间ID查询
     */
    function queryInfoByRoomId(roomId) {
        $.ajax({
            type:"post",
            url:"${request.contextPath}/apps/platform/roomInfo",
            data:{"roomId":roomId},
            cache:false,
            success:function(data){
              $("#queryInfo").modal();
              var room=data.roomInfo;
                $("#rId").text(room.roomid);
                $("#ctime").text(room.createtime);
                var json=data.userList;
                var str="                    <thead>\n" +
                      "                    <tr>\n" +
                      "                        <th>玩家ID</th>\n" +
                      "                        <th>玩家名称</th>\n" +
                      "                        <th>玩家类型</th>\n" +
                      "                        <th>余额</th>\n" +
                      "                        <th>系统</th>\n" +
                      "                    </tr>\n" +
                      "                    </thead>";
                if(json.length>0){
                    str+="<tbody>";
                  for(var i=0;i<json.length;i++){
                     str+="<tr>";
                      str+="<td>"+json[i].id+"</td>";
                      str+="<td>"+json[i].username+"</td>";
                      str+="<td>"+json[i].playertype+"</td>";
                      str+="<td>"+json[i].goldcoins+"</td>";
                      str+="<td>"+json[i].ostype+"</td>";
                      str+="</tr>";
                  }
              str+="</tbody>";
                }else {
                    str+="<tbody>" +
                            "<tr>当前房间中没有玩家</tr>";
                    str+="</tbody>";
                }

              document.getElementById("table").innerHTML=str;
            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });




    }


</script>
</html>
