<div class="layui-layout layui-layout-content">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="site-h1" style="background-color:#FFFFFF;color: #ec5326">
					玩家列表<#if playersList??>（${playersList.totalElements}/ 当前在线:${online}）</#if>
				</h1>

				<form  action="/apps/platform/gameusers" method="post" style="margin-left: 25px">

					在线:<select  name="online" style="height: 24px ;width: 80px">
					        <option value="false">全部</option>
					        <option value="true">在线</option>
				          </select>
					玩家账户:<input type="text"  name="username" style="width: 100px" value="${username}"/>
					金币:<input type="text" style="width: 50px;" name="minGoldCoins" value="${minGoldCoins}">-<input type="text"  name="maxGoldCoins" style="width: 50px;" value="${maxGoldCoins}"/>

					<button class="layui-btn layui-btn-sm layui-btn-normal" type="submit">查询</button>
					<button class="layui-btn layui-btn-sm layui-btn-normal" type="reset">重置</button>
				</form>

				<div class="row" style="padding:5px;">
					<div class="col-lg-12">
						<#if playersList?? && playersList.content??>
						<table class="layui-table" lay-skin="line">
							<thead>
								<tr>
									<th>用户名</th>
									<th>注册时间</th>
									<th>地区</th>
									<th>金币</th>
									<th>最后登录时间</th>
									<!--<th>钻石</th>-->
									<th>在线</th>
									<th>系统</th>
									<th>浏览器</th>
									<th>有效账号</th>
									<th style="white-space:nowrap;width:1%;" nowrap="nowrap">操作</th>
								</tr>
							</thead>
							<tbody>
								<#list playersList.content as player>
								<tr>
									<td>${player.username!''}</td>
									<td>${player.createtime?string('yyyy-MM-dd HH:mm:ss')}</td>
									<td>${player.country!''}<#if player.province?? && player.province!="0">${player.province!''}</#if><#if player.city?? && player.city!="0">${player.city!''}</#if></td>
									<td>${player.goldcoins!''}</td>
								   <td>${player.lastlogintime?string('yyyy-MM-dd HH:mm:ss')}</td>
									<!--<td>${player.cards!''}</td>
									<td>${player.diamonds!''}</td>-->
									<td>
										<#if player.online> 
										<i class="layui-icon" style="color:#19a55d;">&#xe618;</i> 
										</#if>
									</td>
									<td>${player.ostype!''}</td>
									<td>${player.browser!''}</td>
									<td>
										<#if player.disabled == false> 
										<i class="layui-icon" style="color:#19a55d;">&#xe618;</i> 
										</#if>
									</td>
									<td style="white-space:nowrap;width:1%;" nowrap="nowrap">
										<a href="/apps/platform/gameusers/edit.html?id=${player.id!''}" data-toggle="ajax" data-width="550" data-height="400" data-title="编辑用户信息"> <i class="layui-icon">&#xe642;</i>
											编辑
										</a> 
									</td>
								</tr>
								</#list>
							</tbody>
						</table>
						</#if>
					</div> 	   
				</div>
			</div>	
		</div>
		<div class="row" style="padding:5px;">
			<div class="col-lg-12" id="page" style="text-align:center;"></div>
		</div>
</div>
<script>
	layui.use('laypage', function(){
		  var laypage = layui.laypage;
		  laypage.render({
				elem: 'page'
				,count: <#if playersList??>${playersList.totalElements}<#else>0</#if> //总记录
				,limit: <#if playersList??>${playersList.size}<#else>0</#if> //总记录
				,curr:<#if playersList??>${playersList.number+1}<#else>0</#if>
				,groups: 5 //连续显示分页数
				,jump:function(data , first){
					if(!first){
						location.href = "/apps/platform/gameusers.html?p="+data.curr ;
					}
				}
		   });
		});
</script>