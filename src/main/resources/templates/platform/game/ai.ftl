<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>盘古游戏</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/jquery.pagination.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">


</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg" style="margin-left: 215px">
        <div class="wrapper wrapper-content animated fadeInRight">

            <ul class="breadcrumb" style="background-color: #dddbdf">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row" style="margin-left: 5px">
                        <form method="post" action="/apps/platform/gameusers" id="formId" class="form-inline">

                            <div clss="well">
                                <div class="form-group" style="width: 100%">
                                    在线:${total}/${online}&nbsp;
                                    <button type="button" class="btn btn-primary" onclick="$('#addAi').modal();">新增AI</button>
                                    查询AI:<input type="text" class="form-control"
                                                style="width: 15%;border: 1px solid #ccc;border-radius: 4px"
                                                name="username"  value="${username}" placeholder="请输入AI账号">

                                    金币:<input type="text" class="form-control"
                                               style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                               name="minGoldCoins" value="${minGoldCoins}" placeholder="最低金币">-
                                        <input type="text" class="form-control"
                                               style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                               name="maxGoldCoins"  value="${maxGoldCoins}" placeholder="最大金币">

                                    在线:<select class="form-control"  name="online" id="online" style="width: 9%;border: 1px solid #ccc;border-radius: 4px">
                                            <option value="false">全部</option>
                                            <option value="true">在线</option>
                                         </select>
                                    <input name="playertype" value="ai" style="display: none">
                                    <button type="submit" class="btn btn-primary">查询</button>
                                    <button type="reset" id="reset" class="btn  btn-danger">清空</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </ul>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                                       id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th>用户名</th>
                                        <th>注册时间</th>
                                        <th>金币</th>
                                        <th>最后登录时间</th>
                                        <th>在线</th>
                                        <th>有效账号</th>
                                       <#-- <th>操作</th>-->
                                    </tr>
                                    </thead>
                                    <tbody>
                         <#if playUserList??>
                            <#list playUserList as info>
                            <tr>
                                <td>${info.username!''}</td>
                                <td>${info.createtime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td>${info.goldcoins!''}</td>
                                <td>${info.lastlogintime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td>
										<#if info.online>
                                           在线
                                        </#if>
                                </td>
                                <td>
										<#if info.disabled == true>
                                           禁用
                                            <#else >
                                            正常
                                        </#if>
                                </td>
                                <td>
                              <#--  <div class="btn-group ">
                                    <button style="background-color: #e4e8e5" class="btn btn-white btn-sm edit"
                                            onclick="queryInfoByGameId()">查看详情
                                    </button>
                                </div>-->
                                </td>
                            </tr>
                            </#list>
                         </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<div class="modal fade" id="addAi" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 180px;width: 900px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">新增AI</h4>
            </div>
            <div class="modal-body">
                <form id="addAiForm" enctype="multipart/form-data" action="${request.contextPath}/apps/platform/create/ai"  method="post">
                    <div class="form-group">
                        <label  class="control-label">新增数量:</label>
                        <input type="text" style="height:30px" class="form-control" name="times" id="times" placeholder="请输入整数"/>
                    </div>
                    <div class="form-group">
                        <label  class="control-label">AI总数:</label>
                        <input type="text" style="height:30px" class="form-control" value="${total}" disabled="disabled"/>
                    </div>
                    <div class="form-group">
                        <label  class="control-label">当前在线:</label>
                        <input type="text" style="height:30px" class="form-control" value="${online}" disabled="disabled"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="close">关闭</button>
                        <button type="submit"  class="btn btn-primary" style="background: #ea7137; border-color: #f29365">提交</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<script src="${request.contextPath}/statics/js/jquery.js"></script>
<script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
<script src="${request.contextPath}/statics/js/My97DatePicker/WdatePicker.js"></script>
<script src="${request.contextPath}/statics/layui.all.js"></script>
<script src="${request.contextPath}/statics/js/jquery.form-3.14.js"></script>
<script>

    $(document).ready(function () {
        $("#reset").click(function () {
            var resetArr = $(this).parents("form").find(":input");
            for (var i = 0; i < resetArr.length; i++) {
                if (i > 0) {
                    resetArr.eq(i).val("");
                }
            }
            return false;
        });

    });

    $(function(){
        var load;
        $("#addAiForm").ajaxForm({
            success:function (result){
                layer.close(load);
                layer.msg(result.message);
                setTimeout(function(){
                    //3秒后刷新
                    layer.close();
                    location.reload();
                },2000);

            },
            beforeSubmit:function(){
                //判断参数
                if($.trim($("#times").val()) == ''){
                    layer.msg('请输入新增的数量',function(){});
                    $("#times").parent().removeClass('has-success').addClass('has-error');
                    return !1;
                }else{
                    $("#times").parent().removeClass('has-error').addClass('has-success');
                }
                load = layer.load('正在提交！！！');
            },
            dataType:"json",
            clearForm:false
        });

    });


</script>

</body>

</html>
