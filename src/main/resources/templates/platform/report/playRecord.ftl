<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>盘古游戏</title>
    <meta name="keyword" content="">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="webkit">
    <meta name="Author" content="zifan">
    <meta name="copyright" content="胡桃夹子。All Rights Reserved">
    <link href="${request.contextPath}/statics/css/bootstrap.min.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/font-awesome/css/font-awesome.css" rel="stylesheet">
<#--    <link href="${request.contextPath}/statics/css/plugins/toastr/toastr.min.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/plugins/iCheck/green.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/animate.css" rel="stylesheet">-->
    <link href="${request.contextPath}/statics/css/jquery.pagination.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/style.css" rel="stylesheet">
    <link href="${request.contextPath}/statics/css/layui.css" rel="stylesheet">


</head>

<body class="fixed-sidebar">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg" style="margin-left: 215px">
        <div class="wrapper wrapper-content animated fadeInRight">

            <ul class="breadcrumb" style="background-color: #dddbdf">
                <div class="ibox-content m-b-sm border-bottom">
                    <div class="row" style="margin-left: 5px">
                        <form method="post" action="/apps/report/playRecord" id="formId" class="form-inline">
                            <div clss="well">
                                <div class="form-group">
                                    游戏名称:
                                    <select class="form-control" name="gameType" id="gameType" onchange="gameTypeChange();"
                                            style="width: 9%;border: 1px solid #ccc;border-radius: 4px">
                                        <option value="1" id="ddz">斗地主</option>
                                        <option value="2" id="zjh">炸金花</option>
                                        <option value="3" id="niuniu">牛牛</option>
                                        <option value="4" id="dzpk">德州扑克</option>
                                        <option value="5" id="hhdz">红黑大战</option>
                                    </select>
                                    游戏编号:<input type="text" class="form-control"
                                                style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                                name="gameId"  <#--value="${orderModel.actionId!}"--> placeholder="输入游戏编号">

                                    房间号:<input type="text" class="form-control"
                                               style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                               name="roomId"  <#--value="${orderModel.orderId!}"--> placeholder="输入房间号">
                                 <#--   用户:<input type="text" class="form-control"
                                              style="width: 12%;border: 1px solid #ccc;border-radius: 4px"
                                              name="playerId" &lt;#&ndash; value="${orderModel.account!}"&ndash;&gt; placeholder="输入用户名">-->


                                <#--    <input name="startTime" class="Wdate" type="text" id="d4311" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'d4312\')||\'2020-10-01 00:00:00\'}'})"
                                           style="width: 10%;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择开始日期" &lt;#&ndash;value="${orderModel.startTime!}"&ndash;&gt;> --
                                         <input   id="d4312" class="Wdate" type="text" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'d4311\')}',maxDate:'2020-10-01 00:00:00'})"
                                             name="overTime"   style="width: 10%;height: 30px;border: 1px solid #ccc;border-radius: 4px"   placeholder="请选择结束日期" &lt;#&ndash;value="${orderModel.overTime!}"&ndash;&gt;>
                                       -->
                                 <#--   用户类型:
                                    <select class="form-control" name="playerType" style="width: 10%;border: 1px solid #ccc;border-radius: 4px">
                                        <option value="">请选择</option>
                                        <option value="ai" id="isUser">注册玩家</option>
                                        <option value="normal" id="isAi">AI</option>
                                    </select>-->


                                    <button type="submit" class="btn btn-primary">查询</button>
                                    <button type="reset" id="reset" class="btn  btn-danger">清空</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </ul>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="table-responsive ">
                                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                                       id="editable-sample">
                                    <thead>
                                    <tr>
                                        <th>牌局编号</th>
                                        <th>房间编号</th>
                                        <th>游戏类型</th>
                                        <#--<th>庄家用户名</th>-->
                                        <th>开牌时间</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                         <#if RecordList??>
                            <#list RecordList as info>
                            <tr>
                                <td>${info.gameId}</td>
                                <td>${info.roomId} </td>
                                <td>
                                     <#if gameType=="1">
                                         斗地主
                                     <#elseif gameType=="2">
                                         炸金花
                                     <#elseif gameType=="3">
                                         牛牛
                                     <#elseif gameType=="4">
                                         德州
                                     <#elseif gameType=="5">
                                         红黑
                                     </#if>


                                </td>
                              <#--  <td>${info.gameSubType}</td>-->
                                <td>${info.gameTime?string('yyyy-MM-dd HH:mm:ss')}</td>
                                <td>
                                    <div class="btn-group ">
                                        <button style="background-color: #e4e8e5" class="btn btn-white btn-sm edit"
                                                onclick="queryInfoByGameId('${info.gameId}','${gameType}')">查看详情
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            </#list>
                         </#if>
                                    </tbody>
                                </table>
                            </div>
                        ${page}
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>

<div class="modal fade" id="queryInfo" tabindex="-1" role="dialog" aria-labelledby="addroleLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="margin-top: 180px;width: 900px">
            <div class="modal-header" style="background: #beb04d">
                <h4 class="modal-title" id="addroleLabel">显示牌局详情</h4>
            </div>
            <div class="form-group" style="margin-left: 10px">
                <label  class="control-label" >牌局ID:</label>
                <div id="gId"></div>
            </div>
            <div class="form-group" style="margin-left: 10px">
                <label  class="control-label" >房间编号:</label>
                <div id="rId"></div>
            </div>
           <#-- <div class="form-group" style="margin-left: 10px">
                <label  class="control-label" >游戏类型:</label>
                <div id="gt"></div>
            </div>-->
            <div class="modal-body">
                <table class="table table-centerbody table-striped table-condensed text-nowrap"
                       id="table">
                </table>

            </div>

        </div>
    </div>
</div>


<script src="${request.contextPath}/statics/js/jquery.js"></script>
<script src="${request.contextPath}/statics/js/bootstrap.min.js"></script>
<script src="${request.contextPath}/statics/js/My97DatePicker/WdatePicker.js"></script>
<script src="${request.contextPath}/statics/layui.all.js"></script>

</body>


<script>
    //时间选择器
    /*  laydate.render({
          elem: '#startTime'
          ,type: 'datetime'
          ,theme: '#393D49'
      });
      laydate.render({
          elem: '#overTime'
          ,type: 'datetime'
          ,theme: '#393D49'
      });*/

    $(document).ready(function () {
        var gameType=${gameType};

        if (gameType == 1) {
            $("#ddz").attr("selected", true);
        } else if (gameType == 2) {
            $("#zjh").attr("selected", true);
        } else if (gameType == 3) {
            $("#niuniu").attr("selected", true);
        } else if (gameType == 4) {
            $("#dzpk").attr("selected", true);
        } else if (gameType == 5) {
            $("#hhdz").attr("selected", true);
        }

        $("#reset").click(function () {
            var resetArr = $(this).parents("form").find(":input");
            for (var i = 0; i < resetArr.length; i++) {
                if (i > 0) {
                    resetArr.eq(i).val("");
                }
            }
            return false;
        });

    });

    function gameTypeChange() {
        $("#formId").submit();
    }

    /**
     * @author Chris
     * @date 2018/5/7 12:57
     * @todo   根据游戏ID查看牌局详情
     */
    function queryInfoByGameId(gameId,gameType) {
        $.ajax({
            type:"post",
            url:"${request.contextPath}/apps/report/queryInfoByGameId",
            data:{"gameType":gameType,"gameId":gameId},
            cache:false,
            success:function(data){
              $("#queryInfo").modal();
              var json=data.playerInfo;
                $("#gId").text(json[0].gameId);
                $("#rId").text(json[0].roomId);
                $("#gt").text(json[0].gameType);
                var str="                    <thead>\n" +
                      "                    <tr>\n" +
                      "                        <th>玩家ID</th>\n" +
                    /*  "                        <th>下注类型</th>\n" +*/
                      "                        <th>下注金额</th>\n" +
                      "                        <th>输赢金额</th>\n" +
                      "                        <th>牌型</th>\n" +
                      "                        <th>庄闲</th>\n" +
                      "                        <th>用户类型</th>\n" +
                      "                        <th>下注时间</th>\n" +

                      "                    </tr>\n" +
                      "                    </thead>";
              str+="<tbody>";
              for(var i=0;i<json.length;i++){
                 str+="<tr>";
                  str+="<td>"+json[i].playerId+"</td>";
                 /* str+="<td>"+json[i].gameSubType+"</td>";*/
                  str+="<td>"+json[i].coin+"</td>";
                  str+="<td>"+json[i].winLoseCoin+"</td>";
                  str+="<td>"+json[i].cards+"</td>";
                  str+="<td>"+json[i].isBanker+"</td>";
                  str+="<td>"+json[i].playerType+"</td>";
                  str+="<td>"+json[i].gameTime+"</td>";

                  str+="</tr>";
              }
              str+="</tbody>";

              document.getElementById("table").innerHTML=str;
            },error:function(data){
                layer.msg(data.message,function(){}),!1;
            }
        });




    }


</script>
</html>
