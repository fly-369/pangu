
<div class="layui-layout layui-layout-content">
    <div class="layui-body" style="left: 10px">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="site-h1" style="background-color:#FFFFFF;">
                    后台操作记录查询
                </h1>
                <div class="row" style="padding:5px;">
                    <div class="col-lg-12">
                        <table class="layui-table" lay-skin="line">
                            <thead>
                            <tr>
                                <th>编号</th>
                                <th>事件</th>
                                <th>参数</th>
                                <th>创建时间</th>
                                <th>执行时间</th>
                                <th>操作人</th>
                                <th>IP地址</th>
                            </tr>
                            </thead>
                            <tbody>
                            <#if operation?? && operation.content??>
                            <#list operation.content as info>
                            <tr>
                                <td>${info.id}</td>
                                <td>${info.operation} </td>
                                <td></td>
                                <td>${info.createTime}</td>
                                <td>${info.time}</td>
                                <td>${info.username}</td>
                                <td>${info.ip}</td>
            </tr>
        </#list>
    </#if>
    </tbody>
    </table>
</div>
</div>
<div class="row" style="padding:5px;">
    <div class="col-lg-12" id="page" style="text-align:center;"></div>
</div>
</div>
</div>
</div>
</div>
<script>
    layui.use('laypage', function(){
        var laypage = layui.laypage;
        laypage.render({
            elem: 'page'
            ,count: <#if operation??>${operation.totalElements}<#else>0</#if> //总记录
            ,limit: <#if operation??>${operation.size}<#else>0</#if> //总记录
            ,curr:<#if operation??>${operation.number+1}<#else>0</#if>
            ,groups: 5 //连续显示分页数
            ,jump:function(data , first){
            if(!first){
                location.href = "/apps/report/operation.html?p="+data.curr ;
            }
        }
    });
    });
</script>