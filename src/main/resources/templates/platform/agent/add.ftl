<div class="uk-layui-form">
    <form class="layui-form" action="/apps/agent/add/agent.html" method="post" enctype="multipart/form-data">
        <div class="layui-collapse">
            <div class="layui-colla-item">
                <div class="layui-colla-content layui-show">
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">商户名称:</label>
                            <div class="layui-input-inline" style="width:190px;">
                                <input type="text" name="agent_name" required lay-verify="required" autocomplete="off"
                                       class="layui-input">
                                <font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">商户Code</label>
                            <div class="layui-input-inline" style="width:190px;">
                                <input type="text" name="agent_code" required lay-verify="required" autocomplete="off"
                                       class="layui-input">
                                <font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>

                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-inline">
                            <label class="layui-form-label">商户前缀:</label>
                            <div class="layui-input-inline" style="width:190px;">
                                <input type="text" name="prefix" required lay-verify="required" autocomplete="off"
                                       class="layui-input">
                                <font color="red" style="position: absolute;right:-100px;top:0px;">*（必填项）</font>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="layui-form-button">
            <div class="layui-button-block" style="margin:0 auto;width:200px;">
                <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>

<script>
    layui.use('form', function () {
        var form = layui.form;
        form.render(); //更新全部
    });
    layui.use('element', function () {
        var element = layui.element;
    });

</script>