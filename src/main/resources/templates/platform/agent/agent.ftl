<div class="col-lg-12">
    <h1 class="site-h1" style="background-color:#FFFFFF;">
        代理商查询<#if agentList??>（${agentList.totalElements!''}）</#if>
        <span style="float:right;">
						<div class="layui-btn-group">
						  <a href="/apps/agent/add.html}" title="添加代理商" data-toggle="ajax" data-width="500"
                             data-height="300" class="layui-btn layui-btn-sm">
						  <i class="layui-icon">&#xe608;</i>添加代理商</a>
						</div>
					</span>
    </h1>
    <div class="row" style="padding:5px;">
        <div class="col-lg-12">
            <table class="layui-table" lay-skin="line">
                <thead>
                <tr>
                    <th>编号</th>
                    <th>代理商名称</th>
                    <th>代理商Code</th>
                    <th>秘钥</th>
                    <th>平台前缀</th>
                    <th>当前状态</th>
                    <th>创建时间</th>
                    <th style="white-space:nowrap;" nowrap="nowrap">操作</th>
                </tr>
                </thead>
                <tbody>
                            <#if agentList?? && agentList.content??>
                            <#list agentList.content as info>
                            <tr>
                                <td>
                                    ${info.id}
                                </td>
                                <td>${info.agent_name} </td>
                                <td>${info.agent_code}</td>
                                <td>${info.agent_key} </td>
                                <td>${info.prefix}</td>
                                <td>
                                    <#if info.status==0>
                                        正常
                                    <#elseif info.status==1>
                                    停用
                                    </#if>
                                </td>
                                <td>${info.create_time}</td>
                                <td style="white-space:nowrap;" nowrap="nowrap">
                                    <a href="/apps/shop/edit.html?id=${info.id!''}" data-toggle="ajax" data-width="950"
                                       data-height="600" data-title="编辑用户信息">
                                        <i class="layui-icon">&#xe642;</i>
                                        编辑
                                    </a>
                                    <#if !(wares.warestype?? && wares.warestype == "0") && wares.datastatus != true >
                                    <a href="/apps/agent/delete.html?id=${info.id!''}" style="margin-left:10px;"
                                       data-toggle="tip" data-title="请确认是否删除记录？">
                                        <i class="layui-icon" style="color:red;">&#x1006;</i>
                                        删除
                                    </a>
                                    </#if></td>
                            </tr>
                            </#list>
                            </#if>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row" style="padding:5px;">
        <div class="col-lg-12" id="page" style="text-align:center;"></div>
    </div>
</div>
<script>
    layui.use('laypage', function () {
        var laypage = layui.laypage;
        laypage.render({
            elem: 'page'
            , count: <#if agentList??>${agentList.totalElements}<#else>0</#if> //总记录
            , limit: <#if agentList??>${agentList.size}<#else>0</#if> //总记录
            , curr:<#if agentList??>${agentList.number+1}<#else>0</#if>
            , groups: 5 //连续显示分页数
            , jump: function (data, first) {
                if (!first) {
                    location.href = "/apps/platform/online/gameroom.html?p=" + data.curr;
                }
            }
        });
    });
</script>