package com.pangu.core.statemachine.config;

public interface AbstractTransitionConfigurer<I> {
	I and() ;
}
