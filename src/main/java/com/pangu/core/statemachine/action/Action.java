package com.pangu.core.statemachine.action;

import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;

public interface Action<T,S> {
	void execute(Message<T> message , BeiMiExtentionTransitionConfigurer<T, S> configurer); 
}
