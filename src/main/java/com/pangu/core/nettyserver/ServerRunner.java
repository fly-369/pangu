package com.pangu.core.nettyserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.corundumstudio.socketio.SocketIONamespace;
import com.corundumstudio.socketio.SocketIOServer;
import com.pangu.core.BMDataContext;
import com.pangu.core.nettyserver.handler.ChessEventHandler;
import com.pangu.core.nettyserver.handler.GameEventHandler;

/**
 * @ClassName: ServerRunner
 * @Description:  socket服务器配置
 * @date 2018年3月21日 下午3:28:26
 *
 */
@Order(value=1)
@Component
public class ServerRunner implements CommandLineRunner {
	private final SocketIOServer server;
	private final SocketIONamespace gameSocketNameSpace;
	private final SocketIONamespace chessSocketNameSpace;

	@Autowired
	public ServerRunner(SocketIOServer server) {
		this.server = server;
		gameSocketNameSpace = server.addNamespace(BMDataContext.NameSpaceEnum.GAME.getNamespace());
		chessSocketNameSpace = server.addNamespace(BMDataContext.NameSpaceEnum.CHESS.getNamespace());

	}

	@Bean(name = "chessNamespace")
	public SocketIONamespace getChessSocketIONameSpace(SocketIOServer server) {
		// 增加事件监听器
		gameSocketNameSpace.addListeners(new GameEventHandler(server));
		chessSocketNameSpace.addListeners(new ChessEventHandler(server));
		return gameSocketNameSpace;
	}

	public void run(String... args) throws Exception {
		server.start();
		BMDataContext.setIMServerStatus(true); // IMServer 启动成功
//		BMDataContext.getRedBlcakGameEngine().initRedBlcakGameEngine();

	}
}