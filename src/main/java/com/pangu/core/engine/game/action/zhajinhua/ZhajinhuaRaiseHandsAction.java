package com.pangu.core.engine.game.action.zhajinhua;

import org.apache.commons.lang3.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.zhajinhua.CreateZhajinhuaRaiseHandsTask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.web.model.GameRoom;

/**
 * 准备发底牌
 * 
 * @author iceworld
 *
 * @param <T>
 * @param <S>
 */
public class ZhajinhuaRaiseHandsAction<T, S> implements Action<T, S> {

	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		String room = (String) message.getMessageHeaders().getHeaders().get("room");
		if (!StringUtils.isBlank(room)) {
			GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room,
					BMDataContext.SYSTEM_ORGI);
			if (gameRoom != null) {
				CacheHelper.getExpireCache().put(gameRoom.getId(),
						new CreateZhajinhuaRaiseHandsTask(0, gameRoom, gameRoom.getOrgi()));
			}
		}
	}
}
