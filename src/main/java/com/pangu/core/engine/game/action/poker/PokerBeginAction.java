package com.pangu.core.engine.game.action.poker;

import org.apache.commons.lang3.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.poker.PokerBeginTask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.web.model.GameRoom;

/**
  * @ClassName: PokerBeginAction
  * @Description: 开始游戏
  * @author 老王
  * @date 2018年5月18日 下午8:33:05
  *
  * @param <T>
  * @param <S>
  */
public class PokerBeginAction<T,S> implements Action<T, S>{

	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		String room = (String)message.getMessageHeaders().getHeaders().get("room") ;
		if(StringUtils.isBlank(room)){
			return;
		}
		GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room, BMDataContext.SYSTEM_ORGI) ; 
		if(gameRoom!=null){
			int interval = (int) message.getMessageHeaders().getHeaders().get("interval") ;
			CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new PokerBeginTask(interval , gameRoom , gameRoom.getOrgi()));
		}
	}

}
