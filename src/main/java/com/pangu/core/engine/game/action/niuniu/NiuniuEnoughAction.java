package com.pangu.core.engine.game.action.niuniu;

import org.apache.commons.lang3.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.niuniu.NiuniuBeginTask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.web.model.GameRoom;

/**
 * 下注完成，开牌，但是开牌要等几秒钟，一是保证数据的完整，然后触发前端的动作
 * 
 * @author iceworld
 *
 */
public class NiuniuEnoughAction<T, S> implements Action<T, S> {
	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		String room = (String) message.getMessageHeaders().getHeaders().get("room");
		if (StringUtils.isBlank(room)) {
			return;
		}
		GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room,
				BMDataContext.SYSTEM_ORGI);

		if (gameRoom != null) {
			int interval = (int) message.getMessageHeaders().getHeaders().get("interval");
			CacheHelper.getExpireCache().put(gameRoom.getRoomid(),
					new NiuniuBeginTask(interval, gameRoom, gameRoom.getOrgi()));
		}
	}
}
