package com.pangu.core.engine.game.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName: BetResult
 * @Description: 计算牌型结果
 * @author 老王
 * @date 2018年6月9日 下午7:31:21
 *       <p>
 *       type=-1 AllIN: type=10 加注：type=2 过牌：type=0 跟注:type=1
 * 
 *
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DecisionBetResult {

	private int type;// 弃牌：type=-1 AllIN: type=10 加注：type=2    过牌：type=0  跟注:type=1

	private double coin;

}
