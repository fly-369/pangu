package com.pangu.core.engine.game.task.zhajinhua;

import java.util.ArrayList;
import java.util.List;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.ActionTaskUtils;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.util.GameUtils;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

/**
 * 
 * Description:创建炸金花的ai
 *
 * @author abo
 * @date 2018年4月16日
 */
public class CreateZhajinhuaAITask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public CreateZhajinhuaAITask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000;
	}

	public void execute() {
		// 先删除房间里面的ai，理论上是没有的，执行生成AI
		// GameUtils.removeGameRoom(gameRoom.getId(), gameRoom.getPlayway(), orgi);
		List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(),
				gameRoom.getOrgi());
		List<PlayUserClient> temp = new ArrayList<PlayUserClient>();
		for (PlayUserClient playUserClient : playerList) {
			if (BMDataContext.PlayerTypeEnum.AI.toString().equals(playUserClient.getPlayertype())) {
				// 移除金额为小于50 AI 金额大于20000的ai
//				if(playUserClient.getGoldcoins() < BMDataContext.GAME_AI_MIN_COIN 
//						|| playUserClient.getGoldcoins() > BMDataContext.GAME_AI_MAX_COIN) {
				if(playUserClient.getGoldcoins() < BMDataContext.GAME_AI_MIN_COIN) {
					temp.add(playUserClient);
					//缓存删掉ai
					CacheHelper.getGamePlayerCacheBean().delete(playUserClient.getId(), orgi);
					System.out.println("--------------------------删除ai"+playUserClient.getId()+"金额还剩"+playUserClient.getGoldcoins());

				}
			}
		}
		playerList.removeAll(temp);
		// 加入ai数量，房间数减去当前人数
		int aicount = gameRoom.getPlayers() - playerList.size();
		if (aicount > 0) {
			for (int i = 0; i < aicount; i++) {
				PlayUserClient playerUser = CacheHelper.getQuenePlayerCache().poll(gameRoom.getOrgi());
				if (playerUser == null) {
					System.out.println("--------------------------队列里面没有可用AI,请重启或者薪资");
				}else {
					// 按照加入房间的时间排序，有玩家离开后，重新发送玩家信息列表，重新座位
					playerUser.setPlayerindex(System.currentTimeMillis());
					playerUser.setRoomid(gameRoom.getId());
					playerUser.setRoomready(true);
					// 将用户加入到 room ，MultiCache
					CacheHelper.getGamePlayerCacheBean().put(playerUser.getId(), playerUser, orgi);
					playerList.add(playerUser);
				}
			}
		}
		
		
		
		
		// 需要时用房间的人员（起码要有一个普通玩家）和最大标准人数比较，如果没满就放入队列
		GameUtils.changeRoomQuene(gameRoom.getId(), gameRoom.getPlayway());
		// 向前端房间发送信息加入了ai
		ActionTaskUtils.sendPlayers(gameRoom, playerList);

		// 发起到下个状态,当所有人都ready，开始移交到下个状态
		ActionTaskUtils.roomReady(gameRoom, super.getGame(gameRoom.getPlayway(), orgi));
	}
}
