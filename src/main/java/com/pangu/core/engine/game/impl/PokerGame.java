package com.pangu.core.engine.game.impl;

import java.util.Arrays;
import java.util.List;

import com.pangu.core.BMDataContext;
import com.pangu.core.BMDataContext.GameStatusEnum;
import com.pangu.core.engine.game.ChessGameEngine;
import com.pangu.core.engine.game.iface.ChessGame;
import com.pangu.util.GameUtils;
import com.pangu.util.PokerUtilsPro;
import com.pangu.util.rules.model.Board;
import com.pangu.util.rules.model.NiuniuPosition;
import com.pangu.util.rules.model.PokerBoard;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: PokerGame
 * @Description: 处理德州扑克相关业务
 * @author 老王
 * @date 2018年4月3日 下午12:58:21
 *
 */
@Slf4j
public class PokerGame implements ChessGame {


	@Override
	public Board process(List<PlayUserClient> playUsers, GameRoom gameRoom, GamePlayway playway, String banker,
			int cardsnum) {
		log.debug("============执行德州发牌==================");
		gameRoom.setCurrentnum(gameRoom.getCurrentnum() + 1);
		Board board = new PokerBoard();
		board.setCards(null);
		board.setPosition(0);
		NiuniuPosition[] np = new NiuniuPosition[9];
		// 初始化位置
		for (int i = 0; i < 9; i++) {
			NiuniuPosition n = new NiuniuPosition();
			n.setPosition(i);
			n.setCards(new byte[2]);
			np[i] = n;
		}

		// 为了更好的控制好牌，我们先把位置绑定好用户信息。
		// 还要做一个事情，play 对号入座
		// 去确定这个人是第一个人
		// //设计下注的下一个人是谁
		int inx = 0;
		int ready = 0;// 站起人计数器
		boolean hasBlank = false;
		for (PlayUserClient playUser : playUsers) {
			if (playUser.getGamestatus() == GameStatusEnum.READY.toString()) {
				ready++;
				continue;
			}
			// ai设置
			if (BMDataContext.PlayerTypeEnum.AI.toString().equals(playUser.getPlayertype())) {
				np[inx].setAi(true);
			}
			if (playUser.isBlank()) {
				hasBlank = true;
				np[inx].setIsbanker(true);
				np[inx].setNext_player(true);
			}
			np[inx].setPlayer(playUser.getId());
			np[inx].setOdds(1);
			np[inx].setMycoins(playUser.getGoldcoins());
			inx++;
		}

		if (!hasBlank) {
			inx = 0;
			int next = new java.util.Random().nextInt(playUsers.size() - ready);
			for (PlayUserClient playUser : playUsers) {
				if (next == inx) {
					hasBlank = true;
					np[inx].setIsbanker(true);
					np[inx].setNext_player(true);
				}
				inx++;
			}
		}
		byte cards[] =null; 
		/*******控制牌*********/
		//@ FIXME 具体发什么牌的策略，已经加入奖金池策略，可以优化
		if (BMDataContext.getContext().getBean(ChessGameEngine.class).isBeginCheat(BMDataContext.GameTypeEnum.NIUNIU.toString().toLowerCase(), playway.getOrgi())) {
			log.info("执行欺骗操作······························");	
			 cards = PokerUtilsPro.shuffleAiWinner(np);//得到AI赢的
		}else {
			 cards = PokerUtilsPro.shuffle();
		}
		/*******控制牌*********/
		board.setCards(cards);// 洗牌完成

		// gameRoom.getCardsnum()//每人发两张牌
		for (int i = 0; i < 9 * 2; i++) {
			int pos = i % 9;
			np[pos].getCards()[i / 9] = cards[i];
		}
		for (NiuniuPosition tempPlayer : np) {
			Arrays.sort(tempPlayer.getCards());// 排序。很重要
			tempPlayer.setCards(GameUtils.reverseCards(tempPlayer.getCards()));
			tempPlayer.setCards(tempPlayer.getCards());
		}
		// 发9个用户的牌
		board.setNiuniuPosition(np);
		//记录发牌次数
		board.setPosition(1);
		return board;
	}


}
