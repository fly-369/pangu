package com.pangu.core.engine.game.task.niuniu;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.web.model.GameRoom;

/**
 * @ClassName: NiuniuCreateAITask
 * @Description:  结束时候处理相关业务，暂时没有
 * @author 老王
 * @date 2018年5月15日 下午8:00:55
 *
 */

public class NiuniuDealTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

	public void execute() {
		
		super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.ENTER.toString(),
				0);
	}
	
	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public NiuniuDealTask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000;
	}

}
