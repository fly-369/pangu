package com.pangu.core.engine.game.task.poker;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: NiuniuCreateAITask
 * @Description: 处理结算后的相关数据
 * @author 老王
 * @date 2018年5月15日 下午8:00:55
 *
 */
@Slf4j
public class PokerDealTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

	public void execute() {
		log.info("我是分割线=============================================");
		// @ FIXME 结算相关逻辑可以转移至此处
		super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.ENTER.toString(), 0);
	}

	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public PokerDealTask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000;
	}

}
