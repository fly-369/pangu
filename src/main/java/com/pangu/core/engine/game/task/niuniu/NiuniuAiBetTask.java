package com.pangu.core.engine.game.task.niuniu;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.util.GameUtils;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: NiuniuAiBetTask
 * @Description:  AI下注
 * @author 老王
 * @date 2018年5月17日 下午8:16:37
 *
 */
@Slf4j
public class NiuniuAiBetTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public NiuniuAiBetTask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000; // 5秒后执行
	}

	@Override
	public void execute() {
		ExecutorService executorService = Executors.newCachedThreadPool();
		List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), orgi);
		for (int i = 0; i < new java.util.Random().nextInt(3) + 1; i++) {
			for (PlayUserClient playUserClient : list) {
				if (BMDataContext.BEIMI_GAME_AI.equals(playUserClient.getPlayertype())) {
					executorService.execute(() -> {
						bet(gameRoom, playUserClient);
					});

				}
			}
		}
	}

	/*
	 * 每一个ai的下注模拟
	 */
	public static void bet(GameRoom gameRoom, PlayUserClient playUserClient) {
		boolean isBet = GameUtils.getChance(80);
		if (!isBet) {
			return;
		}
		GameUtils.Waittime();// 等待时间
		int randomBetClass = new Random().nextInt(4) + 1;// 下注位置
		long coinRandom = GameUtils.getCoin(playUserClient.getGoldcoins());
		BMDataContext.getNiuniuEngine().betGameRequest(gameRoom, playUserClient, randomBetClass, coinRandom);
		log.info("{}:ai开始下注了哦·····", playUserClient.getUsername());
	}
}
