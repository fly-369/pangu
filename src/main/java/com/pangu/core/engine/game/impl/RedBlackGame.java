package com.pangu.core.engine.game.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.pangu.core.engine.game.iface.ChessGame;
import com.pangu.util.GameUtils;
import com.pangu.util.ProcessCard;
import com.pangu.util.UKTools;
import com.pangu.util.rules.model.Board;
import com.pangu.util.rules.model.NiuniuPosition;
import com.pangu.util.rules.model.Player;
import com.pangu.util.rules.model.RedBlackBoard;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Description:红黑发牌
 *
 * @author abo
 * @date 2018年3月26日
 */
@Slf4j
public class RedBlackGame implements ChessGame {

	@Override
	public Board process(List<PlayUserClient> playUsers, GameRoom gameRoom, GamePlayway playway, String banker,
			int cardsnum) {
		return null;
	}

	public Board process(GameRoom gameRoom, GamePlayway playway, int cardsnum) {
		// log.info("============执行红黑发牌==================");
		gameRoom.setCurrentnum(gameRoom.getCurrentnum() + 1);
		Board board = new RedBlackBoard();
		board.setCards(null);

		List<Byte> temp = new ArrayList<Byte>();
		// 加入出去大小王的52张牌
		for (int i = 0; i < 52; i++) {
			temp.add((byte) i);
		}
		// 洗牌次数，参数指定，建议洗牌次数 为1次，多次洗牌的随机效果更好，例如：7次
		for (int i = 0; i < playway.getShuffletimes() + 7; i++) {
			Collections.shuffle(temp);
		}
		byte[] cards = new byte[52];
		for (int i = 0; i < temp.size(); i++) {
			cards[i] = temp.get(i);
		}

		board.setCards(cards);

		board.setRatio(1); // 默认倍率 1

		NiuniuPosition[] niuniuPositions = new NiuniuPosition[2];

		NiuniuPosition np = new NiuniuPosition();
		np.setCards(new byte[cardsnum]);
		np.setPosition(1);
		niuniuPositions[0] = np;

		np = new NiuniuPosition();
		np.setCards(new byte[cardsnum]);
		np.setPosition(2);
		niuniuPositions[1] = np;
		// 牌局放入扑克
		board.setNiuniuPosition(niuniuPositions);

		// 分牌
		for (int i = 0; i < gameRoom.getCardsnum() * 2; i++) {
			int pos = i % niuniuPositions.length;
			niuniuPositions[pos].getCards()[i / niuniuPositions.length] = cards[i];
		}

		board.setRoom(gameRoom.getId());
		return board;
	}

	public static void main(String[] args) {
		List<Byte> temp = new ArrayList<Byte>();
		// 加入出去大小王的52张牌
		for (int i = 0; i < 52; i++) {
			temp.add((byte) i);
		}
		// 洗牌次数，参数指定，建议洗牌次数 为1次，多次洗牌的随机效果更好，例如：7次
		for (int i = 0; i < 6 + 1; i++) {
			Collections.shuffle(temp);
		}
	}

}
