package com.pangu.core.engine.game.task.niuniu;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.ActionTaskUtils;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.model.PlayerRecordNiuniu;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.util.AIBet;
import com.pangu.util.rules.model.TopPlayer;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

/**
  * @ClassName: NiuniuBetTask
  * @Description:  下注节点task
  * @author 老王
  * @date 2018年3月31日 下午3:05:52
  *
  */
@Slf4j
public class NiuniuBetTask   extends AbstractTask implements ValueWithExpiryTime  , BeiMiGameTask {
	@Override
	public void execute() {
		log.info("等待15秒下注·························");
		gameRoom.setStatus(BMDataContext.GameStatusEnum.BET.toString());
		gameRoom.setStatusTime(System.currentTimeMillis());//前端使用，中途进入游戏的时候，用于恢复数据
		CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
		// 下注
		super.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom , BeiMiGameEvent.ENOUGH.toString(),BMDataContext.GAME_BET_TIME);
		//模拟Ai下注
		//发送排行榜数据
	}
	/*
	 * 发送下注记录到前端
	 */
	public void sendTopData(List<PlayerRecordNiuniu> pr) {
		TopPlayer	toper =  (TopPlayer) CacheHelper.getGamePlayerTopCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
		if(toper==null||pr==null) {
			return;
		}
		
		for (PlayerRecordNiuniu playerRecord2 : pr) {
			for (PlayUserClient play : toper.getPlayer()) {
				if(play.getId().equals(playerRecord2.getPlayerId())) {
					PlayerRecordNiuniu nn = new PlayerRecordNiuniu();
					try {
						BeanUtils.copyProperties(playerRecord2, nn);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					//BMDataContext.getNiuniuEngine().betSendRequest(gameRoom, nn);
					ActionTaskUtils.sendEvent(nn.getCommand(), nn, gameRoom);
				}
			}
		}
			
	}

	private long timer  ;
	private GameRoom gameRoom = null ;
	private String orgi ;
	
	public NiuniuBetTask(long timer , GameRoom gameRoom, String orgi){
		super();
		this.timer = timer ;
		this.gameRoom = gameRoom ;
		this.orgi = orgi ;
	}
	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis()+timer*1000;	//5秒后执行
	}
	
	
	

}
