package com.pangu.core.engine.game.iface;

import com.pangu.core.BMDataContext;
import com.pangu.core.engine.game.Message;
import com.pangu.util.event.UserEvent;

/**
 * @ClassName: Record
 * @Description: 下注记录接口
 * @author 老王
 * @date 2018年6月4日 下午3:13:19
 *
 */
public abstract class Record implements UserEvent, Message {

	private String command =BMDataContext.BEIMI_BETRECORD_EVENT;

	private static final long serialVersionUID = 1L;

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command=command;
	}

}
