package com.pangu.core.engine.game.action;

import org.apache.commons.lang3.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.ActionTaskUtils;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.util.GameUtils;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;

/**
 * 创建房间的人，房卡模式下的 房主， 大厅模式下的首个进入房间的人
 * @author iceworld
 *
 */
@Slf4j
public class JoinAction<T,S> implements Action<T, S>{
	
	/**
	 * JOIN事件，检查是否 凑齐一桌子，如果凑齐了，直接开始，并取消计时器
	 * 如果不够一桌子，啥也不做，等人活等计时器到事件
	 * 撮合成功的，立即开启游戏
	 * 通知所有成员的消息在 GameEventHandler里处理了
	 * 
	 */
	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T,S> configurer) {
		log.info("执行JoinAction");
		String room = (String)message.getMessageHeaders().getHeaders().get("room") ;
		if(!StringUtils.isBlank(room)){
			GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room, BMDataContext.SYSTEM_ORGI) ; 
			if(gameRoom!=null){
				/**
				 * 发送一个 Enough 事件
				 */
				ActionTaskUtils.roomReady(gameRoom, GameUtils.getGame(gameRoom.getPlayway() , gameRoom.getOrgi()));
			}
		}
	}
}
