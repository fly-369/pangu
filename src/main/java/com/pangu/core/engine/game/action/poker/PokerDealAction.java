package com.pangu.core.engine.game.action.poker;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.poker.PokerDealTask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;
/**
  * @ClassName: BetAction
  * @Description:  处理下注业务
  * @author 老王
  * @date 2018年3月31日 下午2:54:18
  *
  * @param <T>
  * @param <S>
  */
@Slf4j
public class PokerDealAction <T,S> implements Action<T, S> {

	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		log.info("等待下一局下注的·············");
		String room = (String) message.getMessageHeaders().getHeaders().get("room");
		GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room,
				BMDataContext.SYSTEM_ORGI);
		if(gameRoom!=null){
			int interval = (int) message.getMessageHeaders().getHeaders().get("interval") ;
			CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new PokerDealTask(interval , gameRoom , gameRoom.getOrgi()));
		}
		
	}

}
