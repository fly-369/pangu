package com.pangu.core.engine.game.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

import com.pangu.core.engine.game.iface.ChessGame;
import com.pangu.util.GameUtils;
import com.pangu.util.ProcessCard;
import com.pangu.util.rules.model.Board;
import com.pangu.util.rules.model.DuZhuBoard;
import com.pangu.util.rules.model.Player;
import com.pangu.util.rules.model.SanGongBoard;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Description:
 *
 * @author abo
 * @date 2018年3月26日
 */
@Slf4j
public class SangongGame implements ChessGame {

	@Override
	public Board process(List<PlayUserClient> playUsers, GameRoom gameRoom, GamePlayway playway, String banker,
			int cardsnum) {
		log.info("============执行三公发牌==================");
		gameRoom.setCurrentnum(gameRoom.getCurrentnum() + 1);
		Board board = new SanGongBoard();
		board.setCards(null);
		List<Byte> temp = new ArrayList<Byte>();
		// 加入出去大小王的52张牌
		for (int i = 0; i < 52; i++) {
			temp.add((byte) i);
		}
		// 洗牌次数，参数指定，建议洗牌次数 为1次，多次洗牌的随机效果更好，例如：7次
		for (int i = 0; i < playway.getShuffletimes() + 1; i++) {
			Collections.shuffle(temp);
		}
		byte[] cards = new byte[52];
		for (int i = 0; i < temp.size(); i++) {
			cards[i] = temp.get(i);
		}
		board.setCards(cards);

		board.setRatio(15); // 默认倍率 15
		int random = playUsers.size() * gameRoom.getCardsnum();

		board.setPosition((byte) new Random().nextInt(random)); // 按照人数计算在随机界牌 的位置，避免出现在底牌里

		Player[] players = new Player[playUsers.size()];

		int inx = 0;
		for (PlayUserClient playUser : playUsers) {
			Player player = new Player(playUser.getId());
			player.setCards(new byte[cardsnum]);
			players[inx++] = player;
		}
		// 分牌
		for (int i = 0; i < gameRoom.getCardsnum() * playUsers.size(); i++) {
			// int pos = i % players.length;
			// players[pos].getCards()[i / players.length] = cards[i];
			// if (i == board.getPosition()) {
			// players[pos].setRandomcard(true); // 起到地主牌的人
			// }
			int pos = i % players.length;
			players[pos].getCards()[i / players.length] = cards[i];
			// System.out.println(players[pos].getPlayuser()+"发牌为："+players[pos].getCards()[i
			// / players.length]);
		}

		for (Player tempPlayer : players) {
			Arrays.sort(tempPlayer.getCards());
			tempPlayer.setCards(GameUtils.reverseCards(tempPlayer.getCards()));
			byte[] cs = tempPlayer.getCards();
			String cardStr = "";
			for (int i = 0; i < cs.length; i++) {
				cardStr += ProcessCard.byteCardToString(cs[i]) + "|";
			}
		}
		board.setRoom(gameRoom.getId());
		Player tempbanker = players[0];
		if (!StringUtils.isBlank(banker)) {
			for (int i = 0; i < players.length; i++) {
				Player player = players[i];
				if (player.equals(banker)) {
					if (i < (players.length - 1)) {
						tempbanker = players[i + 1];
					}
				}
			}

		}
		board.setPlayers(players);
		if (tempbanker != null) {
			board.setBanker(tempbanker.getPlayuser());
		}
		return board;
	}

}
