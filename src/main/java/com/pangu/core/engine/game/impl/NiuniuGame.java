package com.pangu.core.engine.game.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.engine.game.ChessGameEngine;
import com.pangu.core.engine.game.iface.ChessGame;
import com.pangu.util.NiuNiuUtilsPro;
import com.pangu.util.rules.model.Board;
import com.pangu.util.rules.model.NiuniuBoard;
import com.pangu.util.rules.model.NiuniuPosition;
import com.pangu.util.rules.model.Player;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

/**
  * @ClassName: NiuniuGame
  * @Description: 牛牛发牌核心算法
  * @author 老王
  * @date 2018年6月7日 下午1:46:13
  *
  */
@Slf4j
public class NiuniuGame implements ChessGame {
	/*
	 * 开启牛牛，固定为五个用户，不管有沒有用戶，都会发牌 固定庄家，当然可以根据真实玩家的压的数目调节牌面大小
	 */
	@Override
	public Board process(List<PlayUserClient> playUsers, GameRoom gameRoom, GamePlayway playway, String banker,
			int cardsnum) {
		log.info("============执行牛牛发牌==================");
		gameRoom.setCurrentnum(gameRoom.getCurrentnum() + 1);
		Board board = new NiuniuBoard();
		board.setCards(null);

		Player[] players = new Player[playUsers.size()];
		int inx = 0;
		for (PlayUserClient playUser : playUsers) {
			if (inx == 0 && StringUtils.isBlank(banker)) {
				board.setBanker(playUser.getId()); // 必须设置系统为庄家，也就是 必须固定一个
			}
			Player player = new Player(playUser.getId());
			player.setCards(new byte[cardsnum]);
			players[inx++] = player;
		}

		// 关键代码
		NiuniuPosition[] np = null;
		//判断奖金池里面的数据是否进行干预操作
		if (BMDataContext.getContext().getBean(ChessGameEngine.class).isBeginCheat(BMDataContext.GameTypeEnum.NIUNIU.toString().toLowerCase(), playway.getOrgi())) {
			log.info("执行欺骗操作······························");	
			np = NiuNiuUtilsPro.shuffle(playway.getScore(), BMDataContext.getNiuniuEngine().getBiggerSeat(playUsers));//BiggerSeat 下注最大的玩家
		} else {
			np = NiuNiuUtilsPro.shuffle(playway.getScore());
		}
		board.setNiuniuPosition(np);
		board.setPlayers(players);
		board.setRatio(playway.getScore());
		board.setRoom(gameRoom.getId());// 设置房间ID
		return board;
	}

}
