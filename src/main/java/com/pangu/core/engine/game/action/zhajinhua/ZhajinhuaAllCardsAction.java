package com.pangu.core.engine.game.action.zhajinhua;

import org.apache.commons.lang3.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.zhajinhua.CreateZhajinhuaAllCardsTask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.web.model.GameRoom;

/**
 * 
 * Description:炸金花结算action类
 *
 * @author abo
 * @date 2018年4月28日
 * @param <T>
 * @param <S>
 */
public class ZhajinhuaAllCardsAction<T, S> implements Action<T, S> {

	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		String room = (String) message.getMessageHeaders().getHeaders().get("room");
		if (!StringUtils.isBlank(room)) {
			GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room,
					BMDataContext.SYSTEM_ORGI);
			if (gameRoom != null) {
				CacheHelper.getExpireCache().put(gameRoom.getId(),
						new CreateZhajinhuaAllCardsTask(0, gameRoom, gameRoom.getOrgi()));
			}
		}
	}
}
