package com.pangu.core.engine.game.task.zhajinhua;

import java.util.List;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.ActionTaskUtils;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.util.GameUtils;
import com.pangu.util.rules.model.Board;
import com.pangu.util.rules.model.NextPlayer;
import com.pangu.util.rules.model.Player;
import com.pangu.util.rules.model.TopPlayer;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CreateZhajinhuaRaiseHandsTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public CreateZhajinhuaRaiseHandsTask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000; // 5秒后执行
	}

	public void execute() {
		List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), orgi);
		// 发牌
		Board board = GameUtils.playGame(playerList, gameRoom, gameRoom.getLastwinner(), gameRoom.getCardsnum());
		for (PlayUserClient playerUser : playerList) {
			// 每个人收到的 牌面不同，所以不用 ROOM发送广播消息，而是用 遍历房间里所有成员发送消息的方式
			// UserBoard sb = new UserBoard(board, playerUser.getId(), "playing");
			TopPlayer tp = new TopPlayer();
			tp.setPlayer(playerList);
			tp.setCommand("playing");
			// 发送给前端，开始游戏
			ActionTaskUtils.sendEvent(playerUser, tp);

			playerUser.setGamestatus(BMDataContext.GameStatusEnum.PLAYING.toString());
			// 更新状态到 PLAYING
			if (CacheHelper.getApiUserCacheBean().getCacheObject(playerUser.getId(), playerUser.getOrgi()) != null) {
				CacheHelper.getApiUserCacheBean().put(playerUser.getId(), playerUser, orgi);
			}
		}
		log.info("--------------发完底牌---------------通知前端准备开局");
		// 初始化牌局的下个player
		for (Player player : board.getPlayers()) {
			if (player.getPlayuser().equals(board.getBanker())) {// 抢到地主的人
				board.setNextplayer(new NextPlayer(player.getPlayuser(), false));
				break;
			}
		}
		// 更新牌局状态
		CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
		// 开始打牌，等待3秒钟
		super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.PLAYCARDS.toString(),BMDataContext.GAME_PLAYING_WAIT_TIME); // 通知状态机


		// 发送一个 开始打牌的事件 ， 判断当前出牌人是 玩家还是 AI，如果是 AI，则默认 1秒时间，如果是玩家，则超时时间是25秒
//		PlayUserClient playUserClient = ActionTaskUtils.getPlayUserClient(gameRoom.getId(),
//				lastHandsPlayer.getPlayuser(), orgi);
//		if (BMDataContext.PlayerTypeEnum.NORMAL.toString().equals(playUserClient.getPlayertype())) {
//			super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.PLAYCARDS.toString(), 25); // 应该从
//			// 游戏后台配置参数中获取
//		} else {
//			super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.PLAYCARDS.toString(), 3); // 应该从游戏后台配置参数中获取
//		}
	}
}
