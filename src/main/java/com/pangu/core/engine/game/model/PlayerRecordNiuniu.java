package com.pangu.core.engine.game.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.pangu.core.engine.game.Message;
import com.pangu.util.UKTools;
import com.pangu.util.event.UserEvent;

/**
 * 
 * Description:玩家日志表
 *
 * @author abo
 * @date 2018年3月30日
 */
@Document(indexName = "beimi", type = "uk_playrecord_niuniu")
@Table(name = "bm_game_playrecord_niuniu")
@Entity
@Proxy(lazy = false)
public class PlayerRecordNiuniu   implements UserEvent, Message,java.io.Serializable{

	
	private String command ;
	private static final long serialVersionUID = 1L;
	private String id  = UKTools.getUUID();
	private String gameId;// 游戏局id
	private String roomId;// 房间id
	private String playerId;// 用户id
	private String gameType;// 游戏大类：牛牛、红黑、斗地主、
	private String gameSubType;// 下注小类：红黑大战的红，红黑大战的黑，牛牛的方块
	private Date gameTime = new Date();
	private double coin;// 实际下注
	private double winLoseCoin;// 输赢金额
	private String cards;
	private String isBanker;// 庄闲:0庄，1闲
	private String playerType;// 用户类型：1真人，2ai
	private String summaryId;// 结算id
	private String orgi ;
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	
	public String getId() {
		return id;
	}
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getGameType() {
		return gameType;
	}
	public void setGameType(String gameType) {
		this.gameType = gameType;
	}
	public String getGameSubType() {
		return gameSubType;
	}
	public void setGameSubType(String gameSubType) {
		this.gameSubType = gameSubType;
	}

	public Date getGameTime() {
		return gameTime;
	}
	public void setGameTime(Date gameTime) {
		this.gameTime = gameTime;
	}
	public double getCoin() {
		return coin;
	}
	public void setCoin(double coin) {
		this.coin = coin;
	}
	public double getWinLoseCoin() {
		return winLoseCoin;
	}
	public void setWinLoseCoin(double winLoseCoin) {
		this.winLoseCoin = winLoseCoin;
	}
	public String getCards() {
		return cards;
	}
	public void setCards(String cards) {
		this.cards = cards;
	}
	public String getIsBanker() {
		return isBanker;
	}
	public void setIsBanker(String isBanker) {
		this.isBanker = isBanker;
	}
	public String getPlayerType() {
		return playerType;
	}
	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}
	public String getSummaryId() {
		return summaryId;
	}
	public void setSummaryId(String summaryId) {
		this.summaryId = summaryId;
	}
	@Override
	@Transient
	public String getCommand() {
		return command;
	}
	@Override
	public void setCommand(String command) {
		this.command=command;
	}

}
