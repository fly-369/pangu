package com.pangu.core.engine.game.action.zhajinhua;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.zhajinhua.CreateZhajinhuaPlayCardsTask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.util.rules.model.ZhajinhuaBoard;
import com.pangu.web.model.GameRoom;

/**
 * 
 * Description: 炸金花开始游戏
 *
 * @author abo
 * @date 2018年4月17日
 * @param <T>
 * @param <S>
 */
public class ZhajinhuaPlayCardsAction<T, S> implements Action<T, S> {
	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		// 获取到房间ID,数据校验
		String room = (String) message.getMessageHeaders().getHeaders().get("room");
		// if (!StringUtils.isBlank(room)) {
		// return;
		// }
		GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room,
				BMDataContext.SYSTEM_ORGI);
		if (gameRoom == null) {
			return;
		}
		// 数据校验完毕，进行业务处理
		// 获取到牌局
		ZhajinhuaBoard board = (ZhajinhuaBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(),
				gameRoom.getOrgi());
		// 获取到传过来的定时器的间隔时间
		int interval = (int) message.getMessageHeaders().getHeaders().get("interval");
		// 获取到庄家或者地主
		String nextPlayer = board.getBanker();
//		NextPlayer nextp = board.getNextplayer();
//		String playerId = nextp.getNextplayer();
//		// 若下一个出牌的人不为空则覆盖庄家，否则庄家先出牌
//		if (!StringUtils.isBlank(playerId)) {
//			// nextPlayer = board.getNextplayer().getNextplayer();
//			nextPlayer = playerId;
//		}
		// 驱动流转
		CacheHelper.getExpireCache().put(gameRoom.getId(),
				new CreateZhajinhuaPlayCardsTask(interval, nextPlayer, gameRoom, gameRoom.getOrgi()));

	}
}
