package com.pangu.core.engine.game.task.poker;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.model.DecisionBetResult;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.util.GameUtils;
import com.pangu.util.rules.model.PokerBoard;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: PokerAutoTask
 * @Description: 自动打牌引擎。
 * @author 老王
 * @date 2018年5月3日 下午1:52:48
 *
 */
@Slf4j
public class PokerAiAutoTask extends AbstractTask implements BeiMiGameTask {

	public void execute() {
		log.info("Ai思考中·············");
		// 1.思考一下下
		// @ FIXME 随机时间可以优化得更加智能
		GameUtils.Waittime(1, 3);
		// 2.得到当前的用户信息
		PokerBoard board = (PokerBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(),
				gameRoom.getOrgi());
		// 做下注，加倍，过牌，弃牌的决策
		DecisionBetResult result = BMDataContext.getPokerEngine().decision(board);
		log.info("执行：{}操作，金额:{}", result.getType(), result.getCoin());
		BMDataContext.getPokerEngine().betGameRequest(board.getCurrplayer(), gameRoom, result.getCoin(),
				result.getType(), BMDataContext.PlayerTypeEnum.AI.toString().toLowerCase());
		super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.AUTO.toString(), 0);
	}

	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public PokerAiAutoTask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000;
	}

	public boolean getIsGiveUp() {
		return new java.util.Random().nextInt(2) == 0 ? true : false;
	}

}
