package com.pangu.core.engine.game.iface;

import java.util.List;

import com.pangu.util.rules.model.Board;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

/**
 * 棋牌游戏接口API
 * @author iceworld
 *
 */
public interface ChessGame {
	/**
	 * 创建一局新游戏
	 * @return
	 */
	public Board process(List<PlayUserClient> playUsers , GameRoom gameRoom ,GamePlayway playway ,  String banker , int cardsnum) ;
}
