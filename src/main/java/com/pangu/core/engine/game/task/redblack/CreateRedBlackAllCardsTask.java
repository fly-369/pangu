package com.pangu.core.engine.game.task.redblack;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.model.Summary;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.util.rules.model.Board;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Description:结算红黑，数据存数据库
 *
 * @author abo
 * @date 2018年4月4日
 */
public class CreateRedBlackAllCardsTask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public CreateRedBlackAllCardsTask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000; // 5秒后执行
	}

	public void execute() {
		// 结算并且保存记录
		Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
		Summary summary = board.summary(board, gameRoom, gameRoom.getGamePlayway());
		sendEvent("settlement", summary, gameRoom); // 通知所有客户端结束牌局，进入结算
	}
}
