package com.pangu.core.engine.game.action.sangong;

import org.apache.commons.lang3.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheConfigTools;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.sangong.CreateSangongAITask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.web.model.AiConfig;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.SysGameConfig;

/**
 * 创建房间的人，房卡模式下的 房主， 大厅模式下的首个进入房间的人
 * @author iceworld
 *
 */
public class SangongEnterAction<T,S> implements Action<T, S>{
	
	/**
	 * 进入房间后开启 5秒计时模式，计时结束后未撮合玩家成功 的，召唤机器人，
	 * 撮合成功的，立即开启游戏
	 */
	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T,S> configurer) {
		String room = (String)message.getMessageHeaders().getHeaders().get("room") ;
		if(!StringUtils.isBlank(room)){
			GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room, BMDataContext.SYSTEM_ORGI) ; 
			if(gameRoom!=null){
				if(!gameRoom.isCardroom()) {
					//AiConfig aiConfig = CacheConfigTools.getAiConfig(gameRoom.getOrgi());
					SysGameConfig gameConfig = CacheConfigTools.getSysGameConfig(gameRoom.getOrgi()) ;
					if (gameConfig.isEnableai()) {
						//ai入场等待计时器
						CacheHelper.getExpireCache().put(gameRoom.getOrgi(), new CreateSangongAITask(gameConfig.getTimeout(), gameRoom, gameRoom.getOrgi()));
					}
				}
				/**
				 * 更新状态
				 */
				gameRoom.setStatus(configurer.getTarget().toString());
				CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
				
//				CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new CreateSangongBeginTask(1 , gameRoom , gameRoom.getOrgi()));
				

			}
		}
	}
}
