package com.pangu.core.engine.game.action.niuniu;

import org.apache.commons.lang3.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheConfigTools;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.ActionTaskUtils;
import com.pangu.core.engine.game.ChessGameEngine;
import com.pangu.core.engine.game.task.niuniu.NiuniuCreateAITask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.util.rules.model.GameStatus;
import com.pangu.util.rules.model.TopPlayer;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.SysGameConfig;

public class NiuniuEnterAction<T, S> implements Action<T, S> {

	/**
	 * 进入房间后开启 ,立即开启游戏
	 */
	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		String room = (String) message.getMessageHeaders().getHeaders().get("room");
		if (StringUtils.isBlank(room)) {
			return;
		}
		GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room,
				BMDataContext.SYSTEM_ORGI);
		/*
		 * 发送top给前端
		 */
		BMDataContext.getContext().getBean(ChessGameEngine.class).sendTopPlayer(gameRoom, gameRoom.getOrgi());
		
		gameRoom.setStatus(BMDataContext.GameStatusEnum.BET.toString());
		GameStatus status = new GameStatus(BMDataContext.GameStatusEnum.BET.toString(), "开始下注");
		ActionTaskUtils.sendEvent(BMDataContext.BEIMI_GAMESTATUS_EVENT, status, gameRoom);
		CacheHelper.getGameStatusCacheBean().put(gameRoom.getRoomid(), status, gameRoom.getOrgi());
		if (gameRoom != null) {
			if (!gameRoom.isCardroom()) {
					CacheHelper.getExpireCache().put(gameRoom.getRoomid(),
							new NiuniuCreateAITask(1, gameRoom, gameRoom.getOrgi()));
			
			}
			/**
			 * 更新状态
			 */
			// gameRoom.setStatus(configurer.getTarget().toString());
			CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
		

		}
	}
}
