package com.pangu.core.engine.game.task.poker;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;

/**
  * @ClassName: NiuniuBetTask
  * @Description:  德州 盲注下注task
  * @author 老王
  * @date 2018年3月31日 下午3:05:52
  *
  */
@Slf4j
public class PokerBetTask   extends AbstractTask implements ValueWithExpiryTime  , BeiMiGameTask {
	@Override
	public void execute() {
		log.info("等待20秒下注·························");
		gameRoom.setStatus(BMDataContext.GameStatusEnum.BET.toString());
		gameRoom.setStatusTime(System.currentTimeMillis());
		CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
		// 下注
		super.getGame(gameRoom.getPlayway(), gameRoom.getOrgi()).change(gameRoom , BeiMiGameEvent.ENOUGH.toString(),10);
		//模拟Ai下注
		log.info("AI下注情况记录表存入缓存······");
	}


	private long timer  ;
	private GameRoom gameRoom = null ;
	private String orgi ;
	
	public PokerBetTask(long timer , GameRoom gameRoom, String orgi){
		super();
		this.timer = timer ;
		this.gameRoom = gameRoom ;
		this.orgi = orgi ;
	}
	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis()+timer*1000;	//5秒后执行
	}
	
	
	

}
