package com.pangu.core.engine.game.task.poker;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.ActionTaskUtils;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.util.rules.model.GameStatus;
import com.pangu.util.rules.model.NiuniuPosition;
import com.pangu.util.rules.model.PokerBoard;
import com.pangu.util.rules.model.PokerCards;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: PokerAutoTask
 * @Description: 自动打牌引擎。当前下注的人保存在board中
 * @author 老王
 * @date 2018年5月3日 下午1:52:48
 *
 */
@Slf4j
public class PokerAutoTask extends AbstractTask implements BeiMiGameTask {

	private static final int showCardTime = 10;// 牌展现的时间
	private static final int PlayTime = 2;// AI下注预留时间

	public void execute() {
		log.info("进入打牌");
		// 解决真人没有操作的情况
		// 1.获取到当前牌局,以及位置信息：当前的下注人以及下一个下注的人
		PokerBoard board = (PokerBoard) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(),
				gameRoom.getOrgi());
		// 检查是否
		BMDataContext.getPokerEngine().cheakIsBet(board, gameRoom);
		NiuniuPosition[] po = board.getNiuniuPosition();
		NiuniuPosition curr = board.getCurrent(po);// 当前的下注的人
		if (curr == null) {
			log.error("出问题了··························");
			return;
		}
		board.setCurrplayer(curr.getPlayer());
		NiuniuPosition next = board.getNext(po);// 下一个下注的人,记住：这个时候索引会改变，这样会有问题
		// 2.若这个是该轮下注的最后一个人。
		// 3.第一轮下注

		// 若都放弃了，或者只有一个人了留下了，便直接结算
		if (this.checkIsOver(po)) {
			log.info("准备结束了······················");
			super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.SUMMARY.toString(), 0);
			return;
		}

		if (next == null) {// 这是最后一个人，发牌
			this.playCard(board);
		} else {
			this.playerBet(curr, next);
			CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, orgi);
		}

	}

	/**
	 * @Description :若都放弃了，或者只有一个人了留下了，便直接结算
	 * @author 老王
	 * 
	 */
	public boolean checkIsOver(NiuniuPosition[] po) {
		int count = 0;
		for (NiuniuPosition niuniuPosition : po) {
			if (niuniuPosition.getOdds() > 0) {
				count = count++;
			}
		}
		log.info("放弃人数：{}", count);
		return count > 1 ? true : false;

	}

	private void playCard(PokerBoard board) {
		byte[] c = board.getCards();
		switch (board.getPosition()) {
		case 1:
			byte[] lasthands = { c[14], c[15], c[16] };
			PokerCards pc = new PokerCards(c[14], c[15], c[16]);
			ActionTaskUtils.sendEvent(pc.getCommand(), pc, gameRoom);
			board.setLasthands(lasthands);
			board.setPosition(2);
			CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, orgi);
			super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.AUTO.toString(), showCardTime);
			return;
		case 2:
			pc = new PokerCards(c[17]);
			ActionTaskUtils.sendEvent(pc.getCommand(), pc, gameRoom);
			board.setPosition(3);
			byte[] lasthands_2 = { c[14], c[15], c[16], c[17] };
			board.setLasthands(lasthands_2);
			CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, orgi);
			super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.AUTO.toString(), showCardTime);
			return;
		case 3:
			pc = new PokerCards(c[18]);
			ActionTaskUtils.sendEvent(pc.getCommand(), pc, gameRoom);
			board.setPosition(4);// 结束
			byte[] lasthands_3 = { c[14], c[15], c[16], c[17], c[18] };
			board.setLasthands(lasthands_3);
			CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, orgi);
			super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.AUTO.toString(), showCardTime);
			return;
		case 4:
			super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.SUMMARY.toString(), 0);
			return;
		default:
			log.error("这是一个错误的选择，请检查代码逻辑···");
			return;
		}
	}

	private void playerBet(NiuniuPosition curr, NiuniuPosition next) {
		if (curr.getOdds() < 0 || next.getOdds() < 0) {// 弃牌了next
			log.info(curr.getPlayer() + "该用户弃牌了");
			super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.AUTO.toString(), 0);
		} else {
			this.changeStatue(curr.getPlayer());
			log.info(curr.getPlayer() + ":执行用户相关操作。弃牌或者押注或者加注····");
			if (curr.isAi()) {
				super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.PLAYCARDS.toString(),
						PlayTime);
			} else {
				log.info("真人开始下注······");
				super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.AUTO.toString(), 15);
			}
		}
	}

	/**
	 * @Description :用于前台控制是否可以下注，每一个人的下注时间权限控制
	 * @author 老王
	 * 
	 */
	private void changeStatue(String userid) {
		GameStatus gs = new GameStatus();
		gs.setGamestatus(BMDataContext.GameStatusEnum.BET.toString());
		gs.setGamestatusMessage("请开始下注···");
		gs.setUserid(userid);
		gs.setTime(System.currentTimeMillis());
		CacheHelper.getGameStatusCacheBean().put(gameRoom.getId(), gs, orgi);
		ActionTaskUtils.sendEvent(gs.getCommand(), gs, gameRoom);
	}

	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public PokerAutoTask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000;
	}

}
