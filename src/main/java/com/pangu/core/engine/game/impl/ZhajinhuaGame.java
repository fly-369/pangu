package com.pangu.core.engine.game.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.iface.ChessGame;
import com.pangu.core.engine.game.model.PlayerRecordJinhua;
import com.pangu.util.GameUtils;
import com.pangu.util.rules.model.Board;
import com.pangu.util.rules.model.Player;
import com.pangu.util.rules.model.ZhajinhuaBoard;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Description:金花发牌,生成庄家
 *
 * @author abo
 * @date 2018年3月26日
 */
@Slf4j
public class ZhajinhuaGame implements ChessGame {

	@Override
	public Board process(List<PlayUserClient> playUsers, GameRoom gameRoom, GamePlayway playway, String banker,
			int cardsnum) {
		log.info("============执行金花发牌，定庄家==================");
		gameRoom.setCurrentnum(gameRoom.getCurrentnum() + 1);
		Board board = new ZhajinhuaBoard();
		board.setCards(null);
		List<Byte> temp = new ArrayList<Byte>();
		// 不要大小王
		for (int i = 0; i < 52; i++) {
			temp.add((byte) i);
		}
		/**
		 * 洗牌次数，参数指定，建议洗牌次数 为1次，多次洗牌的随机效果更好，例如：7次
		 */
		for (int i = 0; i < playway.getShuffletimes() + 1; i++) {
			Collections.shuffle(temp);
		}
		byte[] cards = new byte[54];
		for (int i = 0; i < temp.size(); i++) {
			cards[i] = temp.get(i);
		}
		board.setCards(cards);

		board.setRatio(1); // 默认倍率 1
//		int random = playUsers.size() * gameRoom.getCardsnum();
		// 金花这个字段放的当前房间的基础分数
		board.setPosition(playway.getScore()); 

		Player[] players = new Player[playUsers.size()];

		int inx = 0;
		for (PlayUserClient playUser : playUsers) {
			Player player = new Player(playUser.getId());
			player.setCards(new byte[cardsnum]);
			players[inx++] = player;
		}
		// 随机发牌
		for (int i = 0; i < gameRoom.getCardsnum() * gameRoom.getPlayers(); i++) {
			int pos = i % players.length;
			players[pos].getCards()[i / players.length] = cards[i];
		}
		// 排序
		for (Player tempPlayer : players) {
			Arrays.sort(tempPlayer.getCards());
			tempPlayer.setCards(GameUtils.reverseCards(tempPlayer.getCards()));
		}
		board.setRoom(gameRoom.getId());
		// 随机庄家
		int r = randomBanker(players.length);
		Player tempbanker = players[r];
		tempbanker.setBanker(true);
		players[r] = tempbanker;
		if (tempbanker != null) {
			board.setBanker(tempbanker.getPlayuser());
		}
		players[r].setBanker(true);
		
		//重新根据庄家进行排序，庄家排第一
		players = BMDataContext.getZhajinhuaEngine().playerSortByBanker(players);
		board.setPlayers(players);

		for (PlayUserClient playUser : playUsers) {
			// 完成进房间下注
			// 获取玩法，拿到最低下注额
			int score = playway.getScore();
			// 把钱先扣掉
			playUser.setGoldcoins(playUser.getGoldcoins() - score);
			// 下注日志
			PlayerRecordJinhua pr = new PlayerRecordJinhua();
			pr.setRoomId(gameRoom.getId());
			pr.setOrgi(BMDataContext.SYSTEM_ORGI);
			pr.setPlayerId(playUser.getId());
			pr.setCoin(score);
			pr.setPlayerType(playUser.getPlayertype().equals(BMDataContext.BEIMI_GAME_AI) ? "2" : "1");
			// 下注以后把下注内容放到缓存，等会去结算
			List<PlayerRecordJinhua> prList = new ArrayList<>();
			prList.add(pr);
			CacheHelper.getGameRecordJinhuaCacheBean().put(playUser.getId(), prList, gameRoom.getOrgi());

			// 设置庄家
			if (playUser.getId().equals(tempbanker.getPlayuser())) {
				playUser.setBlank(true);
			}else {
				playUser.setBlank(false);
			}
			// 放入缓存
			CacheHelper.getGamePlayerCacheBean().put(playUser.getId(), playUser, gameRoom.getOrgi());

		}

		return board;
	}

	/**
	 * 
	 * Description: 根据传入的用户总数，返回随机数来判断谁当庄，比如传入4人，返回0-3的index
	 * 
	 * @author abo
	 * @date 2018年4月17日
	 * @param playerNum
	 * @return
	 */
	public static int randomBanker(int playerNum) {
		int s = (int) (Math.random() * playerNum);
		return s;
	}

}
