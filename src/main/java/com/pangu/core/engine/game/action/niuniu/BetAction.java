package com.pangu.core.engine.game.action.niuniu;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.niuniu.NiuniuAiBetTask;
import com.pangu.core.engine.game.task.niuniu.NiuniuBetTask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.util.UKTools;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;
/**
  * @ClassName: BetAction
  * @Description:  处理下注业务
  * @author 老王
  * @date 2018年3月31日 下午2:54:18
  *
  * @param <T>
  * @param <S>
  */
@Slf4j
public class BetAction <T,S> implements Action<T, S> {

	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		log.info("等待玩家下注的·············");
		String room = (String) message.getMessageHeaders().getHeaders().get("room");
		GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room,
				BMDataContext.SYSTEM_ORGI);
		//执行下注的方法
		CacheHelper.getExpireCache().put(gameRoom.getRoomid(),
				new NiuniuBetTask(0, gameRoom, gameRoom.getOrgi()));
		//执行AI下注模拟的方法
		CacheHelper.getExpireCache().put(UKTools.getUUID(),
				new NiuniuAiBetTask(2, gameRoom, gameRoom.getOrgi()));
		
	}

}
