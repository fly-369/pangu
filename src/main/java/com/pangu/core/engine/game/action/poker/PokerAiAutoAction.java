package com.pangu.core.engine.game.action.poker;

import org.apache.commons.lang3.StringUtils;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.task.poker.PokerAiAutoTask;
import com.pangu.core.statemachine.action.Action;
import com.pangu.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.pangu.core.statemachine.message.Message;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PokerAiAutoAction<T, S> implements Action<T, S> {

	@Override
	public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
		log.info("ai自动打牌·················");
		
		String room = (String) message.getMessageHeaders().getHeaders().get("room");
		if (!StringUtils.isBlank(room)) {
			GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room,
					BMDataContext.SYSTEM_ORGI);
			if (gameRoom != null) {
				int interval = (int) message.getMessageHeaders().getHeaders().get("interval");
				CacheHelper.getExpireCache().put(gameRoom.getRoomid(),
						new PokerAiAutoTask(interval, gameRoom, gameRoom.getOrgi()));
			}
		}

	}

}
