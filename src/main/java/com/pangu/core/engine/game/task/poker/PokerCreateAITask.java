package com.pangu.core.engine.game.task.poker;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheConfigTools;
import com.pangu.core.engine.game.ActionTaskUtils;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.ChessGameEngine;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.SysGameConfig;

/**
 * @ClassName: PokerCreateAITask
 * @Description: 德州创建AI
 * @author 老王
 * @date 2018年5月18日 下午8:00:18
 *
 */
public class PokerCreateAITask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

	public void execute() {
		SysGameConfig gameConfig = CacheConfigTools.getSysGameConfig(gameRoom.getOrgi());
		if (gameConfig.isEnableai()) {
			// @ FIXME 具体ai个数的策略怎么控制,牛牛的策略也可以动态控制
			BMDataContext.getContext().getBean(ChessGameEngine.class).createAI(BMDataContext.GAME_POKER_AI_COUNT,
					gameRoom.getId(), gameRoom.getOrgi());
		}
		ActionTaskUtils.roomReady(gameRoom, super.getGame(gameRoom.getPlayway(), orgi));
		super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.JOIN.toString(), 0);
	}

	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public PokerCreateAITask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000; // 5秒后执行
	}
}
