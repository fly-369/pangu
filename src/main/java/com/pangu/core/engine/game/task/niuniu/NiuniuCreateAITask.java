package com.pangu.core.engine.game.task.niuniu;

import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheConfigTools;
import com.pangu.core.engine.game.ActionTaskUtils;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.BeiMiGameTask;
import com.pangu.core.engine.game.ChessGameEngine;
import com.pangu.core.engine.game.task.AbstractTask;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.SysGameConfig;

/**
 * @ClassName: NiuniuCreateAITask
 * @Description:  创建AI
 * @author 老王
 * @date 2018年5月15日 下午8:00:55
 *
 */

public class NiuniuCreateAITask extends AbstractTask implements ValueWithExpiryTime, BeiMiGameTask {

	public void execute() {
		// 是否启用ai
		SysGameConfig gameConfig = CacheConfigTools.getSysGameConfig(gameRoom.getOrgi());
		if (gameConfig.isEnableai()) {
			// @ FIXME ai个数具体策略的控制
			BMDataContext.getContext().getBean(ChessGameEngine.class).createAI(BMDataContext.GAME_NIUNIU_AI_COUNT, gameRoom.getId(), gameRoom.getOrgi());
		}
		ActionTaskUtils.roomReady(gameRoom, super.getGame(gameRoom.getPlayway(), orgi));
		super.getGame(gameRoom.getPlayway(), orgi).change(gameRoom, BeiMiGameEvent.JOIN.toString(), 0);
	}
	
	private long timer;
	private GameRoom gameRoom = null;
	private String orgi;

	public NiuniuCreateAITask(long timer, GameRoom gameRoom, String orgi) {
		super();
		this.timer = timer;
		this.gameRoom = gameRoom;
		this.orgi = orgi;
	}

	@Override
	public long getCacheExpiryTime() {
		return System.currentTimeMillis() + timer * 1000;
	}

}
