package com.pangu.core;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;

import com.pangu.core.engine.game.GameEngine;
import com.pangu.core.engine.game.NiuniuEngine;
import com.pangu.core.engine.game.PokerEngine;
import com.pangu.core.engine.game.RedBlackGameEngine;
import com.pangu.core.engine.game.SangongGameEngine;
import com.pangu.core.engine.game.ZhajinhuaEngine;

public class BMDataContext {
	
	public static final String USER_SESSION_NAME = "user";
	public static final String GUEST_USER = "guest";
	public static final String IM_USER_SESSION_NAME = "im_user";
	public static final String GUEST_USER_ID_CODE = "BEIMIGUESTUSEKEY" ;
	public static final String SERVICE_QUENE_NULL_STR = "service_quene_null" ;
	public static final String DEFAULT_TYPE = "default"	;		//默认分类代码
	public static final String BEIMI_SYSTEM_DIC = "com.dic.system.template";
	public static final String BEIMI_SYSTEM_GAME_TYPE_DIC = "com.dic.game.type";
	public static final String BEIMI_SYSTEM_GAME_WELFARETYPE_DIC = "com.dic.game.welfare.type";
	public static final String BEIMI_SHOP_WARES_TYPE_DIC = "com.dic.shop.warestype";
	public static final String BEIMI_SYSTEM_GAME_SCENE_DIC = "com.dic.scene.item";
	public static final String BEIMI_SYSTEM_GAME_CLIENTADDR= "com.dic.game.clientaddr";
	

	public static final String BEIMI_SYSTEM_GAME_CARDTYPE_DIC = "com.dic.game.dizhu.cardtype";

	public static final String BEIMI_SYSTEM_GAME_ROOMTITLE_DIC = "com.dic.game.room.title";
	
	public static final String BEIMI_MESSAGE_EVENT = "command" ;
	
	public static final String BEIMI_TEST_EVENT = "test" ;//测试所使用。后期删了吧！
	public static final int GAME_BET_TIME = 15 ;//牛牛下注等待时间
	public static final int GAME_WAIT_TIME = 10 ;//牛牛以及德州等待时间by:laowang
	public static final int GAME_NEXT_TIME = 10 ;//牛牛以及德州下一局等待时间by:laowang
	public static final int GAME_NIUNIU_AI_COIN = 10000 ;//牛牛创建房间ai门坎
	public static final int GAME_NIUNIU_AI_COUNT = 8 ;//牛牛创建房间ai个数，若真人数大于此数，可以不设置ai
	public static final int GAME_POKER_AI_COUNT = 6;//德州创建房间ai个数，若真人数大于此数，可以不设置ai
	public static final int GAME_NIUNIU_AI_MIN_COIN = 50 ;//ai拥有最小金币数

	public static final int GAME_POKER_PLAYERS = 9 ;//ai拥有最小金币数

	public static final int GAME_AI_MIN_COIN = 50;	//AI最小下线金币数
	public static final int GAME_AI_MAX_COIN = 20000;//AI最大下线金币数
	public static final int GAME_PLAYING_WAIT_TIME = 3;//开始游戏前的等待时间

	
	public static final String BEIMI_PLAYERS_EVENT = "players" ;
	
	public static final String BEIMI_BETSTATUS_EVENT = "betstatus" ;
	public static final String BEIMI_BETRECORD_EVENT = "betrecord" ;
	
	public static final String BEIMI_GAMESTATUS_EVENT = "gamestatus" ;
	
	public static final String BEIMI_SETTLEMENT_EVENT = "settlement" ;//结算
	
	public static final String BEIMI_SEARCHROOM_EVENT = "searchroom" ;
	
	public static final String BEIMI_PUSHNOTICE_EVENT = "pushnotice";
	
	public static final String BEIMI_PUSHBROADCAST_EVENT = "broadcast";
	
	public static final String BEIMI_GAME_PLAYWAY = "game_playway";
	
	public static final String BEIMI_GAME_SHOP_WARES = "game_shop_wares";
	
	public static final String BEIMI_SYSTEM_AUTH_DIC = "com.dic.auth.resource";

	public static final String BEIMI_SYSTEM_ROOM = "room" ;
	
	public static final String BEIMI_GAME_TOP = "game_top" ;//游戏排行榜
	
	public static final String BEIMI_GAME_SEAT = "game_seat" ;//德州座位
	
	public static final String BEIMI_GAME_AI = "ai" ;//AI
	public static final String BEIMI_GAME_NORMAL = "normal" ;//真人

	
	public static final String BEIMI_GAME_LASTHANDS="lasthands";//发牌
	
	public static final String BEIMI_NIUNIU_BLANKER = "niuniu_blanker" ;
	
	public static final String PLAYUSERCLIENT = "playUserClient";
	public static final String CLIENTGAMECONFIG = "clientGameConfig";
	public static final String AUTHORIZATION = "authorization";
	
	public static final int JINHUA_ZERO_TIME = 0;//金花AI操作时间
	public static final int JINHUA_AI_GAME_TIME = 10;//金花AI操作时间
	public static final int JINHUA_NORMAL_GAME_TIME = 15;//金花真人操作时间
	public static final int RESET_WAIT_TIME = 10;//下局开始等待时间
	//金花看牌概率
	public static final int JINHUA_LOOKCARD_ODDS = 80;//当前局面是否有人看牌并且继续跟注，那么此时ai有80%的几率看牌
	public static final int JINHUA_LOOKCARD_ODDS1 = 30;//当前局面都没人看牌，那么ai有30%的几率看牌

	public static String SYSTEM_ORGI = "beimi" ;
	
	private static int WebIMPort = 9081 ;
	
	private static boolean imServerRunning = false ;			//IM服务状态
	
	private static ApplicationContext applicationContext ;

	public static Map<String , Boolean> model = new HashMap<String,Boolean>();
	
	public static double BEIMI_BROADCAST_VALUE = 100;//当赢得金额大于此金额的时候保存在广播表，并进行广播
	
	private static GameEngine gameEngine ;
	
	private static NiuniuEngine niuniuEngine ;
	
	private static RedBlackGameEngine redBlackGameEngine;
	
	private static PokerEngine pokerEngine;
	//炸金花游戏引擎
	private static ZhajinhuaEngine zhajinhuaEngine;
	
	public static ZhajinhuaEngine getZhajinhuaEngine() {
		return zhajinhuaEngine;
	}

	public static void setZhajinhuaEngine(ZhajinhuaEngine zhajinhuaEngine) {
		BMDataContext.zhajinhuaEngine = zhajinhuaEngine;
	}

	public static PokerEngine getPokerEngine() {
		return pokerEngine;
	}

	public static void setPokerEngine(PokerEngine pokerEngine) {
		BMDataContext.pokerEngine = pokerEngine;
	}

	public static RedBlackGameEngine getRedBlackGameEngine() {
		return redBlackGameEngine;
	}

	public static void setRedBlackGameEngine(RedBlackGameEngine redBlackGameEngine) {
		BMDataContext.redBlackGameEngine = redBlackGameEngine;
	}

	public static NiuniuEngine getNiuniuEngine() {
		return niuniuEngine;
	}

	public static void setNiuniuEngine(NiuniuEngine niuniuEngine) {
		BMDataContext.niuniuEngine = niuniuEngine;
	}
	
	private static SangongGameEngine sangongGameEngine;
	
	public static SangongGameEngine getSangongGameEngine() {
		return sangongGameEngine;
	}

	public static void setSangongGameEngine(SangongGameEngine sangongGameEngine) {
		BMDataContext.sangongGameEngine = sangongGameEngine;
	}

	public static int getWebIMPort() {
		return WebIMPort;
	}

	public static void setWebIMPort(int webIMPort) {
		WebIMPort = webIMPort;
	}
	
	public static void setApplicationContext(ApplicationContext context){
		applicationContext = context ;
	}
	
	public static void setGameEngine(GameEngine engine){
		gameEngine = engine ;
	}
	
	/**
	 * 根据ORGI找到对应 游戏配置
	 * @param orgi
	 * @return
	 */
	public static String getGameAccountConfig(String orgi){
		return BMDataContext.ConfigNames.ACCOUNTCONFIG.toString()+"_"+orgi ;
	}
	
	/**
	 * 根据ORGI找到对应 游戏配置
	 * @param orgi
	 * @return
	 */
	public static String getGameConfig(String orgi){
		return BMDataContext.ConfigNames.GAMECONFIG.toString()+"_"+orgi ;
	}
	
	/**
	 * 根据ORGI找到对应 游戏配置
	 * @param orgi
	 * @return
	 */
	public static String getGameAiConfig(String orgi){
		return BMDataContext.ConfigNames.AICONFIG.toString()+"_"+orgi ;
	}
	
	/**
	 * 根据ORGI找到对应 游戏配置
	 * @param orgi
	 * @return
	 */
	public static String getSysGameConfig(String orgi){
		return BMDataContext.ConfigNames.SYSGAMECONFIG.toString()+"_"+orgi ;
	}
	
	
	public static ApplicationContext getContext(){
		return applicationContext ;
	}
	
	public static GameEngine getGameEngine(){
		return gameEngine; 
	}
	/**
	 * 系统级的加密密码 ， 从CA获取
	 * @return
	 */
	public static String getSystemSecrityPassword(){
		return "BEIMI" ;
	}
	
	public enum NameSpaceEnum{
		
		SYSTEM("/bm/system") ,
		GAME("/bm/game"),
		CHESS("/bm/chess");
		
		private String namespace ;
		
		public String getNamespace() {
			return namespace;
		}

		public void setNamespace(String namespace) {
			this.namespace = namespace;
		}

		NameSpaceEnum(String namespace){
			this.namespace = namespace ;
		}
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum ModelType{
		ROOM,
		HALL;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum ActRecordType{
		SUBSIDY,	//补贴
		TURN,		//转盘
		SIGN,		//签到
		LOGIN;		//连续登录
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	
	public enum ConfigNames{
		GAMECONFIG,
		AICONFIG,
		ACCOUNTCONFIG,
		SYSGAMECONFIG,
		PLAYWAYCONFIG,
		PLAYWAYGROUP,
		PLAYWAYGROUPITEM;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum UserDataEventType{
		SAVE,UPDATE,DELETE;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum PlayerAction{
		GANG,
		PENG,
		HU,
		CHI,
		GUO;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum CommandMessageType{
		SUBSIDY,
		SUBSIDYFAILD,
		PVACHANGE;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum PlayerPokerAction{
		ALLIN,		//allin
		GUO,			//过牌
		GEN,			//过牌
		JIABEI;		//加倍
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum PlayerGangAction{
		MING,		//明杠
		AN,			//暗杠
		WAN;		//弯杠
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum GameTypeEnum{
		MAJIANG,
		DIZHU, //地主
		NIUNIU, //牛牛
		JINHUA, //金花
		HONGHEI, //红黑
		SANGONG, //三公
		DEZHOU;  //德州
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum PlayerTypeEnum{
		AI,			//AI
		NORMAL,		//普通注册玩家
		OFFLINE,	//托管玩家
		LEAVE;		//离开房间的玩家
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum GameStatusEnum{
		READY,			//AI
		NOTREADY,		//普通玩家
		BET,
		BETOVER,
		MANAGED,
		PLAYING,
		END,//结束
		TIMEOUT;		//登录会话过期
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum CardsTypeEnum{
		ONE(1),		//单张      3~K,A,2
		TWO(2),		//一对	 3~K,A,2
		THREE(3),	//三张	 3~K,A,2
		FOUR(4),	//三带一	 AAA+K
		FORMTWO(41),	//三带对	 AAA+K
		FIVE(5),	//单顺	连子		10JQKA
		SIX(6),		//双顺	连对		JJQQKK
		SEVEN(7),	//三顺	飞机		JJJQQQ
		EIGHT(8),	//飞机	带翅膀	JJJ+QQQ+K+A
		EIGHTONE(81),	//飞机	带翅膀	JJJ+QQQ+KK+AA
		NINE(9),	//四带二			JJJJ+Q+K
		NINEONE(91),	//四带二对			JJJJ+QQ+KK
		TEN(10),	//炸弹			JJJJ
		ELEVEN(11);	//王炸			0+0
		
		private int type ;
		
		CardsTypeEnum(int type){
			this.type = type ;
		} 
		

		public int getType() {
			return type;
		}


		public void setType(int type) {
			this.type = type;
		}
	}
	
	//单张弃牌率90%，对子40%，顺子30%，金花20%，顺金10%，豹子0%
	public enum ZhajinhuaCardsTypeOddoEnum{
		BOMB(0),		//豹子0%的弃牌率
		ShunJin(10),	//顺金10%的弃牌率
		JinHua(20),		//金花20%的弃牌率
		ShunZi(30),		//顺子30%的弃牌率
		DuiZi(40),		//对子40%的弃牌率
		Dan(90);		//单张90%的弃牌率
		private int type ;
		ZhajinhuaCardsTypeOddoEnum(int type){
			this.type = type ;
		} 
		public int getType() {
			return type;
		}
		public void setType(int type) {
			this.type = type;
		}
	}
	
	//6、豹子5、顺金4、金花，3、顺子，2、对子，1、单张
	public enum ZhajinhuaCardsTypeEnum{
		BOMB(6),		//豹子
		ShunJin(5),		//顺金
		JinHua(4),		//金花
		ShunZi(3),		//顺子
		DuiZi(2),		//对子
		Dan(1);			//单张
		private int type ;
		ZhajinhuaCardsTypeEnum(int type){
			this.type = type ;
		} 
		public int getType() {
			return type;
		}
		public void setType(int type) {
			this.type = type;
		}
	}
	
	public enum MessageTypeEnum{
		JOINROOM,
		MESSAGE, 
		END,
		TRANS, STATUS , AGENTSTATUS , SERVICE, WRITING;
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum SearchRoomResultType{
		NOTEXIST,  //房间不存在
		FULL, 		//房间已满员
		OK,			//加入成功
		DISABLE,	//房间启用了 禁止非邀请加入
		INVALID;	//房主已离开房间
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum MaJiangWinType{
		TUI,
		RIVER,
		END,
		LOST;
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum PVActionEnum{
		INCOME,	//
		CONSUME,
		EXCHANGE,
		VERIFY;
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum PVAStatusEnum{
		OK,
		NOTENOUGH,
		FAILD,
		NOTEXIST,
		INVALID;
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	/**
	 * 收入类型 ， 1、充值，2、兑换、3、赢了，4、赠送，5、抽奖，6、接受赠与
	 */
	public enum PVAInComeActionEnum{
		RECHARGE,
		EXCHANGE,
		WIN,
		WELFARE,
		PRIZE,
		GIFT;
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	/**
	 *  支出 1、输了，2、逃跑扣除、3、兑换扣除，4、送好友
	 */
	public enum PVAConsumeActionEnum{
		LOST,
		ESCAPE,
		DEDUCTION,
		SEND,
		SUBSIDY;
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum OrderIdType{
		REGISTER,    //注册
		TRANSTER;	 //转账
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}


	public static void setIMServerStatus(boolean running){
		imServerRunning = running ;
	}
	public static boolean getIMServerStatus(){
		return imServerRunning;
	}
	
}
