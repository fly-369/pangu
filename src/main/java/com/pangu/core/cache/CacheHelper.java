package com.pangu.core.cache;

import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.cache2k.CacheEntry;
import org.cache2k.event.CacheEntryExpiredListener;
import org.cache2k.expiry.ExpiryPolicy;
import org.cache2k.expiry.ValueWithExpiryTime;

import com.pangu.core.cache.hazelcast.CacheBean;
import com.pangu.core.cache.hazelcast.HazlcastCacheHelper;
import com.pangu.core.cache.hazelcast.PlayerCacheBean;
import com.pangu.core.cache.hazelcast.impl.QueneCache;
import com.pangu.core.cache.hazelcast.impl.QuenePlayerCache;
import com.pangu.core.engine.game.BeiMiGameTask;

public class CacheHelper {
	private static CacheHelper instance = new CacheHelper();
	private final Cache<String,ValueWithExpiryTime> expireCache ;
	public CacheHelper(){
		expireCache = new Cache2kBuilder<String, ValueWithExpiryTime>() {}
			.sharpExpiry(true)
			.eternal(false)
			.expiryPolicy(new ExpiryPolicy<String, ValueWithExpiryTime>() {
				@Override
				public long calculateExpiryTime(String key, ValueWithExpiryTime value,
						long loadTime, CacheEntry<String, ValueWithExpiryTime> oldEntry) {
					return value.getCacheExpiryTime();
				}
			})
		    .addListener(new CacheEntryExpiredListener<String, ValueWithExpiryTime>() {
				@Override
				public void onEntryExpired(Cache<String, ValueWithExpiryTime> cache,
						CacheEntry<String, ValueWithExpiryTime> task) {
					/**
					 * 
					 */
					((BeiMiGameTask)task.getValue()).execute();
				}
			})
	    .build();
	}
	
	/**
	 * 获取缓存实例
	 */
	public static CacheHelper getInstance(){
		return instance ;
	}
	private static CacheInstance cacheInstance = new HazlcastCacheHelper();
	
	public static CacheBean getRoomMappingCacheBean() {
		return cacheInstance!=null ? cacheInstance.getOnlineCacheBean() : null;
	}
	public static CacheBean getSystemCacheBean() {
		return cacheInstance!=null ? cacheInstance.getSystemCacheBean() : null ;
	}
	
	public static CacheBean getGameRoomCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameRoomCacheBean() : null ;
	}
	/*
	 *缓存排行榜数据容器 
	 * */
	public static CacheBean getGamePlayerTopCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGamePlayerTopCacheBean() : null ;
	}
	
	/**
	  *
	  * @Title: getGamePlayerTopCacheBean
	  * @Description:  获取到游戏状态缓存
	  * @param @return    设定文件
	  * @return CacheBean    返回类型
	  * @throws
	 */
	public static CacheBean getGameStatusCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameStatusCacheBean() : null ;
	}
	
	public static CacheBean getGameBonusPoolCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameBonusPoolCacheBean() : null ;
	}
	
	public static PlayerCacheBean getGamePlayerCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGamePlayerCacheBean() : null ;
	}
	
	public static CacheBean getGameRecordCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameRecordCacheBean() : null ;
	}
	public static CacheBean getGameRecordNiuNiuCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameRecordNiuNiuCacheBean() : null ;
	}
	public static CacheBean getGameRecordDeZhouCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameRecordDeZhouCacheBean() : null ;
	}
	
	public static CacheBean getGameRecordJinhuaCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameRecordJinhuaCacheBean() : null ;
	}
	
	public static CacheBean getGameRecordHongheiCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameRecordHongheiCacheBean() : null ;
	}
	
	public static CacheBean getApiUserCacheBean() {
		return cacheInstance!=null ? cacheInstance.getApiUserCacheBean() : null ;
	}
	/**
	 * 存放游戏数据的 ，Board
	 * @return
	 */
	public static CacheBean getBoardCacheBean() {
		return cacheInstance!=null ? cacheInstance.getGameCacheBean() : null ;
	}
	
	public static QueneCache getQueneCache(){
		return cacheInstance!=null ? cacheInstance.getQueneCache() : null ;
	}
	
	public static QuenePlayerCache getQuenePlayerCache(){
		return cacheInstance!=null ? cacheInstance.getQuenePlayerCache() : null ;
	}
	
	public static Cache<String,ValueWithExpiryTime> getExpireCache(){
	    return instance.expireCache;
	}
	
	public static CacheBean getGameHistoryBoardCache(){
		return cacheInstance!=null ? cacheInstance.getGameHistoryBoardCache() : null ;
	}
}
