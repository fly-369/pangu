package com.pangu.core.cache;

import java.util.List;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.web.model.AccountConfig;
import com.pangu.web.model.AiConfig;
import com.pangu.web.model.GameConfig;
import com.pangu.web.model.SysGameConfig;
import com.pangu.web.service.repository.jpa.AccountConfigRepository;
import com.pangu.web.service.repository.jpa.AiConfigRepository;
import com.pangu.web.service.repository.jpa.GameConfigRepository;
import com.pangu.web.service.repository.jpa.SysGameConfigRepository;

/**
 * 用于获取缓存配置
 * @author iceworld
 *
 */
public class CacheConfigTools {
	/*public static AccountConfig getGameAccountConfig(String orgi){
		AccountConfig config = (AccountConfig) CacheHelper.getSystemCacheBean().getCacheObject(BMDataContext.getGameAccountConfig(orgi), orgi) ;
		if(config == null){
			AccountConfigRepository accountRes = BMDataContext.getContext().getBean(AccountConfigRepository.class) ;
			List<AccountConfig> gameAccountList = accountRes.findByOrgi(orgi) ;
			if(gameAccountList!=null && gameAccountList.size() >0){
				config = gameAccountList.get(0) ;
			}else{
				config = new AccountConfig() ;
			}
			CacheHelper.getSystemCacheBean().put(BMDataContext.getGameAccountConfig(orgi), config, orgi);
		}
		return config;
	}
	
	public static GameConfig getGameConfig(String orgi){
		GameConfig config = (GameConfig) CacheHelper.getSystemCacheBean().getCacheObject(BMDataContext.getGameConfig(orgi), orgi) ;
		if(config == null){
			GameConfigRepository gameConfigRes = BMDataContext.getContext().getBean(GameConfigRepository.class) ;
			List<GameConfig> gameConfigList = gameConfigRes.findByOrgi(orgi) ;
			if(gameConfigList!=null && gameConfigList.size() >0){
				config = gameConfigList.get(0) ;
			}else{
				config = new GameConfig() ;
			}
			CacheHelper.getSystemCacheBean().put(BMDataContext.getGameConfig(orgi), config, orgi);
		}
		return config;
	}
	
	public static AiConfig getAiConfig(String orgi){
		AiConfig config = (AiConfig) CacheHelper.getSystemCacheBean().getCacheObject(BMDataContext.getGameAiConfig(orgi), orgi) ;
		if(config == null){
			AiConfigRepository aiConfigRes = BMDataContext.getContext().getBean(AiConfigRepository.class) ;
			List<AiConfig> gameAccountList = aiConfigRes.findByOrgi(orgi) ;
			if(gameAccountList!=null && gameAccountList.size() >0){
				config = gameAccountList.get(0) ;
			}else{
				config = new AiConfig() ;
			}
			CacheHelper.getSystemCacheBean().put(BMDataContext.getGameAiConfig(orgi), config, orgi);
		}
		return config;
	}*/
	
	/**
	* @Description: 取系统游戏设置 
	* @return GameConfig  
	* @date 2018年4月20日 下午2:50:44   
	 */
	public static SysGameConfig getSysGameConfig(String orgi){
		SysGameConfig sysgameconfig = (SysGameConfig) CacheHelper.getSystemCacheBean().getCacheObject(BMDataContext.getSysGameConfig(orgi), orgi) ;
		if(sysgameconfig == null){
			SysGameConfigRepository sysgameconfigrepository = BMDataContext.getContext().getBean(SysGameConfigRepository.class) ;
			List<SysGameConfig> gameconfiglist = sysgameconfigrepository.findByOrgi(orgi) ;
			if(gameconfiglist!=null && gameconfiglist.size() >0){
				sysgameconfig = gameconfiglist.get(0) ;
			}else{
				sysgameconfig = new SysGameConfig() ;
			}
			CacheHelper.getSystemCacheBean().put(BMDataContext.getSysGameConfig(orgi), sysgameconfig, orgi);
		}
		return sysgameconfig;
	}
}
