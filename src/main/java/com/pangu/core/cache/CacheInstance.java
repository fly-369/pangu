package com.pangu.core.cache;

import com.pangu.core.cache.hazelcast.CacheBean;
import com.pangu.core.cache.hazelcast.PlayerCacheBean;
import com.pangu.core.cache.hazelcast.impl.QueneCache;
import com.pangu.core.cache.hazelcast.impl.QuenePlayerCache;


public interface CacheInstance {
	
	/**
	 * 游戏记录
	 * @return
	 */
	public CacheBean getGameRecordCacheBean();
	/**
	 * 牛牛记录
	 * @return
	 */
	public CacheBean getGameRecordNiuNiuCacheBean();
	/**
	 * 德州记录
	 * @return
	 */
	public CacheBean getGameRecordDeZhouCacheBean();
	
	/**
	 * 在线用户
	 * @return
	 */
	public CacheBean getOnlineCacheBean();
	
	/**
	 * 系统缓存
	 * @return
	 */
	public CacheBean getSystemCacheBean();
	
	
	/**
	 * 游戏房间
	 * @return
	 */
	public CacheBean getGameRoomCacheBean();
	
	/**
	 * 游戏数据
	 * @return
	 */
	public CacheBean getGameCacheBean();
	
	
	/**
	 * IMR指令
	 * @return
	 */
	public CacheBean getApiUserCacheBean();
	
	/**
	 * 分布式队列
	 * @return
	 */
	
	public QueneCache getQueneCache();
	
	/**
	 * 分布式队列--存放用户消息
	 * @return
	 */
	
	public QuenePlayerCache getQuenePlayerCache();
	
	

	public PlayerCacheBean getGamePlayerCacheBean();
	/**
	 * 获取排行数据
	 * @return
	 */
	public CacheBean getGamePlayerTopCacheBean(); 
	
	/**
	 * 游戏状态记录
	 * @return
	 */
	public CacheBean getGameStatusCacheBean();
	
	/**
	 * 奖金池
	 * @return
	 */
	public CacheBean getGameBonusPoolCacheBean();	
	
	
	
	
	/**
	 * 金花记录
	 * @return
	 */
	public CacheBean getGameRecordJinhuaCacheBean();
	
	/**
	 * 红黑记录
	 * @return
	 */
	public CacheBean getGameRecordHongheiCacheBean();
	
	/**
	 * 
	 * Description:  牌局历史记录
	 * 
	 * @author abo
	 * @date 2018年5月17日  
	 * @return
	 */
	public CacheBean getGameHistoryBoardCache();
	
}