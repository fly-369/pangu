package com.pangu.core.cache.hazelcast;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheInstance;
import com.pangu.core.cache.hazelcast.impl.ApiUserCache;
import com.pangu.core.cache.hazelcast.impl.GameBonusPoolCache;
import com.pangu.core.cache.hazelcast.impl.GameCache;
import com.pangu.core.cache.hazelcast.impl.GameHistoryBoardCache;
import com.pangu.core.cache.hazelcast.impl.GameRecordCache;
import com.pangu.core.cache.hazelcast.impl.GameRecordDeZhouCache;
import com.pangu.core.cache.hazelcast.impl.GameRecordHongheiCache;
import com.pangu.core.cache.hazelcast.impl.GameRecordJinhuaCache;
import com.pangu.core.cache.hazelcast.impl.GameRecordNiuNiuCache;
import com.pangu.core.cache.hazelcast.impl.GameRoomCache;
import com.pangu.core.cache.hazelcast.impl.GameStatusCache;
import com.pangu.core.cache.hazelcast.impl.OnlineCache;
import com.pangu.core.cache.hazelcast.impl.PlayerTopCach;
import com.pangu.core.cache.hazelcast.impl.QueneCache;
import com.pangu.core.cache.hazelcast.impl.QuenePlayerCache;
import com.pangu.core.cache.hazelcast.impl.SystemCache;

/**
 * Hazlcast缓存处理实例类
 * 
 * @author admin
 *
 */
public class HazlcastCacheHelper implements CacheInstance {
	/**
	 * 服务类型枚举
	 * 
	 * @author admin
	 *
	 */
	public enum CacheServiceEnum {
		HAZLCAST_CLUSTER_AGENT_USER_CACHE, HAZLCAST_CLUSTER_AGENT_STATUS_CACHE, HAZLCAST_CLUSTER_QUENE_USER_CACHE, HAZLCAST_ONLINE_CACHE, GAME_PLAYERS_CACHE, HAZLCAST_CULUSTER_SYSTEM, HAZLCAST_GAMEROOM_CACHE, API_USER_CACHE, QUENE_CACHE, HAZLCAST_TASK_CACHE, HAZLCAST_GAME_CACHE,
		HAZLCAST_GAME_RECORD_CACHE, HAZLCAST_GAME_RECORD_NIUNIU_CACHE,  HAZLCAST_GAME_RECORD_DEZHOU_CACHE, HAZLCAST_GAME_RECORD_JINHUA_CACHE,HAZLCAST_GAME_RECORD_HONGHEI_CACHE,GAME_HISTORY_BOARD_CACHE,
		HAZLCAST_GAME_PLAYER_TOP, HAZLCAST_GAME_STATUS, QUENE_PLAYER_CACHE, GAME_BONUSPOOL_CACHE;
		public String toString() {
			return super.toString().toLowerCase();
		}
	}

	@Override
	public CacheBean getOnlineCacheBean() {
		return BMDataContext.getContext().getBean(OnlineCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_ONLINE_CACHE.toString());
	}

	@Override
	public CacheBean getSystemCacheBean() {
		return BMDataContext.getContext().getBean(SystemCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_CULUSTER_SYSTEM.toString());
	}

	@Override
	public CacheBean getGameRoomCacheBean() {
		return BMDataContext.getContext().getBean(GameRoomCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAMEROOM_CACHE.toString());
	}
	
	@Override
	public CacheBean getGameStatusCacheBean() {
		return BMDataContext.getContext().getBean(GameStatusCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAME_STATUS.toString());
	}


	@Override
	public CacheBean getGameCacheBean() {
		return BMDataContext.getContext().getBean(GameCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAME_CACHE.toString());
	}

	@Override
	public CacheBean getApiUserCacheBean() {
		return BMDataContext.getContext().getBean(ApiUserCache.class)
				.getCacheInstance(CacheServiceEnum.API_USER_CACHE.toString());
	}

	@Override
	public QuenePlayerCache getQuenePlayerCache() {
		return BMDataContext.getContext().getBean(QuenePlayerCache.class)
				.getCacheInstance(CacheServiceEnum.QUENE_PLAYER_CACHE.toString());
	}
	
	@Override
	public QueneCache getQueneCache() {
		return BMDataContext.getContext().getBean(QueneCache.class)
				.getCacheInstance(CacheServiceEnum.QUENE_CACHE.toString());
	}

	@Override
	public PlayerCacheBean getGamePlayerCacheBean() {
		return BMDataContext.getContext().getBean(PlayerCacheBean.class)
				.getCacheInstance(CacheServiceEnum.GAME_PLAYERS_CACHE.toString());
	}

	@Override
	public CacheBean getGameRecordCacheBean() {
		return BMDataContext.getContext().getBean(GameRecordCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAME_RECORD_CACHE.toString());
	}
	@Override
	public CacheBean getGameRecordNiuNiuCacheBean() {
		return BMDataContext.getContext().getBean(GameRecordNiuNiuCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAME_RECORD_NIUNIU_CACHE.toString());
	}
	@Override
	public CacheBean getGameRecordDeZhouCacheBean() {
		return BMDataContext.getContext().getBean(GameRecordDeZhouCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAME_RECORD_DEZHOU_CACHE.toString());
	}

	@Override
	public CacheBean getGamePlayerTopCacheBean() {
		return BMDataContext.getContext().getBean(PlayerTopCach.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAME_PLAYER_TOP.toString());
	}
	
	@Override
	public CacheBean getGameRecordJinhuaCacheBean() {
		return BMDataContext.getContext().getBean(GameRecordJinhuaCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAME_RECORD_JINHUA_CACHE.toString());
	}
	
	@Override
	public CacheBean getGameRecordHongheiCacheBean() {
		return BMDataContext.getContext().getBean(GameRecordHongheiCache.class)
				.getCacheInstance(CacheServiceEnum.HAZLCAST_GAME_RECORD_HONGHEI_CACHE.toString());
	}

	@Override
	public CacheBean getGameBonusPoolCacheBean() {
		return BMDataContext.getContext().getBean(GameBonusPoolCache.class)
				.getCacheInstance(CacheServiceEnum.GAME_BONUSPOOL_CACHE.toString());
	}
	
	@Override
	public CacheBean getGameHistoryBoardCache() {
		return BMDataContext.getContext().getBean(GameHistoryBoardCache.class)
				.getCacheInstance(CacheServiceEnum.GAME_HISTORY_BOARD_CACHE.toString());
	}
}
