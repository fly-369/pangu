package com.pangu.core.cache.hazelcast.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.locks.Lock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.SqlPredicate;
import com.pangu.core.cache.hazelcast.CacheBean;
import com.pangu.util.rules.model.GameHistoryBoard;
import com.pangu.web.model.GameRoom;

/**
 * 
 * Description:历史牌局缓存信息
 *
 * @author abo
 * @date 2018年5月17日
 */
@Service("game_history_board_cache")
public class GameHistoryBoardCache implements CacheBean {

	@Autowired
	public HazelcastInstance hazelcastInstance;

	private String cacheName;

	public HazelcastInstance getInstance() {
		return hazelcastInstance;
	}

	public CacheBean getCacheInstance(String cacheName) {
		this.cacheName = cacheName;
		return this;
	}

	@Override
	public void put(String key, Object value, String orgi) {
		getInstance().getMap(getName()).put(key, value);
	}

	@Override
	public void clear(String orgi) {
		getInstance().getMap(getName()).clear();
	}

	@Override
	public Object delete(String key, String orgi) {
		return getInstance().getMap(getName()).remove(key);
	}

	@Override
	public void update(String key, String orgi, Object value) {
		getInstance().getMap(getName()).put(key, value);
	}

	@Override
	public Object getCacheObject(String key, String orgi) {
		// return getInstance().getMap(getName()).get(key);
		PagingPredicate<String, GameHistoryBoard> pagingPredicate = null;
		List boards = new ArrayList();
		pagingPredicate = new PagingPredicate<String, GameHistoryBoard>(new SqlPredicate(" roomId = '" + key + "'"), 100);
		boards.addAll((getInstance().getMap(getName())).values(pagingPredicate));
		return boards;
	}

	public String getName() {
		return cacheName;
	}

	// @Override
	public void service() throws Exception {

	}

	@Override
	public Collection<?> getAllCacheObject(String orgi) {
		return getInstance().getMap(getName()).keySet();
	}

	@Override
	public Object getCacheObject(String key, String orgi, Object defaultValue) {
		return getCacheObject(key, orgi);
	}

	@Override
	public Object getCache() {
		return getInstance().getMap(cacheName);
	}

	@Override
	public Lock getLock(String lock, String orgi) {
		return getInstance().getLock(lock);
	}

	@Override
	public long getSize() {
		return getInstance().getMap(getName()).size();
	}

	@Override
	public long getAtomicLong(String cacheName) {
		return getInstance().getAtomicLong(getName()).incrementAndGet();
	}

	@Override
	public void setAtomicLong(String cacheName, long start) {
		getInstance().getAtomicLong(getName()).set(start);
	}
}
