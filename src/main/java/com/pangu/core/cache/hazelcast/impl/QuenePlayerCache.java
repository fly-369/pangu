package com.pangu.core.cache.hazelcast.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.SqlPredicate;
import com.pangu.web.model.PlayUserClient;

@Service("quene_player_cache")
public class QuenePlayerCache{
	
	private String cacheName ;
	@Autowired
	public HazelcastInstance hazelcastInstance;	
	
	public HazelcastInstance getInstance(){
		return hazelcastInstance ;
	}
	public QuenePlayerCache getCacheInstance(String cacheName){
		this.cacheName = cacheName ;
		return this;
	}
	
	public String getName() {
		return cacheName ;
	}
	
	public void put(PlayUserClient value, String orgi){
		getInstance().getMap(this.getName()).put(value.getId() , value) ;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PlayUserClient poll( String orgi) {
		PlayUserClient player = null;
		/**
		 * 从Map里获取 
		 */
		PagingPredicate<String, PlayUserClient> pagingPredicate = null ;
		List playerList = new ArrayList();
			pagingPredicate = new PagingPredicate<String, PlayUserClient>(  new SqlPredicate(" goldcoins > 0") , 5 );
			playerList.addAll((getInstance().getMap(this.getName())).values(pagingPredicate) ) ;
			if(playerList!=null && playerList.size() > 0){
				player = (PlayUserClient) playerList.get(0);
				getInstance().getMap(this.getName()).delete(player.getId());
			}
		return player;
	}
	/*
	 * 获取到金额大于某一个值的ai
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public PlayUserClient poll( String orgi,int goldcoins) {
		PlayUserClient player = null;
		/**
		 * 从Map里获取 
		 */
		PagingPredicate<String, PlayUserClient> pagingPredicate = null ;
		List playerList = new ArrayList();
			pagingPredicate = new PagingPredicate<String, PlayUserClient>(  new SqlPredicate(" goldcoins >"+goldcoins) , 5 );
			playerList.addAll((getInstance().getMap(this.getName())).values(pagingPredicate) ) ;
			if(playerList!=null && playerList.size() > 0){
				player = (PlayUserClient) playerList.get(0);
				getInstance().getMap(this.getName()).delete(player.getId());
			}
		return player;
	}
	
	
	/*
	 * 删除缓存
	 */
	public void delete(String userid){
		getInstance().getMap(this.getName()).delete(userid);
	}
}
