package com.pangu.core.cache.hazelcast;

import java.util.Collection;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.Predicates;
import com.pangu.web.model.PlayUserClient;

/**
 * 
 * Description:hazelcast查询例子，Hazelcast为分布式查询提供以下API：1、Criteria API，2、分布式SQL查询，
 * 详见官方文档：http://docs.hazelcast.org/docs/latest-dev/manual/html-single/index.html#distributed-query
 *
 * @author abo
 * @date 2018年5月21日
 */
public class HazelcastQueryDemo {

	public static void main(String[] args) {

		HazelcastInstance instance = Hazelcast.newHazelcastInstance();
		// 创建Map
		IMap<String, PlayUserClient> clusterMap1 = instance.getMap("MyMap");
		for (int i = 0; i < 5; i++) {
			PlayUserClient p = new PlayUserClient();
			p.setId(i + "");
			p.setRoomid(i + "xxxxxx" + i);
			clusterMap1.put(p.getId(), p);

		}
		// 1.等于某字段时候进行查询
		// Predicate<String, PlayUserClient> idPredicate = Predicates.equal("roomid",
		// "xxxxxx3");
		// Collection<PlayUserClient> employees = clusterMap1.values(idPredicate);
		// System.out.println(employees.size());
		// for (PlayUserClient playUserClient : employees) {
		// System.out.println("id:" + playUserClient.getId());
		// System.out.println("roomid:" + playUserClient.getRoomid());
		// }

		// 2.多个某字段or时候进行查询
		// Predicate<String, PlayUserClient> idPredicate = Predicates.equal("id", "4");
		// Predicate<String, PlayUserClient> roomIdPredicate =
		// Predicates.equal("roomid", "xxxxxx3");
		// Predicate<String, PlayUserClient> predicate = Predicates.or(idPredicate,
		// roomIdPredicate);
		// Collection<PlayUserClient> employees2 = clusterMap1.values(predicate);
		// System.out.println(employees2.size());
		// for (PlayUserClient playUserClient : employees2) {
		// System.out.println("id:" + playUserClient.getId());
		// System.out.println("roomid:" + playUserClient.getRoomid());
		// }

		// 3.某字段like时候进行查询
		// Predicate<String, PlayUserClient> idPredicate = Predicates.like("roomid",
		// "xxxxxx%");
		// Collection<PlayUserClient> employees = clusterMap1.values(idPredicate);
		// System.out.println(employees.size());
		// for (PlayUserClient playUserClient : employees) {
		// System.out.println("id:" + playUserClient.getId());
		// System.out.println("roomid:" + playUserClient.getRoomid());
		// }

		// 4.不等于
		// Predicate<String, PlayUserClient> idPredicate = Predicates.notEqual("roomid",
		// "3xxxxxx3");
		// Collection<PlayUserClient> employees = clusterMap1.values(idPredicate);
		// System.out.println(employees.size());
		// for (PlayUserClient playUserClient : employees) {
		// System.out.println("id:" + playUserClient.getId());
		// System.out.println("roomid:" + playUserClient.getRoomid());
		// }

		// 5.sql方式查询
		// Collection<PlayUserClient> employees = clusterMap1.values( new SqlPredicate(
		// " roomid = 3xxxxxx3 " ) );
		// System.out.println(employees.size());
		// for (PlayUserClient playUserClient : employees) {
		// System.out.println("id:" + playUserClient.getId());
		// System.out.println("roomid:" + playUserClient.getRoomid());
		// }

		// 6.分页查询
		// Predicate<String, PlayUserClient> idPredicate = Predicates.like("roomid",
		// "%xxxxxx%");
		// PagingPredicate pagingPredicate = new PagingPredicate(idPredicate, 2);
		//
		// Collection<PlayUserClient> employees = clusterMap1.values(pagingPredicate);
		// pagingPredicate.setPage(3);
		// for (PlayUserClient playUserClient : employees) {
		// System.out.print("id:" + playUserClient.getId());
		// System.out.println("|roomid:" + playUserClient.getRoomid());
		// }
		// // 创建队列Queue
		// Queue<String> clusterQueue2 = instance.getQueue("MyQueue");
		// clusterQueue2.offer("Hello hazelcast!");
		// clusterQueue2.offer("Hello hazelcast queue!");

		// Map<Integer, String> clusterMap = instance.getMap("MyMap");
		// Queue<String> clusterQueue = instance.getQueue("MyQueue");

		// System.out.println("Map Value:" + clusterMap.get(1));
		// System.out.println("Queue Size :" + clusterQueue.size());
		// System.out.println("Queue Value 1:" + clusterQueue.poll());
		// System.out.println("Queue Value 2:" + clusterQueue.poll());
		// System.out.println("Queue Size :" + clusterQueue.size());
		instance.shutdown();
	}

	public static void query() {

	}

}
