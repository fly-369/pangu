package com.pangu.web.handler.service.imp;

import com.pangu.web.handler.admin.comment.CommonDao;
import com.pangu.web.handler.admin.entity.OsType;
import com.pangu.web.handler.service.StatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StatisticServiceImpl  implements StatisticService {

    @Autowired
    private CommonDao commonDao;

    @Override
    public List<OsType> OsTypeCount() {
        String sql="SELECT ostype,count(*) num FROM bm_playuser where playertype=\"normal\" GROUP BY ostype;";
        Map<String, Object> parmeterMap = new HashMap<String, Object>();
        List list=commonDao.queryListEntity(sql,parmeterMap,OsType.class);
        if(list.size()>0){
            return list;
        }else {
            return null;
        }

    }
}
