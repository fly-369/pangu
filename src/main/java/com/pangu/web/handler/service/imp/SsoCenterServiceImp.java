/**   
* @Title: SsoCenterServiceImp.java 
* @Package com.pangu.web.handler.service.imp 
* @Description:  
* @author maosheng   
* @date 2018年4月3日 下午3:20:56 
* @version V1.0   
*/
package com.pangu.web.handler.service.imp;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.pangu.util.BrowserClient;
import com.pangu.util.RSATool;
import com.pangu.util.UKTools;
import com.pangu.web.handler.service.SsoCenterService;
import com.pangu.web.model.AgentInfo;
import com.pangu.web.model.AgentTransferLog;
import com.pangu.web.service.repository.jpa.AgentInfoRepository;
import com.pangu.web.service.repository.jpa.AgentTransferLogRepository;

/** 
* @ClassName: SsoCenterServiceImp 
* @Description: 
* @date 2018年4月3日 下午3:20:56 
*  
*/
@Component
@Service
public class SsoCenterServiceImp implements SsoCenterService{
	
	@Autowired
	private AgentTransferLogRepository agentTransferLogRepository ;
	@Autowired
	private AgentInfoRepository agentInfoRepository ;
	
	//判断代理机构是否存在
	@Override
	public AgentInfo judgementAgentExist(String agentcode){
		if(agentcode==null)return null;
		List<AgentInfo> agentinfolist= agentInfoRepository.findByAgentcode(agentcode);
		if(agentinfolist==null||agentinfolist.size()==0) 
			return null;
		else 
			return agentinfolist.get(0);
	}

	@Override
	public JSONObject decryptAgentRegisterData(AgentInfo aginfo, String data) throws Exception {
		if(data==null)return null;
		JSONObject resobj=new JSONObject();
		String decdata=RSATool.AESDecrypt(data, aginfo.getAgent_key());
		String[] arry=decdata.split("&");
		resobj.put("account", arry[0]);
		resobj.put("money", arry[1]);
		resobj.put("orderid", arry[2]);
		resobj.put("gametype", arry[3]);//进入大厅 还是具体某一个游戏
		return resobj;
	}
	
	@Override
	public JSONObject decryptAgentTransferData(AgentInfo aginfo, String data) throws Exception {
		if(data==null)return null;
		JSONObject resobj=new JSONObject();
		String decdata=RSATool.AESDecrypt(data, aginfo.getAgent_key());
		String[] arry=decdata.split("&");
		resobj.put("account", arry[0]);
		resobj.put("money", arry[1]);
		resobj.put("orderid", arry[2]);
		resobj.put("transfertype", arry[3]);
		return resobj;
	}

	@Override
	public void saveTransferLog(HttpServletRequest request, String userid,String username, int transfertype,double money) {
		AgentTransferLog agentTransferLog=new AgentTransferLog();
		agentTransferLog.setUserid(userid);
		agentTransferLog.setUsername(username);
		agentTransferLog.setTransfertype(transfertype);
		agentTransferLog.setMoney(money);
		agentTransferLog.setCreatetime(new Date());
		BrowserClient client = UKTools.parseClient(request) ;
		agentTransferLog.setOstype(client.getOs());
		agentTransferLog.setBrowser(client.getBrowser());
		String ip = UKTools.getIpAddr(request);
		//IP ipdata = IPTools.getInstance().findGeography(ip);
		agentTransferLog.setIp(ip);
		agentTransferLogRepository.save(agentTransferLog);
	}

	@Override
	public void updateAgentCoins(AgentInfo aginfo, double coins, int type) {
		double changeconis=Math.abs(coins);//取绝对值
		double availableconis=aginfo.getAvailablecoins();
		switch(type){ //type: 0/1  转入/转出
         case 0 :
        	availableconis=availableconis-changeconis;
            break;
         case 1 :
        	 availableconis=availableconis+changeconis;
            break;
		}
		aginfo.setAvailablecoins(availableconis);//更新
		agentInfoRepository.saveAndFlush(aginfo);
	}



}
