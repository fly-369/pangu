package com.pangu.web.handler.service;

import com.pangu.web.handler.admin.entity.OsType;

import java.util.List;

public interface StatisticService {

     List<OsType> OsTypeCount();
}
