/**   
* @Title: SsoCenterService.java 
* @Package com.pangu.web.handler.service 
* @Description:  
* @author maosheng   
* @date 2018年4月3日 下午3:16:02 
* @version V1.0   
*/
package com.pangu.web.handler.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.pangu.web.model.AgentInfo;

/** 
* @ClassName: SsoCenterService 
* @Description: 
* @date 2018年4月3日 下午3:16:02 
*/
@Component
public interface SsoCenterService {
	//判断代理机构是否存在
	public abstract AgentInfo judgementAgentExist(String agentcode);
	//解密注册数据
	public abstract JSONObject decryptAgentRegisterData(AgentInfo aginfo,String data)throws Exception;
	//解密转账数据
	public abstract JSONObject decryptAgentTransferData(AgentInfo aginfo,String data)throws Exception;
	//保存转账日志
	public abstract void saveTransferLog(HttpServletRequest request,String userid,String username,int transfertype,double money);
	//处理代理商可用余额  type: 0/1  转入/转出
	public abstract void updateAgentCoins(AgentInfo aginfo,double coins,int type);
}
