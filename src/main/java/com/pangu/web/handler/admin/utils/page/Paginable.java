package com.pangu.web.handler.admin.utils.page;

/**
 * @author Chris
 * @date 2018/5/2 19:08
 * @todo  分页接口
*/
public interface Paginable {

	int getTotalCount();

	int getTotalPage();

	int getPageSize();

	int getPageNo();

	boolean isFirstPage();

	boolean isLastPage();

	int getNextPage();

	int getPrePage();
	}
