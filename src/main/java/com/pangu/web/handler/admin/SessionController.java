package com.pangu.web.handler.admin;

import com.pangu.web.handler.Handler;
import com.pangu.web.handler.admin.shiro.entity.UserOnline;
import com.pangu.web.handler.admin.shiro.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/online")
public class SessionController extends Handler{
	
	@Autowired
	SessionService sessionService;
	
	@RequestMapping("index")
	public String online() {
		return "online";
	}

	@ResponseBody
	@RequestMapping("list")
	public List<UserOnline> list() {
		return sessionService.list();
	}

	@ResponseBody
	@RequestMapping("forceLogout")
	public Map<String,Object> forceLogout(String id) {
		try {
			sessionService.forceLogout(id);
			 resultMap.put("message","踢出用户成功");
		} catch (Exception e) {
			e.printStackTrace();
			resultMap.put("message","踢出用户失败");
		}

		return resultMap;
	}
}
