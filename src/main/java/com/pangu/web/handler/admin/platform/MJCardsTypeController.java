package com.pangu.web.handler.admin.platform;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.pangu.util.Menu;
import com.pangu.web.handler.Handler;
import com.pangu.web.service.repository.jpa.AccountConfigRepository;
import com.pangu.web.service.repository.jpa.AiConfigRepository;
import com.pangu.web.service.repository.jpa.GameConfigRepository;

@Controller
@RequestMapping("/apps/platform")
public class MJCardsTypeController extends Handler{
	
	@Autowired
	private AccountConfigRepository accountRes ;
	
	@Autowired
	private GameConfigRepository gameConfigRes ;
	
	@Autowired
	private AiConfigRepository aiConfigRes ;
	
	@RequestMapping({"/mjcardstype"})
	@Menu(type="platform", subtype="mjcardstype")
	public ModelAndView index(ModelMap map , HttpServletRequest request){
		
		return request(super.createAppsTempletResponse("/platform/game/mjcardstype/index"));
	}
}
