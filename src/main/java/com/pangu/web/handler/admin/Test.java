package com.pangu.web.handler.admin;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

public @Data  class Test implements Serializable {

    private String name;
    private int age;
    private Date lastLoginTime;

}
