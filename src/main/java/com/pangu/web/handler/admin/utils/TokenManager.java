package com.pangu.web.handler.admin.utils;

import com.pangu.web.handler.admin.user.SysUserEntity;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 * @author Chris 
 * @date 2018/3/28 13:13
 * @todo 
*/ 
public class TokenManager {

    /**
     * 获取当前用户的Session
     * @return
     */
    public static Session getSession(){
        return SecurityUtils.getSubject().getSession();
    }

    /**
     * 把值放入到当前登录用户的Session里
     * @param key
     * @param value
     */
    public static void setVal2Session(Object key ,Object value){
        getSession().setAttribute(key, value);
    }

    /**
     * 从当前登录用户的Session里取值
     * @param key
     * @return
     */
    public static Object getVal2Session(Object key){
        return getSession().getAttribute(key);
    }

    public static SysUserEntity getUser() {
        return (SysUserEntity) SecurityUtils.getSubject().getPrincipal();
    }

    public   static Long getUserId() {
        return getUser().getUserId();
    }

    public  static Long getDeptId() {
        return getUser().getDeptId();
    }
}
