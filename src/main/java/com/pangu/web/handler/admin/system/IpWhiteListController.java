package com.pangu.web.handler.admin.system;

import com.pangu.util.Menu;
import com.pangu.web.handler.Handler;
import com.pangu.web.handler.admin.annotation.OpeLog;
import com.pangu.web.handler.admin.user.SysUserEntity;
import com.pangu.web.handler.admin.utils.DateUtils;
import com.pangu.web.handler.admin.utils.TokenManager;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.AgentInfo;
import com.pangu.web.model.IpWhiteList;
import com.pangu.web.service.IpWhiteListService;
import com.pangu.web.service.repository.jpa.IpWhiteListRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.Map;

/**
 * @author Chris
 * @date 2018/4/14 13:07
 * @todo   IP白名单
*/
@Controller
@RequestMapping(value = "/admin/ip")
public class IpWhiteListController extends Handler{


    @Autowired
    private IpWhiteListRepository ipWhiteListRepository;

    @Autowired
    private IpWhiteListService ipWhiteListService;

    /**
     * @author Chris
     * @date 2018/4/14 13:07
     * @todo   查询IP白名单列表
    */
    @RequestMapping("/ipwhitelist")
    @RequiresPermissions("sys:ip:select")
    public String ipwhitelist(String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request, @Valid IpWhiteList ipWhiteList) {
        modelMap.put("findContent", findContent);
        if (pageSize == null) {
            pageSize = 25;
        }
        Pagination pagination = ipWhiteListService.getIpList(ipWhiteList, pageNo, pageSize);
        modelMap.put("list", pagination.getList());
        modelMap.put("page", pagination.getPageHtml());
        return "/system/ip/ipIndex";
    }

    /**
     * @author Chris
     * @date 2018/4/14 14:19
     * @todo  新增IP
    */
    @RequestMapping("/add")
    @RequiresPermissions("sys:ip:select")
    @Menu(type = "admin" , subtype = "ipwhitelist")
    public ModelAndView add(ModelMap map , HttpServletRequest request) {
        return request(super.createRequestPageTempletResponse("/system/ip/add"));
    }

    /**
     * @author Chris
     * @date 2018/4/14 13:54
     * @todo   新增IP
    */

    @OpeLog(value = "新增IP白名单")
    @RequestMapping("/save")
    @RequiresPermissions("sys:ip:add")
    @ResponseBody
    public Map<String, Object> save(ModelMap map , HttpServletRequest request, @Valid String ip, @Valid String remark) {
        //获取操作者名称,
        SysUserEntity user= TokenManager.getUser();
        if(user==null){
            resultMap.put("message","请登录！");
            return resultMap;
        }
        try {
            ipWhiteListRepository.save(IpWhiteList.of().setIp(ip).setOperator(user.getUsername()).setOperate_time(DateUtils.getNowTime()).setRemark(remark));
            resultMap.put("status",200);
            resultMap.put("message","新增IP成功！");
        } catch (Exception e) {
            resultMap.put("status",500);
            resultMap.put("message","新增IP失败");
            e.printStackTrace();
        }
        return resultMap;

    }

    /**
     * @author Chris
     * @date 2018/4/14 13:54
     * @todo   删除IP白名单
     */

    @OpeLog(value = "删除IP白名单")
    @RequestMapping("/delete")
    @RequiresPermissions("sys:ip:delete")
    @ResponseBody
    public Map<String, Object> delete(ModelMap map , HttpServletRequest request, @Valid String id) {
        //获取操作者名称,
        try {
            if(StringUtils.isNotBlank(id)){
                ipWhiteListRepository.delete(IpWhiteList.of().setId(Integer.parseInt(id)));
                resultMap.put("message","删除IP成功！");
            }
            } catch (Exception e) {
            resultMap.put("message",e.getMessage());
            e.printStackTrace();
        }
        return resultMap;

    }






}
