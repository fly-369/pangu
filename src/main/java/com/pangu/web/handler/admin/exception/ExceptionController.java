package com.pangu.web.handler.admin.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Chris
 * @date 2018/4/25 12:30
 * @todo   异常处理
*/
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(org.apache.shiro.authz.AuthorizationException.class)  //当没有权限的时候 ，会被这个方法拦截
    public String handleException(RedirectAttributes redirectAttributes, Exception exception, HttpServletRequest request) {
        redirectAttributes.addFlashAttribute("message", "抱歉！您没有权限执行这个操作，请联系管理员！");
        return "redirect:/403";    // 请求的规则 : /page/operate
    }

}
