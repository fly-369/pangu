package com.pangu.web.handler.admin.platform;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.web.handler.Handler;
import com.pangu.web.model.SysGameConfig;
import com.pangu.web.service.repository.jpa.SysGameConfigRepository;

@Controller
@RequestMapping("/apps/platform")
public class SysGameConfigController extends Handler{
	
	@Autowired
	private SysGameConfigRepository sysGameConfigRepository ;
	
	@RequestMapping({"/sysgameconfig/list"})
	public ModelAndView list(ModelMap map , HttpServletRequest request){
		List<SysGameConfig> sysgameconfiglist = sysGameConfigRepository.findByOrgi(super.getOrgi(request)) ;
		if(sysgameconfiglist.size() > 0){
			map.addAttribute("sysgameconfiglist", sysgameconfiglist.get(0)) ;
		}
		return request(super.createAppsTempletResponse("/platform/desktop/account"));
	}
	
		
	@RequestMapping({"/sysgameconfig/save"})
	public ModelAndView account(ModelMap map , HttpServletRequest request , @Valid SysGameConfig sysgameconfig){
		List<SysGameConfig> sysgameconfiglist = sysGameConfigRepository.findByOrgi(super.getOrgi(request)) ;
		if(sysgameconfiglist.size() > 0){
			SysGameConfig tempConfig = sysgameconfiglist.get(0) ;
			sysgameconfig.setId(tempConfig.getId());
			sysgameconfig.setOrgi(super.getOrgi(request));
			sysgameconfig.setUpdatetime(new Date());
			sysGameConfigRepository.save(sysgameconfig) ;
			CacheHelper.getSystemCacheBean().put(BMDataContext.getSysGameConfig(super.getOrgi(request)), sysgameconfig, super.getOrgi(request));
			return request(super.createRequestPageTempletResponse("redirect:/apps/platform/config/account.html"));
		}else {
			return request(super.createRequestPageTempletResponse("redirect:/apps/platform/config/account.html"));
		}
		
	}
	
}
