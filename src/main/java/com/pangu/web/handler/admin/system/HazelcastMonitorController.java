package com.pangu.web.handler.admin.system;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.hazelcast.com.eclipsesource.json.JsonObject;
import com.pangu.util.Menu;
import com.pangu.web.handler.Handler;

@Controller
@RequestMapping("/admin/monitor")
public class HazelcastMonitorController extends Handler{
	
    @RequestMapping("/hazelcast")
    @Menu(type = "admin" , subtype = "metadata" , admin = true)
    public ModelAndView index(ModelMap map , HttpServletRequest request , HttpServletResponse response) throws SQLException {
    	Map<String , Object> jsonObjectMap = new HashMap< String , Object>();
    	map.addAttribute("systemstatic", new Gson().toJson(jsonObjectMap)) ;
    	
    	response.setCharacterEncoding("UTF-8");
    	response.setContentType("application/json; charset=utf-8");
        return request(super.createRequestPageTempletResponse("/system/monitor/hazelcast"));
    }
    /**
     * 转换统计数据
     * @param json
     * @return
     */
    @SuppressWarnings("unused")
	private Map<String , Object> convert(JsonObject json){
    	Map<String , Object> values = new HashMap<String , Object>();
    	for(String key : json.names()){
    		values.put(key, json.get(key)) ;
    	}
    	return values;
    }
}