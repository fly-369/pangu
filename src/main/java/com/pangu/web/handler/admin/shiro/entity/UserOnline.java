package com.pangu.web.handler.admin.shiro.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

public @Data class UserOnline implements Serializable{
	
	private static final long serialVersionUID = 3828664348416633856L;

    private String id;
    private String userId;
    private String username;
    private String host;
    private String systemHost;
    private String status;
    private Date startTimestamp;
    private Date lastAccessTime;
    private Long timeout;

}
