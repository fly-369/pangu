package com.pangu.web.handler.admin.user;

import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 系统配置信息
 */
@Table( name = "sys_config")
@org.hibernate.annotations.Proxy(lazy = false)
@Entity
public @Data
class SysConfigEntity {
	@Id
	private Long id;
	@NotBlank(message="参数名不能为空")
	private String key;
	@NotBlank(message="参数值不能为空")
	private String value; 
	private String remark;

}
