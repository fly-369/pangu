package com.pangu.web.handler.admin;

import javax.jws.WebParam;
import javax.servlet.http.HttpServletRequest;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.web.handler.admin.entity.OsType;
import com.pangu.web.handler.service.StatisticService;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;
import com.pangu.web.service.repository.jpa.GamePlaywayRepository;
import com.pangu.web.service.repository.jpa.PlayUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.pangu.util.Menu;
import com.pangu.web.handler.Handler;

import java.util.List;
import java.util.Map;

/**
 * @author Chris
 * @date 2018/5/17 14:28
 * @todo  首页
*/
@Controller
@Slf4j
public class AppsController extends Handler {

    private static final String GAME_CODE_JINHUA = "jinhua";
    private static final String GAME_CODE_DIZHU = "dizhu";
    private static final String GAME_CODE_HONEHEI = "honghei";
    private static final String GAME_CODE_DEZHOU = "dezhou";
    private static final String GAME_CODE_NIUNIU = "niuniu";

    private static final String PLAYER_TYPE="normal";
    private static final String AI_TYPE="ai";

    @Autowired
    private GamePlaywayRepository playwayRes;

    @Autowired
    private PlayUserRepository playUserRepository;

    @Autowired
    private StatisticService statisticService;

    /**
     * @author Chris
     * @date 2018/5/17 14:41
     * @todo   首页图表的统计
    */
    @RequestMapping({"/apps/content"})
    @Menu(type = "apps", subtype = "content")
    public ModelAndView content(HttpServletRequest request) {
        //从数据库中查询所有的玩法信息
        List<GamePlayway> gp = playwayRes.findByOrgi(super.getOrgi(request), new Sort(Sort.Direction.ASC, "sortindex"));
        //查询注册玩家总数
        int playerTotal=playUserRepository.countByOrgiAndPlayertype(BMDataContext.SYSTEM_ORGI,PLAYER_TYPE);
        //查询AI总数
        int aiTotal=playUserRepository.countByOrgiAndPlayertype(BMDataContext.SYSTEM_ORGI,AI_TYPE);
        //查询不同终端类型的数量
        List<OsType> osTypeList=statisticService.OsTypeCount();
        int hh=0;
        int jh= 0;
        int ddz= 0;
        int nn= 0;
        int dz= 0;
        int onlineTotal;
        int aiOnlineTotal=0;

        Map<String, GameRoom> map = (Map<String, GameRoom>) CacheHelper.getGameRoomCacheBean().getCache();  //从缓存中取所有游戏房间
        for (int i = 0; i < gp.size(); i++) {  //循环玩法列表
            for (String key : map.keySet()) {  //循环游戏房间
                GameRoom gameRoom = map.get(key);
                if (gp.get(i).getId().equals(gameRoom.getPlayway())) {
                    if (gp.get(i).getCode().equals(GAME_CODE_JINHUA)) { //如果房间中的玩法ID等于游戏玩法
                        //通过房间ID从缓存中获取所有用户信息
                        List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), BMDataContext.SYSTEM_ORGI);
                        if (list.size() > 0) {
                            for(PlayUserClient puc:list){  //循环用户列表
                                if(puc.getPlayertype().equals(PLAYER_TYPE)){  //如果玩家等于真人 ，那么该游戏类型玩家统计+1
                                    jh++;
                                }else {
                                    aiOnlineTotal++;   //如果玩家等于AI ，那么该游戏类型AI统计+1
                                }
                            }
                         /*   jh += list.size();*/
                        }
                    } else if (gp.get(i).getCode().equals(GAME_CODE_NIUNIU)) {
                        List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), BMDataContext.SYSTEM_ORGI);
                        for(PlayUserClient puc:list){
                            if(puc.getPlayertype().equals(PLAYER_TYPE)){
                                nn++;
                            }else {
                                aiOnlineTotal++;
                            }
                        }

                    } else if (gp.get(i).getCode().equals(GAME_CODE_HONEHEI)) {
                        String gameId = gameRoom.getId();
                        List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameId, BMDataContext.SYSTEM_ORGI);
                        for(PlayUserClient puc:list){
                            if(puc.getPlayertype().equals(PLAYER_TYPE)){
                                hh++;
                            }else {
                                aiOnlineTotal++;
                            }
                        }
                    } else if (gp.get(i).getCode().equals(GAME_CODE_DEZHOU)) {
                        List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), BMDataContext.SYSTEM_ORGI);
                        for(PlayUserClient puc:list){
                            if(puc.getPlayertype().equals(PLAYER_TYPE)){
                                dz++;
                            }else {
                                aiOnlineTotal++;
                            }
                        }

                    } else if (gp.get(i).getCode().equals(GAME_CODE_DIZHU)) {
                        List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), BMDataContext.SYSTEM_ORGI);
                        for(PlayUserClient puc:list){
                            if(puc.getPlayertype().equals(PLAYER_TYPE)){
                                ddz++;
                            }else {
                                aiOnlineTotal++;
                            }
                        }

                    }

                }
            }

        }
        onlineTotal=jh+dz+ddz+hh+nn;  //在线总人数，等于各游戏类型相加

        log.info("总在线人数:="+onlineTotal+"各房间在线人数: 金花="+jh+"##德州扑克="+dz+"##斗地主="+ddz+"##红黑大战="+hh+"##牛牛="+nn);
        StringBuffer sb=new StringBuffer();  //封装字符串数据 ，返回到前端页面
        sb.append("[");
        if(jh>0){
            sb.append("{value:"+jh+",name:'炸金花'},");
        }else if(ddz>0){
            sb.append("{value:"+ddz+",name:'斗地主'},");
        }else if(dz>0){
            sb.append("{value:"+dz+",name:'德州扑克'},");
        }else if(hh>0){
            sb.append("{value:"+hh+",name:'红黑大战'},");
        }else if(nn>0){
            sb.append("{value:"+nn+",name:'牛牛'}");
        }
        sb.append("]");

        String ai="["+
        "{value:"+aiOnlineTotal+", name:'在线AI'},"+
        "{value:"+(aiTotal-aiOnlineTotal)+", name:'离线AI'},]";
        StringBuffer os=new StringBuffer();  //终端类型统计 ，封装数据返回前端
        os.append("[");
        if(osTypeList.size()>0){
            for(OsType ot:osTypeList){
             os.append("{value:"+ot.getNum()+",name:'"+ot.getOstype()+"'},");

            }
            }
            os.append("]");

        ModelAndView modelAndView=new ModelAndView("/content");
        modelAndView.addObject("online",sb.toString());
        modelAndView.addObject("playerTotal",playerTotal);
        modelAndView.addObject("onlineTotal",onlineTotal);
        modelAndView.addObject("aiOnlineTotal",aiOnlineTotal);
        modelAndView.addObject("aiTotal",aiTotal);
        modelAndView.addObject("ai",ai);
        modelAndView.addObject("os",os.toString());


        return modelAndView;
    }

    @RequestMapping({"/403"})
    public ModelAndView error() {   //当用户没有权限时，会跳转到403页面
        return new ModelAndView("/403");
    }
}

