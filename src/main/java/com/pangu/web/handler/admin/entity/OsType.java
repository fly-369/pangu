package com.pangu.web.handler.admin.entity;

import lombok.Data;

public @Data class OsType {
    private String ostype;  //终端类型
    private int num;  //终端类型的数量
}
