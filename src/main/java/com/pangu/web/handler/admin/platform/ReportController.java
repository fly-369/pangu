package com.pangu.web.handler.admin.platform;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.util.BeiMiDic;
import com.pangu.web.handler.Handler;
import com.pangu.web.handler.admin.utils.StringUtils;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.*;
import com.pangu.web.service.ReportService;
import com.pangu.web.service.repository.jpa.GamePlaywayRepository;
import com.pangu.web.service.repository.jpa.PlayUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Chris
 * @date 2018/4/13 13:54
 * @todo 统计报表模块
 */
@Controller
@RequestMapping("/apps/report")
@Slf4j
public class ReportController extends Handler {
    @Autowired
    private ReportService reportService;

    @Autowired
    private PlayUserRepository playUserRepository;

    /**
     * @author Chris
     * @date 2018/4/13 13:55
     * @todo 后台操作记录
     */
    @RequestMapping({"/operation"})
    @RequiresPermissions("sys:operation:select")
    public String operation(String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request, @Valid OperationLog operationLog) {
        modelMap.put("findContent", findContent);
        if (pageSize == null) {
            pageSize = 25;
        }
        Pagination pagination = reportService.getOperationRecord(operationLog, pageNo, pageSize);
        modelMap.put("list", pagination.getList());
        modelMap.put("page", pagination.getPageHtml());

        return "/platform/report/operationIndex";
    }

    /**
     * @author Chris
     * @date 2018/4/30 19:22
     * @todo 牌局列表
     */
    @RequestMapping({"/playRecord"})
    @RequiresPermissions("sys:playRecord:select")
    public String playRecord(PlayRecord playRecord, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request) {
        log.info("## Start Query BetsOrder");
        modelMap.put("findContent", findContent);
        if (pageSize == null) {
            pageSize = 25;
        }
        //过去表单中输入的参数
        Pagination pagination = null;
        try {

            if (playRecord != null) {
                if (StringUtils.isBlank(playRecord.getGameType())) {
                    playRecord.setGameType("2");
                }
                HttpSession session = request.getSession();
                session.setAttribute("gameType", playRecord.getGameType());
            }
            pagination = reportService.getPlayRecord(playRecord, pageNo, pageSize);
            modelMap.put("RecordList", pagination.getList());
            modelMap.put("page", pagination.getPageHtml());
            log.info("## Result:" + pagination.getList());
            log.info("## End Query BetsOrder");
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return "/platform/report/playRecord";
    }

    /**
     * @author Chris
     * @date 2018/5/7 14:42
     * @todo 通过牌局ID查询详细信息
     */
    @RequestMapping(value = "/queryInfoByGameId")
    @RequiresPermissions("sys:playRecord:select")
    @ResponseBody
    public Map<String, Object> queryInfoByGameId(String gameId, String gameType, ModelMap modelMap) {
        if (StringUtils.isBlank(gameId)) {
            resultMap.put("message", "查询的牌局编号为空");
            return resultMap;
        }
        try {
            PlayRecord playRecord = new PlayRecord();
            playRecord.setGameType(gameType);
            playRecord.setGameId(gameId);
            List<PlayRecord> list = reportService.queryInfoByGameId(playRecord);
            if(list.size()>0){
                for (PlayRecord pr:list){
                  PlayUser playUser= playUserRepository.findById(pr.getPlayerId());
                      if(playUser!=null){
                          pr.setPlayerId(playUser.getUsername());
                      }
                    if(pr.getPlayerType().equals("1")){
                        pr.setPlayerType("真人");
                    }else if(pr.getPlayerType().equals("2")){
                        pr.setPlayerType("AI");
                    }
                }
            }
            resultMap.put("playerInfo", list);
        } catch (Exception e) {
            resultMap.put("message", e.getMessage());
            e.printStackTrace();
        }
        return resultMap;
    }


}
