package com.pangu.web.handler.admin.user;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


@Table(name = "sys_role")
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
@org.hibernate.annotations.Proxy(lazy = false)
@Entity
public @Data class SysRoleEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long roleId;

	@NotBlank(message="角色名称不能为空")
	private String roleName;
	private String remark;

	private Long deptId;
	@Transient
	private String deptName;
	@Transient
	private List<Long> menuIdList;
	@Transient
	private List<Long> deptIdList;

	@Column(name = "update_time")
	private Date updateTime;

	@ManyToMany(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinTable(name = "sys_role_menu", joinColumns = { @JoinColumn(name = "role_id") }, inverseJoinColumns = { @JoinColumn(name = "menu_id") })
	private java.util.Set<SysMenuEntity> sysMenuEntities;


}
