package com.pangu.web.handler.admin.utils.page;

import java.util.List;


public class Pagination<T> extends SimplePage implements java.io.Serializable,
		Paginable {

	public Pagination() {
	}

	public Pagination(int pageNo, int pageSize, int totalCount) {
		super(pageNo, pageSize, totalCount);
	}

	@SuppressWarnings("unchecked")
	public Pagination(int pageNo, int pageSize, int totalCount, List list) {
		super(pageNo, pageSize, totalCount);
		this.list = list;
	}

	public int getFirstResult() {
		return (pageNo - 1) * pageSize;
	}

	/**
	 * 当前页的数据
	 */
	private List<T> list;

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

	

	public String getWebPage(String page){
		StringBuffer pageHtml = new StringBuffer("<ul class='pagination'>");
		if(this.getPageNo()>1){
			if(this.getPageNo()>5){
				pageHtml.append("<li><a href='javascript:;' onclick='"+ page +"'>首页</a></li>");
			}
			pageHtml.append("<li><a href='"+ page +""+(this.getPageNo() -1) +"'>上一页</a></li>");
		}
		for (int i = (this.getPageNo()-2<=0?1:this.getPageNo()-2),no = 1; i <= this.getTotalPage()&& no <6 ; i++,no++) {
			if (this.getPageNo() == i) {
				pageHtml.append("<li class='active'><a href='javascript:void(0);' >"+i+"</a></li>");
			}else{
				pageHtml.append("<li><a href='"+ page +""+ i +"'>"+i+"</a></li>");
			}
		}
		if(this.getPageNo() < this.getTotalPage()){
			pageHtml.append("<li><a href='"+ page +""+(this.getPageNo()+1) +"'>下一页</a></li>");
		}
		pageHtml.append("</ul>");
		return pageHtml.toString();
	}
	
	
	
	/**Ajxa翻页*/
	public String getSiAjaxPageHtml(){
		StringBuffer pageHtml = new StringBuffer("<span>当前:+"+this.pageNo+"/"+this.getTotalPage()+"页，共查询到:"+this.getTotalCount()+"条记录</span>");

		pageHtml.append("<ul class='pagination'>");
		if(this.getPageNo()>1){
			if(this.getPageNo()>5){
				pageHtml.append("<li><a href='javascript:;' onclick='goPageByAjax(1)'>首页</a></li>");
			}
			pageHtml.append("<li><a href='javascript:;'  onclick='goPageByAjax("+(this.getPageNo() - 1)+")'>上一页</a></li>");
		}
		for (int i = (this.getPageNo()-2<=0?1:this.getPageNo()-2),no = 1; i <= this.getTotalPage()&& no <6 ; i++,no++) {
			if (this.getPageNo() == i) {
				pageHtml.append("<li class='active'><a href='javascript:void(0);' >"+i+"</a></li>");
			}else{
				pageHtml.append("<li><a href='javascript:;' onclick='goPageByAjax("+i+")'>"+i+"</a></li>");
			}
		}
		if(this.getPageNo() < this.getTotalPage()){
			pageHtml.append("<li><a href='javascript:;'  onclick='goPageByAjax("+(this.getPageNo() + 1)+")'>下一页</a></li>");
		}
		pageHtml.append("</ul>");
		return pageHtml.toString();
	}
	
	/**
	 * @author Chris
	 * @date 2018/5/2 19:15
	 * @todo  翻页
	*/
	public String getPageHtml(){

		StringBuffer pageHtml = new StringBuffer("");

		 pageHtml.append("<ul class='pagination'>");
		 pageHtml.append("<span>当前:"+this.pageNo+"/"+this.getTotalPage()+"页，总数:"+this.getTotalCount()+"条记录</span>");
		if(this.getPageNo()>1){
			if(this.getPageNo()>5){
				pageHtml.append("<li><a href='javascript:;' onclick='_submitform(1,"+(this.getPageSize())+")'>首页</a></li>");
			}
			pageHtml.append("<li><a href='javascript:;'  onclick='_submitform("+(this.getPageNo() - 1)+","+(this.getPageSize())+")'>上一页</a></li>");
		}
		for (int i = (this.getPageNo()-2<=0?1:this.getPageNo()-2),no = 1; i <= this.getTotalPage()&& no <6 ; i++,no++) {
			if (this.getPageNo() == i) {
				pageHtml.append("<li class='active'><a href='javascript:void(0);' >"+i+"</a></li>");
			}else{
				pageHtml.append("<li><a href='javascript:;' onclick='_submitform("+i+","+(this.getPageSize())+")'>"+i+"</a></li>");
			}
		}
		if(this.getPageNo() < this.getTotalPage()){
			pageHtml.append("<li><a href='javascript:;'  onclick='_submitform("+(this.getPageNo() + 1)+","+(this.getPageSize())+")'>下一页</a></li>");
		}

		if(this.getPageNo() != this.getTotalPage()){
			pageHtml.append("<li><a href='javascript:;'  onclick='_submitform("+(this.getTotalPage())+","+(this.getPageSize())+")'>尾页</a></li>");
		}
		pageHtml.append("<span >每页显示:<select style=\"border: 1px solid #ccc;border-radius: 4px\" id=\"pageSize\" onchange=\"selectPageSize(this.value);\">");

		  if(this.pageSize==15){
		  	pageHtml.append("  <option id=\"pageSize20\" value=\"20\">20条</option>" +
					"<option id=\"pageSize15\" value=\"15\" selected=\"selected\">15条</option>" +
					"<option id=\"pageSize25\" value=\"25\">25条</option>" +
					"<option id=\"pageSize50\" value=\"50\">50条</option>");

		  }else if (this.pageSize==20){
			  pageHtml.append("  <option id=\"pageSize20\" value=\"20\" selected=\"selected\">20条</option>" +
					  "<option id=\"pageSize15\" value=\"15\" >15条</option>" +
					  "<option id=\"pageSize25\" value=\"25\">25条</option>" +
					  "<option id=\"pageSize50\" value=\"50\">50条</option>");

		  }else if(this.pageSize==25){
			  pageHtml.append("  <option id=\"pageSize20\" value=\"20\">20条</option>" +
					  "<option id=\"pageSize15\" value=\"15\" >15条</option>" +
					  "<option id=\"pageSize25\" value=\"25\" selected=\"selected\">25条</option>" +
					  "<option id=\"pageSize50\" value=\"50\" >50条</option>");

		  }else if (this.pageSize==50){
			  pageHtml.append("  <option id=\"pageSize20\" value=\"20\">20条</option>" +
					  "<option id=\"pageSize15\" value=\"15\">15条</option>" +
					  "<option id=\"pageSize25\" value=\"25\">25条</option>" +
					  "<option id=\"pageSize50\" value=\"50\" selected=\"selected\">50条</option>");

		  }

		pageHtml.append("</select></span>");
		/*pageHtml.append("  <span>每页显示:\n" +
				"                <select id=\"pageSize\" onchange=\"selectPageSize(this.value);\">" );

				"                    <option id=\"pageSize20\" value=\"20\">20条</option>\n" +
				"                    <option id=\"pageSize15\" value=\"15\">15条</option>\n" +
				"                    <option id=\"pageSize25\" value=\"25\">25条</option>\n" +
				"                    <option id=\"pageSize50\" value=\"50\">50条</option>\n" +
						pageHtml.append("");
				"                </select>\n" +
				"                </span>");*/
		pageHtml.append("<span >第:<input style=\"width:50px;border: 1px solid #ccc;border-radius: 4px\" type=\"text\" id=\"goPage\" onkeypress='getKey(this.value)' style="+"height:25px;width:60px"+">页</span>");
		pageHtml.append("</ul>");
		pageHtml.append("<script>");
		pageHtml.append("	function _submitform(pageNo,pageSize){");
		pageHtml.append("		$(\"#formId\").append($(\"<input type='hidden' value='\" + pageNo +\"' name='pageNo'>\"));");
		pageHtml.append("		$(\"#formId\").append($(\"<input type='hidden' value='\" + pageSize +\"' name='pageSize'>\"));");
		pageHtml.append("		$(\"#formId\").submit();");
		pageHtml.append("	}");

		pageHtml.append("	function getKey(pageNo){");
		pageHtml.append("if(event.keyCode==13){ if(isNaN(pageNo)){return ;} if(pageNo>"+this.getTotalPage()+"){return;}");
		pageHtml.append("	var pageSize=$(\"#pageSize\").val();	$(\"#formId\").append($(\"<input type='hidden' value='\" + pageNo +\"' name='pageNo'>\"));");
		pageHtml.append("		$(\"#formId\").append($(\"<input type='hidden' value='\" + pageSize +\"' name='pageSize'>\"));");
		pageHtml.append("		$(\"#formId\").submit();");

		pageHtml.append("	}");
		pageHtml.append("	}");

		pageHtml.append("	function selectPageSize(pageSize){");
		pageHtml.append("		$(\"#formId\").append($(\"<input type='hidden' value='\" + pageSize +\"' name='pageSize'>\")).submit();");

		pageHtml.append("	}");

		pageHtml.append("</script>");
		
		return pageHtml.toString();
	}
	
	
}
