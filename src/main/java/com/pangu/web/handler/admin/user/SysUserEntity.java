package com.pangu.web.handler.admin.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Chris
 * @date 2018/4/21 12:42
 * @todo   系统用户属性
*/

@Entity
@Table(name="sys_user")
@org.hibernate.annotations.Proxy(lazy = false)
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
@AllArgsConstructor
public @Data class SysUserEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 32)
	@GeneratedValue(strategy= GenerationType.AUTO)
	private Long userId;

	@NotBlank(message="用户名不能为空")
	private String username;

	@NotBlank(message="密码不能为空")
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private String password;

	@Transient
    private int online;
	@Transient
	private String sessionId;

	private String salt;

	private String email;

	private String mobile;

	private Integer status;

	private Date lastLoginTime;

	private String lastLoginIp;

	@Transient
	private List<Long> roleIdList;

	private Date createTime;

	private Long deptId;

	@Transient
	private String deptName;

    @ManyToMany(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
    @JoinTable(name = "sys_user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id") })
	private Set<SysRoleEntity> sysRoleEntities;

}
