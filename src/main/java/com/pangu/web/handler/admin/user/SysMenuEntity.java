package com.pangu.web.handler.admin.user;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * @author Chris
 * @date 2018/4/21 14:38
 * @todo  菜单属性表
*/
@Table(name = "sys_menu")
@org.hibernate.annotations.Proxy(lazy = false)
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
@Entity
public @Data class SysMenuEntity implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "menu_id", nullable = false)  //菜单ID不能自增
	private Long menuId;
	private Long parentId;
	@Transient
	private String parentName;
	private String name;
	private String url;
	private String perms;
	private String icon;
	private Integer type;
	private Integer orderNum;
	@Transient
	private Boolean open;
	@Transient
	private List<?> list;

    @Column(name = "create_time")
	private Date createTime;
}
