package com.pangu.web.handler.admin.platform;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.pangu.web.model.*;
import com.pangu.web.service.repository.jpa.SysGameConfigRepository;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.util.BeiMiDic;
import com.pangu.util.Menu;
import com.pangu.web.handler.Handler;
import com.pangu.web.service.repository.jpa.AccountConfigRepository;
import com.pangu.web.service.repository.jpa.AiConfigRepository;
import com.pangu.web.service.repository.jpa.GameConfigRepository;
/**
 * @author Chris
 * @date 2018/4/26 15:25
 * @todo   游戏设置模块
*/
@Controller
@RequestMapping("/apps/platform")
public class GameConfigController extends Handler{
	
	@Autowired
	private AccountConfigRepository accountRes ;
	
	/*@Autowired
	private GameConfigRepository gameConfigRes ;*/

	@Autowired
	private SysGameConfigRepository sysGameConfigRepository;
	
	@Autowired
	private AiConfigRepository aiConfigRes ;
	
	@RequestMapping({"/config/account"})
	@RequiresPermissions("sys:account:select")
	public ModelAndView account(ModelMap map , HttpServletRequest request){
		List<AccountConfig> accountList = accountRes.findByOrgi(super.getOrgi(request)) ;
		if(accountList.size() > 0){
			map.addAttribute("accountConfig", accountList.get(0)) ;
		}
		return request(super.createAppsTempletResponse("/platform/desktop/account"));
	}
	
	@RequestMapping({"/config/account/save"})
	@RequiresPermissions("sys:account:update")
	public ModelAndView account(ModelMap map , HttpServletRequest request , @Valid AccountConfig account){
		List<AccountConfig> accountList = accountRes.findByOrgi(super.getOrgi(request)) ;
		if(accountList.size() > 0){
			AccountConfig tempConfig = accountList.get(0) ;
			account.setId(tempConfig.getId());
		}
		account.setOrgi(super.getOrgi(request));
		account.setCreater(super.getUser(request).getId());
		account.setCreatetime(new Date());
		accountRes.save(account) ;
		CacheHelper.getSystemCacheBean().put(BMDataContext.getGameAccountConfig(super.getOrgi(request)), account, super.getOrgi(request));
		return request(super.createRequestPageTempletResponse("redirect:/apps/platform/config/account.html"));
	}
	
	
	@RequestMapping({"/config/game"})
	@RequiresPermissions("sys:game:select")
	public ModelAndView content(ModelMap map , HttpServletRequest request){
		List<SysGameConfig> gameConfigList = sysGameConfigRepository.findByOrgi(super.getOrgi(request)) ;
		if(gameConfigList.size() > 0){
			map.addAttribute("gameConfig", gameConfigList.get(0)) ;
		}
		List<SysDic> list=BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_SYSTEM_GAME_TYPE_DIC);
		map.addAttribute("gameModelList", BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_SYSTEM_GAME_TYPE_DIC)) ;
		map.addAttribute("welfareTypeList", BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_SYSTEM_GAME_WELFARETYPE_DIC)) ;
		return request(super.createAppsTempletResponse("/platform/config/game"));
	}
	
	@RequestMapping({"/config/game/save"})
	@RequiresPermissions("sys:game:update")
	public ModelAndView game(ModelMap map , HttpServletRequest request , @Valid SysGameConfig game){
		List<SysGameConfig> gameConfigList = sysGameConfigRepository.findByOrgi(super.getOrgi(request)) ;
		if(gameConfigList.size() > 0){
			SysGameConfig tempConfig = gameConfigList.get(0) ;
			game.setId(tempConfig.getId());
			game.setCreatetime(tempConfig.getCreatetime());
		}
		game.setOrgi(super.getOrgi(request));
		//game.setCreater(super.getUser(request).getId());
		game.setUpdatetime(new Date());
		sysGameConfigRepository.save(game) ;
		CacheHelper.getSystemCacheBean().put(BMDataContext.getGameConfig(super.getOrgi(request)), game, super.getOrgi(request));
		return request(super.createRequestPageTempletResponse("redirect:/apps/platform/config/game.html"));
	}
	
	@RequestMapping({"/config/ai"})
	@RequiresPermissions("sys:ai:select")
	public ModelAndView ai(ModelMap map , HttpServletRequest request){
		List<AiConfig> aiConfigList = aiConfigRes.findByOrgi(super.getOrgi(request)) ;
		if(aiConfigList.size() > 0){
			map.addAttribute("aiConfig", aiConfigList.get(0)) ;
		}
		return request(super.createAppsTempletResponse("/platform/config/ai"));
	}

	/**
	 * @author Chris
	 * @date 2018/4/5 12:21
	 * @todo  机器人设置
	*/
	@RequestMapping({"/config/ai/save"})
	@RequiresPermissions("sys:ai:update")
	public ModelAndView aiconfig(ModelMap map , HttpServletRequest request , @Valid AiConfig ai){
		//获取用户
		List<AiConfig> aiConfigList = aiConfigRes.findByOrgi(super.getOrgi(request)) ;
		if(aiConfigList.size() > 0){
			AiConfig tempConfig = aiConfigList.get(0) ;
			ai.setId(tempConfig.getId());
		}
		ai.setOrgi(super.getOrgi(request));
		ai.setCreater(super.getUser(request).getId());
		ai.setCreatetime(new Date());
		aiConfigRes.save(ai) ;
		CacheHelper.getSystemCacheBean().put(BMDataContext.getGameAiConfig(super.getOrgi(request)), ai, super.getOrgi(request));
		return request(super.createRequestPageTempletResponse("redirect:/apps/platform/config/ai.html"));
	}
	
}
