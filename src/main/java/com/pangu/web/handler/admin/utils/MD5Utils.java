package com.pangu.web.handler.admin.utils;


import java.security.MessageDigest;

/**
 * @author Chris 
 * @date 2018/4/3 13:00
 * @todo   MD5加密工具类
*/ 
public class MD5Utils {

        private static final String SALT = "#";

        public static String encode(String account,String password) {
            password = password + SALT + account;
            MessageDigest md5 = null;
            try {
                md5 = MessageDigest.getInstance("MD5");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            char[] charArray = password.toCharArray();
            byte[] byteArray = new byte[charArray.length];

            for (int i = 0; i < charArray.length; i++)
                byteArray[i] = (byte) charArray[i];
            byte[] md5Bytes = md5.digest(byteArray);
            StringBuffer hexValue = new StringBuffer();
            for (int i = 0; i < md5Bytes.length; i++) {
                int val = ((int) md5Bytes[i]) & 0xff;
                if (val < 16) {
                    hexValue.append("0");
                }

                hexValue.append(Integer.toHexString(val));
            }
            return hexValue.toString();
        }

        public static void main(String[] args) {
            System.out.println(MD5Utils.encode("place","123456"));
        }

}
