package com.pangu.web.handler.admin.shiro.service;


import com.pangu.web.handler.admin.shiro.entity.UserOnline;

import java.util.List;

public interface SessionService {
	
	List<UserOnline> list();
	boolean forceLogout(String sessionId);
}
