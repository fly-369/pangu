package com.pangu.web.handler.admin.platform;

import com.pangu.util.MD5;
import com.pangu.web.handler.Handler;
import com.pangu.web.handler.admin.annotation.OpeLog;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.*;
import com.pangu.web.service.AgentService;
import com.pangu.web.service.repository.jpa.AgentInfoRepository;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Map;

/**
 * @author Chris
 * @date 2018/4/5 13:16
 * @todo 代理商
 */
@Controller
@RequestMapping("/apps/agent")
public class AgentController extends Handler {

    @Autowired
    private AgentInfoRepository agentInfoRepository;

    @Autowired
    private AgentService agentService;

    /**
     * @author Chris
     * @date 2018/4/5 13:44
     * @todo 查询代理商
     */
    @RequestMapping({"/query"})
    @RequiresPermissions("sys:agent:select")
    public ModelAndView ai(ModelMap map, HttpServletRequest request) {
    /*    AgentInfo agentInfo=new AgentInfo();
        Example<AgentInfo> userExample;
        ExampleMatcher exampleMatcher = ExampleMatcher.matching()
                .withIgnorePaths("agent_code").
                 withMatcher("create_time", match -> match.endsWith())
                .withMatcher("create_time", match -> match.startsWith())
                // 忽略大小写
                .withIgnoreCase()
                // 忽略为空字段
                .withIgnoreNullValues();
        userExample = Example.of(agentInfo, exampleMatcher);
        Page<AgentInfo> pp  = agentInfoRepository.findAll(userExample,new PageRequest(super.getP(request), super.getPs(request)));*/

        Page<AgentInfo> page = agentInfoRepository.findAll(new PageRequest(super.getP(request), super.getPs(request), Sort.Direction.ASC, "id"));
        map.addAttribute("agentList", page);

        return request(super.createAppsTempletResponse("/platform/agent/agent"));
    }

    /**
     * @author Chris
     * @date 2018/5/12 12:25
     * @todo 查询代理商
     */
    @RequestMapping({"/agent"})
    @RequiresPermissions("sys:agent:select")
    public String getPlayUser(String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request, @Valid AgentInfo agentInfo) {
        modelMap.put("findContent", findContent);
        if (pageSize == null) {
            pageSize = 25;
        }
        Pagination pagination = agentService.getAgent(agentInfo, pageNo, pageSize);
        modelMap.put("list", pagination.getList());
        modelMap.put("page", pagination.getPageHtml());

        return "/platform/agent/agentIndex";
    }

    /**
     * @author Chris
     * @date 2018/4/5 16:23
     * @todo 新增代理商, 页面跳转
     */
    @RequiresPermissions("sys:agent:add")
    @RequestMapping("/add")
    public ModelAndView add() {
        return request(super.createRequestPageTempletResponse("/platform/agent/add"));
    }

    /**
     * @author Chris
     * @date 2018/4/5 16:23
     * @todo 新增代理商
     */

    @OpeLog(value = "新增代理")
    @RequestMapping("/add/agent")
    @RequiresPermissions("sys:agent:add")
    @ResponseBody
    public Map<String, Object> addAgent(AgentInfo agentInfo) {
        try {
            long time = System.currentTimeMillis();  //获取当前时间戳
            //获取商户的Code
            String code = agentInfo.getAgentcode();
            //生成密钥 ,取16位
            String key = MD5.encode(code, String.valueOf(time)).toString().substring(8, 24);
            agentInfo.setAgent_key(key);
            agentInfo.setTotalcoins(50000000);//代理商初始化金额
            agentInfo.setAvailablecoins(50000000);
            agentInfoRepository.save(agentInfo);
            resultMap.put("message", "新增代理商成功");
        } catch (Exception e) {
            resultMap.put("message", e.getMessage());

        }

        return resultMap;
    }

    /**
     * @author Chris
     * @date 2018/4/26 16:27
     * @todo 删除代理
     */
    @RequiresPermissions("sys:agent:delete")
    @ResponseBody
    @RequestMapping(value = "/delete")
    public Map<String, Object> delete(@Valid String id, HttpServletRequest request) {
        try {
            agentInfoRepository.delete(id);
            resultMap.put("message", "删除代理成功！");
        } catch (Exception e) {
            resultMap.put("message", e.getMessage());
            e.printStackTrace();
        }

        return resultMap;
    }


}
