package com.pangu.web.handler.admin.platform;

import com.alibaba.fastjson.JSONObject;
import com.pangu.web.handler.Handler;
import com.pangu.web.handler.admin.annotation.OpeLog;
import com.pangu.web.handler.admin.user.SysUserEntity;
import com.pangu.web.handler.admin.utils.TokenManager;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.Notice;
import com.pangu.web.model.PlayUser;
import com.pangu.web.model.UserQuestion;
import com.pangu.web.service.MessageService;
import com.pangu.web.service.NoticeService;
import com.pangu.web.service.repository.jpa.NoticeRepository;
import com.pangu.web.service.repository.jpa.PlayUserRepository;
import com.pangu.web.service.repository.jpa.UserQuestionRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.Map;

/**
 * @author Chris
 * @date 2018/4/7 17:08
 * @todo   信息管理模块
*/
@Controller
@RequestMapping("/apps/message")
public class MessageContoller extends Handler{

    @Autowired
    private UserQuestionRepository userQuestionRepository;

    @Autowired
    private PlayUserRepository playUserRepository;

    @Autowired
    private NoticeRepository noticeRepository;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private MessageService messageService;

    /**
     * @author Chris
     * @date 2018/4/7 17:19
     * @todo   查询问题记录
    */


    @RequestMapping({"/questions"})
    @RequiresPermissions("sys:question:select")
    public String questions(UserQuestion userQuestion, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request){
        modelMap.put("findContent", findContent);
        if (pageSize == null) {
            pageSize = 25;
        }
        Pagination pagination=messageService.getUserQuestion(userQuestion,pageNo,pageSize);
        modelMap.put("list",pagination.getList());
        modelMap.put("page",pagination.getPageHtml());

        return "/platform/message/questionIndex";
    }

    /**
     * @author Chris
     * @date 2018/4/7 19:39
     * @todo   回复玩家
    */
    @RequestMapping("/reply")
    @ResponseBody
    @RequiresPermissions("sys:question:Reply")
    public Map<String,Object> reply(ModelMap map , HttpServletRequest request , @Valid String userId) {
        //根据玩家ID查询玩家用户名
        if(StringUtils.isNotBlank(userId)){
            PlayUser playUser=playUserRepository.findById(userId);
            if(playUser!=null){
                resultMap.put("userName", playUser.getUsername()) ;
            }else {
                resultMap.put("userName", "没有查询到玩家信息") ;
            }
        }
       return  resultMap;
    }

    /**
     * @author Chris
     * @date 2018/4/7 19:39
     * @todo   回复玩家
     */
    @RequestMapping("/reply/save")
    @ResponseBody
    @OpeLog("回复玩家")
    @RequiresPermissions("sys:question:Reply")
    public Map<String,Object> replySave(ModelMap map , HttpServletRequest request ,Notice notice) {
        SysUserEntity user= TokenManager.getUser();
        if(user==null){
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }

        notice.setNoticetype("1");  //回复类型 ，0 系统公告 / 1 玩家反馈
        notice.setStatus("0");
         notice.setCreatetime(new Date());
         //获取操作管理员的ID
         notice.setCreaterid(user.getUsername());
         try {
             noticeService.saveNotice(notice,request);
             resultMap.put("message","回复成功");
        } catch (Exception e) {
             resultMap.put("message",e.getMessage());
            e.printStackTrace();
        }
      return  resultMap;
    }

    /**
     * @author Chris
     * @date 2018/4/9 14:40
     * @todo   查询系统通知
    */
    @RequestMapping({"/notice"})
    @RequiresPermissions("sys:message:select")
    public String notice(Notice notice, String findContent, ModelMap modelMap, Integer pageNo, Integer pageSize, HttpServletRequest request){
        modelMap.put("findContent", findContent);
        if (pageSize == null) {
            pageSize = 25;
        }
        Pagination pagination=messageService.getNotice(notice,pageNo,pageSize);
        modelMap.put("NoticeList",pagination.getList());
        modelMap.put("page",pagination.getPageHtml());

        return "/platform/message/noticeIndex";
    }

    /**
     * @author Chris
     * @date 2018/5/5 13:06
     * @todo   删除系统通知
    */
    @OpeLog(value = "删除系统通知")
    @RequestMapping({"/deleteNotify"})
    @RequiresPermissions("sys:message:delete")
    public Map<String,Object> deleteNotify(HttpServletRequest request, String id){

        try {
            noticeRepository.delete(id);
            resultMap.put("message","删除系统通知成功");
        } catch (Exception e) {
            e.printStackTrace();
            resultMap.put("message",e.getMessage());
        }
        return resultMap;

    }

    /**
     * @author Chris
     * @date 2018/5/11 13:39
     * @todo   新增系统通知
    */
    @RequestMapping("addNotice")
    @RequiresPermissions("sys:message:add")
    @ResponseBody
    @OpeLog("新增系统通知")
    public Map<String,Object> addNotice(@Valid Notice notice){

        SysUserEntity user= TokenManager.getUser();
        if(user==null){
            resultMap.put("message","你已退出,请重新登录!");
            return resultMap;
        }
        try {
            notice.setCreaterid(user.getUsername());
            notice.setNoticetype("0");
            notice.setCreatetime(new Date());
            notice.setStatus("0");
            noticeRepository.save(notice);
            resultMap.put("message","新增成功");
            
            //向前台推送公告
    		noticeService.sendNotice(notice);
        } catch (Exception e) {
            resultMap.put("message",e.getMessage());
            e.printStackTrace();
        }
        return resultMap;
    }














}
