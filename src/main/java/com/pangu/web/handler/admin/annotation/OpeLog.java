package com.pangu.web.handler.admin.annotation;

import java.lang.annotation.*;

/**
 * @author Chris
 * @date 2018/4/13 15:14
 * @todo   后台操作日志
*/
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpeLog {
	String value() default "";
}
