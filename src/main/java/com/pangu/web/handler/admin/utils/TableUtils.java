package com.pangu.web.handler.admin.utils;

import com.pangu.web.model.PlayRecord;

public class TableUtils {

    /**
     * @author Chris
     * @date 2018/5/2 19:36
     * @todo  返回游戏记录表名
    */
    public static String getPlayRecordTableName(PlayRecord playRecord){
        String tableName="";
        switch (Integer.parseInt(playRecord.getGameType())){
            case 1:
                tableName="bm_game_playrecord_dizhu";
                break;
            case 2:
                tableName="bm_game_playrecord_jinhua";
                break;
            case 3:
                tableName="bm_game_playrecord_niuniu";
                break;
            case 4:
                tableName="bm_game_playrecord_dezhou";
                break;
            case 5:
                tableName="bm_game_playrecord_honghei";
                break;

        }
        return tableName;
        }


}
