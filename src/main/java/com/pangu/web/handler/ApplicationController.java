package com.pangu.web.handler;

import com.pangu.core.BMDataContext;
import com.pangu.util.BeiMiDic;
import com.pangu.util.Menu;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@Controller
public class ApplicationController extends Handler{


	@RequestMapping("/")
	@Menu(type = "apps" , subtype = "index" , access = false)
    public ModelAndView admin(HttpServletRequest request,ModelAndView view) {

	    view.addObject("gameTypeList", BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_SYSTEM_GAME_TYPE_DIC)) ;
        return request(super.createRequestPageTempletResponse("/index"));
    }
}
