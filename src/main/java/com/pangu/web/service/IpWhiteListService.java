package com.pangu.web.service;

import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.AgentInfo;
import com.pangu.web.model.IpWhiteList;

public interface IpWhiteListService {

    Pagination<IpWhiteList> getIpList(IpWhiteList ipWhiteList, Integer pageNo, Integer pageSize);


}
