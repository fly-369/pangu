package com.pangu.web.service;

import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.PlayUser;

import java.util.List;

public interface GameUserService {

    Pagination<PlayUser> getPlayUser(PlayUser playUser, Integer pageNo, Integer pageSize);


    List<PlayUser> queryInfoByUserId(PlayUser playUser);

}
