package com.pangu.web.service;

import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.GameRoom;

import java.util.List;

public interface GameRoomService {
    Pagination<GameRoom> getGameRoom(GameRoom gameRoom, Integer pageNo, Integer pageSize);

    List<GameRoom> queryInfoByGameId(GameRoom GameRoom);

    Pagination<GameRoom> getGameRootByCache(GameRoom gameRoom, Integer pageNo, Integer pageSize);
}
