package com.pangu.web.service.impl;

import com.github.wenhao.jpa.Specifications;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.IpWhiteList;
import com.pangu.web.service.IpWhiteListService;
import com.pangu.web.service.repository.jpa.IpWhiteListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class IpWhiteListServiceImpl implements IpWhiteListService {

    @Autowired
    private IpWhiteListRepository ipWhiteListRepository;


    @Override
    public Pagination<IpWhiteList> getIpList(IpWhiteList ipWhiteList, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Specification<IpWhiteList> specification = Specifications.<IpWhiteList>and()
                .build();
      /*  Sort sort= new Sort(Sort.Direction.DESC, "operate_time");*/
        Page<IpWhiteList> p = ipWhiteListRepository.findAll(specification, new PageRequest(pageNo-1, pageSize));

        Pagination<IpWhiteList> pagination = new Pagination();
        pagination.setList(p.getContent());
        pagination.setTotalCount((int)p.getTotalElements());
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);
        return pagination;
    }
}
