package com.pangu.web.service.impl;

import com.github.wenhao.jpa.Specifications;
import com.pangu.web.handler.admin.comment.CommonDao;
import com.pangu.web.handler.admin.utils.StringUtils;
import com.pangu.web.handler.admin.utils.TableUtils;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.OperationLog;
import com.pangu.web.model.PlayRecord;
import com.pangu.web.service.ReportService;
import com.pangu.web.service.repository.jpa.OperationLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Chris
 * @date 2018/5/2 19:28
 * @todo 报表服务层
 */
@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private CommonDao commonDao;

    @Autowired
    private OperationLogRepository operationLogRepository;

    /**
     * @author Chris
     * @date 2018/5/2 19:31
     * @todo 注单记录
     */
    @Override
    public Pagination<PlayRecord> getPlayRecord(PlayRecord playRecord, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Pagination page = new Pagination();
        page.setPageNo(pageNo);
        page.setPageSize(pageSize);

        int offset = (page.getPageNo() - 1) * page.getPageSize();



        //  List<OrderModel> list=betOrderDao.getBetsOrder(orderModel);
        StringBuffer totalSql = new StringBuffer();
        StringBuffer playSql = new StringBuffer();
        Map<String, Object> parmeterMap = new HashMap<String, Object>();
        Map<String, Object> totalMap = new HashMap<>();
        totalSql.append("select COUNT(DISTINCT game_Id) from " + TableUtils.getPlayRecordTableName(playRecord) + " where 1=1  ");
        playSql.append("select * from " + TableUtils.getPlayRecordTableName(playRecord) + " where 1=1 ");
        if (StringUtils.isNotBlank(playRecord.getGameId())) {
            totalSql.append(" and game_id = :gameId ");
            totalMap.put("gameId", playRecord.getGameId());
            playSql.append("  and game_id = :gameId ");
            parmeterMap.put("gameId", playRecord.getGameId());

        }
        if (StringUtils.isNotBlank(playRecord.getPlayerId())) {
            totalSql.append(" and player_id = :playerId");
            totalMap.put("playerId", playRecord.getPlayerId());
            playSql.append("  and player_id = :playerId");
            parmeterMap.put("playerId", playRecord.getPlayerId());
        }
        playSql.append("GROUP BY game_Id  ORDER BY game_Time DESC LIMIT :filterNo, :pageSize");
        parmeterMap.put("filterNo", offset);
        parmeterMap.put("pageSize", pageSize);

        List list = commonDao.queryListEntity(playSql.toString(), parmeterMap, PlayRecord.class);

        //查询数据量
        int total = commonDao.getCountBy(totalSql.toString(), totalMap);

        Pagination<PlayRecord> pagination = new Pagination();
        pagination.setList(list);
        pagination.setTotalCount(total);
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
    }

    public List<PlayRecord> queryInfoByGameId(PlayRecord playRecord) {

        StringBuffer sql = new StringBuffer();
        Map<String, Object> paraMap = new HashMap<>();
        sql.append("select * from " + TableUtils.getPlayRecordTableName(playRecord) + " where 1=1  ");
        if (StringUtils.isNotBlank(playRecord.getGameId())) {
            sql.append(" and game_id = :gameId ");
            paraMap.put("gameId", playRecord.getGameId());
        }
        List list = commonDao.queryListEntity(sql.toString(), paraMap, PlayRecord.class);
        if (list.size() > 0) {
            return list;
        } else {
            return null;
        }
    }


   /**
    * @author Chris
    * @date 2018/5/14 19:33
    * @todo   查询后台操作记录
   */
    @Override
    public Pagination<OperationLog> getOperationRecord(OperationLog operationLog, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Specification<OperationLog> specification = Specifications.<OperationLog>and()
                .build();
         Sort sort= new Sort(Sort.Direction.DESC, "createTime");
        Page<OperationLog> p = operationLogRepository.findAll(specification, new PageRequest(pageNo-1, pageSize,sort));
        Pagination<OperationLog> pagination = new Pagination();
        pagination.setList(p.getContent());
        pagination.setTotalCount((int)p.getTotalElements());
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);
        return pagination;
    }


}
