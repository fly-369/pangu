package com.pangu.web.service.impl;

import com.github.wenhao.jpa.Specifications;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.Notice;
import com.pangu.web.model.PlayUser;
import com.pangu.web.model.UserQuestion;
import com.pangu.web.service.MessageService;
import com.pangu.web.service.repository.jpa.NoticeRepository;
import com.pangu.web.service.repository.jpa.PlayUserRepository;
import com.pangu.web.service.repository.jpa.UserQuestionRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Chris
 * @date 2018/5/11 12:59
 * @todo   信息管理服务层
*/
@Service
public class MessageServiceImpl implements MessageService{
    @Autowired
    private NoticeRepository noticeRepository;

    @Autowired
    private UserQuestionRepository userQuestionRepository;

    @Autowired
    private PlayUserRepository playUserRepository;

    @Override
    public Pagination<Notice> getNotice(Notice notice, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Specification<Notice> specification = Specifications.<Notice>and()
                .eq(StringUtils.isNotBlank(notice.getNoticetype())&&(notice.getNoticetype().equals("0")||notice.getNoticetype().equals("1")), "noticetype", notice.getNoticetype())
                .between(notice.getStartTime() != null && notice.getEndTime() != null, "createtime", notice.getStartTime(), notice.getEndTime())
                .build();
        Sort sort= new Sort(Sort.Direction.DESC, "createtime");
        Page<Notice> p = noticeRepository.findAll(specification, new PageRequest(pageNo-1,pageSize,sort));
        Pagination<Notice> pagination = new Pagination();
        pagination.setList(p.getContent());
        pagination.setTotalCount((int)p.getTotalElements());
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
    }

    @Override
    public List<Notice> queryInfoByNoticeId(Notice notice) {
        return null;
    }

    @Override
    public Pagination<UserQuestion> getUserQuestion(UserQuestion userQuestion, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Specification<UserQuestion> specification = Specifications.<UserQuestion>and()
                .eq(StringUtils.isNotBlank(userQuestion.getUserid()),"userid",userQuestion.getUserid())
                .eq(userQuestion.getStatus().equals("1")||userQuestion.getStatus().equals("0"),"status",userQuestion.getStatus())
                .build();
        Sort sort= new Sort(Sort.Direction.DESC, "createtime");
        Page<UserQuestion> p = userQuestionRepository.findAll(specification, new PageRequest(pageNo-1,pageSize,sort));
        List<UserQuestion> list=p.getContent();
        if(list.size()>0){
            for (UserQuestion uq:list){
             PlayUser playUser= playUserRepository.findById(uq.getUserid());
             if(playUser!=null){
                 uq.setUserName(playUser.getUsername());
             }
            }
        }
        Pagination<UserQuestion> pagination = new Pagination();
        pagination.setList(list);
        pagination.setTotalCount((int)p.getTotalElements());
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
    }
}
