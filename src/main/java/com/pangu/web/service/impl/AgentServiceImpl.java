package com.pangu.web.service.impl;

import com.github.wenhao.jpa.Specifications;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.AgentInfo;
import com.pangu.web.service.AgentService;
import com.pangu.web.service.repository.jpa.AgentInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Service
public class AgentServiceImpl implements AgentService {

    @Autowired
    private AgentInfoRepository agentInfoRepository;

    @Override
    public Pagination<AgentInfo> getAgent(AgentInfo agentInfo, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        Specification<AgentInfo> specification = Specifications.<AgentInfo>and()
                .build();
        Sort sort= new Sort(Sort.Direction.DESC, "id");
        Page<AgentInfo> p = agentInfoRepository.findAll(specification, new PageRequest(pageNo-1,pageSize,sort));

        Pagination<AgentInfo> pagination = new Pagination();
        pagination.setList(p.getContent());
        pagination.setTotalCount((int)p.getTotalElements());
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
    }
}
