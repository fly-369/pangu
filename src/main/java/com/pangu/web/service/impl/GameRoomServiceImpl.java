package com.pangu.web.service.impl;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.PagingPredicate;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.hazelcast.query.Predicates;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.state.GameEvent;
import com.pangu.web.handler.admin.comment.CommonDao;
import com.pangu.web.handler.admin.utils.StringUtils;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.PlayUserClient;
import com.pangu.web.service.GameRoomService;
import com.pangu.web.service.repository.jpa.GameRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author Chris
 * @date 2018/5/9 16:15
 * @todo   游戏房间服务类
*/
@Service
public class GameRoomServiceImpl implements GameRoomService {

    @Autowired
    private GameRoomRepository gameRoomRepository;

    @Autowired
    private CommonDao commonDao;

    /**
     * @author Chris
     * @date 2018/5/24 20:02
     * @todo  从缓存中查询
    */
   public  Pagination<GameRoom> getGameRootByCache(GameRoom gameRoom, Integer pageNo, Integer pageSize){
       pageNo = null == pageNo ? 1 : pageNo;
       pageSize = null == pageSize ? 10 : pageSize;
       // 创建Map
       IMap<String, GameRoom> map = (IMap<String, GameRoom>) CacheHelper.getGameRoomCacheBean().getCache();
       // 6.分页查询
       Predicate<String,GameRoom> idPredicate = null;

       if(StringUtils.isNotBlank(gameRoom.getRoomid())){
           idPredicate= Predicates.equal("roomid",gameRoom.getRoomid());
       }

       PagingPredicate pagingPredicate = new PagingPredicate(idPredicate, pageSize);
//		 pagingPredicate.nextPage();
       pagingPredicate.setPage(pageNo-1);

       Collection<GameRoom> employees = map.values(pagingPredicate);
       List<GameRoom> list=new ArrayList<>();
//		 pagingPredicate.setPage(3);
       for (GameRoom gr : employees) {
           list.add(gr);
           System.out.print("id:" + gr.getId());
           System.out.println("|roomid:" + gr.getRoomid());
       }
       Pagination<GameRoom> pagination = new Pagination();
       pagination.setList(list);
       pagination.setTotalCount(map.size());
       pagination.setPageNo(pageNo);
       pagination.setPageSize(pageSize);
       return  pagination;
       }

    /**
     * @author Chris
     * @date 2018/5/9 16:15
     * @todo   获取游戏房间信息
    */
    @Override
    public Pagination<GameRoom> getGameRoom(GameRoom gameRoom, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
        int offset = (pageNo - 1) * pageSize;
        //  List<OrderModel> list=betOrderDao.getBetsOrder(orderModel);
        StringBuffer totalSql = new StringBuffer();
        StringBuffer sql = new StringBuffer();
        Map<String, Object> parmeterMap = new HashMap<String, Object>();
        Map<String, Object> totalMap = new HashMap<>();
        totalSql.append("select count(*) from  bm_game_room   where 1=1  ");
        sql.append("select * from bm_game_room where 1=1 ");
        if(StringUtils.isNotBlank(gameRoom.getRoomid())){
            sql.append(" and roomid=:roomId ");
            totalSql.append("  and roomid=:roomId ");
            parmeterMap.put("roomId",gameRoom.getRoomid().trim());
            totalMap.put("roomId",gameRoom.getRoomid().trim());
        }

        sql.append(" ORDER BY CREATETIME DESC LIMIT :filterNo, :pageSize");
        parmeterMap.put("filterNo", offset);
        parmeterMap.put("pageSize", pageSize);

        List list = commonDao.queryListEntity(sql.toString(), parmeterMap, GameRoom.class);

        //查询数据量
        int total = commonDao.getCountBy(totalSql.toString(), totalMap);

        Pagination<GameRoom> pagination = new Pagination();
        pagination.setList(list);
        pagination.setTotalCount(total);
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
    }

    @Override
    public List<GameRoom> queryInfoByGameId(GameRoom GameRoom) {
        return null;
    }
}
