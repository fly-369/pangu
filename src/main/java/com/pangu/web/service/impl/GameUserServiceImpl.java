package com.pangu.web.service.impl;

import com.github.wenhao.jpa.Specifications;
import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.PlayUser;
import com.pangu.web.service.GameUserService;
import com.pangu.web.service.repository.jpa.PlayUserRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Chris
 * @date 2018/5/10 19:59
 * @todo
*/
@Service
public class GameUserServiceImpl implements GameUserService{

    @Autowired
    private PlayUserRepository playUserRepository;

    @Override
    public Pagination<PlayUser> getPlayUser(PlayUser playUser, Integer pageNo, Integer pageSize) {
        pageNo = null == pageNo ? 1 : pageNo;
        pageSize = null == pageSize ? 10 : pageSize;
       Specification<PlayUser> specification = Specifications.<PlayUser>and()
                .eq(StringUtils.isNotBlank(playUser.getPlayertype()),"playertype", playUser.getPlayertype())
                .eq(StringUtils.isNotBlank(playUser.getOrgi()), "orgi", playUser.getOrgi())
               .eq(StringUtils.isNotBlank(playUser.getAgentcode()),"agentcode",playUser.getAgentcode())
                .eq(StringUtils.isNotBlank(playUser.getUsername()), "username", playUser.getUsername())
                .eq(playUser.isOnline(), "online", playUser.isOnline())
                .between(playUser.getMaxGoldCoins() != null && playUser.getMinGoldCoins() != null, "goldcoins", playUser.getMinGoldCoins(), playUser.getMaxGoldCoins())
                .build();
        Sort sort= new Sort(Sort.Direction.DESC, "lastlogintime");
        Page<PlayUser> p = playUserRepository.findAll(specification, new PageRequest(pageNo-1,pageSize,sort));

        Pagination<PlayUser> pagination = new Pagination();
        pagination.setList(p.getContent());
        pagination.setTotalCount((int)p.getTotalElements());
        pagination.setPageNo(pageNo);
        pagination.setPageSize(pageSize);

        return pagination;
        }

    @Override
    public List<PlayUser> queryInfoByUserId(PlayUser playUser) {
        return null;
    }
}
