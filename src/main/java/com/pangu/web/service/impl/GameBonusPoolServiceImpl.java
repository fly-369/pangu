package com.pangu.web.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pangu.core.cache.CacheHelper;
import com.pangu.web.model.GameBonusPool;
import com.pangu.web.service.GameBonusPoolService;
import com.pangu.web.service.repository.jpa.GameBonusPoolRepository;

/**
 * @ClassName: GameBonusPoolServiceImpl
 * @Description:  獎金池的相關操作
 * @author 老王
 * @date 2018年5月14日 下午2:09:02
 *
 */
@Service
public class GameBonusPoolServiceImpl implements GameBonusPoolService {
	@Autowired
	private GameBonusPoolRepository gameBonusPoolRepository;

	@Override
	public Double getByType(String type, String orgi) {
		GameBonusPool gbp = this.getEntityByType(type, orgi);
		return gbp != null ? gbp.getCoin() : 0.0;
	}

	public GameBonusPool getEntityByType(String type, String orgi) {
		return gameBonusPoolRepository.findByTypeAndOrgi(type, orgi);
	}

	@Override
	public boolean updateCoinByType(String type, String orgi, double coin, double valve) {
		return this.updateCoinByType(type, orgi, coin, valve);
	}

	@Override
	public boolean updateCoinByType(String type, String orgi, double coin, double valve, int chance) {
		GameBonusPool gbp = this.getEntityByType(type, orgi);
		if (null == gbp) {
			return false;
		}
		gbp.setCoin(coin);
		gbp.setValve(valve);
		gbp.setUpdatetime(new Date());
		if (101 > chance && chance > 0) {
			gbp.setChance(chance);
		}
		gameBonusPoolRepository.saveAndFlush(gbp);
		// 更新缓存
		CacheHelper.getGameBonusPoolCacheBean().put(gbp.getGameType(), gbp, orgi);
		return true;
	}

	@Override
	public List<GameBonusPool> getAll() {
		
		return gameBonusPoolRepository.findAll();
	}

}
