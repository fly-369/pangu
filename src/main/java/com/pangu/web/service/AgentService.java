package com.pangu.web.service;

import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.AgentInfo;
import com.pangu.web.model.PlayUser;

import java.util.List;

public interface AgentService {

    Pagination<AgentInfo> getAgent(AgentInfo agentInfo, Integer pageNo, Integer pageSize);


}
