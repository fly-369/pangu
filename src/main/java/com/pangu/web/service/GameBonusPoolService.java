package com.pangu.web.service;

import java.util.List;

import com.pangu.web.model.GameBonusPool;

/**
 * @ClassName: GameBonusPoolService
 * @Description:  獎金池的相關操作
 * @author 老王
 * @date 2018年5月14日 下午2:07:12
 *
 */
public interface GameBonusPoolService {
	
	/*
	 * 获取所有的数据
	 */
	public List<GameBonusPool> getAll();
	
	/*
	 * 根据类型获取奖金池的数据
	 */
	public Double getByType(String type, String orgi);

	/*
	 * 根据类型更新奖金池的数据以及阀值和概率
	 */
	public boolean updateCoinByType(String type, String orgi, double coin, double valve,int chance);
	/*
	 * 根据类型更新奖金池的数据以及阀值
	 */
	public boolean updateCoinByType(String type, String orgi, double coin, double valve);

}
