package com.pangu.web.service;

import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.Notice;
import com.pangu.web.model.PlayUser;
import com.pangu.web.model.UserQuestion;

import java.util.List;

public interface MessageService {

    Pagination<Notice> getNotice(Notice notice, Integer pageNo, Integer pageSize);

    Pagination<UserQuestion> getUserQuestion(UserQuestion userQuestion, Integer pageNo, Integer pageSize);

    List<Notice> queryInfoByNoticeId(Notice notice);

}
