package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.core.engine.game.model.PlayerRecord;

public abstract interface PlayerRecordRepository  extends JpaRepository<PlayerRecord, String> {

}
