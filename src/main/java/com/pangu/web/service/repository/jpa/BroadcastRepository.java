package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.Broadcast;

public abstract interface BroadcastRepository extends JpaRepository<Broadcast, String> {

}
