package com.pangu.web.service.repository.jpa;

import com.pangu.web.model.OperationLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public abstract interface OperationLogRepository extends JpaSpecificationExecutor<OperationLog>,JpaRepository<OperationLog, String> {








}
