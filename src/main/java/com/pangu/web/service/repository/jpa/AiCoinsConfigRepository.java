package com.pangu.web.service.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.AccountConfig;
import com.pangu.web.model.AiCoinsConfig;
import com.pangu.web.model.AiCoinsTransfer;

public abstract interface AiCoinsConfigRepository extends JpaRepository<AiCoinsTransfer, String>{
  
}
