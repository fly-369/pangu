package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.core.engine.game.model.PlayerRecordDezhou;

public abstract interface PlayerRecordDezhouRepository  extends JpaRepository<PlayerRecordDezhou, String> {

}
