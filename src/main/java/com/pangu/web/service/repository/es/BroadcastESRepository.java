package com.pangu.web.service.repository.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.pangu.web.model.Broadcast;

public abstract interface BroadcastESRepository extends ElasticsearchCrudRepository<Broadcast, String> {

}
