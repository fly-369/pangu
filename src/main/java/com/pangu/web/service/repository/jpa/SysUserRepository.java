package com.pangu.web.service.repository.jpa;

import com.pangu.web.handler.admin.user.SysUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public abstract interface SysUserRepository extends JpaRepository<SysUserEntity, String>
{
    public abstract  SysUserEntity findSysUserEntityByUsername(String username);

    public abstract  SysUserEntity findSysUserEntityByUserId(Long userId);

    public abstract  SysUserEntity findSysUserEntityByUsernameAndPassword(String username,String password);




}
