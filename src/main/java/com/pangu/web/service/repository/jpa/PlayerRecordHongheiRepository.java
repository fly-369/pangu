package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.core.engine.game.model.PlayerRecordJinhua;

public abstract interface PlayerRecordHongheiRepository extends JpaRepository<PlayerRecordJinhua, String> {

}
