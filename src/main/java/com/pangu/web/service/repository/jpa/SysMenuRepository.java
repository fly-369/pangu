package com.pangu.web.service.repository.jpa;

import com.pangu.web.handler.admin.user.SysMenuEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public abstract interface SysMenuRepository extends JpaRepository<SysMenuEntity, String>
{
     public abstract Page<SysMenuEntity> findSysMenuEntityByParentId(Long parentId, Pageable paramPageable);

     public abstract SysMenuEntity findSysMenuEntityByMenuId(Long menuid);

}
