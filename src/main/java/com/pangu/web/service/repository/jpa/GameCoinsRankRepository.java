package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.GameCoinsRank;

public abstract interface GameCoinsRankRepository extends JpaRepository<GameCoinsRank, String>{
	
}
