package com.pangu.web.service.repository.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.pangu.core.engine.game.model.PlayerRecordJinhua;

public  abstract interface  PlayerRecordJinhuaESRepository   extends ElasticsearchCrudRepository<PlayerRecordJinhua, String>{
	
	  public abstract PlayerRecordJinhua findById(String paramString);

}
