package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.AiCoinsConfig;

public abstract interface AiCoinsTransferRepository extends JpaRepository<AiCoinsConfig, String>{
  
}
