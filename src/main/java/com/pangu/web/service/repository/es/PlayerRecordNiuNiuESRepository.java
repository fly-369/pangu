package com.pangu.web.service.repository.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.pangu.core.engine.game.model.PlayerRecord;
import com.pangu.core.engine.game.model.PlayerRecordNiuniu;

public  abstract interface  PlayerRecordNiuNiuESRepository   extends ElasticsearchCrudRepository<PlayerRecordNiuniu, String>{
	
	  public abstract PlayerRecordNiuniu findById(String paramString);

}
