package com.pangu.web.service.repository.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.PlayUser;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public abstract interface PlayUserRepository extends JpaSpecificationExecutor<PlayUser>,JpaRepository<PlayUser, String>
{
	public abstract PlayUser findById(String paramString);

	public abstract PlayUser findByUsername(String username);

	public abstract PlayUser findByEmail(String email);

	public abstract PlayUser findByUsernameAndPassword(String username, String password);

	public abstract Page<PlayUser> findByOrgiAndOnline(String orgi ,boolean online, Pageable page);

	public abstract Page<PlayUser> findByOrgiAndPlayertype(String orgi,String playertype, Pageable page);

	public abstract Page<PlayUser> findByDatastatus(boolean datastatus , String orgi, Pageable paramPageable);

	//查询ai总数
	public abstract int countByOrgiAndPlayertype(String orgi , String playertype);
	//查询在线ai总数
	public abstract int countByOrgiAndPlayertypeAndOnline(String orgi ,String playertype,boolean online);


	public abstract Page<PlayUser> findByDatastatusAndUsername(boolean datastatus , String orgi ,String username ,Pageable paramPageable);



}
