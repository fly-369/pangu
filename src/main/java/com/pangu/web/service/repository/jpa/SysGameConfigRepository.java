package com.pangu.web.service.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.SysGameConfig;

public abstract interface SysGameConfigRepository extends JpaRepository<SysGameConfig, String>{
	 public abstract List<SysGameConfig> findByOrgi(String orgi);
}
