package com.pangu.web.service.repository.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.pangu.core.engine.game.model.PlayerRecord;

public  abstract interface  PlayerRecordESRepository   extends ElasticsearchCrudRepository<PlayerRecord, String>{
	
	  public abstract PlayerRecord findById(String paramString);

}
