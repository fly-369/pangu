package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.pangu.web.model.GameBonusPool;

/**
  * @ClassName: GameBonusPoolRepository
  * @Description: 
  * @author 老王
  * @date 2018年5月14日 下午1:53:05
  *
  */
public abstract interface GameBonusPoolRepository extends JpaRepository<GameBonusPool, String> {

	@Query("from GameBonusPool where gameType = ? and orgi = ?")
	public abstract GameBonusPool findByTypeAndOrgi(String type, String orgi);

}
