package com.pangu.web.service.repository.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.pangu.web.model.SsoRequestInfo;

public abstract interface SsoRequestInfoESRepository extends ElasticsearchCrudRepository<SsoRequestInfo, String>{
	
	public abstract SsoRequestInfo findByOrderidAndOrderidtype(String orderid ,String orderidtype);
	
}
