package com.pangu.web.service.repository.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.pangu.core.engine.game.model.PlayerRecordHonghei;

public  abstract interface  PlayerRecordHongheiESRepository   extends ElasticsearchCrudRepository<PlayerRecordHonghei, String>{
	
	  public abstract PlayerRecordHonghei findById(String paramString);

}
