package com.pangu.web.service.repository.jpa;

import com.pangu.web.model.IpWhiteList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public abstract interface IpWhiteListRepository extends JpaSpecificationExecutor<IpWhiteList>,JpaRepository<IpWhiteList, String>
{

}
