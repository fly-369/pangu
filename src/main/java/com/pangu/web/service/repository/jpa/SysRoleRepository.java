package com.pangu.web.service.repository.jpa;

import com.pangu.web.handler.admin.user.SysRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract interface SysRoleRepository extends JpaRepository<SysRoleEntity, String> {

    public abstract SysRoleEntity findByRoleId(Long roleId);

    public abstract SysRoleEntity findByRoleName(String roleName);

}
