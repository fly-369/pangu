package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.core.engine.game.model.PlayerRecordNiuniu;

public abstract interface PlayerRecordNiuNiuRepository  extends JpaRepository<PlayerRecordNiuniu, String> {

}
