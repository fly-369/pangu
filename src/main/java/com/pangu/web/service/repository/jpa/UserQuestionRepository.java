package com.pangu.web.service.repository.jpa;

import com.pangu.web.model.Notice;
import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.UserQuestion;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;

public abstract interface UserQuestionRepository extends JpaSpecificationExecutor<UserQuestion>,JpaRepository<UserQuestion, String>{

    @Modifying
    @Query("update UserQuestion set status= ?1,updatetime=?2 where id=?3")
    abstract  void updateStatusById(String status, Date updateTime, String questionId);


}
