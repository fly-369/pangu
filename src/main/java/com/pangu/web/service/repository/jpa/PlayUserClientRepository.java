package com.pangu.web.service.repository.jpa;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.PlayUserClient;

public abstract interface PlayUserClientRepository  extends JpaRepository<PlayUserClient, String>{
	
  public abstract PlayUserClient findById(String paramString);
  
  public abstract List<PlayUserClient> findByPlayertype(String type);
  
  public abstract PlayUserClient findByUsername(String username);
  
  public abstract int countByUsername(String username);
  
  public abstract Page<PlayUserClient> findByDatastatus(boolean datastatus , String orgi, Pageable paramPageable);
  
  public abstract Page<PlayUserClient> findByDatastatusAndUsername(boolean datastatus , String orgi ,String username ,Pageable paramPageable);
}
