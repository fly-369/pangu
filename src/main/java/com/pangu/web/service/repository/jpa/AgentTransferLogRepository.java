package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.AgentTransferLog;

public abstract interface AgentTransferLogRepository extends JpaRepository<AgentTransferLog, String>
{

}
