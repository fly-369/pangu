package com.pangu.web.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.core.engine.game.model.PlayerRecordHonghei;

public abstract interface PlayerRecordJinhuaRepository extends JpaRepository<PlayerRecordHonghei, String> {

}
