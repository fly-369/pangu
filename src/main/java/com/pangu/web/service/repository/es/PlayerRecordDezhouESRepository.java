package com.pangu.web.service.repository.es;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;

import com.pangu.core.engine.game.model.PlayerRecord;
import com.pangu.core.engine.game.model.PlayerRecordDezhou;

public  abstract interface  PlayerRecordDezhouESRepository   extends ElasticsearchCrudRepository<PlayerRecordDezhou, String>{
	
	  public abstract PlayerRecordDezhou findById(String paramString);

}
