package com.pangu.web.service.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.WaresType;

public interface WaresTypeRepository  extends JpaRepository<WaresType, String>{
	
  public abstract WaresType findByIdAndOrgi(String id, String orgi);
  
  public abstract List<WaresType> findByOrgi(String orgi, String code);
}
