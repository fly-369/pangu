package com.pangu.web.service.repository.jpa;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.pangu.web.model.Notice;

public abstract interface NoticeRepository extends JpaSpecificationExecutor<Notice>,JpaRepository<Notice, String>{
	//查询最新15条公告
	public abstract List<Notice> findFirst10ByUseridIsNullOrUserid(@Param("userid")String userid,Sort sort);
	
	@Transactional
	@Modifying
    @Query("update Notice n set n.status=?1 where n.id=?2")
	abstract void updateStatusById(String status,String id);
    
    
}
