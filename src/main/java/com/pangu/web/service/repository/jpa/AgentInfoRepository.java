package com.pangu.web.service.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pangu.web.model.AgentInfo;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public abstract interface AgentInfoRepository extends JpaSpecificationExecutor<AgentInfo>, JpaRepository<AgentInfo, String>{
	
	public abstract List<AgentInfo> findByAgentcode(String agentcode);

}
