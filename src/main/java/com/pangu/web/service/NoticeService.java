package com.pangu.web.service;

import com.pangu.web.model.Notice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Sort;

/**
 * @author Chris
 * @date 2018/4/9 13:48
 * @todo
 */
public interface NoticeService {
	void saveNotice(Notice notice, HttpServletRequest request);

	void sendNotice(Notice notice);
	
	/**
	 * 
	 * Description: 首页公告查询接口
	 * 
	 * @author abo
	 * @date 2018年5月12日
	 * @param notice
	 *            查询对象，这里传入的userid
	 * @param sort
	 *            排序对象
	 * @return
	 */
	List<Notice> getNoticeListByUserId(Notice notice, Sort sort);
}
