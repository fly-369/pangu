package com.pangu.web.service;

import com.pangu.web.handler.admin.user.SysUserEntity;
import org.springframework.data.jpa.repository.Query;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface SysUserService  {
    void saveSysUser(SysUserEntity sysUserEntity, HttpServletRequest request);

}
