package com.pangu.web.service;

import com.pangu.web.handler.admin.utils.page.Pagination;
import com.pangu.web.model.OperationLog;
import com.pangu.web.model.PlayRecord;

import java.util.List;

public interface ReportService {
    Pagination<PlayRecord> getPlayRecord(PlayRecord playRecord, Integer pageNo, Integer pageSize);

    List<PlayRecord> queryInfoByGameId(PlayRecord playRecord);

    Pagination<OperationLog> getOperationRecord(OperationLog operationLog, Integer pageNo, Integer pageSize);
}
