package com.pangu.web.model;

import com.pangu.web.handler.admin.utils.page.Pagination;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Chris
 * @date 2018/4/30 19:25
 * @todo  注单记录
*/

public @Data class PlayRecord implements Serializable{

    private String  id; //注单号
    private String gameId;
    private String roomId;
    private String gameType;
    private String orgi;
    private String gameSubType;
    private Double coin;
    private Double winLoseCoin;
    private String cards;
    private String  isBanker;
    private String playerType; //玩法类型
    private String summaryId; //
    private Date gameTime; //游戏时间
    private String playerId;


}
