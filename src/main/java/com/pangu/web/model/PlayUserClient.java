package com.pangu.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;

import com.pangu.core.engine.game.Message;
import com.pangu.util.UKTools;
import com.pangu.util.event.UserEvent;


/**
 * @author jaddy0302 Rivulet User.java 2010-3-17
 * 玩家客户端，用户登陆的时候会创建
 */
@Document(indexName = "beimi", type = "uk_playuser")
@Entity
@Table(name = "bm_playuser")
@org.hibernate.annotations.Proxy(lazy = false)
public class PlayUserClient implements UserEvent ,Message, java.io.Serializable , Comparable<PlayUserClient>{
	private static final long serialVersionUID = 1L;
    @Id
	private String id = UKTools.getUUID().toLowerCase();
	private String username ;
	private String nickname ;
	private String orgi ;
	private String creater;
	private Date createtime = new Date();
	private Date updatetime = new Date();
	private Date passupdatetime = new Date();
	private long playerindex ;
	private String command ;	//指令
	private String city ;	//城市
	private String province ;//省份
	private boolean login ;		//是否登录
	private boolean online ; 	//是否在线
	private boolean datastatus ;//数据状态，是否已删除	
	private String headimgaddr ; 	//头像地址
	private String roomid ;		//加入的房间ID
	private boolean roomready ;	//在房间中已经准备就绪
	private boolean opendeal ;	//明牌
	private String gamestatus ;	//玩家在游戏中的状态 ： READY : NOTREADY : PLAYING ：MANAGED/托管
	private String agentcode ;  //代理商编码
	private String playertype ;	//玩家类型 ： 玩家：托管玩家，AI
	private String token ;
	private String playerlevel ;//玩家级别 ， 等级
	private int experience  ;	//玩家经验
	private boolean isBlank;//是否是庄家
	private String ostype ;	//客户端类型 IOS/ANDROID
	private String browser ;//客户端浏览器
	private Date lastlogintime = new Date();	//最后登录时间
	private double goldcoins;		//金币数量
	private String status ;		//

	/**
	 * @return the id
	 */
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")	
	public String getId() {
		return id;
	}
	
	@Transient
	public boolean isBlank() {
		return isBlank;
	}

	public void setBlank(boolean isBlank) {
		this.isBlank = isBlank;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getOrgi() {
		return orgi;
	}


	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}


	public String getCreater() {
		return creater;
	}


	public void setCreater(String creater) {
		this.creater = creater;
	}


	public Date getCreatetime() {
		return createtime;
	}


	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}


	public Date getUpdatetime() {
		return updatetime;
	}


	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}


	public Date getPassupdatetime() {
		return passupdatetime;
	}


	public void setPassupdatetime(Date passupdatetime) {
		this.passupdatetime = passupdatetime;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getProvince() {
		return province;
	}


	public void setProvince(String province) {
		this.province = province;
	}


	public boolean isLogin() {
		return login;
	}


	public void setLogin(boolean login) {
		this.login = login;
	}


	public boolean isOnline() {
		return online;
	}


	public void setOnline(boolean online) {
		this.online = online;
	}


	public boolean isDatastatus() {
		return datastatus;
	}


	public void setDatastatus(boolean datastatus) {
		this.datastatus = datastatus;
	}

	public String getPlayerlevel() {
		return playerlevel;
	}


	public void setPlayerlevel(String playerlevel) {
		this.playerlevel = playerlevel;
	}


	public int getExperience() {
		return experience;
	}


	public void setExperience(int experience) {
		this.experience = experience;
	}


	public Date getLastlogintime() {
		return lastlogintime;
	}


	public void setLastlogintime(Date lastlogintime) {
		this.lastlogintime = lastlogintime;
	}
	
	public void setId(String id) {
		this.id = id;
	}


	public String getToken() {
		return token;
	}


	public void setToken(String token) {
		this.token = token;
	}


	public double getGoldcoins() {
		return goldcoins;
	}


	public void setGoldcoins(double goldcoins) {
		this.goldcoins = goldcoins;
	}


	public String getPlayertype() {
		return playertype;
	}


	public void setPlayertype(String playertype) {
		this.playertype = playertype;
	}

	@Transient
	public long getPlayerindex() {
		return playerindex;
	}


	public void setPlayerindex(long playerindex) {
		this.playerindex = playerindex;
	}

	@Transient
	public String getCommand() {
		return command;
	}


	public void setCommand(String command) {
		this.command = command;
	}


	public String getGamestatus() {
		return gamestatus;
	}


	public void setGamestatus(String gamestatus) {
		this.gamestatus = gamestatus;
	}
	
	public String getRoomid() {
		return roomid;
	}


	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	
	public boolean isRoomready() {
		return roomready;
	}


	public void setRoomready(boolean roomready) {
		this.roomready = roomready;
	}
	
	public boolean isOpendeal() {
		return opendeal;
	}


	public void setOpendeal(boolean opendeal) {
		this.opendeal = opendeal;
	}


	@Override
	public int compareTo(PlayUserClient o) {
		return (int) (this.playerindex - o.getPlayerindex());
	}


	public String getOstype() {
		return ostype;
	}


	public void setOstype(String ostype) {
		this.ostype = ostype;
	}


	public String getBrowser() {
		return browser;
	}


	public void setBrowser(String browser) {
		this.browser = browser;
	}


	public String getNickname() {
		return nickname;
	}


	public void setNickname(String nickname) {
		this.nickname = nickname;
	}


	public String getHeadimgaddr() {
		return headimgaddr;
	}

	public void setHeadimgaddr(String headimgaddr) {
		this.headimgaddr = headimgaddr;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAgentcode() {
		return agentcode;
	}

	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}
}
