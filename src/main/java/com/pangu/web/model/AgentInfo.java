package com.pangu.web.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "bm_agent_info")
@org.hibernate.annotations.Proxy(lazy = false)
public @Data  class AgentInfo  implements java.io.Serializable {

	private static final long serialVersionUID = 8699781965163431952L;

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private String id ;
	private String agent_name ;  //代理商名称
	@Column(name = "agent_code", nullable = false)
	private String agentcode ;  //代理商Code
	private String agent_key ;   //代理商KEY
	private String prefix;       //平台前缀
	private double totalcoins;       //总初始金额
	private double availablecoins;   //可使用金额

	private String create_time;
	private String status;  //当前状态
}

