package com.pangu.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;

import com.pangu.core.engine.game.Message;
import com.pangu.util.event.UserEvent;

/**
 * 
 * Description:游戏广播表
 *
 * @author abo
 * @date 2018年4月11日
 */
@Document(indexName = "beimi", type = "uk_broadcast")
@Entity
@Table(name = "bm_broadcast")
@org.hibernate.annotations.Proxy(lazy = false)
public class Broadcast implements UserEvent, Message, java.io.Serializable {
	/**
	 * Description:
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;
	private String username;// 用户名
	private String gametype;// 游戏类型
	private double goldcoins;// 获取的金币数
	private Date createtime;
	@Transient
	private String command; // 指令，不是数据库字段

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGametype() {
		return gametype;
	}

	public void setGametype(String gametype) {
		this.gametype = gametype;
	}

	public double getGoldcoins() {
		return goldcoins;
	}

	public void setGoldcoins(double goldcoins) {
		this.goldcoins = goldcoins;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

}
