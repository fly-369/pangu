package com.pangu.web.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "bm_operation_log")
@org.hibernate.annotations.Proxy(lazy = false)
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
public @Data class OperationLog implements java.io.Serializable{

    private static final long serialVersionUID = 8699781965163431952L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id ;
    private String username;  //用户名
    private String operation; //具体的操作
    private String method;   //请求的方法
    private String params;   //请求的参数
    private long time;  //执行时间
    private String ip;   //操作者的IP
    @Column(name = "create_time")
    private Date createTime;  //创建时间
}
