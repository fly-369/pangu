package com.pangu.web.model;

import java.util.Date;

import com.pangu.core.engine.game.Message;

/**
 * 
 * Description:公告前台信息表
 *
 * @author abo
 * @date 2018年5月12日
 */
public class NoticeMessage implements java.io.Serializable, Message {
	/**
	 * Description:
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String noticetype;
	private String noticetitle;
	private String content;
	private String userid;
	private String userquestionid;
	private String status;
	private String createrid;
	private Date createtime;
	private String startTime;
	private String endTime;
	private String command;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNoticetype() {
		return noticetype;
	}

	public void setNoticetype(String noticetype) {
		this.noticetype = noticetype;
	}

	public String getNoticetitle() {
		return noticetitle;
	}

	public void setNoticetitle(String noticetitle) {
		this.noticetitle = noticetitle;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUserquestionid() {
		return userquestionid;
	}

	public void setUserquestionid(String userquestionid) {
		this.userquestionid = userquestionid;
	}

	public String getCreaterid() {
		return createrid;
	}

	public void setCreaterid(String createrid) {
		this.createrid = createrid;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String getCommand() {
		return this.command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

}
