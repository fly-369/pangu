package com.pangu.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "bm_agenttransferlog")
@org.hibernate.annotations.Proxy(lazy = false)
@Accessors(chain = true)
public @Data class AgentTransferLog implements java.io.Serializable{
	private static final long serialVersionUID = 565678541210332017L;
	@Id  
    @GeneratedValue  
	private Integer  id ;
	private String userid ; //用户ID
	private String username ; //用户名
	private int transfertype ;//交易类型 0:上分 1:下分
	private double money; //交易金额
	private String ostype ;//终端类型
	private String browser ;//浏览器类型
	private String ip ;//IP地址
	private Date createtime;
}
