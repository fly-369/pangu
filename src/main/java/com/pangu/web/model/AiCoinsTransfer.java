package com.pangu.web.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "bm_sys_aicoinstransfer")
@org.hibernate.annotations.Proxy(lazy = false)
public @Data  class AiCoinsTransfer  implements java.io.Serializable {
	private static final long serialVersionUID = 8693781965163431952L;
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	private String id ;
	private String fromusername ;  //转出用户名
	private String tousername ;  //转入用户名
	private double coins ;  //转出金额
	private double fromusernamebal ;  //转出后余额
	private double tousernamebal ;  //转入后余额
	private Date createtime ;  //创建时间
}

