package com.pangu.web.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "bm_game_coinsrank")
@org.hibernate.annotations.Proxy(lazy = false)
public @Data  class GameCoinsRank  implements java.io.Serializable {
	private static final long serialVersionUID = 8699784965163431952L;
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	private String id ;
	private String orgi ;  //租户ID
	private String userid ;  //用户ID
	private String nickname ;  //昵称
	private double wincoins ;  //所赢金币
	private String playerlevel ;  //玩家等级
	private String headimgaddr ;  //头像地址
	private Date createtime ;  //创建时间
	
}

