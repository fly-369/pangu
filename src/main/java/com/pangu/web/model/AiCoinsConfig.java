package com.pangu.web.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "bm_sys_aicoinsconfig")
@org.hibernate.annotations.Proxy(lazy = false)
public @Data  class AiCoinsConfig  implements java.io.Serializable {
	private static final long serialVersionUID = 8699784965163431952L;
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	private String id ;
	private String orgi ;  //租户ID
	private double totalcoins ;  //总初始金额
	private double availablecoins ;  //可用金额
	private double spendcoins ;  //已开销金额
	private Date createtime ;  //创建时间
	private Date updatetime ;  //更新时间
}

