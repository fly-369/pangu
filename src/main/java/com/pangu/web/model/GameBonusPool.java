package com.pangu.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.pangu.util.event.UserEvent;

import lombok.Data;

@Entity
@Table(name = "bm_game_bonuspool")
@org.hibernate.annotations.Proxy(lazy = false)
@Data
public class GameBonusPool implements  UserEvent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8988042477190235024L;
	private String id;
	private String gameType;
	private String orgi;
	private double valve;
	private Date updatetime;
	private double coin;
	private int chance;

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
