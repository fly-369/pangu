package com.pangu.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;

import com.pangu.util.UKTools;
import com.pangu.util.event.UserEvent;


/**
 * 该实体存到ES,只是为了校验代理商的请求是否重复
 */
@Document(indexName = "beimi", type = "bm_requestInfo")
@Entity
@Table(name = "bm_requestInfo")
@org.hibernate.annotations.Proxy(lazy = false)
public class SsoRequestInfo implements UserEvent , java.io.Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String id = UKTools.getUUID().toLowerCase();
	private String orderid ;	//orderid
	private String orderidtype; //orderidtype
	private Date createtime = new Date();
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getOrderidtype() {
		return orderidtype;
	}
	public void setOrderidtype(String orderidtype) {
		this.orderidtype = orderidtype;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	
}
