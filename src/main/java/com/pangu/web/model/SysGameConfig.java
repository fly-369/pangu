package com.pangu.web.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "bm_sys_gameconfig")
@org.hibernate.annotations.Proxy(lazy = false)
public @Data  class SysGameConfig  implements java.io.Serializable {
	private static final long serialVersionUID = 8699784965163431952L;
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	private String id ;
	private String orgi ;  //租户ID
	private boolean enableai ;//启用AI
	private double aiinitcoins ;  //AI初始化金币
	private double aiofflinecoins ;  //AI下线金币阀值
	private double pushcoins ;  //玩家赢钱广播推送阀值
	private int loginexpdays ;  //账号登录的默认有效期时长
	private String gametype ;  //启用的游戏类型
	private int timeout ;      //无AI的时候 等待时长
	private String timeoutmsg ;	//配牌玩家超时后的 提示消息
	private String recmsg ;		//金币不足的提示
	private Date createtime ;  //创建时间
	private Date updatetime ;  //更新时间
}

