package com.pangu.web.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/** 
 * @author Chris 
 * @date 2018/4/14 13:00
 * @todo  IP白名单
*/
@Entity
@Table(name = "bm_ip_white_list")
@org.hibernate.annotations.Proxy(lazy = false)
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
public @Data
class IpWhiteList  implements Serializable{

    private static final long serialVersionUID = 1040910689798969994L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String ip;
    private String remark;
    private String operator;//操作人
    private String operate_time; //操作时间
}
