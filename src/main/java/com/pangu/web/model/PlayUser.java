package com.pangu.web.model;

import java.util.Date;

import javax.persistence.*;

import com.pangu.web.handler.admin.utils.page.Pagination;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;

import com.pangu.util.UKTools;
import com.pangu.util.event.UserEvent;
import sun.security.pkcs11.Secmod;


/**
 * @author jaddy0302 Rivulet User.java 2010-3-17
 * 
 */
@Document(indexName = "beimi", type = "uk_playuser")
@Entity
@Table(name = "bm_playuser")
@org.hibernate.annotations.Proxy(lazy = false)
public @Data class PlayUser  implements UserEvent , java.io.Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	private String id = UKTools.getUUID().toLowerCase();
	private String username ;
	private String password ;
	private String email ;
	private String gender;
	private String mobile ;
	private String birthday ;
	private String nickname ;
	private String country;
	private String region;
	private String useragent ;
	private String isp;
	private String agentcode ;  //代理商编码
	private String playertype ;  //玩家类别 ，区分真人和AI
	private String orgi ;
	private String creater;
	private Date createtime = new Date();
	private Date updatetime = new Date();
	private Date passupdatetime = new Date();
	private String ostype ;	//客户端类型 IOS/ANDROID
	private String browser ;//客户端浏览器
	private String city ;	//城市
	private String province ;//省份
	private boolean login ;		//是否登录
	private boolean online ; 	//是否在线
	private boolean disabled ;	//是否禁用
	private boolean datastatus ;//数据状态，是否已删除	
	private String headimgaddr ; //头像地址
	private String playerlevel ;//玩家级别 ， 等级
	private int experience  ;	//玩家经验
	private Date lastlogintime = new Date();	//最后登录时间
	private double goldcoins;		//金币数量
	private String status ;


	@Transient
	private Double minGoldCoins;  //最大金币
	@Transient
	private Double maxGoldCoins;  //最小金币

}
