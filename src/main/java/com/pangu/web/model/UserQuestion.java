package com.pangu.web.model;

import java.util.Date;

import javax.persistence.*;

import com.pangu.web.handler.admin.utils.page.Pagination;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Entity
@Table(name = "bm_userquestion")
@org.hibernate.annotations.Proxy(lazy = false)
@Accessors(chain = true)
@RequiredArgsConstructor(staticName = "of")
public @Data class UserQuestion implements java.io.Serializable{
	/**
	 * 用户问题反馈表
	 */
	private static final long serialVersionUID = 565678541210332017L;
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id ;
	private String userid ;
	@Transient
	private String userName;
	private String content ;
	private String status="0" ;
	private Date createtime = new Date() ;
	private Date updatetime = new Date() ;
}
