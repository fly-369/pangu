package com.pangu.util.ai;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * Description:金花专属常量类，后续根据此类改成后台控制
 *
 * @author abo
 * @date 2018年6月9日
 */
public class JinhuaConstant {

	// 豹子允许下注比率
	public static final int BOMB_MAX_BET = 100;
	// 顺金允许下注比率
	public static final int SHUNJIN_MAX_BET = 80;
	// 金花允许下注比率
	public static final int JINHUA_MAX_BET = 70;
	// 顺子允许下注比率
	public static final int SHUNZI_MAX_BET = 60;
	// 对子允许下注比率
	public static final int DUIZI_MAX_BET = 50;
	// 单牌允许下注比率
	public static final int DAN_MAX_BET = 30;

	// 是否看牌公式附加参数0-20
	public static final int LOOKCARD_PAMAR_ODDS = 10;
	// 是否跟进公式里面计算的参数
	public static final int LOOKCARD_ODDS = 5;
	//
	public static final Map<Integer, Integer> map;
	
	public static final int JINHUA_BET_TIMES = 5;
	

	/**
	 * 
	 * Description:金花类型权重基础分数
	 *
	 * @author abo
	 * @date 2018年6月9日
	 */
	public enum JinhuaCardsTypeScoreEnum {
		BOMB(6), // 豹子默认分数6开头
		SHUNJIN(5), // 顺金默认分数5开头
		JINHUA(4), // 金花默认分数4开头
		SHUNZI(3), // 顺子默认分数3开头
		DUIZI(2), // 对子默认分数2开头
		DAN(1); // 单张默认分数1开头
		private int type;

		JinhuaCardsTypeScoreEnum(int type) {
			this.type = type;
		}

		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}
	}

	static {
		map = new HashMap<Integer, Integer>();
		map.put(6, 0);
		map.put(5, 10);
		map.put(4, 20);
		map.put(3, 30);
		map.put(2, 40);
		map.put(1, 100);
		// map = Collections.unmodifiableMap(map);
	}

	// 单张弃牌率90%，对子40%，顺子30%，金花20%，顺金10%，豹子0%
	// public enum JinhuaCardsTypeOddoEnum {
	// BOMB(0), // 豹子0%的弃牌率
	// ShunJin(10), // 顺金10%的弃牌率
	// JinHua(20), // 金花20%的弃牌率
	// ShunZi(30), // 顺子30%的弃牌率
	// DuiZi(40), // 对子40%的弃牌率
	// Dan(100); // 单张100%的弃牌率
	// private int type;
	//
	// JinhuaCardsTypeOddoEnum(int type) {
	// this.type = type;
	// }
	//
	// public int getType() {
	// return type;
	// }
	//
	// public void setType(int type) {
	// this.type = type;
	// }
	// }
}
