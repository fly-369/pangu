package com.pangu.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;   
import javax.crypto.Mac;   
import javax.crypto.SecretKey;   
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
   
public abstract class RSATool {   
    public static final String KEY_SHA = "SHA";
    public static final String KEY_MD5 = "MD5";
  
    /**  
     * MAC算法可选以下多种算法  
     *   
     * <pre>  
     * HmacMD5   
     * HmacSHA1   
     * HmacSHA256   
     * HmacSHA384   
     * HmacSHA512  
     * </pre>  
     */  
    public static final String KEY_MAC = "HmacMD5";
  
    /**  
     * BASE64解密  
     *   
     * @param key  
     * @return  
     * @throws Exception  
     */  
    public static byte[] decryptBASE64(String key) throws Exception {
        return hex2BYTE(key);   
    }   
  
    /**  
     * BASE64加密  
     *   
     * @param key  
     * @return  
     * @throws Exception  
     */  
    public static String encryptBASE64(byte[] key) throws Exception {
        return byteHEX(key);   
    }   
  
    /**  
     * MD5加密  
     *   
     * @param data  
     * @return  
     * @throws Exception  
     */  
    public static byte[] encryptMD5(byte[] data) throws Exception {
  
        MessageDigest md5 = MessageDigest.getInstance(KEY_MD5);   
        md5.update(data);   
  
        return md5.digest();   
  
    }   
  
    /**  
     * SHA加密  
     *   
     * @param data  
     * @return  
     * @throws Exception  
     */  
    public static byte[] encryptSHA(byte[] data) throws Exception {
  
        MessageDigest sha = MessageDigest.getInstance(KEY_SHA);   
        sha.update(data);   
  
        return sha.digest();   
  
    }   
  
    /**  
     * 初始化HMAC密钥  
     *   
     * @return  
     * @throws Exception  
     */  
    public static String initMacKey() throws Exception {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_MAC);   
  
        SecretKey secretKey = keyGenerator.generateKey();   
        return encryptBASE64(secretKey.getEncoded());   
    }   
  
    /**  
     * HMAC加密  
     *   
     * @param data  
     * @param key  
     * @return  
     * @throws Exception  
     */  
    public static byte[] encryptHMAC(byte[] data, String key) throws Exception {
  
        SecretKey secretKey = new SecretKeySpec(decryptBASE64(key), KEY_MAC);   
        Mac mac = Mac.getInstance(secretKey.getAlgorithm());   
        mac.init(secretKey);   
  
        return mac.doFinal(data);   
  
    }   
    
    /**
     * 编码byte
     * @param ib
     * @return
     */
    public static String byteHEX(byte[] ib) {
        char[] Digit = { '0','1','2','3','4','5','6','7','8','9',
        'A','B','C','D','E','F' };
        StringBuffer strb = new StringBuffer() ;
        for(byte data: ib){
        	char [] ob = new char[2];
	        ob[0] = Digit[(data >>> 4) & 0X0F];
	        ob[1] = Digit[data & 0X0F];
	        strb.append(ob);
        }
        return strb.toString();
    }
    
    public static byte[] hex2BYTE(String hex) throws IllegalArgumentException {
        if (hex.length() % 2 != 0) {    
            throw new IllegalArgumentException();    
        }    
        char[] arr = hex.toCharArray();    
        byte[] b = new byte[hex.length() / 2];    
        for (int i = 0, j = 0, l = hex.length(); i < l; i++, j++) {    
            String swap = "" + arr[i++] + arr[i];    
            int byteint = Integer.parseInt(swap, 16) & 0xFF;    
            b[j] = new Integer(byteint).byteValue();    
        }    
        return b;    
    } 
    
    /**
	 * MD5 32位加密
	 * @param sourceStr
	 * @return
	 */
	public static String getMD5(String sourceStr) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(sourceStr.getBytes("UTF-8"));
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString();
            
        } catch (NoSuchAlgorithmException e) {
           	e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
         return result;
    }
    
    //AES加密
    public static String AESEncrypt(String sSrc, String sKey){
    	try {
	        byte[] raw = sKey.getBytes("utf-8");
	        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
	        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");//"算法/模式/补码方式"
	        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
	        byte[] encrypted = cipher.doFinal(sSrc.getBytes("utf-8"));
	        return new Base64().encodeToString(encrypted);//此处使用BASE64做转码功能，同时能起到2次加密的作用。
    	 } catch (Exception ex) {
    		 ex.printStackTrace();
             return null;
         }
    }

    //AES解密
    public static String AESDecrypt(String sSrc, String sKey) throws Exception{
        byte[] raw = sKey.getBytes("utf-8");
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] encrypted1 = new Base64().decode(sSrc);//先用base64解密
        byte[] original = cipher.doFinal(encrypted1);
        String originalString = new String(original,"utf-8");
        return originalString;
    }
    
    /*public static void main(String[] args) throws Exception {
        
         * 此处使用AES-128-ECB加密模式，key需要为16位。
         
        String cKey = "1234567df012345x";
        // 需要加密的字串
        String cSrc = "abcabc";
        System.out.println(cSrc);
        // 加密
        String enString = AESEncrypt(cSrc, cKey);
        System.out.println("加密后的字串是：" + enString);
        // 解密
        String DeString = AESDecrypt(enString, cKey);
        System.out.println("解密后的字串是：" + DeString);
    }*/
} 