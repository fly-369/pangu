/**   
* @Title: RespEntity.java 
* @Package com.adminclient.config 
* @Description:  
* @date 2018年3月19日 下午2:24:00 
* @version V1.0   
*/
package com.pangu.util.rules.model;

/** 
* @ClassName: RespEntity 
* @Description: 
* @date 2018年3月19日 下午2:24:00 
*/
public class RespEntity {
    private String code;
    private String msg;
    private Object data;
    
    public RespEntity(RespCode respCode) {
        this.code = respCode.getCode();
        this.msg = respCode.getMsg();
    }

    public RespEntity(RespCode respCode, Object data) {
    	this.code = respCode.getCode();
        this.msg = respCode.getMsg();
        this.data = data;
    }
    
    public RespEntity(String code,String msg,Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	public String toJSON() {
    	return "{\"code\":\""+code+"\",\"msg\":\""+msg+"\",\"data\":\""+data+"\"}";
    }
}
