package com.pangu.util.rules.model;

/** 
* @ClassName: RespCode 
* @Description: 
* @date 2018年3月19日 下午2:24:38 
*/
    
public enum RespCode {
	R0000("0000", "处理成功"),
	R1001("1001", "处理失败，未找到该代理商信息"),
	R1002("1002", "处理失败，加密参数为空"),
	R1003("1003", "处理失败，解密出现异常"),
	R1004("1004", "处理失败，md5签名错误"),
	R1005("1005", "处理失败，转出余额大于用户余额"),
	R1006("1006", "处理失败，未找到该用户"),
	R1007("1007", "处理失败，无效token"),
	R1008("1008", "处理失败，缺失token"),
	R1009("1009", "处理失败，重复请求"),
	
	R9999("9999", "处理失败，系统发生异常");

    private String code;
    private String msg;
    RespCode(String code, String msg) {
    	this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }
    public String getMsg() {
        return msg;
    }
}
