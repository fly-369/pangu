package com.pangu.util.rules.model;

import java.io.Serializable;

import com.pangu.core.BMDataContext;
import com.pangu.core.engine.game.Message;

/**
 * @ClassName: GameStatus
 * @Description: 游戏状态实体
 * @date 2018年4月10日 下午4:26:18
 *
 */
public class GameStatus implements Message, Serializable {

	private static final long serialVersionUID = 1L;

	public GameStatus() {
		super();
	}

	public GameStatus(String gamestatus, String gamestatusMessage) {
		super();
		this.gamestatus = gamestatus;
		this.gamestatusMessage = gamestatusMessage;
		this.time = System.currentTimeMillis();
	}

	private String command =BMDataContext.BEIMI_GAMESTATUS_EVENT;
	private String gamestatus;
	private String gamestatusMessage;// 状态内容
	private long time;// 状态时间
	private String userid;
	private String orgi;
	private String gametype;
	private String playway;
	private boolean cardroom;
	private String roomId;

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getGamestatusMessage() {
		return gamestatusMessage;
	}

	public void setGamestatusMessage(String gamestatusMessage) {
		this.gamestatusMessage = gamestatusMessage;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getGamestatus() {
		return gamestatus;
	}

	public void setGamestatus(String gamestatus) {
		this.gamestatus = gamestatus;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public String getGametype() {
		return gametype;
	}

	public void setGametype(String gametype) {
		this.gametype = gametype;
	}

	public String getPlayway() {
		return playway;
	}

	public void setPlayway(String playway) {
		this.playway = playway;
	}

	public boolean isCardroom() {
		return cardroom;
	}

	public void setCardroom(boolean cardroom) {
		this.cardroom = cardroom;
	}

}
