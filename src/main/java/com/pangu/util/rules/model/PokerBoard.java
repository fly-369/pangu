package com.pangu.util.rules.model;

import com.pangu.core.engine.game.model.Summary;
import com.pangu.util.GameUtils;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;

import lombok.extern.slf4j.Slf4j;

/**
 * @ClassName: NiuniuBoard
 * @Description:  牌局，用于描述当前牌局的内容 ，
 * @author 老王
 * @date 2018年3月23日 下午8:03:09
 *
 */
@Slf4j
public class PokerBoard extends Board implements java.io.Serializable {
	
	private static final long serialVersionUID = 1L;

	/*
	 * 找到下一个玩家，若没有就没有
	 */
	public NiuniuPosition getNext(NiuniuPosition[] po) {
		NiuniuPosition bb = null;
		for (int i = 0; i < po.length; i++) {
			if (po[i].isIsbanker() && po[i].isNext_player()) {
				log.info("第一个开始····················");

			}

			if (po[i].getPlayer() != null && po[i].isNext_player()) {// 得到当前的索引，然后去找下一个
				if (i == po.length - 1 || po[i + 1].getPlayer() == null) {
					po[i].setNext_player(false);
					po[0].setNext_player(true);
					bb = po[0];
					break;
				} else {
					po[i].setNext_player(false);
					po[i + 1].setNext_player(true);
					bb = po[i + 1];
					break;
				}
			}
		}

		if (bb.isIsbanker()) {
			return null;
		}
		return bb;
	}

	/*
	 * 获取当前玩家
	 */
	public NiuniuPosition getCurrent(NiuniuPosition[] po) {
		for (NiuniuPosition niuniuPosition : po) {
			if (niuniuPosition.isNext_player()) {
				return niuniuPosition;
			}
		}
		return null;
	}
	
	public void giveUp(int i) {
		this.getNiuniuPosition()[i].setOdds(-1);
	
	}
	
	private String common_card;
	
	
	
	

	public String getCommon_card() {
		if(getLasthands()!=null) {
			return GameUtils.ByteToString(getLasthands());
		}
		
		return common_card;
	}

	public void setCommon_card(String common_card) {
		this.common_card = common_card;
	}

	@Override
	public byte[] pollLastHands() {
		return null;
	}

	@Override
	public int calcRatio() {
		return 0;
	}

	@Override
	public TakeCards takeCards(Player player, String playerType, TakeCards current) {
		return null;
	}

	/**
	 * 找到玩家的 位置
	 * 
	 * @param board
	 * @param userid
	 * @return
	 */
	@Override
	public int index(String userid) {
		int index = 0;
		for (int i = 0; i < this.getPlayers().length; i++) {
			Player temp = this.getPlayers()[i];
			if (temp.getPlayuser().equals(userid)) {
				index = i;
				break;
			}
		}
		return index;
	}

	@Override
	public Summary summary(Board board, GameRoom gameRoom, GamePlayway playway) {
		// 庄家

		// 闲

		return null;
	}

	@Override
	public boolean isWin() {
		return false;
	}

	@Override
	protected Player next(int index) {
		return null;
	}

	@Override
	public Player nextPlayer(int index) {
		if (index == (this.getPlayers().length - 1)) {
			index = 0;
		} else {
			index = index + 1;
		}
		return this.getPlayers()[index];
	}

	@Override
	public TakeCards takecard(Player player, boolean allow, byte[] playCards) {
		return null;
	}

	@Override
	public TakeCards takecard(Player player) {
		return null;
	}

	@Override
	public TakeCards takeCardsRequest(GameRoom gameRoom, Board board, Player player, String orgi, boolean auto,
			byte[] playCards) {
		return null;
	}

	@Override
	public void dealRequest(GameRoom gameRoom, Board board, String orgi, boolean reverse, String nextplayer) {

	}

	@Override
	public void playcards(Board board, GameRoom gameRoom, Player player, String orgi) {

	}

	@Override
	public TakeCards takecard(Player player, TakeCards last) {
		return null;
	}

}
