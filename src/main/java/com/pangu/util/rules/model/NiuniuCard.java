package com.pangu.util.rules.model;

public enum NiuniuCard {
	NOT_NIU("没牛", 0), NIU_1("牛1", 1), NIU_2("牛2", 2), NIU_3("牛3", 3), NIU_4("牛4", 4), NIU_5("牛5", 5), NIU_6("牛6",
			6), NIU_7("牛7", 7), NIU_8("牛8", 8), NIU_9("牛9", 9), NIU_NIU("牛牛", 10), BOMB("炸弹", 11), Five_Niu("五花牛", 12);

	// 成员变量
	private String name;
	private int index;

	// 构造方法
	private NiuniuCard(String name, int index) {
		this.name = name;
		this.index = index;
	}

	/*
	 * NIU_NIU =10, --牛牛 SILVER_NIU =11, --银牛 GOLD_NIU=12, --金牛 BOMB = 13, --炸弹
	 * SMALL_NIU = 14, --五小牛
	 */
	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}


	// 普通方法
	public static String getName(int index) {
		for (NiuniuCard c : NiuniuCard.values()) {
			if (c.getIndex() == index) {
				return c.name;
			}
		}
		return null;
	}

}
