package com.pangu.util.rules.model;

import com.pangu.core.BMDataContext;
import com.pangu.core.engine.game.Message;

/**
 * 
 * 德州的牌vo
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 */

public class PokerCards implements Message {

	private String command = BMDataContext.BEIMI_GAME_LASTHANDS;

	public PokerCards(int... cards) {
		super();
		this.cards = cards;
	}

	public PokerCards() {
		super();
	}

	public int[] cards;

	public int[] getCards() {
		return cards;
	}

	public void setCards(int[] cards) {
		this.cards = cards;
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

}
