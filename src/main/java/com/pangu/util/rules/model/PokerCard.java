package com.pangu.util.rules.model;

/**
  * @ClassName: PokerCard
  * @Description: 德州扑克牌型算法
  * @author 老王
  * @date 2018年4月19日 下午7:17:05
  *
  */
public enum PokerCard {
	High_card("高牌", 0), One_couple("一对", 1), Two_couple("二对", 2), Three_strip("三条", 3), Shun_card("顺子", 4), flush_card("同花", 5),
	full_card("葫芦",6), four_card("四条", 7), tong_card("同花顺", 8), huang_card("皇家同花顺", 9);

	// 成员变量
	private String name;
	private int index;

	// 构造方法
	private PokerCard(String name, int index) {
		this.name = name;
		this.index = index;
	}

	
	public int getIndex() {
		return index;
	}

	public String getName() {
		return name;
	}


	// 普通方法
	public static String getName(int index) {
		for (PokerCard c : PokerCard.values()) {
			if (c.getIndex() == index) {
				return c.name;
			}
		}
		return null;
	}

}
