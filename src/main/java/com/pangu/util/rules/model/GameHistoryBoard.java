package com.pangu.util.rules.model;

import java.util.Date;

import com.pangu.core.engine.game.Message;

public class GameHistoryBoard implements Message,java.io.Serializable,Comparable<GameHistoryBoard> {

	/**
	 * Description:
	 */
	private static final long serialVersionUID = 7174024916659413006L;
	private String id;
	private String winner;
	private String winnerCardType;
	private Date createTime;
	private String roomId;

	private String command;
	
	

	public GameHistoryBoard(String id) {
		super();
		this.id = id;
	}

	public GameHistoryBoard(String id, String winner, String winnerCardType, Date createTime, String roomId) {
		this.id = id;
		this.winner = winner;
		this.winnerCardType = winnerCardType;
		this.createTime = createTime;
		this.roomId = roomId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	public String getWinnerCardType() {
		return winnerCardType;
	}

	public void setWinnerCardType(String winnerCardType) {
		this.winnerCardType = winnerCardType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	@Override
	public String getCommand() {
		return this.command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	@Override
	public int compareTo(GameHistoryBoard o) {
		Long s = o.getCreateTime().getTime() - this.createTime.getTime();
		return s.intValue();
	}

}
