package com.pangu.util.rules.model;

/**
 * @ClassName: Poker
 * @Description: Poker牌型
 * @author 老王
 * @date 2018年5月29日 下午4:33:43
 *
 */
public class PokerPatterns {
	private int ranking;
	private byte[] cards;

	public int getRanking() {
		return ranking;
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public byte[] getCards() {
		return cards;
	}

	public void setCards(byte[] cards) {
		this.cards = cards;
	}

}
