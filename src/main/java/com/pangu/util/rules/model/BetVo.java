package com.pangu.util.rules.model;

import lombok.Data;

/**
 * @ClassName: BetVo
 * @Description:  前台押注Vo
 * @author 老王
 * @date 2018年4月12日 下午3:51:19
 *
 */
@Data
public class BetVo {
	
	private String orgi;
	/*
	 * 座位
	 */
	private int seat;
	/*
	 * 下注金额
	 */
	private double coin;

}
