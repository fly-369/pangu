package com.pangu.util.rules.model;

import com.pangu.core.engine.game.Message;
import com.pangu.util.GameUtils;

/**
 * @ClassName: NiuniuPosition
 * @Description: 牛牛以及德州的牌型，方便算法的计算
 * @author 老王
 * @date 2018年3月31日 下午3:50:28
 *
 */

public class NiuniuPosition implements Cloneable, Message, java.io.Serializable {

	private static final long serialVersionUID = -8079398557695808123L;
	private byte[] cards;// 排位
	private int position;// 位置
	private int patterns;// 牌型
	private boolean isbanker;// 是否是庄家
	private boolean isWinner;//是否是赢家
	private boolean isAi;//是否是ai
	private int odds;// 赔率
	private double coins;//下注金额
	private double mycoins;//我的金币
	private String cards_string;//
	private String status;// 状态
	private String player;// 绑定的当前玩家v
	private boolean next_player;// 下一个玩家

	@Override
	public Object clone() {
		NiuniuPosition np = null;
		try {
			np = (NiuniuPosition) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return np;
	}

	public double getMycoins() {
		return mycoins;
	}

	public void setMycoins(double mycoins) {
		this.mycoins = mycoins;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isWinner() {
		return isWinner;
	}

	public void setWinner(boolean isWinner) {
		this.isWinner = isWinner;
	}

	public boolean isAi() {
		return isAi;
	}

	public void setAi(boolean isAi) {
		this.isAi = isAi;
	}

	public double getCoins() {
		return coins;
	}

	public void setCoins(double coins) {
		this.coins = coins;
	}

	public boolean isNext_player() {
		return next_player;
	}

	public void setNext_player(boolean next_player) {
		this.next_player = next_player;
	}

	public String getPlayer() {
		return player;
	}

	public void setPlayer(String player) {
		this.player = player;
	}

	public byte[] getCards() {
		return cards;
	}

	public void setCards(byte[] cards) {
		this.cards = cards;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public int getPatterns() {
		return patterns;
	}

	public void setPatterns(int patterns) {
		this.patterns = patterns;
	}

	public boolean isIsbanker() {
		return isbanker;
	}

	public void setIsbanker(boolean isbanker) {
		this.isbanker = isbanker;
	}

	public int getOdds() {
		return odds;
	}

	public void setOdds(int odds) {
		this.odds = odds;
	}

	public String getCards_string() {
		if (cards != null) {
			return GameUtils.ByteToString(cards);
		}
		return cards_string;
	}

	public void setCards_string(String cards_string) {
		this.cards_string = cards_string;
	}

	@Override
	public String getCommand() {
		return null;
	}

	@Override
	public void setCommand(String command) {

	}

}
