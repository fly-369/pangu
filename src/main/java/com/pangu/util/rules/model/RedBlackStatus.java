package com.pangu.util.rules.model;

import java.util.List;

import com.pangu.core.engine.game.Message;
import com.pangu.core.engine.game.model.PlayerRecordHonghei;

public class RedBlackStatus implements Message {

	private String command;
	private String exeDate;
	private String gameStatus;
	private int status;
	private String message;
	private List<PlayerRecordHonghei> PlayerRecordHongheis;

	public String getExeDate() {
		return exeDate;
	}

	public void setExeDate(String exeDate) {
		this.exeDate = exeDate;
	}

	public String getGameStatus() {
		return gameStatus;
	}

	public void setGameStatus(String gameStatus) {
		this.gameStatus = gameStatus;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String getCommand() {
		return this.command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	public List<PlayerRecordHonghei> getPlayerRecordHongheis() {
		return PlayerRecordHongheis;
	}

	public void setPlayerRecordHongheis(List<PlayerRecordHonghei> playerRecordHongheis) {
		PlayerRecordHongheis = playerRecordHongheis;
	}

}
