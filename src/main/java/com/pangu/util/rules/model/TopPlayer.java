package com.pangu.util.rules.model;

import java.io.Serializable;
import java.util.List;

import com.pangu.core.engine.game.Message;
import com.pangu.web.model.PlayUserClient;

/**
  * @ClassName: TopPlayer
  * @Description:  排行榜用户消息存，
  * @author 老王
  * @date 2018年4月10日 下午2:43:42
  *
  */
public class TopPlayer implements Message,Serializable{
	

	
	private static final long serialVersionUID = 1L;
	private List<PlayUserClient> player;
	private String command;


	public List<PlayUserClient> getPlayer() {
		return player;
	}

	public void setPlayer(List<PlayUserClient> player) {
		this.player = player;
	}

	@Override
	public String getCommand() {
		return this.command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

}
