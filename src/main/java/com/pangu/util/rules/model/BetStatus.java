package com.pangu.util.rules.model;

import com.pangu.core.BMDataContext;
import com.pangu.core.engine.game.Message;

/**
 * @ClassName: BetStatus
 * @Description:  下注状态
 * @author 老王
 * @date 2018年4月5日 下午8:36:39
 *
 */
public class BetStatus implements Message {

	private int betstatus;
	private String message;
	private String userid;
	private String orgi;
	private String command=BMDataContext.BEIMI_MESSAGE_EVENT;

	private double betCoin;
	private String betType;

	public String getBetType() {
		return betType;
	}

	public void setBetType(String betType) {
		this.betType = betType;
	}

	public double getBetCoin() {
		return betCoin;
	}

	public void setBetCoin(double betCoin) {
		this.betCoin = betCoin;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getBetstatus() {
		return betstatus;
	}

	public void setBetstatus(int betstatus) {
		this.betstatus = betstatus;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

}
