package com.pangu.util.rules.model;

import com.pangu.core.engine.game.model.Summary;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;

/**
  * @ClassName: NiuniuBoard
  * @Description:  牌局，用于描述当前牌局的内容 ， 
  * @author 老王
  * @date 2018年3月23日 下午8:03:09
  *
  */
public class NiuniuBoard  extends Board implements java.io.Serializable{
	
	private static final long serialVersionUID = 1L;

	public String status;
	
	public String statusTime;
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusTime() {
		return statusTime;
	}

	public void setStatusTime(String statusTime) {
		this.statusTime = statusTime;
	}

	@Override
	public byte[] pollLastHands() {
		return null;
	}

	@Override
	public int calcRatio() {
		return 0;
	}

	@Override
	public TakeCards takeCards(Player player, String playerType, TakeCards current) {
		return null;
	}

	@Override
	public int index(String userid) {
		return 0;
	}

	@Override
	public Summary summary(Board board, GameRoom gameRoom, GamePlayway playway) {
		//庄家
		
		
		
		//闲
		
		
		
		return null;
	}

	@Override
	public boolean isWin() {
		return false;
	}

	@Override
	protected Player next(int index) {
		return null;
	}

	@Override
	public Player nextPlayer(int index) {
		return null;
	}

	@Override
	public TakeCards takecard(Player player, boolean allow, byte[] playCards) {
		return null;
	}

	@Override
	public TakeCards takecard(Player player) {
		return null;
	}

	@Override
	public TakeCards takeCardsRequest(GameRoom gameRoom, Board board, Player player, String orgi, boolean auto,
			byte[] playCards) {
		return null;
	}

	@Override
	public void dealRequest(GameRoom gameRoom, Board board, String orgi, boolean reverse, String nextplayer) {
		
	}

	@Override
	public void playcards(Board board, GameRoom gameRoom, Player player, String orgi) {
		
	}

	@Override
	public TakeCards takecard(Player player, TakeCards last) {
		return null;
	}
	

}
