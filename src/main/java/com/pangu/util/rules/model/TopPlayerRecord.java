package com.pangu.util.rules.model;

import java.io.Serializable;
import java.util.List;

import com.pangu.core.engine.game.Message;
import com.pangu.core.engine.game.model.PlayerRecord;

/**
  * @ClassName: TopPlayer
  * @Description:  排行榜用户消息存，
  * @author 老王
  * @date 2018年4月10日 下午2:43:42
  *
  */
public class TopPlayerRecord implements Message,Serializable{
	

	
	private static final long serialVersionUID = 1L;
	private List<PlayerRecord> pr;
	

	public List<PlayerRecord> getPr() {
		return pr;
	}

	public void setPr(List<PlayerRecord> pr) {
		this.pr = pr;
	}

	@Override
	public String getCommand() {
		return null;
	}

	@Override
	public void setCommand(String command) {
		
	}

}
