package com.pangu.util.rules.model;

import com.pangu.core.engine.game.Message;

/**
 * 
 * Description:炸金花操作类
 *
 * @author abo
 * @date 2018年4月23日
 */
public class ZhajinhuaTakeCards implements Message, java.io.Serializable {

	/**
	 * Description:
	 */
	private static final long serialVersionUID = 1L;

	private String banker;
	private boolean isLookCard;// 是否看牌，true看牌，false未看牌
	private boolean isGiveUpCard;// 是否弃牌，true弃牌，false未弃牌
	private int actionType; // 操作类型：1、加注2、跟注3、弃牌4、比牌
	private String userid;// 当前用户
	private String compareUserid;// 比牌的userId；
	private double betCoin;// 下注金额
	private int currentRatio;// 当前赔率
	private boolean isOver; // 是否结束，结束了就调用结算，true结束，false为结束
	private int time;// 操作时间
	private String playerType;// ai，normal真人
	private String roomId;// 房间id
	private String compareWinUserid;// 比牌赢家userid
	private String timeoutStr;

	public String getTimeoutStr() {
		return timeoutStr;
	}

	public void setTimeoutStr(String timeoutStr) {
		this.timeoutStr = timeoutStr;
	}

	public String getCompareWinUserid() {
		return compareWinUserid;
	}

	public void setCompareWinUserid(String compareWinUserid) {
		this.compareWinUserid = compareWinUserid;
	}

	public String getRoomId() {
		return roomId;
	}

	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	public String getPlayerType() {
		return playerType;
	}

	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}

	private String command;

	@Override
	public String getCommand() {
		return command;
	}

	@Override
	public void setCommand(String command) {
		this.command = command;
	}

	public String getBanker() {
		return banker;
	}

	public void setBanker(String banker) {
		this.banker = banker;
	}

	public boolean isLookCard() {
		return isLookCard;
	}

	public void setLookCard(boolean isLookCard) {
		this.isLookCard = isLookCard;
	}

	public boolean isGiveUpCard() {
		return isGiveUpCard;
	}

	public void setGiveUpCard(boolean isGiveUpCard) {
		this.isGiveUpCard = isGiveUpCard;
	}

	public int getActionType() {
		return actionType;
	}

	public void setActionType(int actionType) {
		this.actionType = actionType;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getCompareUserid() {
		return compareUserid;
	}

	public void setCompareUserid(String compareUserid) {
		this.compareUserid = compareUserid;
	}

	public double getBetCoin() {
		return betCoin;
	}

	public void setBetCoin(double betCoin) {
		this.betCoin = betCoin;
	}

	public int getCurrentRatio() {
		return currentRatio;
	}

	public void setCurrentRatio(int currentRatio) {
		this.currentRatio = currentRatio;
	}

	public boolean isOver() {
		return isOver;
	}

	public void setOver(boolean isOver) {
		this.isOver = isOver;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

}
