package com.pangu.util.disruptor;

import com.lmax.disruptor.EventFactory;
import com.pangu.util.event.UserDataEvent;

public class UserDataEventFactory implements EventFactory<UserDataEvent>{

	@Override
	public UserDataEvent newInstance() {
		return new UserDataEvent();
	}
}
