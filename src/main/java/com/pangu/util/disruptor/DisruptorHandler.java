package com.pangu.util.disruptor;

import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.lmax.disruptor.dsl.Disruptor;
import com.pangu.core.BMDataContext;
import com.pangu.util.event.UserDataEvent;
import com.pangu.util.event.UserEvent;

public class DisruptorHandler {
	/**
	 *
	 * @Description: 放到队列里面进行db持久化，Elasticsearch 或者更多
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void published(UserEvent event, ElasticsearchCrudRepository esRes, JpaRepository dbRes,String command) {
		Disruptor<UserDataEvent> disruptor = (Disruptor<UserDataEvent>) BMDataContext.getContext().getBean("disruptor");
		long seq = disruptor.getRingBuffer().next();
		UserDataEvent userDataEvent = disruptor.getRingBuffer().get(seq);
		userDataEvent.setEvent(event);
		userDataEvent.setDbRes(dbRes);
		userDataEvent.setEsRes(esRes);
		userDataEvent.setCommand(command);
		disruptor.getRingBuffer().publish(seq);
	}

	@SuppressWarnings({ "rawtypes" })
	public static void published(UserEvent event, ElasticsearchCrudRepository esRes, JpaRepository dbRes) {
		published(event, esRes, dbRes, BMDataContext.UserDataEventType.SAVE.toString());
	}

	@SuppressWarnings({ "rawtypes" })
	public static void published(UserEvent event, ElasticsearchCrudRepository esRes) {
		published(event, esRes, null, BMDataContext.UserDataEventType.SAVE.toString());
	}
}
