package com.pangu.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;

import com.pangu.util.rules.model.PokerCard;

/**
 * @ClassName: PokerUtils
 * @Description: 皇家同花顺9 >> 同花顺8 >> 四条7 >> 葫芦6 >> 同花5 >> 顺子4 >> 三条3 >> 两对2 >> 一对1
 *               >> 高牌0,有一个规则 必须从大的一次往下判断
 * @author 老王,
 * @date 2018年4月19日 下午2:52:16 此方法过期了
 *
 */
@Deprecated
public class PokerUtils {

	public static void main(String[] args) {
		byte[] commons = new byte[] { 4, 4, 6, 2, 36 };
		byte[] cards = new byte[] { 3, 24 };
		byte[] cards2 = new byte[] { 3, 24 };
		Arrays.sort(cards);
		Arrays.sort(cards2);
		System.out.println(Compare(commons, cards, cards2));

	}

	/*
	 * 比较两个用户的大小
	 */
	public static int Compare(byte[] common, byte[] banker, byte[] card) {
		banker = GameUtils.sortCards(ArrayUtils.addAll(common, banker));
		card = GameUtils.sortCards(ArrayUtils.addAll(common, card));
		int result_banker = getPokerCard(banker);
		int result_card = getPokerCard(card);

		if (result_banker != result_card) {// 牌型不一样
			return result_banker > result_card ? 1 : 0;
		} else {
			return compByCardsValue(banker, card, result_banker);
		}
	}

	/*
	 * 牌型比较,传入必须是五张牌呦。2张手牌，和五张公牌
	 */
	private static int compByCardsValue(byte[] b, byte[] c, int type) {
		/*
		 * High_card("高牌", 0), One_couple("一对", 1), Two_couple("二对", 2),
		 * Three_strip("三条", 3), Shun_card("顺子", 4), flush_card("同花", 5),
		 * full_card("葫芦",6), four_card("四条", 7), tong_card("同花顺", 8),
		 * huang_card("皇家同花顺", 9);
		 */

		// FIXME 同一个牌型的比较哟
		switch (type) {
		case 0:// 高牌

			break;
		case 1:// 一对
			break;
		case 2:// 二对
			break;
		case 3:// 三条
			break;
		case 4:// 顺子
			break;
		case 5:// 同花
			break;
		case 6:// 葫芦
			break;
		case 7:// 四条
			break;
		case 8:// 同花顺
			break;
		case 9:// 皇家同花顺,两个的话大小是同一样的。
			return 0;
		default:
			break;
		}
		return 0;
	}

	public static int getPokerCard(byte[] b) {
		int[] card = ProcessCard.byteCardToPoint(b);

		if (isBigTongHuaShun(b) > 0) {
			return PokerCard.huang_card.getIndex();// 皇家同花顺
		}
		if (isTongHuaShun(b) > 0) {// 同花顺
			return PokerCard.tong_card.getIndex();
		}
		if (isBomb(card) > 0) {// 四条
			return PokerCard.four_card.getIndex();
		}
		if (isHulu(card) > 0) {// 葫芦
			return PokerCard.full_card.getIndex();
		}
		if (isTongHuaShun(b) > 0) {// 同花
			return PokerCard.flush_card.getIndex();
		}
		if (isShun(card) > 0) {// 顺子
			return PokerCard.Shun_card.getIndex();
		}
		if (isSanTiao(card) > 0) {// 三条
			return PokerCard.Three_strip.getIndex();
		}
		if (isTwo(card) > 0) {// 两对
			return PokerCard.Two_couple.getIndex();
		}
		if (isOne(card) > 0) {// 1对
			return PokerCard.One_couple.getIndex();
		}

		return -1;
	}

	/*
	 * 是否是1对
	 */
	public static int isOne(int[] card) {
		int result = -1;
		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
		// 先分组統計個數
		for (int i = 0; i < card.length; i++) {
			if (!hm.containsKey(card[i])) {
				hm.put(card[i], 1);
			} else {
				hm.put(card[i], (hm.get(card[i])) + 1);
			}
		}
		for (int i : hm.keySet()) {
			if (hm.get(i) == 2) {
				return 1;
			}
		}

		return result;
	}

	/*
	 * 是否是两对
	 */
	public static int isTwo(int[] card) {
		int result = -1;
		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
		// 先分组統計個數
		for (int i = 0; i < card.length; i++) {
			if (!hm.containsKey(card[i])) {
				hm.put(card[i], 1);
			} else {
				hm.put(card[i], (hm.get(card[i])) + 1);
			}
		}
		int count = 0;
		for (int i : hm.keySet()) {
			if (hm.get(i) == 2) {
				count++;
			}
		}

		if (count == 2) {
			return 1;
		}

		return result;
	}

	public int compareTongHuaShun(byte[] b, byte[] c) {

		return 0;
	}

	/*
	 * 是否是同花顺
	 */
	public static int isTongHuaShun(byte[] cards) {
		// 得到花色，分组
		HashMap<Integer, List<Byte>> hm = new HashMap();
		for (byte b : cards) {
			int i = byteCardIntToCardType(b);
			if (!hm.containsKey(i)) {
				List<Byte> c = new ArrayList<>();
				c.add(b);
				hm.put(i, c);
			} else {
				List<Byte> r = hm.get(i);
				r.add(b);
				hm.put(i, r);
			}
		}
		List<Byte> rz = null;
		for (Integer d : hm.keySet()) {
			if (hm.get(d).size() > 4) {
				rz = hm.get(d);
			}
		}
		if (rz == null) {
			return -1;
		}
		byte[] strings = new byte[rz.size()];
		for (int i = 0; i < strings.length; i++) {
			strings[i] = rz.get(i);
		}

		if (isShun(ProcessCard.byteCardToPoint(strings)) > 0) {
			return 1;
		}

		return 0;
	}

	/*
	 * 是否是皇家同花顺
	 */
	public static int isBigTongHuaShun(byte[] cards) {
		// 得到花色，分组
		HashMap<Integer, List<Byte>> hm = new HashMap();
		for (byte b : cards) {
			int i = byteCardIntToCardType(b);
			if (!hm.containsKey(i)) {
				List<Byte> c = new ArrayList<>();
				c.add(b);
				hm.put(i, c);
			} else {
				List<Byte> r = hm.get(i);
				r.add(b);
				hm.put(i, r);
			}
		}
		List<Byte> rz = null;
		for (Integer d : hm.keySet()) {
			if (hm.get(d).size() > 4) {
				rz = hm.get(d);
			}
		}
		if (rz == null) {
			return -1;
		}
		byte[] strings = new byte[rz.size()];
		for (int i = 0; i < strings.length; i++) {
			strings[i] = rz.get(i);
		}
		int[] cd = ProcessCard.byteCardToPoint(strings);
		if (isShun(cd) > 0 && ArrayUtils.contains(cd, 14)) {
			return 1;
		}

		return 0;
	}

	/*
	 * 是否是同花
	 */
	public static int isTongHua(byte[] cards) {
		// 得到花色，分组
		HashMap<Integer, List<Byte>> hm = new HashMap();
		for (byte b : cards) {
			int i = byteCardIntToCardType(b);
			if (!hm.containsKey(i)) {
				List<Byte> c = new ArrayList<>();
				c.add(b);
				hm.put(i, c);
			} else {
				List<Byte> r = hm.get(i);
				r.add(b);
				hm.put(i, r);
			}
		}
		for (Integer d : hm.keySet()) {

			if (hm.get(d).size() > 4) {
				return 1;
			}

		}

		return -1;
	}

	/**
	 * 
	 * Description: 返回花色，0方片，1梅花，2红心，3黑桃
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param b
	 * @return
	 */
	public static int byteCardIntToCardType(byte b) {
		return b % 4;
	}

	/*
	 * 是否是顺子，这里有一个问题 就是A是k后面吗的 A K Q J 10
	 */
	public static int isShun(int[] cards) {
		for (int i = 0; i < cards.length; i++) {
			if (cards[i] == 1) {
				cards[i] = 14;
			}
		}

		Arrays.sort(cards);

		int max = 1; // 最大连续的数字个数
		int count = 1;
		for (int i = 0; i < cards.length - 1; ++i) {
			if (cards[i] + 1 == cards[i + 1]) {
				count++;
				max = max > count ? max : count;
			} else {
				count = 1;
			}
		}
		return max > 4 ? max : -1;
	}

	/*
	 * 是否是顺子，这里有一个问题 就是A是k后面吗的 A K Q J 10
	 */
	public static byte[] getShun(byte[] cards) {

		return null;

	}

	/*
	 * 是否是四条
	 */
	public static int isBomb(int[] card) {
		int result = 0;
		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
		// 先分组統計個數
		for (int i = 0; i < card.length; i++) {
			if (!hm.containsKey(card[i])) {
				hm.put(card[i], 1);
			} else {
				hm.put(card[i], (hm.get(card[i])) + 1);
				if (hm.get(card[i]) == 4) {
					return card[i];
				}
			}
		}
		return result;
	}

	/*
	 * 是否是葫芦
	 */
	public static int isHulu(int[] card) {
		int result = -1;
		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
		// 先分组統計個數
		for (int i = 0; i < card.length; i++) {
			if (!hm.containsKey(card[i])) {
				hm.put(card[i], 1);
			} else {
				hm.put(card[i], (hm.get(card[i])) + 1);
			}
		}
		boolean isTwo = false;
		boolean isThree = false;
		int total = 2;
		for (int i : hm.keySet()) {
			if (hm.get(i) > total) {
				isTwo = true;
				total = 100;
				continue;
			}
			if (hm.get(i) > 1) {
				isThree = true;
			}
		}
		if (isTwo && isThree) {
			return 1;
		}
		return 0;
	}

	/*
	 * 是否是三条
	 */
	public static int isSanTiao(int[] card) {
		int result = -1;
		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
		// 先分组統計個數
		for (int i = 0; i < card.length; i++) {
			if (!hm.containsKey(card[i])) {
				hm.put(card[i], 1);
			} else {
				hm.put(card[i], (hm.get(card[i])) + 1);
			}
		}
		for (int i : hm.keySet()) {
			if (hm.get(i) == 3) {
				return 1;
			}
		}

		return result;
	}

}
