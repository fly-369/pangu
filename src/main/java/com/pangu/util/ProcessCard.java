package com.pangu.util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.pangu.util.rules.model.Board;
import com.pangu.util.rules.model.NiuniuCard;
import com.pangu.util.rules.model.NiuniuPosition;

import lombok.extern.slf4j.Slf4j;

/**
 * @author 老王
 *   牛牛牌型的计算，大小的 比较
 */
@Slf4j
public class ProcessCard {

	private static final int higher = 10;

	/*
	 * 随机产生数组，测试用的
	 */
	public static byte[] randomCommon(int min, int max, int n) {
		if (n > (max - min + 1) || max < min) {
			return null;
		}
		byte[] result = new byte[n];
		int count = 0;
		while (count < n) {
			int num = (int) (Math.random() * (max - min)) + min;
			boolean flag = true;
			for (int j = 0; j < n; j++) {
				if (num == result[j]) {
					flag = false;
					break;
				}
			}
			if (flag) {
				result[count] = (byte) num;
				count++;
			}
		}
		return result;
	}

	public static void main(String[] args) {

		byte[] banker = { 50, 20, 11, 5, 4 };
		byte[] card = { 49, 39, 17, 8, 7 };
		ProcessCard.Compare(banker, card, 10);
	}

	/*
	 * 1.牛牛撞闲2v2,计算赔率
	 */
	public static int Compare(byte[] banker, byte[] card, int score) {
		int result_banker = getNiuCard(banker);
		int result_card = getNiuCard(card);
		if (higher == score) {
			if (result_banker != result_card) {// 牌型不一样
				return result_banker > result_card ? getMultiplesHigh(result_banker) : -getMultiplesHigh(result_card);
			} else {
				return compByCardsValue(banker[4], card[4]) ? getMultiplesHigh(result_banker)
						: -getMultiplesHigh(result_card);
			}
		} else {
			if (result_banker != result_card) {// 牌型不一样
				return result_banker > result_card ? getMultiples(result_banker) : -getMultiples(result_card);
			} else {
				return compByCardsValue(banker[4], card[4]) ? getMultiples(result_banker) : -getMultiples(result_card);
			}
		}

	}

	/**
	 * 高倍场的赔率计算
	 * @param condition
	 * @return
	 */
	public static int getMultiplesHigh(int condition) {
		int bet = 0;
		switch (condition) {
		case 1:
		case 0:
			bet = 1;
			break;
		case 2:
			bet = 2;
			break;
		case 3:
			bet = 3;
			break;
		case 4:
			bet = 4;
			break;
		case 5:
			bet = 5;
			break;
		case 6:
			bet = 6;
			break;
		case 7:
			bet = 7;
			break;
		case 8:
			bet = 8;
			break;
		case 9:
			bet = 9;
			break;
		case 10:
		case 11:
		case 12:
			bet = 10;
			break;
		default:
			break;
		}
		return bet;
	}

	/**
	 * 低倍场的赔率计算
	 * @param condition
	 * @return
	 */
	public static int getMultiples(int condition) {
		int bet = 0;
		switch (condition) {
		case 1:
		case 0:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			bet = 1;
			break;
		case 7:
		case 8:
		case 9:
			bet = 2;
			break;
		case 10:
			bet = 3;
			break;
		case 11:
			bet = 4;
			break;
		case 12:
			bet = 5;
			break;
		default:
			break;
		}
		return bet;
	}

	/**
	 * 比较两张牌的大小
	 */
	public static boolean compByCardsValue(byte banker, byte card) {
		int point_banker = byteCardToPoint(banker);
		int point_card = byteCardToPoint(card);
		if (point_banker != point_card) {
			return point_banker > point_card;
		}
		point_banker = byteCardToColor(banker);
		point_card = byteCardToColor(card);
		return point_banker > point_card;
	}

	public static int getNiuCard(byte[] b) {
		int[] card = byteCardToPoint(b);
		int result = 0;
		if ((result = isBomb(card)) > 0) {
			return NiuniuCard.BOMB.getIndex();
		}
		/*
		 * if ((result = isFiveSmall(card)) > 0) { return "五小牛:" + result; }
		 */
		if ((result = isFiveColor(card)) > 0) {
			return NiuniuCard.Five_Niu.getIndex();
		}
		/*
		 * if ((result = isFourColor(card)) > 0) { return "四花牛:" + result; }
		 */
		if ((result = isNiuniu(card)) > -1) {
			if (result == 0) {
				return NiuniuCard.NIU_NIU.getIndex();
			}
			return result;
		}
		return NiuniuCard.NOT_NIU.getIndex();
	}

	/*
	 * to 牛牛,判断炸弹，返回0 测没有炸弹，否则返回对应的炸弹点数 无关与花色， 所以转换成实际的点数进行比较
	 */
	public static int isBomb(int[] card) {
		int result = 0;
		HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
		// 先分组統計個數
		for (int i = 0; i < card.length; i++) {
			if (!hm.containsKey(card[i])) {
				hm.put(card[i], 1);
			} else {
				hm.put(card[i], (hm.get(card[i])) + 1);
				if (hm.get(card[i]) == 4) {
					return card[i];
				}
			}
		}
		return result;
	}

	/*
	 * 五小牛判断
	 */
	public static int isFiveSmall(int[] card) {
		int result = 0;
		for (int i : card) {
			if (i > 4) {
				return 0;
			}
			result = result + i;
		}
		if (result > 9) {
			return 0;
		}
		return result;
	}

	/*
	 * 五花牛判断 编码在40到51中间 或者点数在11~13
	 */
	public static int isFiveColor(int[] card) {
		for (int c : card) {
			if (11 > c || c > 13) {
				return 0;
			}
		}
		return 1;
	}

	/*
	 * 四花牛判断 点数在11~13 有一个是10
	 */
	public static int isFourColor(int[] card) {
		int i = 0, j = 0;
		for (int c : card) {
			if (10 < c && c < 14) {
				i++;
			} else {
				if (c != 10) {
					return 0;
				}
				j++;
			}
		}
		if (i == 4) {
			return 1;
		}
		return 0;
	}

	/*
	 * 牛牛int[] cards = { 3, 3, 4, 11, 10 };
	 */
	public static int isNiuniu(int[] card) {
		int[] cards = byteCardToPoint(card);// 对象传递，不去改变card
		int lave = 0;
		for (int i : cards) {
			lave += i;
		}
		lave = lave % 10;
		for (int i = 0; i < cards.length - 1; i++) {
			for (int j = i + 1; j < cards.length; j++) {
				if ((cards[i] + cards[j]) % 10 == lave) {
					if (lave == 0)
						return 0;
					else
						return lave;
				}
			}
		}
		return -1;
	}

	/*
	 * to 牛牛,没有大小王 转换为点数
	 */
	public static int[] byteCardToPoint(int[] b) {
		int[] result = new int[b.length];
		int c = 0;
		for (int i : b) {
			if (i > 10)
				i = 10;
			result[c] = i;
			c++;
		}

		return result;

	}

	/*
	 * to 牛牛,没有大小王 转换为点数
	 */
	public static int byteCardToPoint(byte b) {
		return (b - b % 4) / 4 + 1;
	}

	/*
	 * to 牛牛,转换为花色
	 */
	public static int byteCardToColor(byte b) {
		return b % 4;
	}

	/*
	 * to 牛牛,没有大小王 转换为点数
	 */
	public static int[] byteCardToPoint(byte[] b) {
		int[] card = new int[b.length];
		int i = 0;
		for (byte c : b) {
			card[i] = byteCardToPoint(c);
			i++;
		}
		return card;
	}

	public static String byteCardToString(byte[] b) {
		StringBuilder sb = new StringBuilder();
		for (byte c : b) {
			sb.append(c);
			sb.append(",");
		}
		return sb.toString();

	}

	public static String byteCardToString(int[] b) {
		StringBuilder sb = new StringBuilder();
		for (int c : b) {
			sb.append(c);
			sb.append(",");
		}
		return sb.toString();

	}

	public static String byteRedBlackCardToString(byte[] redCards, byte[] blackCards) {
		StringBuilder sb = new StringBuilder();
		sb.append(redCards[0] + ",");
		sb.append(redCards[1] + ",");
		sb.append(redCards[2] + "|");
		sb.append(blackCards[0] + ",");
		sb.append(blackCards[1] + ",");
		sb.append(blackCards[2]);
		// sb.append(parseRedBlackCartTpye(redCards)+"|");
		// sb.append(parseRedBlackCartTpye(blackCards)+"|");
		return sb.toString();

	}

	public static String byteCardToString(byte b) {
		String cards = "";
		String cardType = "";
		if (b == 52) {
			return "小王";
		}
		if (b == 53) {
			return "大王";
		}
		if (b % 4 == 0) {
			cardType = "方片";
		} else if (b % 4 == 1) {
			cardType = "梅花";
		} else if (b % 4 == 2) {
			cardType = "红心";
		} else if (b % 4 == 3) {
			cardType = "黑桃";
		}

		int v = 0;
		v = (b - b % 4) / 4 + 1;

		if (v == 14) {
			v = 1;
		}
		if (v == 15) {
			v = 2;
		}

		// cards = b + "=" + cardType + v;
		cards = cardType + v;
		return cards;
	}

	public static int byteCardToInt(byte b) {
		int cards = 0;

		int v = 0;
		v = (b - b % 4) / 4 + 1;

		if (v == 14) {
			v = 1;
		}
		if (v == 15) {
			v = 2;
		}

		cards = v;
		// if (cards == 10) {
		// cards = 10;
		// }
		if (cards == 11) {
			cards = 20;
		}
		if (cards == 12) {
			cards = 30;
		}
		if (cards == 13) {
			cards = 40;
		}
		return cards;
	}

	/**
	 * 
	 * Description: 解析三公牌型，基本上三公牌型分为4、爆玖，3、炸弹，2、三公，1、点数
	 * 
	 * @author abo
	 * @date 2018年3月28日
	 * @param cards
	 * @return
	 */
	public static int parseSangongCartTpye(byte[] cards) {
		int s = 0;
		if (isBaojiu(cards)) {
			s = 4;
		} else if (isBomb(cards)) {
			s = 3;
		} else if (isSangong(cards)) {
			s = 2;
		} else {
			s = 1;
		}
		return s;
	}

	/**
	 * 
	 * Description:由任意三张不相同的公仔牌构成的牌型，比如KKQ，KQJ
	 * 
	 * @author abo
	 * @date 2018年3月28日
	 * @param cards
	 * @return
	 */
	private static boolean isSangong(byte[] cards) {
		for (int i = 0; i < cards.length; i++) {
			int card = ProcessCard.byteCardToInt(cards[i]);
			if (card <= 10) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * Description:由点数相同的三张牌组成的牌型，比如QQQ，222。
	 * 
	 * @author abo
	 * @date 2018年3月28日
	 * @param cards
	 * @return
	 */
	private static boolean isBomb(byte[] cards) {
		int hisCard = 0;
		for (int i = 0; i < cards.length; i++) {
			int card = ProcessCard.byteCardToInt(cards[i]);
			// i==0的时候初始化上张牌
			if (i == 0) {
				hisCard = card;
				continue;
			}
			// 第二轮开始每轮比较，如果不等于就return false
			if (hisCard != card) {
				return false;
			}

		}
		return true;
	}

	/**
	 * 
	 * Description:由任意三张3构成的牌型，比如333
	 * 
	 * @author abo
	 * @date 2018年3月28日
	 * @param cards
	 * @return
	 */
	private static boolean isBaojiu(byte[] cards) {
		for (int i = 0; i < cards.length; i++) {
			int card = ProcessCard.byteCardToInt(cards[i]);
			if (card != 3) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * Description: 比较金花
	 * 
	 * @author abo
	 * @date 2018年4月2日
	 * @param redCards
	 * @param blackCards
	 * @return
	 */
	public static Map<String, Object> compareRedBlackCards(byte[] redCards, byte[] blackCards) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 封装必要的几种类型数据，解析红黑大战牌型，基本上三公牌型分为6、豹子5、顺金4、金花，3、顺子，2、对子，1、单张
		Map<String, Object> redMap = redBlackTransformation(redCards);
		Map<String, Object> blackMap = redBlackTransformation(blackCards);

		int redCardType = (int) redMap.get("cardType");
		int blackCardType = (int) blackMap.get("cardType");
		// 点数数组2-14
		int[] redNewCards = (int[]) redMap.get("newCards");
		int[] blackNewCard = (int[]) blackMap.get("newCards");
		// 花色数组
		int[] redCardColor = (int[]) redMap.get("cardColor");
		int[] blackCardColor = (int[]) blackMap.get("cardColor");

		int flag = 99;// 默认99
		// 先比较牌型
		if (redCardType > blackCardType) {
			flag = 1;
		} else if (redCardType == blackCardType) { // 花型一样
			if (redCardType == 6) {
				// 如果是豹子，就取第一位进行比较
				int redCard = redNewCards[2];
				int blackCard = blackNewCard[2];
				log.info(redCard + ":" + blackCard);
				if (redCard > blackCard) {
					flag = 1;
				} else if (redCard < blackCard) {
					flag = 2;
				}
			} else if (redCardType == 5) {
				// 顺金
				// 判断是不是特殊的顺子，比如A23是特殊的顺子的话就要重新排列
				redNewCards = specialShunZiFormat(redNewCards);
				blackNewCard = specialShunZiFormat(blackNewCard);
				// 比较最大的一张牌，如果一样就从最大的牌开始比花色
				if (redNewCards[2] > blackNewCard[2]) {
					flag = 1;
				} else if (redNewCards[2] < blackNewCard[2]) {
					flag = 2;
				} else {
					// 比较最大一张的花色
					if (redCardColor[2] > blackCardColor[2]) {
						flag = 1;
					} else if (redCardColor[2] < blackCardColor[2]) {
						flag = 2;
					}
				}
			} else if (redCardType == 4 || redCardType == 1) {
				// 金花和单张
				flag = comparePoint(redNewCards, blackNewCard, redCardColor, blackCardColor);
				if (flag == 0) {
					flag = compareColor(redNewCards, blackNewCard, redCardColor, blackCardColor);
				}
			} else if (redCardType == 3) {
				// 判断是不是特殊的顺子，比如A23是特殊的顺子的话就要重新排列
				redNewCards = specialShunZiFormat(redNewCards);
				blackNewCard = specialShunZiFormat(blackNewCard);
				// 顺子
				// 比较最大的一张牌，如果一样就从最大的牌开始比花色
				if (redNewCards[2] > blackNewCard[2]) {
					flag = 1;
				} else if (redNewCards[2] < blackNewCard[2]) {
					flag = 2;
				} else {
					// 如果第三张一样那就判断花色
					flag = compareColor(redNewCards, blackNewCard, redCardColor, blackCardColor);
				}
			} else if (redCardType == 2) {
				// 对子比较,对子一样比较第三张牌，如果第三都一样的话就比较数组最后一位花色
				int[] redCardTemp = redBlackDoubleMaxNumByArray(redNewCards);
				int[] blackCardTemp = redBlackDoubleMaxNumByArray(blackNewCard);
				// 比较对子大小
				if (redCardTemp[2] > blackCardTemp[2]) {
					flag = 1;
				} else if (redCardTemp[2] < blackCardTemp[2]) {
					flag = 2;
				} else {
					// 如果对子一样比较第三张
					if (redCardTemp[0] > blackCardTemp[0]) {
						flag = 1;
					} else if (redCardTemp[0] < blackCardTemp[0]) {
						flag = 2;
					} else {
						// 如果第三张一样那就判断花色
						flag = compareColor(redNewCards, blackNewCard, redCardColor, blackCardColor);
					}
				}
			}
		} else {
			flag = 2;
		}
		if (flag == 1) {
			log.info("red win");
		} else if (flag == 2) {
			log.info("black win");
		}
		map.put("redCardType", redCardType);
		map.put("blackCardType", blackCardType);
		map.put("flag", flag);
		// 点数数组0-51
		int[] redOldCards = (int[]) redMap.get("oldCards");
		int[] blackOldCard = (int[]) blackMap.get("oldCards");
		// map.put("cardsStr", byteRedBlackCardToString(redCards, blackCards));
		map.put("cardsStr", intRedBlackCardToString(redOldCards, blackOldCard));
//		log.info("newCode==============" + byteRedBlackCardToString(redCards, blackCards));
//		log.info("oldCode==============" + intRedBlackCardToString(redOldCards, blackOldCard));

		return map;
	}

	public static String intRedBlackCardToString(int[] redCards, int[] blackCards) {
		StringBuilder sb = new StringBuilder();
		sb.append(redCards[0] > 51 ? redCards[0] - 52 + "," : redCards[0] + ",");
		sb.append(redCards[1] > 51 ? redCards[1] - 52 + "," : redCards[1] + ",");
		sb.append(redCards[2] > 51 ? redCards[2] - 52 + "," : redCards[2] + "|");
		sb.append(blackCards[0] > 51 ? blackCards[0] - 52 + "," : blackCards[0] + ",");
		sb.append(blackCards[1] > 51 ? blackCards[1] - 52 + "," : blackCards[1] + ",");
		sb.append(blackCards[2] > 51 ? blackCards[2] - 52 + "," : blackCards[2]);
		// sb.append(parseRedBlackCartTpye(redCards)+"|");
		// sb.append(parseRedBlackCartTpye(blackCards)+"|");
		return sb.toString();

	}

	/**
	 * 
	 * Description: 重新排列一下
	 * 
	 * @author abo
	 * @date 2018年4月3日
	 * @param cards
	 * @return
	 */
	public static int[] specialShunZiFormat(int[] cards) {
		int one = cards[0];
		int two = cards[1];
		int three = cards[2];
		if (three == 14 && one == 2 && two == 3) {
			cards[0] = three;
			cards[1] = one;
			cards[2] = two;
		}
		return cards;
	}

	/**
	 * 
	 * Description: 逐一从最大位点数比较下来，金花和单牌可以用此 方法
	 * 
	 * @author abo
	 * @date 2018年4月2日
	 * @param redNewCards
	 * @param blackNewCard
	 * @param redCardColor
	 * @param blackCardColor
	 * @return 1为红胜2为黑胜0为还需要继续比较
	 */
	public static int comparePoint(int[] redNewCards, int[] blackNewCard, int[] redCardColor, int[] blackCardColor) {
		int flag = 0;
		// 逐一从最大的一张开始比较
		if (redNewCards[2] > blackNewCard[2]) {
			flag = 1;
		} else if (redNewCards[2] < blackNewCard[2]) {
			flag = 2;
		} else {
			if (redNewCards[1] > blackNewCard[1]) {
				flag = 1;
			} else if (redNewCards[1] < blackNewCard[1]) {
				flag = 2;
			} else {
				if (redNewCards[0] > blackNewCard[0]) {
					flag = 1;
				} else if (redNewCards[0] < blackNewCard[0]) {
					flag = 2;
				}
			}
		}
		return flag;
	}

	/**
	 * 
	 * Description:如果前面点数一样就逐一从最大位开始比较花色
	 * 
	 * @author abo
	 * @date 2018年4月2日
	 * @param redNewCards
	 * @param blackNewCard
	 * @param redCardColor
	 * @param blackCardColor
	 * @return 1为红胜2为黑胜
	 */
	public static int compareColor(int[] redNewCards, int[] blackNewCard, int[] redCardColor, int[] blackCardColor) {
		int flag = 0;
		// 如果三张一样就开始逐一比较花色
		// 比较最大一张的花色
		if (redCardColor[2] > blackCardColor[2]) {
			flag = 1;
		} else if (redCardColor[2] < blackCardColor[2]) {
			flag = 2;
		} else {
			// 如果一样比较第二张的花色
			if (redCardColor[1] > blackCardColor[1]) {
				flag = 1;
			} else if (redCardColor[1] < blackCardColor[1]) {
				flag = 2;
			} else {
				// 如果一样比较第一张的花色
				if (redCardColor[0] > blackCardColor[0]) {
					flag = 1;
				} else {
					flag = 2;
				}
			}
		}
		return flag;
	}

	/**
	 * 
	 * Description:重新对卡牌数组进行计算， 把对子放在序列的二三位，第一位放单张
	 * 
	 * @author abo
	 * @date 2018年4月2日
	 * @param cards
	 * @return
	 */
	public static int[] redBlackDoubleMaxNumByArray(int[] cards) {
		int[] array = new int[3];
		// 上一次的值，用于判断对子的位置，是AAB还是ABB
		// int one = byteCardToRedBlackInt((byte) cards[0]);
		// int two = byteCardToRedBlackInt((byte) cards[1]);
		// int three = byteCardToRedBlackInt((byte) cards[2]);
		int one = cards[0];
		int two = cards[1];
		int three = cards[2];
		// AAB
		if (one == two) {
			array[0] = three;
			array[1] = one;
			array[2] = two;
		}
		// ABB
		if (two == three) {
			array[0] = one;
			array[1] = two;
			array[2] = three;
		}
		// ABA理论上不会有这种模式的对子
		if (one == three) {
			array[0] = two;
			array[1] = one;
			array[2] = three;
		}
		return array;
	}

	/**
	 * 
	 * Description: 把花色、类型、数值重新封装map
	 * 
	 * @author abo
	 * @date 2018年4月2日
	 * @param card
	 * @return
	 */
	public static Map<String, Object> redBlackTransformation(byte[] card) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 初始化数组，先把A加52
		card = (byte[]) byteCardToRedBlackSort(card);
		map.put("cardType", parseRedBlackCartTpye(card));// 6、豹子5、顺金4、金花，3、顺子，2、对子，1、单张
		int[] newCards = new int[3];
		int[] cardColorArray = new int[3];
		int[] oldCards = new int[3];
		for (int i = 0; i < card.length; i++) {
			oldCards[i] = card[i];
			newCards[i] = byteCardToRedBlackInt(card[i]);
			cardColorArray[i] = byteCardIntToCardType(card[i]);
		}
		map.put("oldCards", oldCards);// 转换A后的原始值0-51，--A的话加52，如方片A是0，那么这里就是0+52
		Arrays.sort(newCards);
		map.put("newCards", newCards);// 转换成2-14的集合值，这里的A是14
		map.put("cardColor", cardColorArray);// 花色
		return map;
	}

	/**
	 * 
	 * Description:重新封装byte数组，把A的点数加52点，方便后续比较
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param cards
	 * @return
	 */
	public static byte[] byteCardToRedBlackSort(byte[] cards) {
		for (int i = 0; i < cards.length; i++) {
			int s = cards[i];
			if (s < 4) {// 如果是A那么就加52
				cards[i] = (byte) (cards[i] + 52);
			}
		}
		Arrays.sort(cards);// 重新排序
		return cards;
	}

	/**
	 * 
	 * Description:返回原始数据，A改为14，最大
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param b
	 * @return
	 */
	public static int byteCardToRedBlackInt(byte b) {
		int cards = 0;
		int v = 0;
		v = (b - b % 4) / 4 + 1;
		cards = v;
		if (cards == 1) {
			cards = 14;
		}
		return cards;
	}

	/**
	 * 
	 * Description: 返回花色，0方片，1梅花，2红心，3黑桃
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param b
	 * @return
	 */
	public static int byteCardIntToCardType(byte b) {
		return b % 4;
	}

	/**
	 * 
	 * Description: 解析红黑大战牌型，基本上三公牌型分为6、豹子5、顺金4、金花，3、顺子，2、对子，1、单张
	 * 
	 * @author abo
	 * @date 2018年3月28日
	 * @param cards
	 * @return
	 */
	public static int parseRedBlackCartTpye(byte[] cards) {
		Arrays.sort(cards);// 排序
		String cardsStr = byteCardToRedBlackString(cards);
		int s = 0;
		if (isBomb(cards)) {
			s = 6;// 6、豹子
			log.info("豹子：" + cardsStr);
		} else if (isShunJin(cards)) {
			s = 5;// 5、顺金
			log.info("顺金：" + cardsStr);
		} else if (isJinHua(cards)) {
			s = 4;// 4、金花
			log.info("金花：" + cardsStr);
		} else if (isShunZi(cards)) {
			s = 3;// 3、顺子
			log.info("顺子：" + cardsStr);
		} else if (isDoubleCard(cards)) {
			s = 2;// 2、对子
			log.info("对子：" + cardsStr);
		} else {
			s = 1;// 1、单张
			log.info("单张：" + cardsStr);
		}
		return s;
	}

	/**
	 * 
	 * Description: 判断是不是对子
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param cards
	 * @return
	 */
	private static boolean isDoubleCard(byte[] cards) {
		int fristCard = byteCardToRedBlackInt(cards[0]);
		int secondCard = byteCardToRedBlackInt(cards[1]);
		int threeCard = byteCardToRedBlackInt(cards[2]);
		if (fristCard == secondCard) {
			return true;
		}
		if (fristCard == threeCard) {
			return true;
		}
		if (secondCard == threeCard) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * Description: 判断是不是顺子
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param cards
	 * @return
	 */
	private static boolean isShunZi(byte[] cards) {
		int hisCard = 0;
		for (int i = 0; i < cards.length; i++) {
			int card = byteCardToRedBlackInt(cards[i]);
			if (i == 0) {
				hisCard = card;
				continue;
			}
			// 如果不是等于1那么就不是顺子，或者非A23的情况
			if ((card - hisCard) != 1 && (card - hisCard) != 11) {
				return false;
			}
			hisCard = card;
		}
		return true;
	}

	/**
	 * 
	 * Description: 判断是不是金花
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param cards
	 * @return
	 */
	private static boolean isJinHua(byte[] cards) {
		int hisCard = 99;
		for (int i = 0; i < cards.length; i++) {
			int card = byteCardIntToCardType(cards[i]);
			if (i == 0) {
				hisCard = card;
				continue;
			}
			// 判断是不是同一花色
			if (card != hisCard) {
				return false;
			}
			hisCard = card;
		}
		return true;
	}

	/**
	 * 
	 * Description: 判断是否是顺金：判断是不是金花，是金花以后判断是不是顺子
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param cards
	 * @return
	 */
	private static boolean isShunJin(byte[] cards) {
		if (!isJinHua(cards)) {
			return false;
		}
		if (!isShunZi(cards)) {
			return false;
		}
		return true;
	}

	public static String byteCardToRedBlackString(byte[] b) {
		StringBuilder sb = new StringBuilder();
		for (byte c : b) {
			sb.append("[");
			sb.append(byteCardToRedBlackString(c));
			sb.append("],");

		}
		return sb.toString();
	}

	public static String byteCardToRedBlackString(byte b) {
		String cards = "";
		String cardType = "";
		if (b % 4 == 0) {
			cardType = "方片";
		} else if (b % 4 == 1) {
			cardType = "梅花";
		} else if (b % 4 == 2) {
			cardType = "红心";
		} else if (b % 4 == 3) {
			cardType = "黑桃";
		}
		int v = 0;
		v = (b - b % 4) / 4 + 1;

		if (v == 1) {
			v = 14;
		}
		cards = cardType + v;
		return cards;
	}

	/**
	 * 
	 * Description: 谁是赢家，红方还是黑方
	 * 
	 * @author abo
	 * @date 2018年3月28日
	 * @param plays
	 * @return 返回red或者black
	 */
	public static Map<String, Object> whoIsWinner(Board board) {
		NiuniuPosition[] nps = board.getNiuniuPosition();
		NiuniuPosition red = nps[0];
		NiuniuPosition black = nps[1];
		// 解析红黑大战牌型，基本上三公牌型分为6、豹子5、顺金4、金花，3、顺子，2、对子，1、单张
		Map<String, Object> map = ProcessCard.compareRedBlackCards(red.getCards(), black.getCards());
		return map;
	}

	/**
	 * 
	 * Description:金花开牌信息
	 * 
	 * @author abo
	 * @date 2018年4月17日
	 * @param cards
	 * @return
	 */
	public static Map<String, Object> openJinhuaCards(byte[] cards) {
		// 封装必要的几种类型数据，解析红黑大战牌型，基本上三公牌型分为6、豹子5、顺金4、金花，3、顺子，2、对子，1、单张
		Map<String, Object> map = redBlackTransformation(cards);
		map.put("cardsStr", byteCardToRedBlackString(cards));
		return map;
	}
}
