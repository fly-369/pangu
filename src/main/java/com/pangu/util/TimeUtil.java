package com.pangu.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TimeUtil {
	public static final String PATTERN_DATE = "yyyy-MM-dd";
	public static final String PATTERN_STANDARD = "yyyy-MM-dd HH:mm:ss";

	/**
	 * 
	 * Description: 传入秒数的倒计时
	 * 
	 * @author abo
	 * @date 2018年4月5日
	 * @param time
	 *            秒数
	 */
	public static void countDown(int time) {
		for (int i = 0; i < time; i++) {
			try {
				Thread.sleep(1000);
				// log.info("倒计时开始：" + (time - i));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 获取日期格式的字符串 add by maosheng 参数为整数则往后推多少天，参数为负数则往前面推多少天
	 */
	public static String getDateString(Date date, Integer year, Integer month, Integer day, Integer hour,
			Integer minute, Integer second, String pattern) {// 正数表示往后，负数表示往前
		SimpleDateFormat formatter = new SimpleDateFormat(pattern);
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);// 取时间
		if (year != null)
			calendar.add(calendar.YEAR, year);
		if (month != null)
			calendar.add(calendar.MONTH, month);
		if (day != null)
			calendar.add(calendar.DATE, day);
		if (hour != null)
			calendar.add(calendar.HOUR_OF_DAY, hour);
		if (minute != null)
			calendar.add(calendar.MINUTE, minute);
		if (second != null)
			calendar.add(calendar.SECOND, second);
		date = calendar.getTime();
		return formatter.format(date);
	}

	public static void main(String[] args) {
		String s = TimeUtil.getDateString(new Date(), null, null, null, null, null, 15, TimeUtil.PATTERN_STANDARD);
		System.out.println(new Date());
		System.out.println(s);
	}
}
