package com.pangu.util;

import com.alibaba.fastjson.JSON;
import com.pangu.util.rules.model.NiuniuPosition;

/**
 * @ClassName: PokerUtilsProTest
 * @Description: 牌型算法测试类
 * @author 老王
 * @date 2018年6月6日 下午3:38:19
 *
 */
public class PokerUtilsProTest {

	public static void main(String[] args) {
		NiuniuPosition[] p= getNP();
		byte[] cards = PokerUtilsPro.shuffleAiWinner(p);
		System.out.println("改变后："+JSON.toJSON(cards));

	}

	/**
	 * @Description :随机初始化位置
	 * @author 老王
	 * 
	 */
	public static NiuniuPosition[] getNP() {
		NiuniuPosition[] p = new NiuniuPosition[9];
		
		for (int i = 0; i < p.length; i++) {
			NiuniuPosition t = new NiuniuPosition();
			t.setPlayer(i+"");
			p[i] =t;
		}
		p[3].setAi(true);
		return p;

	}

}
