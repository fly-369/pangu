package com.pangu.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.pangu.util.rules.model.NiuniuPosition;

/**
  * @ClassName: NiuNiuUtilsPro
  * @Description: 牛牛洗牌算法
  * @author 老王
  * @date 2018年6月7日 下午1:45:12
  *
  */
public class NiuNiuUtilsPro {

	private static final int shuffletimes = 20;

		/**
			 * @Description :牛牛正常洗牌策略
			 * @author 老王
			 *              <p>
			 */
	public static NiuniuPosition[] shuffle(int score) {
		// 生成52张牌
		List<Byte> temp = new ArrayList<Byte>();
		for (int i = 0; i < 52; i++) {
			temp.add((byte) i);
		}

		for (int i = 0; i < shuffletimes; i++) {
			Collections.shuffle(temp);
		}
		byte[] cards = new byte[52];
		for (int i = 0; i < temp.size(); i++) {
			cards[i] = temp.get(i);
		}
		NiuniuPosition[] np = new NiuniuPosition[5];
		for (int i = 0; i < 5; i++) {
			NiuniuPosition n = new NiuniuPosition();
			if (i == 0)
				n.setIsbanker(true);
			n.setPosition(i);
			n.setCards(new byte[5]);
			np[i] = n;
		}
		// gameRoom.getCardsnum()//写死算了 ，反正牛牛都是五张吧！
		for (int i = 0; i < 5 * 5; i++) {
			int pos = i % 5;
			np[pos].getCards()[i / 5] = cards[i];
		}
		for (NiuniuPosition tempPlayer : np) {
			Arrays.sort(tempPlayer.getCards());// 排序。很重要
			tempPlayer.setCards(GameUtils.reverseCards(tempPlayer.getCards()));
			tempPlayer.setCards(tempPlayer.getCards());
		}
		NiuniuPosition banker_seat = null;
		// 得到庄家
		for (NiuniuPosition p : np) {
			if (p.isIsbanker()) {
				banker_seat = p;
			}
		}
		for (NiuniuPosition player : np) {
			int bet = ProcessCard.Compare(banker_seat.getCards(), player.getCards(), score);
			if (!player.isIsbanker()) {
				player.setOdds(bet);
			}
			player.setPatterns(ProcessCard.getNiuCard(player.getCards()));
		}

		return np;
	}

	/**
	 * @Description :非正常洗牌，
	 * seat：
	 *              <P>
	 */
	public static NiuniuPosition[] shuffle(int score, int seat) {
		NiuniuPosition[] np = null;
		if (seat < 1) {
			return shuffle(score);
		}
		do {
			np = shuffle(score);
		} while (np[seat].getOdds() > 0);
		return np;
	}

}
