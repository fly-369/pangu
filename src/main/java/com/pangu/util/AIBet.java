package com.pangu.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.model.PlayerRecord;
import com.pangu.core.engine.game.model.PlayerRecordHonghei;
import com.pangu.core.engine.game.model.PlayerRecordNiuniu;
import com.pangu.web.model.PlayUserClient;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * Description:模拟ai自动下注
 *
 * @author abo
 * @date 2018年3月30日
 */
@Slf4j
public class AIBet {

	/**
	 * 
	 * Description:随机数进行是否下注判断，这里根据随机数的单双判断，双下注，单不下注
	 * 
	 * @author abo
	 * @date 2018年3月30日
	 * @return
	 */
	public static boolean isBet() {
		long coinRandom = Math.round(Math.random() * 10);
		// 对4取模有65-86
		// 对3取模有56-76
		// 对2取模有37-63
		if (coinRandom % 4 == 0) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * Description: AI模拟下注，只处理AI的下注
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param roomId
	 *            房间id
	 * @param orgi
	 * @param betClass
	 *            下注类型，这里是数字，比如红黑有3种类型，那么传入3，百人牛牛有4种类型，那么传入4
	 * @return
	 */
	public static List<PlayerRecord>  bet(String roomId, String orgi, int betClass) {
		List<PlayerRecord> result = new ArrayList<>();
		List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(roomId, orgi);
		int count = 0;
		for (PlayUserClient playUserClient : list) {
			// 只生成ai的下注,不是ai这里就退出
			if (!playUserClient.getPlayertype().equals(BMDataContext.BEIMI_GAME_AI)) {
				continue;
			}
			boolean flag = isBet();
			// 根据随机数去判断这把是否下注
			if (!flag) {
				count++;
				// log.info(playUserClient.getUsername() + "放弃下注");
				continue;
			}
			// 下注类型
			// long randomBetClass = Math.round(Math.random() * (betClass) + 1);
			int randomBetClass = new Random().nextInt(betClass) + 1;
			// randomBetClass += 1;// 因为索引从0开始，这里+1
			// 根据钱包里面钱的随机数进行下注，看后期这个数是否还除以10.不然的话太大
			long coinRandom = Math.round(Math.random() * playUserClient.getGoldcoins() / 10 + 1);
			PlayerRecord pr = new PlayerRecord();
			pr.setRoomId(roomId);
			pr.setOrgi("beimi");
			pr.setGameSubType(randomBetClass + "");
			pr.setPlayerId(playUserClient.getId());
			pr.setCoin(coinRandom);
			pr.setIsBanker("1");// 0庄，1闲
			pr.setPlayerType(BMDataContext.PlayerTypeEnum.AI.toString());
			pr.setPlayerType("2");
			// 下注以后把下注内容放到缓存，等会去结算
			List<PlayerRecord> prList = new ArrayList<>();
			prList.add(pr);
			result.add(pr);
			CacheHelper.getGameRecordCacheBean().put(playUserClient.getId(), prList, orgi);
		
		}
		log.info("=================放弃下注人数：" + count);
		return result;
	}
	
	public static List<PlayerRecordNiuniu>  betNiuNiu(String roomId, String orgi, int betClass) {
		List<PlayerRecordNiuniu> result = new ArrayList<>();
		List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(roomId, orgi);
		int count = 0;
		for (PlayUserClient playUserClient : list) {
			// 只生成ai的下注,不是ai这里就退出
			if (!playUserClient.getPlayertype().equals(BMDataContext.BEIMI_GAME_AI)) {
				continue;
			}
			boolean flag = isBet();
			// 根据随机数去判断这把是否下注
			if (!flag) {
				count++;
				// log.info(playUserClient.getUsername() + "放弃下注");
				continue;
			}
			// 下注类型
			// long randomBetClass = Math.round(Math.random() * (betClass) + 1);
			int randomBetClass = new Random().nextInt(betClass) + 1;
			// randomBetClass += 1;// 因为索引从0开始，这里+1
			// 根据钱包里面钱的随机数进行下注，看后期这个数是否还除以10.不然的话太大
			long coinRandom = Math.round(Math.random() * (playUserClient.getGoldcoins()/10 )/ 10 + 1);
			PlayerRecordNiuniu pr = new PlayerRecordNiuniu();
			pr.setRoomId(roomId);
			pr.setOrgi(playUserClient.getOrgi());
			pr.setGameSubType(randomBetClass + "");
			pr.setPlayerId(playUserClient.getId());
			pr.setCoin(coinRandom);
			pr.setIsBanker("1");// 0庄，1闲
			pr.setPlayerType(BMDataContext.PlayerTypeEnum.AI.toString());
			// 下注以后把下注内容放到缓存，等会去结算
			List<PlayerRecordNiuniu> prList = new ArrayList<>();
			prList.add(pr);
			result.add(pr);
			CacheHelper.getGameRecordNiuNiuCacheBean().put(playUserClient.getId(), prList, orgi);
		}
		log.info("=================放弃下注人数：" + count);
		return result;
	}

	
	/**
	 * 
	 * Description: AI模拟下注，只处理AI的下注
	 * 
	 * @author abo
	 * @date 2018年3月31日
	 * @param roomId
	 *            房间id
	 * @param orgi
	 * @param betClass
	 *            下注类型，这里是数字，比如红黑有3种类型，那么传入3，百人牛牛有4种类型，那么传入4
	 * @return
	 */
	public static List<PlayerRecordHonghei>  hongheiBet(String roomId, String orgi, int betClass) {
		List<PlayerRecordHonghei> result = new ArrayList<PlayerRecordHonghei>();
		List<PlayUserClient> list = CacheHelper.getGamePlayerCacheBean().getCacheObject(roomId, orgi);
		int count = 0;
		for (PlayUserClient playUserClient : list) {
			// 只生成ai的下注,不是ai这里就退出
			if (!playUserClient.getPlayertype().equals(BMDataContext.BEIMI_GAME_AI)) {
				continue;
			}
			boolean flag = isBet();
			// 根据随机数去判断这把是否下注
			if (!flag) {
				count++;
				// log.info(playUserClient.getUsername() + "放弃下注");
				continue;
			}
			// 下注类型
			// long randomBetClass = Math.round(Math.random() * (betClass) + 1);
			int randomBetClass = new Random().nextInt(betClass) + 1;
			// randomBetClass += 1;// 因为索引从0开始，这里+1
			// 根据钱包里面钱的随机数进行下注，看后期这个数是否还除以10.不然的话太大
			long coinRandom = Math.round(Math.random() * playUserClient.getGoldcoins() / 100 + 1);
			PlayerRecordHonghei pr = new PlayerRecordHonghei();
			pr.setRoomId(roomId);
			pr.setOrgi("beimi");
			pr.setGameSubType(randomBetClass + "");
			pr.setPlayerId(playUserClient.getId());
			pr.setCoin(coinRandom);
			pr.setIsBanker("1");// 0庄，1闲
			pr.setPlayerType(BMDataContext.PlayerTypeEnum.AI.toString());
			pr.setPlayerType("2");
			// 下注以后把下注内容放到缓存，等会去结算
			List<PlayerRecordHonghei> prList = new ArrayList<PlayerRecordHonghei>();
			prList.add(pr);
			result.add(pr);
			CacheHelper.getGameRecordHongheiCacheBean().put(playUserClient.getId(), prList, orgi);
		
		}
		log.info("=================放弃下注人数：" + count);
		return result;
	}

}