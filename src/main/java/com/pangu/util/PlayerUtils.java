package com.pangu.util;

import java.util.ArrayList;
import java.util.List;

import com.pangu.web.model.PlayUserClient;

/**
 * @ClassName: PlayerUtils
 * @Description: 
 * @author 老王
 * @date 2018年4月5日 下午7:04:24
 *
 */
public class PlayerUtils {

	public static void main(String[] args) {
		PlayUserClient a1 = new PlayUserClient();
		a1.setGoldcoins(4);
		PlayUserClient a2 = new PlayUserClient();
		a2.setGoldcoins(2);
		PlayUserClient a3 = new PlayUserClient();
		a3.setGoldcoins(6);
		List<PlayUserClient> list = new ArrayList<>();
		list.add(a1);
		list.add(a2);
		list.add(a3);
		for (PlayUserClient playUserClient : list) {
			System.out.println(playUserClient.getGoldcoins());
		}
		System.out.println("====================我是分割线======================");

		list = getTopPlayersByMoney(list, 2);
		for (PlayUserClient playUserClient : list) {
			System.out.println(playUserClient.getGoldcoins());
		}

	}

	/*
	 * 获取到排名前n位的玩家,注意排除掉庄家哦
	 */
	public static List<PlayUserClient> getTopPlayersByMoney(List<PlayUserClient> playerList, int num) {
		if (playerList == null || playerList.size() == 0) {
			return new ArrayList<PlayUserClient>();
		}
		for (PlayUserClient playUserClient : playerList) {
			if (playUserClient.isBlank()) {
				playerList.remove(playUserClient);
				break;
			}
		}
		for (int i = 0; i < playerList.size() - 1; i++) {
			for (int j = 0; j < playerList.size() - 1 - i; j++) {
				if (playerList.get(j).getGoldcoins() < playerList.get(j + 1).getGoldcoins()) {
					PlayUserClient temp = playerList.get(j);
					playerList.set(j, playerList.get(j + 1));
					playerList.set(j + 1, temp);
				}
			}
		}

		// 清洗数据

		List<PlayUserClient> result = new ArrayList<>();

		for (PlayUserClient playUserClient : playerList) {
			PlayUserClient puc = new PlayUserClient();
			puc.setBlank(playUserClient.isBlank());
			puc.setUsername(playUserClient.getUsername());
			puc.setGoldcoins(playUserClient.getGoldcoins());
			puc.setNickname(playUserClient.getNickname());
			puc.setHeadimgaddr(playUserClient.getHeadimgaddr());
			puc.setExperience(playUserClient.getExperience());
			puc.setCity(playUserClient.getCity());
			puc.setPlayerlevel(playUserClient.getPlayerlevel());
			result.add(puc);

			// 该方法暂未实现····请稍后
		}
		playerList = result;
		if (playerList.size() <= num) {
			return playerList;
		}

		return playerList.subList(0, num);
	}

	/**
	 * 
	 * Description:获取到排名前n位的玩家,注意排除掉庄家哦(修改上面方法存在的几个问题，金额为0的情况）
	 * 
	 * @author abo
	 * @date 2018年5月15日
	 * @param playerList
	 * @param num
	 * @return
	 */
	public static List<PlayUserClient> getNewTopPlayersByMoney(List<PlayUserClient> playerList, int num) {
		if (playerList == null || playerList.size() == 0) {
			return null;
		}
		for (PlayUserClient playUserClient : playerList) {
			if (playUserClient.isBlank()) {
				playerList.remove(playUserClient);
				break;
			}
		}
		for (int i = 0; i < playerList.size() - 1; i++) {
			for (int j = 0; j < playerList.size() - 1 - i; j++) {
				if (playerList.get(j).getGoldcoins() < playerList.get(j + 1).getGoldcoins()) {
					PlayUserClient temp = playerList.get(j);
					playerList.set(j, playerList.get(j + 1));
					playerList.set(j + 1, temp);
				}
			}
		}
		// 清洗数据
		List<PlayUserClient> result = new ArrayList<>();

		for (PlayUserClient playUserClient : playerList) {
			PlayUserClient puc = new PlayUserClient();
			puc.setBlank(playUserClient.isBlank());
			puc.setNickname(playUserClient.getNickname());
			puc.setGoldcoins(playUserClient.getGoldcoins());
			puc.setHeadimgaddr(playUserClient.getHeadimgaddr());
			puc.setExperience(playUserClient.getExperience());
			puc.setPlayerlevel(playUserClient.getPlayerlevel());
			puc.setId(playUserClient.getId());
			result.add(puc);
		}
		playerList = result;
		if (playerList.size() <= num) {
			return playerList;
		}
		return playerList.subList(0, num);
	}

}
