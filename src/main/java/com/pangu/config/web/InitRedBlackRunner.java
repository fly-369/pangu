package com.pangu.config.web;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.pangu.core.BMDataContext;

/**
 * 
 * Description:系统启动立即执行初始化redblacck房间的方法。
 *
 * @author abo
 * @date 2018年4月4日
 */
@Order(value=2)
@Component
public class InitRedBlackRunner implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		// play可以从缓存里面去取，先固定
//		BMDataContext.getRedBlackGameEngine().initRedBlcakGameEngine("ff80808162b818350162b82ab7ef000b", BMDataContext.SYSTEM_ORGI);
	}

}
