package com.pangu.config.web;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.core.engine.game.GameEngine;
import com.pangu.core.engine.game.NiuniuEngine;
import com.pangu.core.engine.game.PokerEngine;
import com.pangu.core.engine.game.RedBlackGameEngine;
import com.pangu.core.engine.game.SangongGameEngine;
import com.pangu.core.engine.game.ZhajinhuaEngine;
import com.pangu.web.model.GameBonusPool;
import com.pangu.web.model.GamePlayway;
import com.pangu.web.model.GameRoom;
import com.pangu.web.model.Generation;
import com.pangu.web.model.PlayUserClient;
import com.pangu.web.model.SysDic;
import com.pangu.web.model.SystemConfig;
import com.pangu.web.service.repository.jpa.GameBonusPoolRepository;
import com.pangu.web.service.repository.jpa.GamePlaywayRepository;
import com.pangu.web.service.repository.jpa.GameRoomRepository;
import com.pangu.web.service.repository.jpa.GenerationRepository;
import com.pangu.web.service.repository.jpa.PlayUserClientRepository;
import com.pangu.web.service.repository.jpa.SysDicRepository;
import com.pangu.web.service.repository.jpa.SystemConfigRepository;

/**
 * @ClassName: StartedEventListener
 * @Description:  系统启动的时候把所有的配置加载到缓存当中
 * @date 2018年3月22日 下午2:40:09
 *
 */
@Component
public class StartedEventListener implements ApplicationListener<ContextRefreshedEvent> {
	
	@Resource
	private GameEngine gameEngine;
	@Resource
	private NiuniuEngine niuniuEngine;
	@Resource
	private SangongGameEngine sangongGameEngine ;
	@Resource
	private PokerEngine pokerEngine ;
	@Resource
	private RedBlackGameEngine redBlackGameEngine;
	@Resource
	private ZhajinhuaEngine zhajinhuaEngine;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (BMDataContext.getContext() == null) {
			BMDataContext.setApplicationContext(event.getApplicationContext());
		}
		BMDataContext.setGameEngine(gameEngine);
		BMDataContext.setNiuniuEngine(niuniuEngine);
		//设置三公的游戏引擎
    	BMDataContext.setSangongGameEngine(sangongGameEngine);
    	//设置红黑大战游戏引擎
    	BMDataContext.setRedBlackGameEngine(redBlackGameEngine);
    	//德州扑克游戏赢钱
    	BMDataContext.setPokerEngine(pokerEngine);
    	//炸金花引擎
    	BMDataContext.setZhajinhuaEngine(zhajinhuaEngine);
    	/**
		 * 加载系统常量定义
		 */
    	SysDicRepository sysDicRes = event.getApplicationContext().getBean(SysDicRepository.class);
		List<SysDic> sysDicList = sysDicRes.findAll();
		for (SysDic dic : sysDicList) {
			CacheHelper.getSystemCacheBean().put(dic.getId(), dic, dic.getOrgi());
			if (dic.getParentid().equals("0")) {
				List<SysDic> sysDicItemList = new ArrayList<SysDic>();
				for (SysDic item : sysDicList) {
					if (item.getDicid() != null && item.getDicid().equals(dic.getId())) {
						sysDicItemList.add(item);
					}
				}
				CacheHelper.getSystemCacheBean().put(dic.getCode(), sysDicItemList, dic.getOrgi());
			}
		}
		/**
		 * 加载系统全局配置
		 */
		SystemConfigRepository systemConfigRes = event.getApplicationContext().getBean(SystemConfigRepository.class);
		SystemConfig config = systemConfigRes.findByOrgi(BMDataContext.SYSTEM_ORGI);
		if (config != null) {
			CacheHelper.getSystemCacheBean().put("systemConfig", config, BMDataContext.SYSTEM_ORGI);
		}

		GamePlaywayRepository playwayRes = event.getApplicationContext().getBean(GamePlaywayRepository.class);
		List<GamePlayway> gamePlaywayList = playwayRes.findAll();
		if (gamePlaywayList != null) {
			for (GamePlayway playway : gamePlaywayList) {
				CacheHelper.getSystemCacheBean().put(playway.getId(), playway, playway.getOrgi());
			}
		}

		GameRoomRepository gameRoomRes = event.getApplicationContext().getBean(GameRoomRepository.class);
		List<GameRoom> gameRoomList = gameRoomRes.findAll();
		if (gameRoomList != null) {
			for (GameRoom gameRoom : gameRoomList) {
				if (gameRoom.isCardroom()) {
					gameRoomRes.delete(gameRoom);// 回收房卡房间资源
				} else {
					CacheHelper.getQueneCache().put(gameRoom, gameRoom.getOrgi());
					CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
				}
			}
		}
		//ai放缓存里面 by ：laowang 对应表 bm_playuser
		PlayUserClientRepository playerRes = event.getApplicationContext().getBean(PlayUserClientRepository.class);
		List<PlayUserClient> playerList = playerRes.findByPlayertype(BMDataContext.BEIMI_GAME_AI);
		for (PlayUserClient playUserClient : playerList) {
			CacheHelper.getQuenePlayerCache().put(playUserClient, playUserClient.getOrgi());
		}

		GenerationRepository generationRes = event.getApplicationContext().getBean(GenerationRepository.class);
		List<Generation> generationList = generationRes.findAll();
		for (Generation generation : generationList) {
			CacheHelper.getSystemCacheBean().setAtomicLong(BMDataContext.ModelType.ROOM.toString(),generation.getStartinx());
		}
		//初始化奖金池
		GameBonusPoolRepository gbp = event.getApplicationContext().getBean(GameBonusPoolRepository.class);
		List<GameBonusPool> gbp_list = gbp.findAll();
		for (GameBonusPool gameBonusPool : gbp_list) {
			CacheHelper.getGameBonusPoolCacheBean().put(gameBonusPool.getGameType(), gameBonusPool, gameBonusPool.getOrgi());
		}
		
	}
}