package com.pangu.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pangu.core.engine.game.BeiMiGameEnum;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.action.poker.PokerAiAutoAction;
import com.pangu.core.engine.game.action.poker.PokerAutoAction;
import com.pangu.core.engine.game.action.poker.PokerBeginAction;
import com.pangu.core.engine.game.action.poker.PokerDealAction;
import com.pangu.core.engine.game.action.poker.PokerEndAction;
import com.pangu.core.engine.game.action.poker.PokerEnterAction;
import com.pangu.core.statemachine.BeiMiStateMachine;
import com.pangu.core.statemachine.config.StateConfigurer;
import com.pangu.core.statemachine.config.StateMachineTransitionConfigurer;

@Configuration
public class PokerStateMachineConfig<T, S>  {
	
	@Bean("poker")
	public BeiMiStateMachine<String,String> create() throws Exception{
		BeiMiStateMachine<String,String> beiMiStateMachine = new BeiMiStateMachine<String,String>();
		this.configure(beiMiStateMachine.getConfig());
		this.configure(beiMiStateMachine.getTransitions());
		return beiMiStateMachine;
	}
	
    public void configure(StateConfigurer<String,String> states)
            throws Exception {
        states
            .withStates()
                .initial(BeiMiGameEnum.NONE.toString())
                    .state(BeiMiGameEnum.CRERATED.toString())
                    .state(BeiMiGameEnum.READY.toString())
                    .state(BeiMiGameEnum.PLAY.toString())
                    .state(BeiMiGameEnum.END.toString())
                    .state(BeiMiGameEnum.NONE.toString());
	}

    public void configure(StateMachineTransitionConfigurer<String, String> transitions)
            throws Exception {
		/**
		 * 状态切换：BEGIN->WAITTING->READY->PLAY->END
		 */
        transitions
	        .withExternal()	
		    	.source(BeiMiGameEnum.NONE.toString()).target(BeiMiGameEnum.CRERATED.toString())
		    	.event(BeiMiGameEvent.ENTER.toString()).action(new PokerEnterAction<String,String>())
		    	.and()
            .withExternal()	
                .source(BeiMiGameEnum.CRERATED.toString()).target(BeiMiGameEnum.READY.toString())
                .event(BeiMiGameEvent.JOIN.toString()).action(new PokerBeginAction<String,String>())
                .and()
            .withExternal()
                .source(BeiMiGameEnum.READY.toString()).target(BeiMiGameEnum.PLAY.toString())
                .event(BeiMiGameEvent.AUTO.toString()).action(new PokerAutoAction<String,String>())	//自动出牌
                .and()
           .withExternal()
                .source(BeiMiGameEnum.READY.toString()).target(BeiMiGameEnum.PLAY.toString())
                .event(BeiMiGameEvent.PLAYCARDS.toString()).action(new PokerAiAutoAction<String,String>())	//ai出牌
                .and()
            .withExternal()
                .source(BeiMiGameEnum.PLAY.toString()).target(BeiMiGameEnum.END.toString())
                .event(BeiMiGameEvent.SUMMARY.toString()).action(new PokerEndAction<String,String>())	//抢地主 
                .and()
            .withExternal()
                .source(BeiMiGameEnum.END.toString()).target(BeiMiGameEnum.NONE.toString())
                .event(BeiMiGameEvent.DEAL.toString()).action(new PokerDealAction<String,String>())	//抢地主 
            ;
    }
}
