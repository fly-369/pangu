package com.pangu.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pangu.core.engine.game.BeiMiGameEnum;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.action.zhajinhua.ZhajinhuaAllCardsAction;
import com.pangu.core.engine.game.action.zhajinhua.ZhajinhuaEnterAction;
import com.pangu.core.engine.game.action.zhajinhua.ZhajinhuaPlayCardsAction;
import com.pangu.core.engine.game.action.zhajinhua.ZhajinhuaRaiseHandsAction;
import com.pangu.core.statemachine.BeiMiStateMachine;
import com.pangu.core.statemachine.config.StateConfigurer;
import com.pangu.core.statemachine.config.StateMachineTransitionConfigurer;

/**
 * 
 * Description:炸金花状态机配置
 *
 * @author abo
 * @date 2018年4月16日  
 * @param <T>
 * @param <S>
 */
@Configuration
public class ZhajinhuaStateMachineConfig<T, S>  {
	
	@Bean("zhajinhua")
	public BeiMiStateMachine<String,String> create() throws Exception{
		BeiMiStateMachine<String,String> beiMiStateMachine = new BeiMiStateMachine<String,String>();
		this.configure(beiMiStateMachine.getConfig());
		this.configure(beiMiStateMachine.getTransitions());
		return beiMiStateMachine;
	}
	
    public void configure(StateConfigurer<String,String> states)
            throws Exception {
        states
            .withStates()
                .initial(BeiMiGameEnum.NONE.toString())
                    .state(BeiMiGameEnum.CRERATED.toString())
                    .state(BeiMiGameEnum.READY.toString())
                    .state(BeiMiGameEnum.PLAY.toString())
                    .state(BeiMiGameEnum.END.toString());
	}

    public void configure(StateMachineTransitionConfigurer<String, String> transitions)
            throws Exception {
    	//CRERATED创建房间
    	//READY初始化ai，发牌
    	//PLAY看牌、弃牌、加注、跟注、比牌、比牌
    	//END结算
        transitions
	        .withExternal()	
		    	.source(BeiMiGameEnum.NONE.toString()).target(BeiMiGameEnum.CRERATED.toString())
		    	.event(BeiMiGameEvent.ENTER.toString()).action(new ZhajinhuaEnterAction<String,String>())//进入房间
		    	.and()
            .withExternal()
                .source(BeiMiGameEnum.CRERATED.toString()).target(BeiMiGameEnum.READY.toString())
                .event(BeiMiGameEvent.ENOUGH.toString()).action(new ZhajinhuaRaiseHandsAction<String,String>())//加入ai
                .and()
            .withExternal()
                .source(BeiMiGameEnum.READY.toString()).target(BeiMiGameEnum.PLAY.toString())
                .event(BeiMiGameEvent.PLAYCARDS.toString()).action(new ZhajinhuaPlayCardsAction<String,String>())
                .and()
            .withExternal()
                .source(BeiMiGameEnum.PLAY.toString()).target(BeiMiGameEnum.END.toString())
                .event(BeiMiGameEvent.ALLCARDS.toString()).action(new ZhajinhuaAllCardsAction<String,String>())
                .and()
            ;
    }
}
