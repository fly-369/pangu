/*
package com.pangu.config.web.Filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.web.util.matcher.RequestMatcher;

import com.pangu.core.BMDataContext;
import com.pangu.core.cache.CacheHelper;
import com.pangu.util.rules.model.RespCode;
import com.pangu.util.rules.model.RespEntity;

*/
/**
  * @ClassName: ApiRequestMatchingFilter
  * @Description:  api 过滤器，前台api 匹配角色为 matchAnyRoles = false 
  * @date 2018年3月21日 下午3:12:47
  *
  *//*

public class ApiRequestMatchingFilter implements Filter {
    private RequestMatcher[] ignoredRequests;
    private RequestMatcher[] exRequests;

    public ApiRequestMatchingFilter(RequestMatcher[] exRequests , RequestMatcher... matcher) {
    	this.exRequests = exRequests ;
        this.ignoredRequests = matcher;
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
         HttpServletRequest request = (HttpServletRequest) req;
         boolean matchAnyRoles = false ;
         for(RequestMatcher anyRequest : ignoredRequests ){
        	 if(anyRequest.matches(request)){
        		 matchAnyRoles = true ;
        	 }
         }
         if(exRequests!=null){
	         for(RequestMatcher anyRequest : exRequests ){
	        	 if(anyRequest.matches(request)){
	        		 matchAnyRoles = false ;
	        	 }
	         }
         }
         String authorization = request.getHeader("authorization") ;
         if(StringUtils.isBlank(authorization)) {
        	 authorization = request.getParameter("authorization") ;
         }
         if(matchAnyRoles){
        	 if(StringUtils.isBlank(authorization)){//authorization为空
        		 HttpServletResponse response = (HttpServletResponse) resp ;
         		 response.setHeader("Access-Control-Allow-Origin", "*");
         		response.setHeader("Access-Control-Allow-Headers", "authorization,Origin, X-Requested-With, Content-Type, Accept");
 	        	 response.setContentType("application/json; charset=utf-8");
 	        	 response.getWriter().write(new RespEntity(RespCode.R1008).toJSON());
        	 }else if(CacheHelper.getApiUserCacheBean().getCacheObject(authorization, BMDataContext.SYSTEM_ORGI) == null) {
        		 HttpServletResponse response = (HttpServletResponse) resp ;
         		 response.setHeader("Access-Control-Allow-Origin", "*");
         		response.setHeader("Access-Control-Allow-Headers", "authorization,Origin, X-Requested-With, Content-Type, Accept");
 	        	 response.setContentType("application/json; charset=utf-8");
 	        	 response.getWriter().write(new RespEntity(RespCode.R1007).toJSON());
        	 }else{
        		 chain.doFilter(req,resp);
        	 }
         }else{
        	 chain.doFilter(req,resp);
         }
    }

	@Override
	public void destroy() {
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
}*/
