package com.pangu.config.web.model;

import com.pangu.core.engine.game.state.GameEvent;
import com.pangu.core.statemachine.impl.BeiMiMachineHandler;
import com.pangu.core.statemachine.impl.MessageBuilder;
import com.pangu.web.model.GameRoom;

public class Game { 

	private final BeiMiMachineHandler handler; 

	public Game(BeiMiMachineHandler handler) { 
		this.handler = handler; 
	} 
	public void change(GameRoom gameRoom , String event) { 
		change(gameRoom, event , 0);
	}
	
	public void change(GameRoom gameRoom , String event , int interval ) { 
		handler.handleEventWithState(MessageBuilder.withPayload(event).setHeader("room", gameRoom.getId()).setHeader("interval", interval).build(), event);
	}
	
	public void change(GameEvent gameEvent) { 
		change(gameEvent , 0); 
	} 
	
	public void change(GameEvent gameEvent , int interval ) { 
		handler.handleEventWithState(MessageBuilder.withPayload(gameEvent.getEvent()).setHeader("room", gameEvent.getRoomid()).setHeader("interval", interval).build(), gameEvent.getEvent()); 
	} 
}
