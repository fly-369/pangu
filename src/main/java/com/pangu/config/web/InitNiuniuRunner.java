package com.pangu.config.web;

import com.pangu.web.service.GameBonusPoolService;
import com.pangu.web.service.repository.jpa.GamePlaywayRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @ClassName: InitNiuniuRunner
 * @Description: 系统启动立即执行初始化百人牛牛房间的方法。
 * @author 老王
 * @date 2018年3月31日 下午12:52:15
 *
 */
@Component
@Slf4j
public class InitNiuniuRunner implements CommandLineRunner {

	@Autowired
	GamePlaywayRepository gamePlaywayRepository;
	@Autowired
	GameBonusPoolService gameBonusPoolService;

	@Override
	public void run(String... arg0) throws Exception {
		
		/*
		GameBonusPool coin = (GameBonusPool) CacheHelper.getGameBonusPoolCacheBean()
				.getCacheObject(BMDataContext.GameTypeEnum.NIUNIU.toString().toLowerCase(), BMDataContext.SYSTEM_ORGI);
		log.info("牛牛奖金池大小：{}", coin.getCoin());
		log.info("牛牛赢概率：{}", coin.getChance());

		coin = (GameBonusPool) CacheHelper.getGameBonusPoolCacheBean()
				.getCacheObject(BMDataContext.GameTypeEnum.DEZHOU.toString().toLowerCase(), BMDataContext.SYSTEM_ORGI);

		double coin1 = coin.getCoin();
		log.info("德州奖金池大小：{}", coin.getCoin());
		log.info("德州赢概率：{}", coin.getChance());


		// @ FIXME 控制是否启动牛牛以及德州扑克

		List<GamePlayway> play = gamePlaywayRepository.findAll();
		for (GamePlayway gamePlayway : play) {
			// && gamePlayway.getScore() == 5 
			//包括牛牛的高倍房以及低倍房
			if (BMDataContext.GameTypeEnum.NIUNIU.toString().toLowerCase().equals(gamePlayway.getCode())) {
				BMDataContext.getNiuniuEngine().CreateGame(gamePlayway.getId(), BMDataContext.SYSTEM_ORGI);
			}

			if (BMDataContext.GameTypeEnum.DEZHOU.toString().toLowerCase().equals(gamePlayway.getCode())) {
				BMDataContext.getPokerEngine().CreateGame(gamePlayway.getId(), BMDataContext.SYSTEM_ORGI, null);
			}

		} 
*/
	}
	
}
