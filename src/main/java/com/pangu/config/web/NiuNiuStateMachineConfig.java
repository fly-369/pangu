package com.pangu.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pangu.core.engine.game.BeiMiGameEnum;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.action.niuniu.BetAction;
import com.pangu.core.engine.game.action.niuniu.NiuniuBeginAction;
import com.pangu.core.engine.game.action.niuniu.NiuniuDealAction;
import com.pangu.core.engine.game.action.niuniu.NiuniuEndAction;
import com.pangu.core.engine.game.action.niuniu.NiuniuEnterAction;
import com.pangu.core.statemachine.BeiMiStateMachine;
import com.pangu.core.statemachine.config.StateConfigurer;
import com.pangu.core.statemachine.config.StateMachineTransitionConfigurer;

@Configuration
public class NiuNiuStateMachineConfig<T, S>  {
	
	@Bean("niuniu")
	public BeiMiStateMachine<String,String> create() throws Exception{
		BeiMiStateMachine<String,String> beiMiStateMachine = new BeiMiStateMachine<String,String>();
		this.configure(beiMiStateMachine.getConfig());
		this.configure(beiMiStateMachine.getTransitions());
		return beiMiStateMachine;
	}
	
    public void configure(StateConfigurer<String,String> states)
            throws Exception {
        states
            .withStates()
                .initial(BeiMiGameEnum.NONE.toString())
                    .state(BeiMiGameEnum.CRERATED.toString())
                    .state(BeiMiGameEnum.WAITTING.toString())
                    .state(BeiMiGameEnum.READY.toString())
                    .state(BeiMiGameEnum.BEGIN.toString())
                    .state(BeiMiGameEnum.PLAY.toString())
                    .state(BeiMiGameEnum.END.toString());
	}

    public void configure(StateMachineTransitionConfigurer<String, String> transitions)
            throws Exception {
		/**
		 * 状态切换：BEGIN->WAITTING->READY->PLAY->END
		 */
        transitions
	        .withExternal()	
		    	.source(BeiMiGameEnum.NONE.toString()).target(BeiMiGameEnum.CRERATED.toString())
		    	.event(BeiMiGameEvent.ENTER.toString()).action(new NiuniuEnterAction<String,String>())
		    	.and()
		    .withExternal()	
	        	.source(BeiMiGameEnum.CRERATED.toString()).target(BeiMiGameEnum.WAITTING.toString())
	        	.event(BeiMiGameEvent.JOIN.toString()).action(new BetAction<String, String>())
	        	.and()
            .withExternal()	
                .source(BeiMiGameEnum.WAITTING.toString()).target(BeiMiGameEnum.READY.toString())
                .event(BeiMiGameEvent.ENOUGH.toString()).action(new NiuniuBeginAction<String,String>())
                .and()
            .withExternal()
                .source(BeiMiGameEnum.PLAY.toString()).target(BeiMiGameEnum.END.toString())
                .event(BeiMiGameEvent.ALLCARDS.toString()).action(new NiuniuEndAction<String,String>())	//抢地主 
                .and()
                .withExternal()
                .source(BeiMiGameEnum.END.toString()).target(BeiMiGameEnum.NONE.toString())
                .event(BeiMiGameEvent.DEAL.toString()).action(new NiuniuDealAction<String,String>())	//抢地主 
                .and()
            ;
    }
}
