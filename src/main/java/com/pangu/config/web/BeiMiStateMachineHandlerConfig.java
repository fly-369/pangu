package com.pangu.config.web;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pangu.config.web.model.Game;
import com.pangu.core.statemachine.BeiMiStateMachine;
import com.pangu.core.statemachine.impl.BeiMiMachineHandler;

@Configuration
public class BeiMiStateMachineHandlerConfig {

	@Resource(name = "dizhu")
	private BeiMiStateMachine<String, String> dizhuConfigure;
	@Resource(name = "majiang")
	private BeiMiStateMachine<String, String> maJiangConfigure;
	@Resource(name = "niuniu")
	private BeiMiStateMachine<String, String> niuNiuConfigure;
	@Resource(name = "sangong")
	private BeiMiStateMachine<String, String> sanGongConfigure;
	//炸金花
	@Resource(name = "zhajinhua")
	private BeiMiStateMachine<String, String> zhaJinHuaConfigure;
	@Resource(name = "poker")
	private BeiMiStateMachine<String, String> pokerConfigure;
	

	@Bean("dizhuGame")
	public Game dizhu() {
		return new Game(new BeiMiMachineHandler(this.dizhuConfigure));
	}

	@Bean("majiangGame")
	public Game majiang() {
		return new Game(new BeiMiMachineHandler(this.maJiangConfigure));
	}

	@Bean("niuniuGame")
	public Game niuniuGame() {
		return new Game(new BeiMiMachineHandler(this.niuNiuConfigure));
	}
	
	@Bean("sanGongGame")
	public Game sanGongGame() {
		return new Game(new BeiMiMachineHandler(this.sanGongConfigure));
	}
	
	@Bean("zhajinhuaGame")
	public Game zhajinhuaGame() {
		return new Game(new BeiMiMachineHandler(this.zhaJinHuaConfigure));
	}
	@Bean("pokerGame")
	public Game pokerGame() {
		return new Game(new BeiMiMachineHandler(this.pokerConfigure));
	}
}
