package com.pangu.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.pangu.core.engine.game.BeiMiGameEnum;
import com.pangu.core.engine.game.BeiMiGameEvent;
import com.pangu.core.engine.game.action.sangong.SangongAllCardsAction;
import com.pangu.core.engine.game.action.sangong.SangongAutoAction;
import com.pangu.core.engine.game.action.sangong.SangongEnterAction;
import com.pangu.core.statemachine.BeiMiStateMachine;
import com.pangu.core.statemachine.config.StateConfigurer;
import com.pangu.core.statemachine.config.StateMachineTransitionConfigurer;

/**
 * 
 * Description:三公的状态机配置类
 * 
 * @author abo
 * @date 2018年3月23日
 * @param <T>
 * @param <S>
 */
@Configuration
public class SangongStateMachineConfig<T, S> {

	/**
	 * 
	 * Description:创建实例化bean
	 * 
	 * @author abo
	 * @date 2018年3月23日
	 * @return
	 * @throws Exception
	 */
	@Bean("sangong")
	public BeiMiStateMachine<String, String> create() throws Exception {
		BeiMiStateMachine<String, String> beiMiStateMachine = new BeiMiStateMachine<String, String>();
		this.configure(beiMiStateMachine.getConfig());
		this.configure(beiMiStateMachine.getTransitions());
		return beiMiStateMachine;
	}

	public void configure(StateConfigurer<String, String> states) throws Exception {
		states.withStates().initial(BeiMiGameEnum.NONE.toString()).state(BeiMiGameEnum.CRERATED.toString())
		.state(BeiMiGameEnum.PLAY.toString())
				.state(BeiMiGameEnum.END.toString());
	}

	/**
	 * 
	 * Description:配置庄切换，并再切换状态的时候执行相应的方法
	 * 
	 * @author abo
	 * @date 2018年3月23日
	 * @param transitions
	 * @throws Exception
	 */
	public void configure(StateMachineTransitionConfigurer<String, String> transitions) throws Exception {

		// NONE, //无状态
		// CRERATED,
		// PLAY, //打牌
		// END; //结束

		// ENTER, //创建房间 （仅第一个加入房间的人触发的事件），加入ai然后开始游戏
		// ENOUGH, //凑够一桌子
		// ALLCARDS, //1、单个玩家打完牌（地主，推到胡）；2、打完桌面的所有牌（血战，血流，德州）
		transitions.withExternal()
				.source(BeiMiGameEnum.NONE.toString()).target(BeiMiGameEnum.CRERATED.toString())
				.event(BeiMiGameEvent.ENTER.toString()).action(new SangongEnterAction<String,String>())
				.and().withExternal()
				// 选择玩法，创建房间，加入ai凑齐一桌，开始游戏
				.source(BeiMiGameEnum.CRERATED.toString()).target(BeiMiGameEnum.PLAY.toString())
				.event(BeiMiGameEvent.ENOUGH.toString()).action(new SangongAutoAction<String, String>()).and()
				.withExternal()
				// 结算				
				.source(BeiMiGameEnum.PLAY.toString()).target(BeiMiGameEnum.END.toString())
				.event(BeiMiGameEvent.RAISEHANDS.toString()).action(new SangongAllCardsAction<String, String>()).and();
	}
}
